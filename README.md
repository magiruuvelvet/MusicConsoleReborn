# Music Console Reborn

A simple console app which organizes all of your media files for fast and easy access.

> **FreeBSD** [![FreeBSD Build Status](https://jenkins.magiruuvelvet.gdn/job/MusicConsoleReborn/badge/icon)](https://jenkins.magiruuvelvet.gdn/job/MusicConsoleReborn/lastBuild/console) | **Linux** [![pipeline status](https://gitlab.com/magiruuvelvet/MusicConsoleReborn/badges/master/pipeline.svg)](https://gitlab.com/magiruuvelvet/MusicConsoleReborn/commits/master)

<br>

##### Brief Overview

Music Console Reborn is the second rewrite of my command line shell dedicated to just music and videos. You just enter some search terms which may match one of your thousands of files and it gets played in your media player of choice.


## Features

- **over 15 built-in commands** to get the best out of your multimedia collection
- **name filters** for audio, video and module tracker formats
- **advanced search algorithm and filter** to find exactly what you want
- **customizable players** to play every song and video in the player of your choice
- **command line history** for those with a bad memory and lazy typers
- **real-time playlist generator** to automatically generate a playlist real quick
- **custom playlist format** to create awesome playlists, the sky is the limit
- **shuffle randomly** through your entire library
- **play random** songs and videos
- **minimalistic and customizable** terminal output to avoid unnecessary clutter while you listen to your favorite songs
- **tab completion** (auto completion) for metadata, file names and commands
- **input hints** for metadata, file names and commands
- **cache mechansim** for fast startup times

#### New in the REBORN version

- much better command line parsing and management. no more broken commands!
- commands can have spaces now (quotes or escaped space).
- tab completion and input hints
- fresh new matching algorithm and syntax. no more false positives and much faster search results. additionally search results are now cached per given search term.
- much better and faster cache generation. also heavily reduced the size of the cache.
- ffmpeg is now used for metadata reading. supports more formats and provides better results.
- cache invalidation!! the cache automatically updates whenever your library has changed or when files inside the library were changed.

## Requirements

- OS: anything UN\*X-related (Linux, \*BSD, macOS, etc.)
- C++17 friendly compiler (Clang 6.0+ or GCC 7.3+)
- Qt Core 5.9+ (older versions might work too, not tested) **TO BE REMOVED IN THE FUTURE**
- [ICU4C](http://site.icu-project.org/) `uc` (a recent version, recommended v.58 or higher)
- PCRE2-32 10.21+ (Perl compatible regular expressions, UTF-32 version)
- ffmpeg 3.x/4.x (*optimal*, `libavformat/avformat.h` for building and optimally at runtime)
- CMake 3.8+ (for building, older versions might work too)
- pkg-config (for some CMake checks)
<br><br>
- *Optimal:* **UTF-8** and **24-bit Color** compatible Terminal Emulator
- *Recommended Player:* **[MPV](https://mpv.io)** <br>
  This is the default player of Music Console Reborn. You can change it to whatever you like though.

## Building

- Create a dedicated build directory and switch to it. Example: `mkdir build && cd build`
- Release build (recommended): `cmake -DCMAKE_BUILD_TYPE=Release (optimal options) ..`
- Debug build: `cmake -DCMAKE_BUILD_TYPE=Debug -DINCLUDE_GIT_VERSION_DATA=ON (optimal options) ..`
- If everything went fine you should find a binary `MusicConsoleReborn` in the build directory.

##### Selecting a Metadata Reader Backend

Set the CMake option `-DMETADATA_READER_BACKEND=` at command line. Possible values are:
 - `ffmpeg`: dynamically loaded ffmpeg metadata reader using libavformat (default)
 - `ffmpeg-ld`: linked ffmpeg metadata reader
 - `stub`: disable metadata reading

##### Note about dynamically loaded libavformat

When you update ffmpeg (API changes) or transfer the binary to another system which may have a different version of ffmpeg, the behavior is undefined. If you are lucky the ffmpeg metadata backend works either out of the box or gracefully bails out and returns empty metadata. If not you may experience a segmentation fault, since it is not possible to validate function prototypes during dynamic loading.

Uninstalling ffmpeg always results in a graceful bail out, but the backend returns empty metadata.

##### Note about linked ffmpeg

The linked ffmpeg metadata reader is directly linked to the application and you can generally trust the ld-loader of you operating system. The application is not so portable anymore and may break on ffmpeg updates. In this case you need to recompile Music Console Reborn.

The linked method is a fallback when the dynamically loaded ffmpeg doesn't work for you.


## Documentation

### Basics

##### First Start

Starting the program for the first time will create a directory with the configuration files in
> `$XDG_CONFIG_HOME/マギルゥーベルベット/musicconsole-reborn`

and than start to scan your home directory for media files. If you don't want your entire home directory to be scanned, you can run the program with `--library-path="your new path here"` to set a new path. This way you can always change your path without modifying the settings file by hand. The program exits after processing command line arguments, so you need to restart it without arguments.

The first start, depending on how much music and videos you own, can take up to several minutes and consume a lot of temporary memory (~300MB for about 5000 files). During this process Music Console Reborn is searching for media files, reading its metadata and generating advanced search terms for the search algorithm. All this data is written to the cache, which will be re-used every time you start the program again. This means the second startup is more or less instant because Music Console Reborn only needs to scan the filesystem for media files and than read the existing data from the cache. If you extend your collection, Music Console Reborn extends the cache by the newly added files. Due to an currently unknown memory leak, it is recommended to restart the program after the first scan completed to restore normal memory usage (about ~30MB for 5000 files). Rescanning the library too often also leaks memory. Investigation in progress...

You can manually rescan the filesystem if your library has changed using the `::rescan` command. The rescan process repeats the scan and uses data from the cache if any.

##### Program Exit Codes

- `0` No error / normal shutdown
- `2` Unknown command line options
- `3` Error parsing command line options
- `5` An instance of Music Console Reborn is already running
- `10` Application was killed by signal (`SIGINT`, `SIGTERM`, `SIGQUIT`)
- any other: (unknown) crash

### Command Prompt

The command prompt is the place where you activate all the magic of Music Console Reborn ;P

###### Keyboard Interactions

- `Ctrl+C` (`SIGINT`) cancels the current prompt and starts a new one. very common in shells. SIGINT is only handled when the prompt is active, otherwise it interrupts the program.

###### About Arguments

Arguments within `(parenthesis)` are optimal.

- `type` can be one of the filter commands for `audio`/`video`/`mod`

**Pro tip!** Every command can be customized in the settings.

###### One-liner

The prompt has the ability to expand one-liner. This means you can execute multiple commands in one go. Commands are separated using `&&`. If you put `&&` inside quotes like so `"&&"`, the command will not be splitted at this location, but actually contain this characters. This feature is currently in beta state and has some issues.

###### Auto completion

*see* **The Search Algorithm and filter possibilities** *for more details*

###### Input hints

During typing the prompt will show you a specific amount of hints which matches the current input. Hints are shown below the input line. Using the shortcuts `Ctrl+Arrow Up` and `Ctrl+Arrow Down` you can move through all available input hints. Pressing tab on the active hint auto completes it.

###### About breakable (infinite) loops

Some commands seem to be infinite, but don't worry, they are actually not. There is an exit condition ─ this condition is a check if the user hit the enter key.

So for example: if you use the default player **mpv** and hit enter this usually exits the player, so you quickly need to hit enter again to stop such loops. If the default wait interval of 300ms is too short for you, than you can increase it in the settings. The loop end condition actually waits for some time before continuing.

This feature is used by the following commands:

- `shuffle` (loops until user hits enter)
- `repeat` (loops until user hits enter)
- `playlist` (exits normally too when the playlist is done, can be stopped early by hitting enter)

### Commands

**Hint!** *All commands below describe the configuration key, rather than its default value.*

- *(nothing)* <br>
  By omitting a command you can directly search for matching media files using the entered search term. The result will be played in the `_allplayer` (see **Player Configuration** for more details). <br>
  <br>
  **Hint!** Omitting a command actually executes the **default command** which is user configurable. The initial default command is `all` which is the actual command which is executed when you omit a command.
  <br><br>

- `audio` *search term* <br>
  Search only for audio files. The result will be played in the audio player.
  <br><br>

- `video` *search term* <br>
  Search only for video files. The result will be played in the video player.
  <br><br>

- `mod` *search term* <br>
  Search only for module tracker files. The result will be played in the module tracker player.
  <br><br>

- `all` *search term* <br>
  Search for any type of files. The result will be played in the `_allplayer`.
  <br><br>

- `search` (`type`) *search term* <br>
  Searches for media files matching the given search term and prints all results on screen. Can be filtered by type.
  <br><br>

- `random` (`type`) (*search term*) <br>
  Plays a random media file of the given type in the corresponding player or in the `_allplayer` when no `type` was provided. Search terms are optimal here. If you want more fine control of what should be played specify some search terms.<br>
  <br>
  **Hint!** This command obtains its random numbers from `/dev/urandom` with `/dev/random` as fallback. The numbers are mostly guaranteed to be random.
  <br><br>

- `shuffle` (`type`) (*search term*) <br>
  Shuffles randomly through your library. Specify a `type` and search terms to fine tune the results.
  <br>*This is my most favorite command, and it will be yours too.* :P
  <br><br>

- `repeat` ((`type`) *search term*) <br>
  Plays the result again and again and again and again... until you hit enter. Can be filtered by type. If you omit all arguments the last played media file is repeated.
  <br><br>

- `history` (*limit*) <br>
  Prints the command line history on screen. Specifying a `limit` as number limits the output to it. Example: a limit of `10` only prints the last 10 items in the history.
  <br><br>

- `statistics` <br>
  Prints several different stats on screen.<br>
  &nbsp;&nbsp;× System Info (CPU, RAM, CPU Extensions)<br>
  &nbsp;&nbsp;× Library Info (count of all media files)<br>
  &nbsp;&nbsp;× Program Uptime in `HH:MM:SS` format
  <br><br>

- `rescan` <br>
  Rescans the library and updates the cache. Use this command if you added or removed files from the library path.
  <br><br>

- `playlist` (`type`) *search term* <br>
  Generates a playlist of matching media files and starts to play them. The order is alphabetically and can't be changed.
  <br><br>

- `playlist` `crossfade` (`type`) *search term* <br>
  Music Console Reborn supports crossfade albums with smooth transitions. The delay from kbhit can be pretty annoying, therefore there is a dedicated feature which plays all found media files in one go. Note that your player must support multiple input files and optimally precache upcoming files to ensure a smooth transition. mpv is a good player choice for this. Crossfade albums are always played in the `_allplayer`. The type filter just fine tunes the search results.
  <br><br>

- `playlist` `file_load` *file* <br>
  Loads the given playlist file and plays it. The playlist can be canceled early by hitting the enter key. More about Playlists is documented in the **Playlists** section. <br>
  <br>
  *Notice: the new file based playlist format is mostly backwards compatible to the old MusicConsole.js format. The behavior of quotes and escaping has changed and will cause issues. The `"search terms"` are now parsed using the command line parser and merged back together. Also: single quote support has been removed and will be forwarded to the parser as-is, so don't forget to escape them. -> `\'`* 
  <br><br>

- `exit` {absolute match} <br>
  This command quits the application. But there is something you should know about it: This command is the only command in the application which needs to match absolutely, which means it will not be executed when you pass arguments to it. This makes it possible to not accidently quit the app when something in your library starts with the word "exit" and you wanna look it up :) <br>
  If you need an example here: entering `EXIT TUNES` will not quit the app but start to search for that phrase in the library.
  <br><br>

- `clear` <br>
  Clears the entire terminal using ANSI escape sequences.
  <br><br>

- `config` args <br>
  Invokes the built-in WIP configuration manager, which lets you change settings at runtime. Settings are applied immediately. Currently nothing can be changed inside the program, but you can reload the configuration file with the argument `reload` when it changed on disk.
  <br><br>

### Settings

There are a lot of customizable options in Music Console, because everyone is different and prefers different things. Here is a quick overview off all the options and its explanation.

#### `settings.json` ─ Core Settings

This file contains the core settings of Music Console Reborn. If the file contains syntax errors or when keys are missing you will be notified about the broken parts. Until you fixed your settings file, Music Console Reborn uses its built-in default configuration and never tries to fix or change your settings file.

- `"commands"`: All command strings.<br>
You can modify every command. If there are duplicates, Music Console Reborn will tell you, but you can never execute the duplicated commands (from top to bottom in the settings file). You can't disable commands, but you can give them some ridiculous name.<br><br>

**Hint!** All key names represent the documented commands above. Replace them with your values to execute them.

<br>

- `"library"`: Everything related to the library.

   - `"rootpath"`: The path which Music Console should scan and use at startup.<br><br>
   - `"playlist_paths"`: Is an array of paths where Music Console Reborn should look for playlist files using the `playlist file_load` command.<br><br>
   - `"prefixdeletionpatterns"`: Is an array of phrases which should be excluded from the search terms of each file. It gives you the possibility to get more relevant results by ignoring common prefixes in your library, like `Music/` or `Videos/`. This phrases are only prefixes and have no support for wildcards or regular expressions.<br><br>
   - `"movetobottomphrases"`: Is an array of phrases which should move matching filenames to the bottom of the internal file list to manipulate its order. This is useful for example to move instrumental tracks behind its vocal tracks so that you need to explicitly add `instrumental` to the search term to play that track. As default this array has already some common phrases which indicate instrumental tracks.<br><br>
   - `"audioformats"`/`"videoformats"`/`"modformats"`: These are arrays of file extensions which should be recognized as media files. By default this includes a lot of common and uncommon media formats. Make sure to read the documentation of you media player about supported formats.

<br>

- `"player"`: Player configuration.<br>
These are the default players. Every player has a `"command"` and an array or `"arguments"` which will be passed to the player. The `%f` is a placeholder which is replaced by the media file. If there is no placeholder in the argument list, it will be added as last argument to the player.<br><br>
Names of the default players are `"audioplayer"`, `"videoplayer"`, `"modplayer"` and `"_allplayer"`. You can also have per filetype overrides. For this scroll down to the players.json description. It also includes an example.<br><br>
**Hint!** The `"_allplayer"` is used when omitting a type filter in supported commands.

<br>

- `"prompt"`: Customize the prompt and history ignore patterns.

   - `"line"`: The string which should be used to display the prompt. The use of escape sequences is supported here.<br><br>
     **Be aware** that you need to wrap all escape sequences and non-printable characters inside `\\001` (start) and `\\002` (end) to tell the prompt to ignore this characters. This has something to do with line wrapping. If you don't do that, than expect strange display issues.<br><br>
     Example without escape sequences: `"> "` -> shows as `> `<br>
     Example with escape sequnces: `\\001\\x1b[1;38;2;21;64;93m\\002> \\001\\x1b[0m\\002` -> shows as `> `, while the `> ` is colored. The input line itself can not be colored, if you try to do so, the behavior is undefined. If you forget the `\\001` and `\\002` here than the prompt will think the characters used to create the escape sequence are part of the line and this will cause display issues later, because escape sequences takes up no physical space, but the prompt expects actual characters there.<br><br>

   - `"kbhit_sleep_interval"`: This is the time in **seconds** how long the commands `shuffle`, `repeat` and `playlist` should wait before playing the next entry. The default value is `0.3` seconds. You can use any amount of seconds here ─ please note that the *minimum* possible value is `0.15` seconds, everything below this value is too fast and you will most likely not be able to cancel any loops with it. The sleep interval will reset to `0.3` in this case.<br><br>

   - `"metadata_title_split"`: This array defines strings where the song title (tagged songs) should be splitted, and the results added individually to the auto completion and input hints list. This feature exists for poorly tagged songs which contain the artist and title in the same field seperated by a specific character. You can add as many characters as you want. By default this array is empty.<br><br>

   - `"hints_enabled"`: Whenever input hints should be shown or not.<br><br>

   - `"hint_rows"`: When input hints are enabled, this property sets the maximum number of results which should be shown below the input line.<br><br>

   - `"hint_color"`: Sets the color of the input hints. Only the 16 standard ANSI colors are supported here. `black`, `red`, `green`, `brown`, `blue`, `magenta`, `cyan`, `gray`, `lightgray`, `brightred`, `brightgreen`, `yellow`, `brightblue`, `brightmagenta`, `brightcyan`, `white` or `default` to use the default terminal color. The default hint color is `gray`.<br><br>

   - `"highlighter_enabled"`: Whenever the syntax highlighter should be used or not. This feature is currently experimental and disabled by default.<br><br>

   - `"histignore"`: Array of regular expressions. Every input which matches any of this patterns are not added to the command line history. The regular expressions must be compatible with Perl (the regex format Qt uses).<br><br>
   **Pro tip!** Lines starting with a space are never added to the command line history.

<br>

- `"appearance"`: Customize the output of media files.<br><br>
Here you can change the display appearance of the media files and what tags should be shown. All this fields have full ANSI escape sequence support. If your terminal emulator supports True Color you can create a rainbow :D<br><br>
× `"artist"`, `"album"`, `"albmartist"`, `"title"`, `"genre"`, `"track"`, `"total_tracks"`: the available metadata fields.<br><br>
× `"extension"`: the file extension.<br><br>
× `"path"`: the relative path of the media file with `"prefixdeletionpatterns"` removed.<br><br>
All the listed fields above must contain a `%s` which is replaced by the actual value, or leave it away to hide that field. To customize the actual line which is displayed in the terminal you need to modify this 2 fields.<br><br>
× `"print_tagged"`: is used when the media files has at least one tagged field.<br>
× `"print_plain"`: is used when there are no tags available (mostly videos).<br><br>
This 2 fields should have placeholders which are replaced by the formatted fields from above. A placeholder starts with a dollar sign `$`, followed by its name (example: `$title`). You can also add any other characters and phrases which will be printed as static text.<br><br>
**Important!** The `print_tagged` line must have groups. A group is everything between paranthesis `()`. A single group can only have 1 placeholder inside it and optimally some static text. When a metadata field is empty this entire group is deleted, rather than displayed on the screen. To use a paranthesis inside a group, you must escape it with a backslash like so -> `\(`, `\)`.<br><br>
*Example:* `"($extension )($artist ─ )($title )($album )(\\($albmartist\\))"`<br>
This field for example will not show an empty ` ─ ` on the screen when the current file has no artist. Besides that, it will also not show redundant spaces when other metadata is missing. The `$albmartist` field is displayed inside paranthesis as they were escaped there. If the current file doesn't has an album artist tagged, it will delete the entire group, rather than showing `()` on screen.<br><br>
**Colorful sample:** <br>
```json
"appearance": {
    "artist": "\\x1b[3m%s\\x1b[0m",
    "album": "\\x1b[38;2;140;140;140m%s\\x1b[0m",
    "albmartist": "%s",
    "title": "\\x1b[1m%s\\x1b[0m",
    "genre": "%s",
    "track": "%s",
    "total_tracks": "%s",
    "extension": "\\x1b[1;38;2;0;97;167m[%s]\\x1b[0m",
    "path": "%s",
    "print_plain": "$extension $path",
    "print_tagged": "($extension )($artist ─ )($title )($album )(\\($albmartist\\))"
}
```

![Sample 1](.github/sample_appearance1.png "Sample 1") <br>
![Sample 2](.github/sample_appearance2.png "Sample 2") <br>
![Sample 3](.github/sample_appearance3.png "Sample 3") <br>
![Sample 4](.github/sample_appearance4.png "Sample 4")

<br>

- `"randomizer"`: Configure the built-in randomizer.

   - `"historysize"`: The history size is used to remember a fixed amount of numbers which were generated previously by the generator. If any of the number in the history matches the currently generated number a new number will be generated, to prevent infinite loops there is a protection which stops the loop when the maximum possible attempts were made. To disable the memorization, set this value to `0`. Expect the same song/video to play twice in a row without this feature ;)

<br>

#### `players.json` ─ Player overrides per filetype

This file contains all player overrides per filetype. Its a JSON file. Besides the default players you can also specify another player for a specific filetype. This is useful if the default player can't handle a specific format. For example `mpv` can't play Bink2 videos, but the official Bink Player can.

The JSON must be structured like this

```json
{
  "extension": {
    "command": "your_player_here",
    "arguments": [
      "--im-an-argument",
      "%f"
    ]
  },
  "bk2": {
    "command": "BinkPlayer",
    "arguments": [
      "%f"
    ]
  }
}
```

Where `extension` is, is any filetype which should use another player. If the file contains syntax errors, Music Console Reborn will inform you about this. The file stays intact and all player overrides will be unavailable until you fix the file.


<br>

### The Search Algorithm and filter possibilities

Let me describe my search algorithm in detail here, so that you understand what happens under to hood after you hit Enter on the prompt.

The input is just a simple space separated phrase, nothing really special. This simple input will be converted into a case insensitive matching regular expression. All regex reserved characters are escaped to match them accordingly. Spaces are replaced with wild cards which matches everything in between.

But wait there is even more. I talked about advanced filter possibilities earlier. What are those anyway? To fine tune results and to match more than just one pattern I developed a multi-match system. It allows you to enter multiple search terms and combine them in many ways.

**Pro tip!** As an extend you can also directly input a regular expression, by prefixing your input with `reg:` or `regex:`!

The different multi-match types described in detail:

- `MAIN` or **default** <br>
  This is a regular search term and also the base match.
  <br><br>

- `WITHOUT` or **without any of this** or `wo:` <br>
  This type allows you to exclude phrases which should not match. Whenever one of these patterns match, the file is skipped immediately. The `MAIN` pattern will be never tried.
  <br><br>

- `WITHOUT_GENRE` or **without genre** or `wg:` <br>
  Specific to tagged media files. Will exclude every songs which matches the given genre.
  <br><br>

<br>

**Hint!** All filters also have built-in aliases for convenience. This aliases are prefixed with `sp:`. <br>
  - `bottomphrases`: automatically appends all `"movetobottomphrases"` in one go.
  - `empty`: is replaced with `^(?![\s\S])`. A regular expression which matches empty strings.

<br>

Enough theory, now its time for some examples.

**Hint!** You can add spaces where you want. There is no need to quote the input.

1. `初音ミク` <br>
   Will match everything which contains the phrase `初音ミク` in it. <br>
   The resulting regex looks like this: `.*初音ミク.*`.
   <br><br>

2. `初音ミク wo:VOCALOID` <br>
   This will match everything which contains the phrase `初音ミク`, but not the phrase `VOCALOID` in it. <br>
   The 2 resulting regexes looks like this: `.*初音ミク.*` and `.*VOCALOID.*`
   <br><br>

3. (command shuffle) + `wo:sp:bottomphrases wg:pop` <br>
   Will skip all files which contain any of the `"movetobottomphrases"` or were the genre tag matches the regex `.*pop.*`.
   <br><br>

4. `reg:.* wo:reg:.*` <br>
   The first (the `MAIN` filter) will match everything, but since the `WITHOUT` filter is also the same pattern, nothing will be found.
   <br><br>

5. `reg:.* wo:Instrumental` <br>
   Will match everything, except the pattern `.*Instrumental.*`.
   <br><br>

6. `something wo:reg:^something.*$` <br>
   Will match everything which contains the phrase `something`, but only if it doesn't start with it.
   <br><br>

7. `wg:reg:^pop$` <br>
   Will match the genre `pop` absolutely, rather than doing an contains match.
   <br><br>

8. (command shuffle) + `wg:sp:empty` <br>
   Will only play files which have a genre tagged. So when the genre field is empty these files are skipped.
   <br><br>

**Pro tip!** All filenames and metadata is registered in the prompt for auto completion. Simple start to write something and press the tab key for blazingly fast inputs. You can also auto complete commands and filters. The auto completer is even case insensitive and transforms the input to the correct case on matching results. Just note that you need to escape spaces with a backslash for follow up matches, otherwise it restarts the search with the entire list.

*Example:* type `wo:s` \<press tab\> --\> `wo:sp:bottomphrases`



### ...

The documentation is not finished yet! You can take a look at the [old doc](https://github.com/magiruuvelvet/MusicConsole.js/blob/master/README.md) though. Be warned that the old documentation is mostly invalid for this version of Music Console.
