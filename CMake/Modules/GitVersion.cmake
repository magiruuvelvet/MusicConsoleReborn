#
# Module: GitVersion
#
# Description:
# Generates a header file containing the git version
# from a template.
#
# Author:
# マギルゥーベルベット (magiruuvelvet)
#


find_package(Git)

if (GIT_FOUND)
    execute_process(COMMAND ${GIT_EXECUTABLE} rev-list HEAD --count
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
        RESULT_VARIABLE exit_code
        OUTPUT_VARIABLE GIT_VERSION
        OUTPUT_STRIP_TRAILING_WHITESPACE)
    if (NOT ${exit_code} EQUAL 0)
        message(WARNING "git rev-list failed, unable to include the commit count.")
        set(GIT_VERSION "unknown")
    endif()

    execute_process(COMMAND ${GIT_EXECUTABLE} rev-parse --short HEAD
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
        RESULT_VARIABLE exit_code
        OUTPUT_VARIABLE GIT_VERSION_
        OUTPUT_STRIP_TRAILING_WHITESPACE)
    if (NOT ${exit_code} EQUAL 0)
        message(WARNING "git rev-parse --short failed, unable to include the commit hash.")
        set(GIT_VERSION_ "unknown")
    endif()

    # Include full hash as well next to the short version
    execute_process(COMMAND ${GIT_EXECUTABLE} rev-parse HEAD
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
        RESULT_VARIABLE exit_code
        OUTPUT_VARIABLE GIT_BRANCH_HASH
        OUTPUT_STRIP_TRAILING_WHITESPACE)
    if (NOT ${exit_code} EQUAL 0)
        message(WARNING "git rev-parse HEAD failed, unable to include the commit hash.")
        set(GIT_VERSION_ "unknown")
    endif()

    string(STRIP ${GIT_VERSION} GIT_VERSION)
    string(STRIP ${GIT_VERSION_} GIT_VERSION_)
    string(STRIP ${GIT_VERSION}-${GIT_VERSION_} GIT_VERSION)
    message(STATUS "Git version: " ${GIT_VERSION})

    execute_process(COMMAND ${GIT_EXECUTABLE} rev-parse --abbrev-ref HEAD
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
        RESULT_VARIABLE exit_code
        OUTPUT_VARIABLE GIT_BRANCH
        OUTPUT_STRIP_TRAILING_WHITESPACE)
    if (NOT ${exit_code} EQUAL 0)
        message(WARNING "git rev-parse --abbrev-ref failed, unable to include the branch name.")
        set(GIT_BRANCH "(gone)")
    endif()
else()
    message(WARNING "git not found, unable to include version.")
    set(GIT_VERSION "unknown")
endif()

message(STATUS "Git branch: ${GIT_BRANCH}")

set(GitVersionFile "${GENERATED_DIR}/GitVersion.hpp")
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/CMake/Templates/GitVersion.hpp.in
               ${GitVersionFile})
