#include "I18n.hpp"

#include <cstdlib>
#include <memory>
#include <fstream>

#include <Utils/ExpandEnvironment.hpp>

#include <QFile>
#include <QFileInfo>

#include <spirit_po/spirit_po.hpp>

namespace {
    std::shared_ptr<spirit_po::default_catalog> messageCatalog;
}

bool I18n::setToActiveLocale()
{
    // get $LANG environment variable
    String LANG = ExpandEnvironment::ExpandEnvironmentVariables("$LANG");
    LANG.simplify();

    // check if $LANG is empty
    if (LANG.isEmpty())
    {
        return false;
    }

    // remove encoding and other extras
    auto encoding_pos = LANG.find_first_of('.');
    LANG = LANG.substr(0, encoding_pos);

    // split language and region
    auto locale = LANG.split('_', String::SkipEmptyParts);
    if (locale.empty())
    {
        return false;
    }

    // extract language and region from split array
    String lang, region;
    if (locale.size() == 1)
    {
        lang = locale.at(0);
    }
    else if (locale.size() > 1)
    {
        lang = locale.at(0);
        region = locale.at(1);
    }

    String file;

    // try language_region
    if (QFileInfo((":/" + lang + "_" + region + ".po").c_str()).exists())
    {
        file = ":/" + lang + "_" + region + ".po";
    }
    // fallback to language only
    else if (QFileInfo((":/" + lang + ".po").c_str()).exists())
    {
        file = ":/" + lang + ".po";
    }

    if (file.isEmpty())
    {
        return false;
    }

    // read embedded po file
    QFile po(file.c_str());
    if (po.open(QIODevice::ReadOnly))
    {
        auto contents = po.readAll();
        po.close();
        return I18n::setCatalogFromMemory(contents.constData());
    }

    return false;
}

bool I18n::setCatalogFromFile(const String &poFile)
{
    try
    {
        std::ifstream ifs(poFile);
        std::string poFileStream{std::istreambuf_iterator<char>{ifs}, std::istreambuf_iterator<char>()};

        messageCatalog.reset(new spirit_po::default_catalog{spirit_po::default_catalog::from_range(poFileStream)});
        return true;
    }
    catch (...)
    {
        messageCatalog.reset();
        return false;
    }
}

bool I18n::setCatalogFromMemory(const String &poContents)
{
    try
    {
        messageCatalog.reset(new spirit_po::default_catalog{spirit_po::default_catalog::from_range(poContents)});
        return true;
    }
    catch (...)
    {
        messageCatalog.reset();
        return false;
    }
}

const String I18n::get(const String &msgid)
{
    if (messageCatalog)
    {
        return messageCatalog->gettext_str(msgid);
    }
    else
    {
        return msgid;
    }
}

const String I18n::pget(const String &msgid, const String &context)
{
    if (messageCatalog)
    {
        return messageCatalog->pgettext_str(context, msgid);
    }
    else
    {
        return msgid;
    }
}

const String I18n::nget(const String &msgid, const String &msgid_plural, uint plural)
{
    if (messageCatalog)
    {
        return messageCatalog->ngettext_str(msgid, msgid_plural, plural);
    }
    else
    {
        return msgid;
    }
}

const String I18n::npget(const String &msgid, const String &msgid_plural, const String &context, uint plural)
{
    if (messageCatalog)
    {
        return messageCatalog->npgettext_str(context, msgid, msgid_plural, plural);
    }
    else
    {
        return msgid;
    }
}
