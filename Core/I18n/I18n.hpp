#ifndef I18N_HPP
#define I18N_HPP

#include <Types/String.hpp>

class I18n final
{
    I18n() = delete;

public:
    static bool setToActiveLocale();

    static bool setCatalogFromFile(const String &poFile);
    static bool setCatalogFromMemory(const String &poContents);

    static const String get(const String &msgid);
    static const String pget(const String &msgid, const String &context);
    static const String nget(const String &msgid, const String &msgid_plural, uint plural);
    static const String npget(const String &msgid, const String &msgid_plural, const String &context, uint plural);

private:

};

#endif // I18N_HPP
