#include "KbHit.hpp"

#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/select.h>

#include <Core/Logger.hpp>

int KbHit::sleep_internal = static_cast<int>(0.3 * 1e+6);

namespace {
    inline static int kbhit(int seconds, int microseconds)
    {
        struct timeval tv;
        fd_set fds;
        tv.tv_sec = seconds;
        tv.tv_usec = microseconds;
        FD_ZERO(&fds);
        FD_SET(STDIN_FILENO, &fds); // STDIN_FILENO is 0
        select(STDIN_FILENO+1, &fds, nullptr, nullptr, &tv);
        return FD_ISSET(STDIN_FILENO, &fds);
    }
}

void KbHit::SetSleepInterval(const double &interval)
{
    if (interval >= 0.15)
        sleep_internal = static_cast<int>(interval * 1e+6);
    else
    {
        LOG_ERROR("KbHit: sleep interval too small. must be >= 0.15; using default of 0.3");
        sleep_internal = static_cast<int>(0.3 * 1e+6);
    }
}

int KbHit::kbhit()
{
    return ::kbhit(0, sleep_internal);
}
