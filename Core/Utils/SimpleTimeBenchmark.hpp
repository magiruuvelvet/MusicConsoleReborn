#ifndef SIMPLETIMEBENCHMARK_HPP
#define SIMPLETIMEBENCHMARK_HPP

#include <Core/Logger.hpp>
#include <chrono>

class SimpleTimeBenchmark final
{
    SimpleTimeBenchmark() = delete;

    using TimePoint = std::chrono::high_resolution_clock::time_point;
    using Rep = std::chrono::high_resolution_clock::rep;

public:
    inline static TimePoint Tick()
    { return std::chrono::high_resolution_clock::now(); }

    inline static Rep Duration(const TimePoint &tp1, const TimePoint &tp2)
    { return std::chrono::duration_cast<std::chrono::microseconds>(tp2 - tp1).count(); }

    inline static void PrintDuration(const TimePoint &tp1, const TimePoint &tp2)
    { LOG_INFO("Duration: %iµs", Duration(tp1, tp2)); }
};

#endif // SIMPLETIMEBENCHMARK_HPP
