#include "SearchTerms.hpp"

#include <Core/Logger.hpp>

uint qHash(const SearchTerms &search_terms, uint seed)
{
    uint hash = 0;
    for (auto&& f : search_terms.filters)
        hash += qHash(f.rawData, seed) + qHash(f.type, seed);
    return hash;
}

SearchTerms::SearchTerms()
{
}

/**
 * startsWith -> reg: interpret as regular expression
 * find and extract
 *   wo:""  until end or next    (without this term)
 *   wg:""  ---#---              (exclude this genre)
 *
 * Notice: an empty reg ex is equal to ".*" and matches everything
 *
 */

const QRegularExpression SearchTerms::FILTER_COMMANDS = QRegularExpression("\\ wo\\:|\\ wg\\:",
                                                        QRegularExpression::CaseInsensitiveOption);
const QRegularExpression SearchTerms::MAIN_EMPTY_CHECK = QRegularExpression("^\\wo\\:.*$|^\\wg\\:.*$",
                                                         QRegularExpression::CaseInsensitiveOption);
QByteArrayList SearchTerms::movetobottomphrases;

SearchTerms::SearchTerms(const QString &searchTerms)
{
    QString dd = searchTerms;

    // prepend a space when the MAIN filter is empty
    if (MAIN_EMPTY_CHECK.match(dd).hasMatch())
        dd.prepend(' ');

    // split all filter commands apart
    auto split = dd.split(FILTER_COMMANDS, QString::KeepEmptyParts);
    for (auto& entry : split) entry = entry.simplified();

    // find position of all filter commands
    QList<FilterType> types{MAIN};
    int pos = -1;
    while ((pos = dd.indexOf(FILTER_COMMANDS, pos + 1)) != -1)
    {
        const auto &cmd = dd.mid(pos + 1, 2);

        if (cmd.compare("wo", Qt::CaseInsensitive) == 0)
            types.append(WITHOUT);
        else if (cmd.compare("wg", Qt::CaseInsensitive) == 0)
            types.append(WITHOUT_GENRE);
    }

    for (auto i = 0; i < split.size(); i++)
    {
        const auto &current = split.at(i);
        Filter filter;
        filter.type = types.at(i);
        QString tmpRawData;

        // user already specified a regular expression
        if (current.startsWith("reg:", Qt::CaseInsensitive) ||
            current.startsWith("regex:", Qt::CaseInsensitive))
        {
            tmpRawData = current.mid(4);
        }

        else if (current.startsWith("sp:", Qt::CaseInsensitive))
        {
            const auto &cmd = current.mid(3);

            if (cmd.isEmpty())
            {
                LOG_WARNING("SearchTerms: empty \"sp:\"", cmd);
            }

            else if (cmd.compare("bottomphrases", Qt::CaseInsensitive) == 0)
            {
                for (auto ph : movetobottomphrases)
                {
                    ph = EscapeReservedChars(ph);
                    ph = ph.simplified();
                    ph = QString(ph).replace(QRegularExpression("[\\s]"), ".*").toUtf8();

                    if (ph == ".*")
                        ph.clear();

                    if (!ph.isEmpty())
                    {
                        ph.prepend(".*");
                        ph.append(".*");
                    }

                    tmpRawData.append("^" + ph + "$|");
                }
                tmpRawData.remove(tmpRawData.size() - 1, 2);
            }

            else if (cmd.compare("empty", Qt::CaseInsensitive) == 0)
            {
                // regular expression to match an entirely empty string
                // seems a bit unnesecarlily complex to match literally nothing :/
                tmpRawData = R"(^(?![\s\S]))";
            }

            else
            {
                LOG_WARNING("SearchTerms: unknown \"sp:\": %s", cmd);
            }
        }

        // generate regular expression from raw input
        else
        {
            tmpRawData = current;
            // escape reserved characters
            tmpRawData = EscapeReservedChars(tmpRawData.toUtf8());
            tmpRawData = tmpRawData.simplified();
            tmpRawData.replace(QRegularExpression("[\\s]"), ".*");

            if (tmpRawData == ".*")
                tmpRawData.clear();

            if (!tmpRawData.isEmpty())
            {
                tmpRawData.prepend(".*");
                tmpRawData.append(".*");
            }

            // fix empty main filter
            if (filter.type == MAIN && tmpRawData.isEmpty())
            {
                tmpRawData = ".*";
            }
        }

        filter.rawData = tmpRawData.toUtf8();
        filter.data = QRegularExpression(filter.rawData, QRegularExpression::CaseInsensitiveOption);
        filters.append(filter);
    }
}

SearchTerms::~SearchTerms()
{
}

void SearchTerms::SetMoveToBottomPhrases(const QByteArrayList &mtbp)
{
    movetobottomphrases = mtbp;
}

const QByteArray SearchTerms::EscapeReservedChars(const QByteArray &input)
{
    QByteArray res = input;

    // QRegularExpression(R"([\.\:\^\$\*\+\-\?\(\)\[\]\{\}\\\|])")

#define ESCAPE_CHAR(CHAR) \
    case CHAR: { \
                          /* hack to concatenate 2 chars */ \
        res.replace(i, 1, (QString('\\')+CHAR).toUtf8().constData()); \
        i++; \
    } break

    for (auto i = 0; i < res.size(); i++)
    {
        switch (res.at(i))
        {
            ESCAPE_CHAR('.');
            ESCAPE_CHAR(':');
            ESCAPE_CHAR('^');
            ESCAPE_CHAR('$');
            ESCAPE_CHAR('*');
            ESCAPE_CHAR('+');
            ESCAPE_CHAR('-');
            ESCAPE_CHAR('?');
            ESCAPE_CHAR('(');
            ESCAPE_CHAR(')');
            ESCAPE_CHAR('[');
            ESCAPE_CHAR(']');
            ESCAPE_CHAR('{');
            ESCAPE_CHAR('}');
            ESCAPE_CHAR('\\');
            ESCAPE_CHAR('|');
        }
    }
    return res;
}
