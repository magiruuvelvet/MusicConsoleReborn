#ifndef RANDOMIZER_HPP
#define RANDOMIZER_HPP

#include <vector>
#include <cinttypes>
#include <type_traits>

class Randomizer final
{
    Randomizer() = delete;

public:
    static void SetHistorySize(std::uint64_t size);

    static std::uint64_t GetRandomNumber();
    static std::uint64_t GetRandomNumber(std::uint64_t min, std::uint64_t max);

    template<typename T = signed int>
    static T GetRandomInt()
    {
        static_assert(std::is_integral<T>::value &&
                      std::is_signed<T>::value,
                      "Randomizer::GetRandomInt(): must be a signed integral type");
        return static_cast<T>(GetRandomNumber());
    }

    template<typename T = signed int>
    static T GetRandomInt(T min, T max)
    {
        static_assert(std::is_integral<T>::value &&
                      std::is_signed<T>::value,
                      "Randomizer::GetRandomInt(min, max): must be a signed integral type");
        return static_cast<T>(GetRandomNumber(static_cast<std::uint64_t>(min), static_cast<std::uint64_t>(max)));
    }

    template<typename T = unsigned int>
    static T GetRandomUInt()
    {
        static_assert(std::is_integral<T>::value &&
                      std::is_unsigned<T>::value,
                      "Randomizer::GetRandomUInt(): must be a unsigned integral type");
        return static_cast<T>(GetRandomNumber());
    }

    template<typename T = unsigned int>
    static T GetRandomUInt(T min, T max)
    {
        static_assert(std::is_integral<T>::value &&
                      std::is_unsigned<T>::value,
                      "Randomizer::GetRandomUInt(min, max): must be a unsigned integral type");
        return static_cast<T>(GetRandomNumber(static_cast<std::uint64_t>(min), static_cast<std::uint64_t>(max)));
    }

    template<typename T = float>
    static T GetRandomFloat()
    {
        static_assert(std::is_same<T, float>::value,
                      "Randomizer::GetRandomFloat(): must be a 32-bit floating point type");
        return static_cast<T>(GetRandomNumber());
    }

    template<typename T = float>
    static T GetRandomFloat(T min, T max)
    {
        static_assert(std::is_same<T, float>::value,
                      "Randomizer::GetRandomFloat(): must be a 32-bit floating point type");
        return static_cast<T>(GetRandomNumber(static_cast<std::uint64_t>(min), static_cast<std::uint64_t>(max)));
    }

    template<typename T = double>
    static T GetRandomDouble()
    {
        static_assert(std::is_same<T, double>::value ||
                      std::is_same<T, long double>::value,
                      "Randomizer::GetRandomDouble(): must be a 64-bit or 128-bit floating point type");
        return static_cast<T>(GetRandomNumber());
    }

    template<typename T = double>
    static T GetRandomDouble(T min, T max)
    {
        static_assert(std::is_same<T, double>::value ||
                      std::is_same<T, long double>::value,
                      "Randomizer::GetRandomDouble(): must be a 64-bit or 128-bit floating point type");
        return static_cast<T>(GetRandomNumber(static_cast<std::uint64_t>(min), static_cast<std::uint64_t>(max)));
    }

private:
    static std::uint64_t QueryRandomDevice(bool *error = nullptr);
    static std::uint64_t QueryRandomDevice_Helper(const char *random_device, bool *error = nullptr);
    static std::uint64_t _GetRangedRandomNumberHelper(std::uint64_t min, std::uint64_t max, std::uint64_t seed);

    static std::vector<std::uint64_t> history;
    static const std::uint64_t max_attempts;
};

#endif // RANDOMIZER_HPP
