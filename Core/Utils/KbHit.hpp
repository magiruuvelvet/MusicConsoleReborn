#ifndef KBHIT_HPP
#define KBHIT_HPP

#include <iostream>

// Waits a specific time for user response to break out of a(n) (infinite) loop.
// Break key == Carriage Return [ONLY]

class KbHit final
{
    KbHit() = delete;

public:
    static void SetSleepInterval(const double &interval);
    static int kbhit();

private:
    static int sleep_internal;
};

// start of non-infinite breakable loop
#define KBHIT_NOT_INFINITE(KBHIT_LOOP_END_CONDITION) \
    bool kbhit_normal_loop_end = false; \
    while (!KbHit::kbhit()) \
    { \
        if (KBHIT_LOOP_END_CONDITION) \
        { \
            kbhit_normal_loop_end = true; \
            break; \
        }

// end of non-infinite breakable loop
#define KBHIT_NOT_INFINITE_END \
    } \
    if (!kbhit_normal_loop_end) \
    { \
        std::cin.clear(); \
        std::cin.get(); \
    }

// start of kbhit infinite loop
#define KBHIT \
    while (!KbHit::kbhit()) \
    {

// end of kbhit infinite loop
#define KBHIT_END \
    } \
    std::cin.clear(); \
    std::cin.get();

#endif // KBHIT_HPP
