#include "AppearanceController.hpp"

#include <Utils/TermColor.hpp>

#include <Core/Logger.hpp>

AppearanceSettings AppearanceController::settings;
const char *AppearanceController::TERM_RESET = TermColor::code(TermColor::Reset);

void AppearanceController::SetAppearanceSettings(const AppearanceSettings &s)
{
    settings = s;
}

// "artist"       // metadata: artist
// "album"        // metadata: album
// "albmartist"   // metadata: album_artist
// "title"        // metadata: title
// "genre"        // metadata: genre
// "track"        // metadata: track
// "total_tracks" // metadata: total_tracks
// "extension"    // file extension {fileInfo.suffix()}
// "path"         // relative path to the model {searchTerms[0]}

// "print_tagged" // when MediaFile::hasMetadata() returns true
// "print_plain"  // ---"--- returns false

const String AppearanceController::GetPrettyLine(const MediaFile *mf)
{
    if (!mf)
        return {};

    String line;

    // print_tagged
    if (mf->hasMetadata())
    {
        PreparePrintTaggedLine(line, mf);
    }

    // print_plain
    else
    {
        line = settings.print_plain;
        PreparePrintPlainLine(line, mf);
    }

    // append terminal reset to avoid further output with buggy formatting
    line.append(TERM_RESET);
    return line;
}

void AppearanceController::Print(const MediaFile *mf)
{
    // faster than LOG_*
    std::printf("%s\n", *GetPrettyLine(mf));
}

void AppearanceController::PreparePrintTaggedLine(String &line, const MediaFile *mf)
{
    auto groups = Groups(settings.print_tagged);
    for (auto& group : groups)
    {
        if (CountPlaceholders(group) > 1)
        {
            LOG_ERROR("AppearanceController: group (%s) has more than 1 placeholder!", group);
            continue;
        }

        for (auto&& i : std::map<String, Field>{
            {"$artist",       artist},
            {"$album",        album},
            {"$albmartist",   albmartist},
            {"$title",        title},
            {"$genre",        genre},
            {"$track",        track},
            {"$total_tracks", total_tracks},
            {"$extension",    extension},
            {"$path",         path},
        })
        {
            ReplaceGroup(line, group, i.first, i.second, ValueFor(mf, i.second));
            if (group.isEmpty())
                break;
        }
    }
}

void AppearanceController::PreparePrintPlainLine(String &line, const MediaFile *mf)
{
    line.replace("$extension",     PrepareField(extension, mf->fileextension));
    line.replace("$path",          PrepareField(path, mf->prettyPath()));
}

const String AppearanceController::PrepareField(const Field &field, const String &s)
{
    switch (field)
    {
        case artist:       return String(settings.artist).replace("%s", s);
        case album:        return String(settings.album).replace("%s", s);
        case albmartist:   return String(settings.albmartist).replace("%s", s);
        case title:        return String(settings.title).replace("%s", s);
        case genre:        return String(settings.genre).replace("%s", s);
        case track:        return String(settings.track).replace("%s", s);
        case total_tracks: return String(settings.total_tracks).replace("%s", s);
        case extension:    return String(settings.extension).replace("%s", s);
        case path:         return String(settings.path).replace("%s", s);
    }
}

// FIXME: add back reference? (std::move)
const String AppearanceController::ValueFor(const MediaFile *mf, const Field &field)
{
    switch (field)
    {
        case artist:       return mf->metadata.artist;
        case album:        return mf->metadata.album;
        case albmartist:   return mf->metadata.album_artist;
        case title:        return mf->metadata.title;
        case genre:        return mf->metadata.genre;
        case track:        return String::number(mf->metadata.track);
        case total_tracks: return String::number(mf->metadata.total_tracks);
        case extension:    return mf->fileextension;
        case path:         return mf->prettyPath(); // FIXME: QByteArray constructor is used here!
    }
}

void AppearanceController::ReplaceGroup(String &line, String &group, const String &placeholder,
                                        const Field &field, const String &value)
{
    if (group.isEmpty())
        return;

    if (!value.isEmpty() && group.contains(placeholder))
    {
        line.append(String(group).replace(placeholder, PrepareField(field, value)));
        group.clear();
    }
}

const StringList AppearanceController::Groups(const String &input)
{
    StringList groups;
    String::size_type open = String::npos, close = String::npos;
    for (String::size_type i = 0; i < input.size(); i++)
    {
        if (open == String::npos)
        {
            open = input.index_of('(', i);
            open != String::npos ? i = open : int();
            if (i != 0)
            {
                if (input.at(i-1) == '\\')
                {
                    open = String::npos;
                    continue;
                }
            }
        }

        if (close == String::npos)
        {
            close = input.index_of(')', i);
            close != String::npos ? i = close : int();
            if (i != 0)
            {
                if (input.at(i-1) == '\\')
                {
                    close = String::npos;
                    continue;
                }
            }
        }

        if (open != String::npos && open != String::npos)
        {
            groups.append(input.mid(open+1, close - open - 1)
                          .replace("\\(", "(")
                          .replace("\\)", ")"));
            open = String::npos;
            close = String::npos;
        }
    }
    return groups;
}

uint AppearanceController::CountPlaceholders(const String &group)
{
    uint count = 0;
    for (String::size_type i = 0; i < group.size(); i++)
    {
        if (group.at(i) == '$')
        {
            if (i != 0 && group.at(i-1) == '\\')
            {
                continue;
            }
            else
            {
                count++;
            }
        }
    }
    return count;
}
