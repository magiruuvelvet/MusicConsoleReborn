#ifndef EXPANDENVIRONMENT_HPP
#define EXPANDENVIRONMENT_HPP

#include <QString>

class QRegularExpression;

class ExpandEnvironment final
{
    ExpandEnvironment() = delete;

public:
    static const QByteArray ExpandEnvironmentVariables(const QByteArray &s);
    static const QByteArray Home2Tilde(const QByteArray &s);

private:
    static const QRegularExpression posix_env_variables;
};

#endif // EXPANDENVIRONMENT_HPP
