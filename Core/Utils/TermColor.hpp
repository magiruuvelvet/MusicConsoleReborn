#ifndef TERMCOLOR_HPP
#define TERMCOLOR_HPP

#include <string>

#include <Types/BitwiseFlags.hpp>

class TermColor final
{
    TermColor() = delete;

public:
    using ColorCode = unsigned char; // 8-bit unsigned

    struct RgbColor {
        ColorCode R = 0,
                  G = 0,
                  B = 0;
    };

    enum Style {
        Reset            = 0x00,   // 0

        Regular          = 0x01,   // <empty>
        Bold             = 0x02,   // 1
        Italic           = 0x04,   // 3

        Underline        = 0x08,   // 4
        Blinking         = 0x16,   // 5
        StrikeThrough    = 0x32,   // 9
    };
    DECLARE_BITWISE_FLAGS(StyleOptions, Style)

    // foreground color and style
    static const std::string fg(const RgbColor &color, const StyleOptions &options = Regular)
    { return c(color, options, "38;2"); }

    // background color
    static const std::string bg(const RgbColor &color)
    { return c(color, Regular, "48;2"); }

    // reset style
    static inline const std::string &reset()
    { return reset_seq; }

    // style code
    static inline constexpr const char *code(const Style &style)
    {
        switch (style)
        {
            case Reset:         return "\033[0m";

            case Regular:       return "";
            case Bold:          return "\033[1m";
            case Italic:        return "\033[3m";

            case Underline:     return "\033[4m";
            case Blinking:      return "\033[5m";
            case StrikeThrough: return "\033[9m";
        }
    }

private:
    static const std::string reset_seq;

    static inline const std::string c(const RgbColor &color, const StyleOptions &options, const std::string &modifier)
    {
        std::string style = "\033[";

        if (options.testFlag(Reset))         style.append("0;");

        if (options.testFlag(Regular))       {}
        if (options.testFlag(Bold))          style.append("1;");
        if (options.testFlag(Italic))        style.append("3;");

        if (options.testFlag(Underline))     style.append("4;");
        if (options.testFlag(Blinking))      style.append("5;");
        if (options.testFlag(StrikeThrough)) style.append("9;");

        style.append(modifier + ';');
        style.append(std::to_string(color.R) + ';');
        style.append(std::to_string(color.G) + ';');
        style.append(std::to_string(color.B) + ';');

        style.erase(style.end() - 1);
        style.append("m");
        return style;
    }
};

DECLARE_OPERATORS_FOR_BITWISE_FLAGS(TermColor::StyleOptions);

#endif // TERMCOLOR_HPP
