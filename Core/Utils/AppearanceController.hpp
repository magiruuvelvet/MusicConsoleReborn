#ifndef APPEARANCECONTROLLER_HPP
#define APPEARANCECONTROLLER_HPP

#include <Media/MediaFile.hpp>

struct AppearanceSettings
{
    String artist;
    String album;
    String albmartist;
    String title;
    String genre;
    String track;
    String total_tracks;
    String extension;
    String path;
    String print_tagged;
    String print_plain;
};

class AppearanceController final
{
    AppearanceController() = delete;

public:
    static void SetAppearanceSettings(const AppearanceSettings &settings);
    static const String GetPrettyLine(const MediaFile *mf);
    static void Print(const MediaFile *mf);

private:
    static AppearanceSettings settings;
    static const char *TERM_RESET;

    enum Field {
        artist,
        album,
        albmartist,
        title,
        genre,
        track,
        total_tracks,
        extension,
        path,
    };

    static void PreparePrintTaggedLine(String &line, const MediaFile *mf);
    static void PreparePrintPlainLine(String &line, const MediaFile *mf);
    static const String PrepareField(const Field &field, const String &s); // %s
    static const String ValueFor(const MediaFile *mf, const Field &field);
    static void ReplaceGroup(String &line, String &group, const String &placeholder,
                             const Field &field, const String &value);

    static const StringList Groups(const String &input);
    static uint CountPlaceholders(const String &group);
};

#endif // APPEARANCECONTROLLER_HPP
