#ifndef PREFIXREMOVER_HPP
#define PREFIXREMOVER_HPP

#include <QByteArray>
#include <QFileInfo>

class PrefixRemover final
{
    PrefixRemover() = delete;

public:
    static void SetPrefixDeletionPatterns(const QByteArrayList &prefixdeletionpatterns);
    static const QByteArray RemovePrefix(const QFileInfo &fileinfo);

private:
    static QByteArrayList prefixdeletionpatterns;
};

#endif // PREFIXREMOVER_HPP
