#include "PrefixRemover.hpp"

QByteArrayList PrefixRemover::prefixdeletionpatterns;

void PrefixRemover::SetPrefixDeletionPatterns(const QByteArrayList &pdp)
{
    prefixdeletionpatterns = pdp;
}

const QByteArray PrefixRemover::RemovePrefix(const QFileInfo &fileinfo)
{
    QString removed = fileinfo.absoluteFilePath();
    for (auto&& prefix : prefixdeletionpatterns)
    {
        if (removed.startsWith(prefix))
        {
            removed.remove(0, prefix.size());
            break;
        }
    }
    return removed.toUtf8();
}
