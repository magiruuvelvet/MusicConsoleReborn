#ifndef SEARCHTERMS_HPP
#define SEARCHTERMS_HPP

#include <QRegularExpression>

class SearchTerms final
{
    friend uint qHash(const SearchTerms&, uint seed);

public:
    SearchTerms();
    SearchTerms(const QString &searchTerms);
    ~SearchTerms();

    static void SetMoveToBottomPhrases(const QByteArrayList &movetobottomphrases);
    static const QByteArray EscapeReservedChars(const QByteArray &input);

    enum FilterType {
        MAIN,           // <first element in the list>
        WITHOUT,        // wo:
        WITHOUT_GENRE,  // wg:
    };

    struct Filter {
        QRegularExpression data;
        QByteArray rawData;
        FilterType type;

        // this filter
        inline bool isEmpty() const
        { return this->rawData.isEmpty(); }
        inline bool isValid() const
        { return this->data.isValid(); }
    };

    // complete isValid check for all filters
    inline bool allFiltersValid() const
    {
        for (auto&& f : GetAllFilters())
            if (!f.isValid())
                return false;
        return true;
    }

    // complete isEmpty check for all filters
    inline bool allFiltersEmpty() const
    {
        for (auto&& f : GetAllFilters())
            if (!f.isEmpty())
                return false;
        return true;
    }

    // check if one or more filters are empty
    inline bool someFiltersEmpty() const
    {
        for (auto&& f : GetAllFilters())
            if (f.isEmpty())
                return true;
        return false;
    }

    // main filter
    inline bool isEmpty() const
    { return filters.at(0).rawData.isEmpty(); }
    inline bool isValid() const
    { return filters.at(0).data.isValid(); }

    // main filter
    inline const QRegularExpression &data() const
    { return filters.at(0).data; }
    inline const QByteArray &rawData() const
    { return filters.at(0).rawData; }

    // get filters of the given type
    inline const QList<Filter> GetFilters(FilterType type) const
    {
        QList<Filter> f;
        for (auto&& fil : filters)
            if (fil.type == type)
                f.append(fil);
        return f;
    }

    // returns all filters
    inline const QList<Filter> &GetAllFilters() const
    { return filters; }

    // returns all sub filters, doesn't return the main filter
    inline const QList<Filter> GetAllSubFilters() const
    { return filters.mid(1); }

    inline bool operator== (const SearchTerms &other) const
    {
        if (filters.size() != other.filters.size())
            return false;
        for (auto i = 0; i < filters.size(); i++)
            if (filters.at(i).type != other.filters.at(i).type ||
                filters.at(i).rawData != other.filters.at(i).rawData)
                return false;
        return true;
    }
    inline bool operator!= (const SearchTerms &other) const
    { return !operator==(other); }

private:
    QList<Filter> filters;

    static const QRegularExpression FILTER_COMMANDS;
    static const QRegularExpression MAIN_EMPTY_CHECK;
    static QByteArrayList movetobottomphrases;
};

// QHash support
extern uint qHash(const SearchTerms &search_terms, uint seed = 0);

#endif // SEARCHTERMS_HPP
