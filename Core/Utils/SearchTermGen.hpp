#ifndef SEARCHPATHGEN_HPP
#define SEARCHPATHGEN_HPP

#include <QByteArrayList>

#define SEARCHTERMGEN_NAMESPACE \
    namespace SearchTermGens

class SearchTermGen
{
public:
    SearchTermGen();
    virtual ~SearchTermGen();

    virtual const QByteArrayList processString(const QByteArray &input) const = 0;
};

#endif // SEARCHPATHGEN_HPP
