#ifndef PROCESSUPTIME_HPP
#define PROCESSUPTIME_HPP

#include <chrono>
#include <memory>

#include <Types/StrFmt.hpp>

class ProcessUptime final
{
    ProcessUptime() = delete;

public:
    inline static void Start()
    { _start = GetCurrentTime(); }

    inline static std::uint64_t ElapsedSeconds()
    { return static_cast<std::uint64_t>(
          std::chrono::duration_cast<std::chrono::seconds>(GetCurrentTime() - _start).count()
      );
    }
    inline static const std::string ElapsedSecondsStr()
    { return fmt::format("%lli", ElapsedSeconds()); }

    inline static const std::string ElapsedHMS()
    {
        std::uint64_t minutes = ElapsedSeconds() / 60;
        std::uint64_t seconds = ElapsedSeconds() % 60;
        std::uint64_t hours   = minutes / 60;
                      minutes = minutes % 60;
        return fmt::format("%02lli:%02lli:%02lli", hours, minutes, seconds);
    }

private:
    using timer_t = std::chrono::time_point<std::chrono::steady_clock, std::chrono::steady_clock::duration>;
    static timer_t _start;

    inline static timer_t GetCurrentTime()
    { return std::chrono::steady_clock::now(); }
};

#endif // PROCESSUPTIME_HPP
