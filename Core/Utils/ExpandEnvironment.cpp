#include "ExpandEnvironment.hpp"

#include <Core/MusicConsoleReborn.hpp>

#include <QRegularExpression>

const QRegularExpression ExpandEnvironment::posix_env_variables =
    QRegularExpression(R"(\$(([a-zA-Z_]+[a-zA-Z0-9_]*)|\{([a-zA-Z_]+[a-zA-Z0-9_]*)\}))");

const QByteArray ExpandEnvironment::ExpandEnvironmentVariables(const QByteArray &s)
{
    const auto &vars = posix_env_variables.match(s);
    if (vars.hasMatch())
    {
        auto captured = vars.capturedTexts();
        captured.removeDuplicates();
        for (auto i = 0; i < captured.size(); i++)
        {
            if (captured.at(i).startsWith('$'))
            {
                captured.removeAt(i);
                i--;
            }
        }

        QByteArray expanded = s;
        for (auto&& var : captured)
        {
            if (MusicConsoleReborn::processEnvironment().contains(var))
            {
                const auto &varExpanded = MusicConsoleReborn::processEnvironment().value(var).toUtf8();
                const auto &varBegin = s.indexOf(var) - 1;
                expanded.replace(varBegin, var.size() + 1, varExpanded);
            }
        }

        return expanded;
    }

    // no match, return original
    return s;
}

const QByteArray ExpandEnvironment::Home2Tilde(const QByteArray &s)
{
    static const auto HOME = MusicConsoleReborn::processEnvironment().value("HOME").toUtf8();
    if (s.startsWith(HOME))
        return '~' + s.mid(HOME.size());
    return s;
}
