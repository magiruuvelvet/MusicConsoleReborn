#include "UniversalJapaneseKanaGen.hpp"

#include <map>
#include <vector>

SEARCHTERMGEN_NAMESPACE {

// include private helpers, not for standalone usage
namespace {
    #include "Private/KanaFix.cxx"
    #include "Private/KanaCompare.cxx"

    static const KanaFix kanaFix;
    static const KanaCompare kanaCompare;
}

const QByteArrayList UniversalJapaneseKanaGen::processString(const QByteArray &input) const
{
    QString data(input);

    // fix (broken?) dakuten coding
    kanaFix.fixChar(data);

    QByteArrayList searchMap;
    int size = 0;

    // append fixed string, if you use a "shitty" IME, the original file path is also included in the list
    // so it doesn't really matter ;)
    searchMap.append(data.toUtf8());

    // create a string for only hiragana and only katakana, for a more comfortable lookup
    QString hiragana = data;
    QString katakana = data;
    QString halfwidth = data;

    size = hiragana.size();
    for (auto i = 0; i < size; i++)
        hiragana.replace(i, 1, kanaCompare.toHiragana(hiragana.at(i)));

    size = katakana.size();
    for (auto i = 0; i < size; i++)
        katakana.replace(i, 1, kanaCompare.toKatakana(katakana.at(i)));

    // string size changes
    for (auto i = 0; i < halfwidth.size(); i++)
        halfwidth.replace(i, 1, kanaCompare.toHalfwidthKatakana(halfwidth.at(i)));

    // append the strings
    searchMap.append(hiragana.toUtf8());
    searchMap.append(katakana.toUtf8());
    searchMap.append(halfwidth.toUtf8());

    return searchMap;
}

}
