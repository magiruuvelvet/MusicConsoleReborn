#ifndef UNICODELATINGEN_HPP
#define UNICODELATINGEN_HPP

#include "../SearchTermGen.hpp"

#include <QMap>

SEARCHTERMGEN_NAMESPACE {

class UnicodeLatinGen : public SearchTermGen
{
public:
    const QByteArrayList processString(const QByteArray &input) const override;

private:
    static const QMap<QChar, QChar> LATIN_MAP;
    using Iter = QMap<QChar, QChar>::const_iterator;

    static bool isLatinChar(const QChar &c);
    static bool isLatin1Char(const QChar &c);
    static bool isFullwidthLatinChar(const QChar &c);

    static QChar toLatin1(const QChar &c);
    static QChar toFullwidthLatin(const QChar &c);
};

}

#endif // UNICODELATINGEN_HPP
