#ifndef UNIVERSALJAPANESEKANAGEN_HPP
#define UNIVERSALJAPANESEKANAGEN_HPP

#include "../SearchTermGen.hpp"

SEARCHTERMGEN_NAMESPACE {

class UniversalJapaneseKanaGen : public SearchTermGen
{
public:
    const QByteArrayList processString(const QByteArray &input) const override;
};

}

#endif // UNIVERSALJAPANESEKANAGEN_HPP
