#ifndef UNICODEWHITESPACEFIXER_HPP
#define UNICODEWHITESPACEFIXER_HPP

#include "../SearchTermGen.hpp"

SEARCHTERMGEN_NAMESPACE {

class UnicodeWhitespaceFixer : public SearchTermGen
{
public:
    inline static const QByteArray processStringStatic(const QByteArray &input)
    {
        UnicodeWhitespaceFixer f;
        return f.processString(input).at(0);
    }

    const QByteArrayList processString(const QByteArray &input) const override;

private:
    static bool isWhitespace(const ushort &c);
    static const QList<ushort> WHITESPACE_CHARS;
};

}

#endif // UNICODEWHITESPACEFIXER_HPP
