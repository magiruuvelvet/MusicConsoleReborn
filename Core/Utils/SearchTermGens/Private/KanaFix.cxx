class KanaFix
{
public:
  enum class Mode {
      Default,
      Abnormal
  };

  // Parses the whole string and fixes all kana characters with (broken?) dakuten.
  void fixChar(QString &str) const
  {
      // analyze string for kanas
      for (int i = 0; i < str.size(); i++)
      {
          if (this->isKana(str.at(i).unicode()))
          {
              // check for dakuten after kana
              if ( (i+1) > str.size() ) break;
              if (this->isDakuten(str.at(i+1).unicode()))
              {
                  /// replace: kana+dakuten with dakuten included kana
                  str.replace(i, 2, this->getKanaWDakuten(str.at(i).unicode(), str.at(i+1).unicode()));
              } // endOf: isDakuten
          } // endOf: isKana
      }
  }

  // Parses the whole string and seperates all kana characters to its kana and dakuten.
  void breakChar(QString &str, Mode mode = Mode::Default) const
  {
      // analyze string for kanas
      for (int i = 0; i < str.size(); i++)
      {
          if (this->isKanaWDakuten(str.at(i).unicode()))
          {
              std::vector<QChar> data = this->getKanaDakutenSep(str.at(i).unicode(), mode);
              str.replace(i, data.at(0));
              str.insert(i+1, data.at(1));
              i++;
          } // endOf: isKana
      }
  }

private:
  enum class Dakuten {
    // JIS X 0216
    VOICED_SOUND_MARK,   // "
    VOICED_SOUND_MARK_1, //
    VOICED_SOUND_MARK_2, //
    SEMI_VOICED_SOUND    // °
  };

  bool isKana(ushort c) const
  {
      for (auto it = this->kana_map.begin(); it != this->kana_map.end(); it++)
          if (c == it->unicode())
              return true;
      return false;
  }

  bool isKanaWDakuten(ushort c) const
  {
      for (auto it = this->kana_dakuten_map.begin(); it != this->kana_dakuten_map.end(); it++)
          if (c == it->unicode())
              return true;
      return false;
  }

  bool isDakuten(ushort c) const
  {
      for (auto it = this->dakuten_map.begin(); it != this->dakuten_map.end(); it++)
          if (c == it->second.unicode())
              return true;
      return false;
  }

  QChar getKanaWDakuten(ushort kana, ushort dakuten) const
  {
    #define check_dakuten(type) dakuten == this->dakuten_map.at(Dakuten::type)

    // search kana pos in kana_map
    int pos = 0;
    for (; pos < (int) this->kana_map.size(); pos++)
        if (kana == this->kana_map.at(pos).unicode())
            break;

    // check type of dakuten(濁点)
    if (check_dakuten(SEMI_VOICED_SOUND))           // °
        return this->kana_dakuten_map.at(pos + 10);

    else if (check_dakuten(VOICED_SOUND_MARK)   ||  // "
             check_dakuten(VOICED_SOUND_MARK_1) ||
             check_dakuten(VOICED_SOUND_MARK_2))
        return this->kana_dakuten_map.at(pos);

    return QChar();
    #undef check_dakuten
  }

  std::vector<QChar> getKanaDakutenSep(ushort kana, Mode mode) const
  {
      // search kana pos in kana_dakuten_map
      int pos = 0;
      for(; pos < (int) this->kana_dakuten_map.size(); pos++)
          if (kana == this->kana_dakuten_map.at(pos).unicode())
              break;
      bool isSemi = false;
      if (pos > (int) this->kana_map.size()) { pos-= 10; isSemi = true; }

      std::vector<QChar> data; data.clear(); data.resize(2);
      switch (mode)
      {
          case Mode::Default:
              data[0] = this->kana_map.at(pos);
              if (isSemi) data[1] = this->dakuten_map.at(Dakuten::SEMI_VOICED_SOUND);
              else        data[1] = this->dakuten_map.at(Dakuten::VOICED_SOUND_MARK);
              //return QString(data, 2);
              return data;
              break;

          case Mode::Abnormal:
              data[0] = this->kana_map.at(pos);
              if (isSemi) data[1] = this->dakuten_map.at(Dakuten::SEMI_VOICED_SOUND);
              else        data[1] = this->dakuten_map.at(Dakuten::VOICED_SOUND_MARK_1);
              //return QString(data, 2);
              return data;
              break;
      }

      //return QString();
      return data;
  }

  // Contains all kanas that can have a dakuten (濁点).
  static const std::vector<QChar> kana_map;

  // Contains all kanas with dakuten (濁点) included.
  static const std::vector<QChar> kana_dakuten_map;

  // Contains all kinds on dakuten, actually there are only 2 of it ;-)
  static const std::map<Dakuten, QChar> dakuten_map;
};

#define KanaMap(id_h, id_k) QChar(id_h), QChar(id_k)
const std::vector<QChar> KanaFix::kana_map = {
    KanaMap(0x304B, 0x30AB), // か, カ
    KanaMap(0x304D, 0x30AD), // き, キ
    KanaMap(0x304F, 0x30AF), // く, ク
    KanaMap(0x3051, 0x30B1), // け, ヶ
    KanaMap(0x3053, 0x30B3), // こ, コ

    KanaMap(0x3055, 0x30B5), // さ, サ
    KanaMap(0x3057, 0x30B7), // し, シ
    KanaMap(0x3059, 0x30B9), // す, ス
    KanaMap(0x305B, 0x30BB), // せ, セ
    KanaMap(0x305D, 0x30BD), // そ, ソ

    KanaMap(0x305F, 0x30BF), // た, タ
    KanaMap(0x3061, 0x30C1), // ち, チ
    KanaMap(0x3064, 0x30C4), // つ, ツ
    KanaMap(0x3066, 0x30C6), // て, テ
    KanaMap(0x3068, 0x30C8), // と, ト

    KanaMap(0x306F, 0x30CF), // は, ハ
    KanaMap(0x3072, 0x30D2), // ひ, ヒ
    KanaMap(0x3075, 0x30D5), // ふ, フ
    KanaMap(0x3078, 0x30D8), // へ, ヘ
    KanaMap(0x307B, 0x30DB), // ほ, ホ
};

const std::vector<QChar> KanaFix::kana_dakuten_map = {
    KanaMap(0x304C, 0x30AC), // が, ガ
    KanaMap(0x304E, 0x30AE), // ぎ, ギ
    KanaMap(0x3050, 0x30B0), // ぐ, グ
    KanaMap(0x3052, 0x30B2), // げ, ゲ
    KanaMap(0x3054, 0x30B4), // ご, ゴ

    KanaMap(0x3056, 0x30B6), // ざ, ザ
    KanaMap(0x3058, 0x30B8), // じ, ジ
    KanaMap(0x305A, 0x30BA), // ず, ズ
    KanaMap(0x305C, 0x30BC), // ぜ, ゼ
    KanaMap(0x305E, 0x30BE), // ぞ, ゾ

    KanaMap(0x3060, 0x30C0), // だ, ダ
    KanaMap(0x3062, 0x30C2), // ぢ, ヂ
    KanaMap(0x3065, 0x30C5), // づ, ヅ
    KanaMap(0x3067, 0x30C7), // で, デ
    KanaMap(0x3069, 0x30C9), // ど, ド

    KanaMap(0x3070, 0x30D0), // ば, バ
    KanaMap(0x3073, 0x30D3), // び, ビ
    KanaMap(0x3076, 0x30D6), // ぶ, ブ
    KanaMap(0x3079, 0x30D9), // べ, ベ
    KanaMap(0x307C, 0x30DC), // ぼ, ボ

    KanaMap(0x3071, 0x30D1), // ぱ, パ
    KanaMap(0x3074, 0x30D4), // ぴ, ピ
    KanaMap(0x3077, 0x30D7), // ぷ, プ
    KanaMap(0x307A, 0x30DA), // ぺ, ペ
    KanaMap(0x307D, 0x30DD), // ぽ, ポ
};
#undef KanaMap

const std::map<KanaFix::Dakuten, QChar> KanaFix::dakuten_map = {
  {KanaFix::Dakuten::SEMI_VOICED_SOUND,   QChar(0x309A)}, // °  <- ICU/X11 [*NIX]; iOS/Android; ...
  {KanaFix::Dakuten::VOICED_SOUND_MARK,   QChar(0x309B)}, // "  <- ICU/X11 [*NIX]; ...
  {KanaFix::Dakuten::VOICED_SOUND_MARK_1, QChar(0x3099)}, // "  <- iOS/Android & some IMEs for Windows
  {KanaFix::Dakuten::VOICED_SOUND_MARK_2, QChar(0xFF9E)}, // "  <- ???
};
