class KanaCompare
{
public:
    bool compare(const QString &str1, const QString &str2) const
    {
        if (str1.size() != str2.size())
            return false;

        for (int i = 0; i < str1.size(); i++)
        {
            if (!this->compare_helper(str1.at(i).unicode(), str2.at(i).unicode()))
                return false;
        } return true;
    }

    // Convert Functions
    QChar toHiragana(const QChar &c) const
    {
        if (!this->is_kana(c.unicode()))
            return c;

        // check if char is katakana
        for (const auto &it : this->kana_map)
            if (c == it.second)
                // convert
                return QChar(it.first);
        return c;
    }

    QChar toKatakana(const QChar &c) const
    {
        if (!this->is_kana(c.unicode()))
            return c;

        // check if char is hiragana
        for (const auto &it : this->kana_map)
            if (c == it.first)
                // convert
                return QChar(it.second);
        return c;
    }

    // halfwidth-katakana with dakutens are not available in the unicode table, refer to unicode combining characters for details
    QString toHalfwidthKatakana(const QChar &c) const
    {
        if (!this->is_kana(c.unicode()))
            return QString(c);

        // convert to katakana
        QChar _c = c;
        _c = this->toKatakana(c);

        // convert to halfwidth
        for (const auto &it : this->halfwidth_katakana_map)
            if (_c == it.first)
                return it.second;
        return _c;
    }

private:
    static const std::map<QChar, QChar> kana_map;
    static const std::map<QChar, QString> halfwidth_katakana_map;

    bool compare_helper(ushort c1, ushort c2) const
    {
        // check if params are same
        if (c1 == c2) return true;

        // begin with actual compare
        for (const auto &it : this->kana_map)
        {
            for (const auto &it2 : this->halfwidth_katakana_map)
            {
                if (c1 == it.first  && c2 == it.first  ) return true;
                if (c1 == it.second && c2 == it.second ) return true;
                if (c1 == it.first  && c2 == it.second ) return true;
                if (c1 == it.second && c2 == it.first  ) return true;

                if (c1 == it.first   && QString(QChar(c2)) == it2.second ) return true;
                if (c1 == it.second  && QString(QChar(c2)) == it2.second ) return true;
            }
        }

        return false;
    }

    bool is_kana(ushort c) const
    {
        for (const auto &it : this->kana_map)
            if (c == it.first || c == it.second)
                return true;
        return false;
    }

    bool is_halfwidth_katakana(ushort c) const
    {
        for (const auto &it : this->halfwidth_katakana_map)
            if (QString(QChar(c)) == it.second)
                return true;
        return false;
    }
};

#define KanaMap(hiragana, katakana) {QChar(L##hiragana), QChar(L##katakana)}
const std::map<QChar, QChar> KanaCompare::kana_map = { // Hiragana, Katakana
    KanaMap('あ', 'ア'), KanaMap('い', 'イ'), KanaMap('う', 'ウ'), KanaMap('え', 'エ'), KanaMap('お', 'オ'),
    KanaMap('か', 'カ'), KanaMap('き', 'キ'), KanaMap('く', 'ク'), KanaMap('け', 'ケ'), KanaMap('こ', 'コ'),
    KanaMap('さ', 'サ'), KanaMap('し', 'シ'), KanaMap('す', 'ス'), KanaMap('せ', 'セ'), KanaMap('そ', 'ソ'),
    KanaMap('た', 'タ'), KanaMap('ち', 'チ'), KanaMap('つ', 'ツ'), KanaMap('て', 'テ'), KanaMap('と', 'ト'),
    KanaMap('な', 'ナ'), KanaMap('に', 'ニ'), KanaMap('ぬ', 'ヌ'), KanaMap('ね', 'ネ'), KanaMap('の', 'ノ'),
    KanaMap('は', 'ハ'), KanaMap('ひ', 'ヒ'), KanaMap('ふ', 'フ'), KanaMap('へ', 'ヘ'), KanaMap('ほ', 'ホ'),
    KanaMap('ま', 'マ'), KanaMap('み', 'ミ'), KanaMap('む', 'ム'), KanaMap('め', 'メ'), KanaMap('も', 'モ'),
    KanaMap('や', 'ヤ'), /*　　*/             KanaMap('ゆ', 'ユ'), /*　　*/             KanaMap('よ', 'ヨ'),
    KanaMap('ら', 'ラ'), KanaMap('り', 'リ'), KanaMap('る', 'ル'), KanaMap('れ', 'レ'), KanaMap('ろ', 'ロ'),
    KanaMap('わ', 'ワ'), KanaMap('ゐ', 'ヰ'), /*　　*/             KanaMap('ゑ', 'ヱ'), KanaMap('を', 'ヲ'),

    KanaMap('が', 'ガ'), KanaMap('ぎ', 'ギ'), KanaMap('ぐ', 'グ'), KanaMap('げ', 'ゲ'), KanaMap('ご', 'ゴ'),
    KanaMap('ざ', 'ザ'), KanaMap('じ', 'ジ'), KanaMap('ず', 'ズ'), KanaMap('ぜ', 'ゼ'), KanaMap('ぞ', 'ゾ'),
    KanaMap('だ', 'ダ'), KanaMap('ぢ', 'ヂ'), KanaMap('づ', 'ヅ'), KanaMap('で', 'デ'), KanaMap('ど', 'ド'),
    KanaMap('ば', 'バ'), KanaMap('び', 'ビ'), KanaMap('ぶ', 'ブ'), KanaMap('べ', 'ベ'), KanaMap('ぼ', 'ボ'),
    KanaMap('ぱ', 'パ'), KanaMap('ぴ', 'ピ'), KanaMap('ぷ', 'プ'), KanaMap('ぺ', 'ペ'), KanaMap('ぽ', 'ポ'),

    KanaMap('っ', 'ッ'), KanaMap('ん', 'ン'),
    KanaMap('ぁ', 'ァ'), KanaMap('ぃ', 'ィ'), KanaMap('ぅ', 'ゥ'), KanaMap('ぇ', 'ェ'), KanaMap('ぉ', 'ォ'),
    KanaMap('ゃ', 'ャ'), /*　　*/             KanaMap('ゅ', 'ュ'), /*　　*/             KanaMap('ょ', 'ョ'),

    KanaMap('ー', 'ｰ')
};
#undef KanaMap

#define KanaMap(katakana, halfwidth) {QChar(L##katakana), QString::fromUtf8(halfwidth)}
const std::map<QChar, QString> KanaCompare::halfwidth_katakana_map = { // Katakana, Half-width Katakana
    KanaMap('ア', "ｱ"), KanaMap('イ', "ｲ"), KanaMap('ウ', "ｳ"), KanaMap('エ', "ｴ"), KanaMap('オ', "ｵ"),
    KanaMap('カ', "ｶ"), KanaMap('キ', "ｷ"), KanaMap('ク', "ｸ"), KanaMap('ケ', "ｹ"), KanaMap('コ', "ｺ"),
    KanaMap('サ', "ｻ"), KanaMap('シ', "ｼ"), KanaMap('ス', "ｽ"), KanaMap('セ', "ｾ"), KanaMap('ソ', "ｿ"),
    KanaMap('タ', "ﾀ"), KanaMap('チ', "ﾁ"), KanaMap('ツ', "ﾂ"), KanaMap('テ', "ﾃ"), KanaMap('ト', "ﾄ"),
    KanaMap('ナ', "ﾅ"), KanaMap('ニ', "ﾆ"), KanaMap('ヌ', "ﾇ"), KanaMap('ネ', "ﾈ"), KanaMap('ノ', "ﾉ"),
    KanaMap('ハ', "ﾊ"), KanaMap('ヒ', "ﾋ"), KanaMap('フ', "ﾌ"), KanaMap('ヘ', "ﾍ"), KanaMap('ホ', "ﾎ"),
    KanaMap('マ', "ﾏ"), KanaMap('ミ', "ﾐ"), KanaMap('ム', "ﾑ"), KanaMap('メ', "ﾒ"), KanaMap('モ', "ﾓ"),
    KanaMap('ヤ', "ﾔ"), /*  　*/            KanaMap('ユ', "ﾕ"), /*  　*/            KanaMap('ヨ', "ﾖ"),
    KanaMap('ラ', "ﾗ"), KanaMap('リ', "ﾘ"), KanaMap('ル', "ﾙ"), KanaMap('レ', "ﾚ"), KanaMap('ロ', "ﾛ"),
    KanaMap('ワ', "ﾜ"), /*　　*/            /*　　*/            /*　　*/             KanaMap('ヲ', "ｦ"),

    KanaMap('ガ', "ｶﾞ"), KanaMap('ギ', "ｷﾞ"), KanaMap('グ', "ｸﾞ"), KanaMap('ゲ', "ｹﾞ"), KanaMap('ゴ', "ｺﾞ"),
    KanaMap('ザ', "ｻﾞ"), KanaMap('ジ', "ｼﾞ"), KanaMap('ズ', "ｽﾞ"), KanaMap('ゼ', "ｾﾞ"), KanaMap('ゾ', "ｿﾞ"),
    KanaMap('ダ', "ﾀﾞ"), KanaMap('ヂ', "ﾁﾞ"), KanaMap('ヅ', "ﾂﾞ"), KanaMap('デ', "ﾃﾞ"), KanaMap('ド', "ﾄﾞ"),
    KanaMap('バ', "ﾊﾞ"), KanaMap('ビ', "ﾋﾞ"), KanaMap('ブ', "ﾌﾞ"), KanaMap('ベ', "ﾍﾞ"), KanaMap('ボ', "ﾎﾞ"),
    KanaMap('パ', "ﾊﾟ"), KanaMap('ピ', "ﾋﾟ"), KanaMap('プ', "ﾌﾟ"), KanaMap('ペ', "ﾍﾟ"), KanaMap('ポ', "ﾎﾟ"),

    KanaMap('ッ', "ｯ"), KanaMap('ン', "ﾝ"),
    KanaMap('ァ', "ｧ"), KanaMap('ィ', "ｨ"), KanaMap('ゥ', "ｩ"), KanaMap('ェ', "ｪ"), KanaMap('ォ', "ｫ"),
    KanaMap('ャ', "ｬ"), /*　　*/            KanaMap('ュ', "ｭ"), /*  　*/            KanaMap('ョ', "ｮ"),

    KanaMap('ー', "ｰ")
};
#undef KanaMap
