include(SetCppStandard)

# Core Component files
file(GLOB_RECURSE SourceListCore
    "*.cpp"
    "*.hpp"
    "*.c"
    "*.h"
)

# Find runtime dependencies
find_package(Qt5Core REQUIRED)
find_package(ICU 58 COMPONENTS uc REQUIRED)

# Embedded assets
qt5_add_resources(RCC_SOURCES "${PROJECT_SOURCE_DIR}/Core/EmbeddedAssets.qrc")
add_custom_target(MusicConsoleEmbeddedAssets DEPENDS ${RCC_SOURCES})

# Core Component
add_library("CoreModule" STATIC ${SourceListCore} ${RCC_SOURCES})
add_dependencies("CoreModule" MusicConsoleEmbeddedAssets)

SetCppStandard("CoreModule" 17)
set_target_properties("CoreModule" PROPERTIES PREFIX "")
set_target_properties("CoreModule" PROPERTIES OUTPUT_NAME "module-core")

# Print found ICU on screen
if (ICU_FOUND)
    message(STATUS "ICU include dir: ${ICU_INCLUDE_DIRS}")
    message(STATUS "ICU::uc library: ${ICU_UC_LIBRARY}")
endif()

# Find pcre2 (UTF-32)
message(STATUS "Checking for pcre2 (UTF-32 version)...")
pkg_check_modules(PCRE2_UTF32 REQUIRED libpcre2-32>=10.21)
if (NOT PCRE2_UTF32_FOUND)
    message(FATAL_ERROR "Unable to find libpcre2 (UTF-32 version).")
endif()
target_include_directories("CoreModule" PRIVATE "${PCRE2_UTF32_INCLUDEDIR}")
message(STATUS "libpcre2 headers found in ${PCRE2_UTF32_INCLUDEDIR}")
message(STATUS "libpcre2 library: ${PCRE2_UTF32_LDFLAGS}")

# Add selection for metadata reader backend
#  > defaults to "ffmpeg"
set(METADATA_READER_BACKEND "ffmpeg" CACHE STRING "Metadata Reader Backend")
if ("${METADATA_READER_BACKEND}" STREQUAL "ffmpeg")
    add_definitions(-DUSE_FFMPEG_METADATA_READER)
elseif ("${METADATA_READER_BACKEND}" STREQUAL "ffmpeg-ld")
    add_definitions(-DUSE_LINKED_FFMPEG_METADATA_READER)
elseif("${METADATA_READER_BACKEND}" STREQUAL "stub")
    add_definitions(-DUSE_STUB_METADATA_READER)
else()
    message(FATAL_ERROR "Unknown Metadata Reader Backend! use one of > ffmpeg, ffmpeg-ld, stub")
endif()
message(STATUS "Metadata Reader Backend: ${METADATA_READER_BACKEND}")

# we need libavformat/avformat.h for the ffmpeg metadata reader
if ("${METADATA_READER_BACKEND}" STREQUAL "ffmpeg")
    pkg_check_modules(LIBAVFORMAT REQUIRED libavformat)
    if (NOT LIBAVFORMAT_FOUND)
        message(FATAL_ERROR "Unable to find libavformat headers.")
    endif()
    target_include_directories("CoreModule" PRIVATE "${LIBAVFORMAT_INCLUDEDIR}")
    message(STATUS "libavformat headers found in ${LIBAVFORMAT_INCLUDEDIR}")
endif()

# we need libavformat and libavutil for the linked ffmpeg metadata reader
if ("${METADATA_READER_BACKEND}" STREQUAL "ffmpeg-ld")
    pkg_check_modules(LIBAVFORMAT REQUIRED libavformat)
    pkg_check_modules(LIBAVUTIL REQUIRED libavutil)
    if (NOT LIBAVFORMAT_FOUND)
        message(FATAL_ERROR "Unable to find libavformat.")
    endif()
    if (NOT LIBAVUTIL_FOUND)
        message(FATAL_ERROR "Unable to find libavutil.")
    endif()
    target_include_directories("CoreModule" PRIVATE "${LIBAVFORMAT_INCLUDEDIR}")
    target_include_directories("CoreModule" PRIVATE "${LIBAVUTIL_INCLUDEDIR}")
    message(STATUS "libavformat headers found in ${LIBAVFORMAT_INCLUDEDIR}")
    message(STATUS "libavformat headers found in ${LIBAVUTIL_INCLUDEDIR}")
endif()

# Core Component Include Directories
target_include_directories("CoreModule" PRIVATE "${PROJECT_SOURCE_DIR}/Core/Core/3rdParty/valijson/include")
target_include_directories("CoreModule" PRIVATE "${PROJECT_SOURCE_DIR}/Core/Core/3rdParty/rapidjson/include")
target_include_directories("CoreModule" PRIVATE "${PROJECT_SOURCE_DIR}/Core/I18n/spirit-po")
target_include_directories("CoreModule" PRIVATE "${PROJECT_SOURCE_DIR}/Core/I18n/spirit-po/include")
target_include_directories("CoreModule" PRIVATE "${ICU_INCLUDE_DIRS}")

# Core Component Dependencies
target_link_libraries("CoreModule"
    Qt5::Core
    "${ICU_UC_LIBRARY}"
    "${PCRE2_UTF32_LDFLAGS}"
)

if ("${METADATA_READER_BACKEND}" STREQUAL "ffmpeg-ld")
    target_link_libraries("CoreModule"
        "${LIBAVFORMAT_LDFLAGS}"
        "${LIBAVUTIL_LDFLAGS}"
    )
endif()

# Export header file path
target_include_directories("CoreModule" PRIVATE "${PROJECT_SOURCE_DIR}/Core")
set(COREMODULE_INCLUDE_DIR "${PROJECT_SOURCE_DIR}/Core" PARENT_SCOPE)
