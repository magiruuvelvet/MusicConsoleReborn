#include "PlaylistParser.hpp"

#include <Core/MusicConsoleReborn.hpp>
#include <Media/MediaLibraryModel.hpp>
#include <Core/ConfigManager.hpp>
#include <Core/Logger.hpp>

#include <Core/CommandParseHelper.hpp>

#include <QFile>
#include <QRegularExpression>

const QRegularExpression PlaylistParser::extract_string(R"("(?:[^"\\]|\\.)*")");
const QByteArray PlaylistParser::extension = ".plist";
const QByteArray PlaylistParser::header = "MUSICCONSOLE PLAYLIST\n";

PlaylistParser::ParseResult PlaylistParser::Parse(const QByteArray &name, PlaylistQueue *target)
{
    if (!target)
    {
        LOG_FATAL("PlaylistParser: target PlaylistQueue address missing!");
        return InvalidPointer;
    }

    // lookup for the playlist file in all playlist paths
    // first match is returned, cross directory conflicts are not handled
    const auto &file = FindFilePath(name);
    if (file.isEmpty())
        return FileNotFound;

    // try to open file
    QFile f(file);
    if (!f.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        LOG_ERROR("PlaylistParser: unable to read \"%s\"!", file);
        return FileOpenError;
    }

    // read all file contents
    const auto &data = f.readAll();
    f.close();

    if (!data.startsWith(header))
    {
        LOG_ERROR("PlaylistParser: \"%s\" doesn't seem to be a playlist.", file);
        LOG_HINT("Make sure the file starts with \"%s<\\n>\".", header.mid(0, header.size() - 1));
        return NotAPlaylistFile;
    }

    // split data into lines
    const auto &lines = data.split('\n');

    uint linecount = 2;
    for (auto&& line : lines.mid(1))
    {
        const auto &lineT = line.trimmed();

        // validate line
        QByteArray type_string, search_terms, player_override;
        const auto &res = ParseLine(lineT, linecount, type_string, search_terms, player_override);

        // line has syntax errors
        if (res == LineFailed)
        {
            LOG_ERROR("PlaylistParser: %s: line %u: has syntax errors, line will be skipped.", name, linecount);
            linecount++;
            continue;
        }
        // skip comments
        else if (res == LineComment)
        {
            linecount++;
            continue;
        }
        // valid playlist entry
        else
        {
            // use the command line parser to escape quotes and other chars
            QByteArray escaped_search_terms;
            if (!search_terms.isEmpty())
            {
                escaped_search_terms = CommandParseHelper::ParseSimple(search_terms).join(' ');
            }

            MediaType media_type;
            enum {
                FindNormal,
                FindRandom,
                _FindNothing,
            } find_mode = _FindNothing;

            // T:
            if (type_string.size() == 1)
            {
                media_type = ParseType(type_string.at(0));
                find_mode = FindNormal;
            }

            // R:T
            else
            {
                // Random
                if (type_string.at(0) == 'R')
                {
                    media_type = ParseType(type_string.at(2));
                    find_mode = FindRandom;
                }
            }

            const MediaType player_override_type = ParsePlayerOverride(player_override, media_type);

            // enqueue item to playlist
            const MediaFile *res = nullptr;
            switch (find_mode)
            {
                case FindNormal:
                    res = MusicConsoleReborn::library()->find(SearchTerms(escaped_search_terms), media_type);
                    if (res)
                        target->enqueue(res, player_override_type);
                    else
                        LOG_WARNING("PlaylistParser: line %u: nothing found matching \"%s\".", linecount, escaped_search_terms);
                    break;
                case FindRandom:
                    res = MusicConsoleReborn::library()->random(SearchTerms(escaped_search_terms), media_type);
                    if (res)
                        target->enqueue(res, player_override_type);
                    break;
                case _FindNothing:
                    LOG_WARNING("PlaylistParser: line %u: unknown mode fall through.", linecount);
                    break;
            }

            linecount++;
        }
    }

    return Success;
}

const QByteArrayList PlaylistParser::GetAllPlaylists()
{
    const auto &paths = Config()->GetLibraryPlaylistPaths();

    QByteArrayList playlistNames;
    for (auto&& path : paths)
    {
        const auto &entryList = QDir(path).entryInfoList(QDir::Files);
        for (auto&& entry : entryList)
        {
            const auto &baseName = entry.baseName().toUtf8();
            if (!playlistNames.contains(baseName))
            {
                playlistNames.append(baseName);
            }
        }
    }
    return playlistNames;
}

const QByteArray PlaylistParser::FindFilePath(const QByteArray &name)
{
    const auto &paths = Config()->GetLibraryPlaylistPaths();
    for (auto&& path : paths)
    {
        const QDir dir(path);
        if (!(dir.exists() && dir.isReadable()))
        {
            LOG_ERROR("PlaylistParser: directory \"%s\" not readable.", path);
            continue;
        }

        const auto &fullpath = path + '/' + name + extension;
        if (dir.entryInfoList(QDir::Files).contains(QFileInfo(fullpath)))
        {
            return fullpath;
        }
    }

    return {};
}

PlaylistParser::LineParseResult PlaylistParser::ParseLine(const QByteArray &line, const uint &l,
                                                          QByteArray &type_string, QByteArray &search_terms,
                                                          QByteArray &player_override)
{
    if (line.startsWith('#') || line.isEmpty())
        return LineComment;

    static const QRegularExpression check1(R"(^[AVMN]\:(.*)$)");
    static const QRegularExpression check2(R"(^[R]\:[AVMN]\:(.*)$)");

    const auto &match1 = check1.match(line);
    const auto &match2 = check2.match(line);

    if (check1.match(line).hasMatch())
    {
        const auto &extracted_search_terms = extract_string.match(match1.capturedTexts().at(1));
        if (!extracted_search_terms.hasMatch())
        {
            LOG_ERROR("PlaylistParser: line %u: missing search terms.", l);
            return LineFailed;
        }

        type_string = line.mid(0, 1);
        search_terms = extracted_search_terms.capturedTexts().at(0).toUtf8();
        player_override = line.mid(type_string.size() + 1 + search_terms.size()).simplified();

        return LineSuccess;
    }
    else if (check2.match(line).hasMatch())
    {
        const auto &extracted_search_terms = extract_string.match(match2.capturedTexts().at(1));
        if (!extracted_search_terms.hasMatch())
        {
            LOG_ERROR("PlaylistParser: line %u: missing search terms.", l);
            return LineFailed;
        }

        type_string = line.mid(0, 3);
        search_terms = extracted_search_terms.capturedTexts().at(0).toUtf8();
        player_override = line.mid(type_string.size() + 1 + search_terms.size()).simplified();

        return LineSuccess;
    }
    else
    {
        LOG_ERROR("PlaylistParser: line %u: doesn't start with a valid type sequence.", l);
        return LineFailed;
    }
}

MediaType PlaylistParser::ParseType(const char c)
{
    switch (c)
    {
        case 'A': return AUDIO;
        case 'V': return VIDEO;
        case 'M': return MOD;
        default: return NONE;
    }
}

MediaType PlaylistParser::ParsePlayerOverride(const QByteArray &override, const MediaType &def)
{
    if (override.isEmpty())
        return def;

    if (override == "audio")
        return AUDIO;

    if (override == "video")
        return VIDEO;

    if (override == "mod" || override == "module")
        return MOD;

    LOG_WARNING("PlaylistParser: unknown player override: %s", override);
    return def;
}
