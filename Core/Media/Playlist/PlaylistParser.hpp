#ifndef PLAYLISTPARSER_HPP
#define PLAYLISTPARSER_HPP

#include <QByteArray>

#include "../PlaylistQueue.hpp"

class QRegularExpression;

class PlaylistParser final
{
    PlaylistParser() = delete;

public:
    enum ParseResult
    {
        Success,
        FileNotFound,
        FileOpenError,
        NotAPlaylistFile,

        Unknown,
        InvalidPointer,
    };

    static ParseResult Parse(const QByteArray &name, PlaylistQueue *target);

    // returns just the name of all found playlists
    static const QByteArrayList GetAllPlaylists();

private:
    static const QByteArray FindFilePath(const QByteArray &name);

    enum LineParseResult
    {
        LineSuccess,
        LineFailed,
        LineComment,
    };
    static LineParseResult ParseLine(const QByteArray &line, const uint &l,
                                     QByteArray &type_string, QByteArray &search_terms,
                                     QByteArray &player_override);

    static MediaType ParseType(const char);
    static MediaType ParsePlayerOverride(const QByteArray &override, const MediaType &def);

    static const QRegularExpression extract_string;
    static const QByteArray extension;
    static const QByteArray header;
};

#endif // PLAYLISTPARSER_HPP
