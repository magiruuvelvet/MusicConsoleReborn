#include "MediaLibraryModel.hpp"
#include "MediaFileFactory.hpp"
#include "Cache/MediaCache.hpp"

#include <Core/ConfigManager.hpp>
#include <Core/Logger.hpp>
#include <QDirIterator>

#include <Utils/Randomizer.hpp>
#include <Types/IteratorUtils.hpp>

uint qHash(const MediaLibraryModel::CacheEntry &cache_entry, uint seed)
{
    return
        qHash(cache_entry.search_terms, seed)
      + qHash(cache_entry.type, seed)
      + qHash(cache_entry.max, seed);
}

MediaLibraryModel::MediaLibraryModel()
{
}

MediaLibraryModel::MediaLibraryModel(const QByteArray &path)
{
    _path = QDir(path);
}

MediaLibraryModel::~MediaLibraryModel()
{
    _mediaFiles.clear();

    _sortedList.clear();
    _filteredList.clear();
    _cache.clear();
}

void MediaLibraryModel::setDirectory(const QByteArray &path)
{
    _path = QDir(path);
}

bool MediaLibraryModel::isDirectoryValid() const
{
    return _path.exists() && _path.isReadable();
}

void MediaLibraryModel::setFilters(MediaType type, const StringList &filters)
{
    StringList namefilters;
    for (auto&& ext : filters)
        namefilters.append("*." + ext);
    _filters.insert({type, namefilters});
}

void MediaLibraryModel::scan()
{
    // initialize cache when not already done
    static const auto mcache_init = ([]{
        MediaCache::Init(ConfigManager::GetCacheDirectory());
        return nullptr;
    })(); (void) mcache_init;

    if (!isDirectoryValid())
    {
        LOG_ERROR("MediaLibraryModel: can not perform scan on invalid directory.");
        return;
    }

    // delete current list
    _mediaFiles.clear();

    // clear cached data
    _sortedList.clear();
    _filteredList.clear();
    _cache.clear();

    QFileInfoList files;
    const auto &res = dirIteratorInternal(_path,
                          _filters.at(AUDIO) +
                          _filters.at(VIDEO) +
                          _filters.at(MOD),
                          files);

    // return on iteration failure
    if (!res)
        return;

    // initialize the metadata reader
    MediaFileFactory::InitMetadataReader();

    // loop over all files and clear the list at the same time
    while (!files.isEmpty())
    {
        const auto &current = files.takeFirst();

        if (current.isReadable())
        {
            if (MediaCache::hasFile(current))
            {
                bool isOutdated;
                auto m = MediaFileFactory::CreateFromCache(MediaCache::GetObject(current), &isOutdated);
                if (isOutdated)
                {
                    LOG_NOTICE("MediaLibraryModel: cache file for %s is outdated and will be updated.", current);
                    const auto &type = matchesFilter(current.suffix().toUtf8().constData());
                    m = MediaFileFactory::CreateMediaFile(current, type);
                    MediaCache::addFile(m);
                    _mediaFiles.insert(m->hash, m);
                }
                else
                {
                    if (m && m->hash.size() != 0)
                    {
                        _mediaFiles.insert(m->hash, m);
                    }

                    else
                    {
                        LOG_ERROR("MediaLibraryModel: failed to create MediaFile object from cache for: %s", current);
                        m.reset();
                    }
                }
            }
            else
            {
                const auto &type = matchesFilter(current.suffix().toUtf8().constData());

                auto m = MediaFileFactory::CreateMediaFile(current, type);
                if (m && m->hash.size() != 0)
                {
                    MediaCache::addFile(m);
                    _mediaFiles.insert(m->hash, m);
                }
                else
                {
                    LOG_ERROR("MediaLibraryModel: failed to create a MediaFile object for: %s", current);
                    m.reset();
                }
            }
        }
        else
        {
            LOG_WARNING("MediaLibraryModel: %s: file is not readable and was skipped.",
                        current);
        }
    }

    // deinitialize the metadata reader
    MediaFileFactory::DeinitMetadataReader();

    // create sorted list
    if (!_mediaFiles.isEmpty())
    {
        const auto &MoveToBottomPhrases = Config()->GetMoveToBottomPhrases();
        QList<const MediaFile*> def, bottom;

        for (auto&& mf : _mediaFiles)
        {
            bool added = false;
            for (auto&& phrase : MoveToBottomPhrases)
            {
                if (QString(mf->prettyPath()).contains(phrase, Qt::CaseInsensitive))
                {
                    bottom.append(mf.get());
                    added = true;
                    break;
                }
            }
            if (!added)
            {
                def.append(mf.get());
            }
        }

        // sort lists alphabetically
        std::sort(def.begin(), def.end(), [](const MediaFile *m1, const MediaFile *m2){
            return m1->prettyPath() < m2->prettyPath();
        });
        std::sort(bottom.begin(), bottom.end(), [](const MediaFile *m1, const MediaFile *m2){
            return m1->prettyPath() < m2->prettyPath();
        });

        // append both lists to the main sorted list
        _sortedList.append(def);
        _sortedList.append(bottom);

        // create filter lists, preserves sorting
        QList<const MediaFile*>
            _filteredAudio,
            _filteredVideo,
            _filteredMod;

        for (auto&& mf : _sortedList)
        {
            switch (mf->type)
            {
                case AUDIO: _filteredAudio.append(mf); break;
                case VIDEO: _filteredVideo.append(mf); break;
                case MOD:   _filteredMod.append(mf); break;
                case NONE: break;
            }
        }

        _filteredList.insert(AUDIO, _filteredAudio);
        _filteredList.insert(VIDEO, _filteredVideo);
        _filteredList.insert(MOD, _filteredMod);
        _filteredList.insert(NONE, _sortedList);
    }
}

const MediaFile *MediaLibraryModel::find(const SearchTerms &search_term,  const MediaType &type) const
{
    const auto &res = findInternal(search_term, type, 1);
    return res.isEmpty() ? nullptr : res.at(0);
}

const QList<const MediaFile*> &MediaLibraryModel::findMultiple(const SearchTerms &search_terms, const MediaType &type) const
{
    return findInternal(search_terms, type);
}

const MediaFile *MediaLibraryModel::random(const MediaType &type) const
{
    if (_filteredList[type].isEmpty())
        return nullptr;
    return _filteredList[type].at(Randomizer::GetRandomNumber(0, _filteredList[type].size() - 1));
}

const MediaFile *MediaLibraryModel::random(const SearchTerms &search_terms, const MediaType &type) const
{
    if (_filteredList[type].isEmpty())
        return nullptr;
    if (search_terms.isEmpty())
        return random(type);
    const auto &results = findMultiple(search_terms, type);
    if (results.isEmpty())
        return nullptr;
    return results.at(Randomizer::GetRandomNumber(0, results.size() - 1));
}

const StringList MediaLibraryModel::GenerateReplxxAutocompletionList() const
{
    StringList acList;
    for (auto&& mf : _mediaFiles)
    {
        // avoid adding empty entries to the autocompleter to prevent issues
        // also simplify the metadata using QByteArray::simplified() to avoid
        // some other glitches which may happen because of this.

        for (auto&& searchTerm : mf->searchTerms)
            acList.append(searchTerm.simplified());

        if (mf->hasMetadata())
        {
            if (!mf->metadata.artist.isEmpty())
                acList.append(mf->metadata.artist.simplified());
            if (!mf->metadata.album_artist.isEmpty())
                acList.append(mf->metadata.album_artist.simplified());
            if (!mf->metadata.title.isEmpty())
            {
                const auto &title = mf->metadata.title.simplified();

                acList.append(title);

                for (auto&& i : Config()->GetMetadataTitleSplitList())
                {
                    const auto &splitTitle = QString(title).split(i, QString::SkipEmptyParts, Qt::CaseInsensitive);
                    for (auto&& st : splitTitle)
                        acList.append(st.trimmed().toUtf8());
                }
            }
            if (!mf->metadata.album.isEmpty())
                acList.append(mf->metadata.album.simplified());

            // {artist + title} combination
            if (!mf->metadata.artist.isEmpty() && !mf->metadata.title.isEmpty())
                acList.append(mf->metadata.artist + ' ' + mf->metadata.title);

            // {album artist + title} combination
            if (!mf->metadata.album_artist.isEmpty() && !mf->metadata.title.isEmpty())
                acList.append(mf->metadata.album_artist + ' ' + mf->metadata.title);

            // {album + title} combination
            if (!mf->metadata.album.isEmpty() && !mf->metadata.title.isEmpty())
                acList.append(mf->metadata.album + ' ' + mf->metadata.title);
        }

        else
        {
            for (auto&& searchTerm : mf->searchTerms)
            {
                const auto &pathSplit = searchTerm.split('/');
                //acList.append(pathSplit);
                // FIXME: operator QByteArray() is used here!!
                for (auto&& path : pathSplit)
                    acList.append(path);
            }
        }
    }

    // remove duplicates and sort list
    //IteratorUtils::remove_duplicates_and_sort<StringList, String>(acList);
    IteratorUtils::RemoveDuplicates(acList);
    acList.sort();

    return acList;
}

bool MediaLibraryModel::dirIteratorInternal(const QDir &path, const StringList &filters,
                                            QFileInfoList &files)
{
    if (!(path.exists() && path.isReadable()))
    {
        LOG_CRITICAL("MediaLibraryModel: can not open directory for iteration: %s", path);
        LOG_WARNING("Your library will be empty.");
        return false;
    }

    QStringList _filters;
    for (auto&& f : filters)
        _filters.append(f);

    QDirIterator iter(path.path(), _filters,
                      QDir::NoDotAndDotDot | QDir::Files,
                      QDirIterator::Subdirectories | QDirIterator::FollowSymlinks);

    while (iter.hasNext())
    {
        iter.next(); // QDirIterator points to nothing at the beginning

        files.append(iter.fileInfo());
    }

    return true;
}

const QList<const MediaFile*> &MediaLibraryModel::findInternal(const SearchTerms &search_terms, const MediaType &type, const qint64 max) const
{
    static const QList<const MediaFile*> __empty = {};

    QList<const MediaFile*> results;

    // generate cache hash key
    const auto &cache_entry = CacheEntry{search_terms, type, max};
    // check if hash is already in cache and return cached pointer list
    if (_cache.contains(cache_entry))
        return _cache[cache_entry];

    // no cached list found, start the search algorithm

    if (!search_terms.allFiltersValid())
    {
        LOG_ERROR("Search terms are invalid: %s", search_terms);
        return __empty;
    }

    if (search_terms.someFiltersEmpty())
    {
        LOG_ERROR("Search terms may not contain empty parts: %s", search_terms);
        return __empty;
    }

    qint64 added = 0;
    for (auto&& mf : _filteredList[type])
    {
        bool add = false;
        for (auto&& term : mf->searchTerms)
        {
            // loop break helpers
            bool done = false;
            bool match = false;

            // query all filters, except main filter
            for (auto&& filter : search_terms.GetAllSubFilters())
            {
                // wg: (without genre)
                if (filter.type == SearchTerms::WITHOUT_GENRE)
                {
                    if (filter.data.match(mf->metadata.genre).hasMatch())
                    {
                        done = true;
                        match = true;
                        break;
                    }
                }

                // wo: (without)
                else if (filter.type == SearchTerms::WITHOUT)
                {
                    if (filter.data.match(term).hasMatch())
                    {
                        done = true;
                        match = true;
                        break;
                    }
                }
            } // filter loop

            // <main> | main must be outside the previous loop
            //          only check against it when there was no match for the other filters
            if (!match)
            {
                if (search_terms.data().match(term).hasMatch())
                {
                    done = true;
                    add = true;
                    break;
                }
            }

            // next file
            if (done)
                break;

        } // mf->searchTerms loop

        if (add)
        {
            if (added <= max || max < 0)
            {
                results.append(mf);
                added++;
            }
            else
            {
                break;
            }
        }
    }

    // add pointer list to the cache for the current cache key
    _cache.insert(cache_entry, results);
    return _cache[cache_entry];
}
