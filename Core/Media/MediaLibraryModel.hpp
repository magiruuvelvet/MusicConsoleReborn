#ifndef MEDIALIBRARYMODEL_HPP
#define MEDIALIBRARYMODEL_HPP

#include <QDir>
#include <QHash>
#include <QMap>
#include <QFileInfo>
#include <QFileInfoList>

#include <Utils/SearchTerms.hpp>

#include <map>
#include <memory>

#include "MediaFile.hpp"

#include <Types/UnicodeString.hpp>

namespace DevCommands { class SearchTermTest; }

class MediaLibraryModel final
{
public:
    MediaLibraryModel();
    MediaLibraryModel(const QByteArray &path);
    ~MediaLibraryModel();

    void setDirectory(const QByteArray &path);
    bool isDirectoryValid() const;
    inline const QDir &path() const
    { return _path; }
    inline const QByteArray pathString() const
    { return _path.path().toUtf8(); }

    // just the file extensions
    void setFilters(MediaType type, const StringList &filters);

    void scan();

    inline int count() const
    { return _mediaFiles.size(); }
    inline int count(const MediaType &type) const
    { return _filteredList[type].size(); }

    // finds a file in the library, only the first match is returned; nullptr when nothing was found
    const MediaFile *find(const SearchTerms &search_term, const MediaType &type = NONE) const;
    // finds all matching files in the library and returns them; empty list when nothing was found
    const QList<const MediaFile*> &findMultiple(const SearchTerms &search_terms, const MediaType &type = NONE) const;

    // returns a random file, the result is never nullptr as long as the library is not empty
    const MediaFile *random(const MediaType &type = NONE) const;
    // returns a random file matching the search terms, returns nullptr when nothing was found
    const MediaFile *random(const SearchTerms &search_terms, const MediaType &type = NONE) const;

    const StringList GenerateReplxxAutocompletionList() const;

    // lookup MediaFile by its hash, returns nullptr when not found
    inline const MediaFile *at(const QByteArray &hash) const
    { return _mediaFiles.value(hash, nullptr).get(); }

    // last played media file
    inline void setLastPlayed(const MediaFile *mf)
    { _lastPlayed = mf; }
    inline const MediaFile *lastPlayed() const
    { return _lastPlayed; }

private:
    QDir _path;

    QHash<QByteArray, std::shared_ptr<MediaFile>> _mediaFiles;  // main MediaFile container (pointer targets)
    std::map<MediaType, StringList> _filters;  // file type filters (extensions)

    const MediaFile *_lastPlayed = nullptr;

    // don't copy files, just point to them (also keep this list readonly)
    QList<const MediaFile*> _sortedList;
    QHash<MediaType, QList<const MediaFile*>> _filteredList;

    struct CacheEntry {
        const SearchTerms search_terms;
        const MediaType type;
        const qint64 max;
        inline bool operator== (const CacheEntry &other) const
        {
            return search_terms == other.search_terms &&
                   type == other.type &&
                   max == other.max;
        }
        inline bool operator!= (const CacheEntry &other) const
        { return !operator==(other); }
    };
    friend uint qHash(const CacheEntry&, uint seed);
    friend class DevCommands::SearchTermTest;
    mutable QHash<CacheEntry, QList<const MediaFile*>> _cache;

    inline MediaType matchesFilter(const String &ext)
    {
        for (auto&& t : {AUDIO, VIDEO, MOD})
            for (auto&& f : _filters.at(t))
                if (!f.isEmpty() && UnicodeString(f).ends_with(ext, UnicodeString::CaseInsensitive))
                    return t;
        return NONE;
    }

    static bool dirIteratorInternal(const QDir &path, const StringList &filters,
                                    QFileInfoList &files);

    // internal search helper, stops when max is reached; "-1" -> no limit (default)
    const QList<const MediaFile*> &findInternal(const SearchTerms &search_terms, const MediaType &type, const qint64 max = -1) const;
};

// QHash support
extern uint qHash(const MediaLibraryModel::CacheEntry &cache_entry, uint seed = 0);

#endif // MEDIALIBRARYMODEL_HPP
