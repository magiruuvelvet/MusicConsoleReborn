#ifndef STUBMETADATAREADER_HPP
#define STUBMETADATAREADER_HPP

#ifdef USE_STUB_METADATA_READER

#include "../MetadataReader.hpp"

class StubMetadataReader : public MetadataReader
{
public:
    StubMetadataReader();

    const Metadata ReadMetadataFromFile(const String &file) const override;
};

#endif // USE_STUB_METADATA_READER

#endif // STUBMETADATAREADER_HPP
