#if defined(USE_FFMPEG_METADATA_READER) || defined(USE_LINKED_FFMPEG_METADATA_READER)

#include "FFMpegMetadataReader.hpp"

#include <Core/Logger.hpp>

// required for working with structs during dynamic loading
extern "C" {
    #include <libavformat/avformat.h>
}

#ifdef USE_FFMPEG_METADATA_READER

#include <QLibrary>

std::unique_ptr<QLibrary> FFMpegMetadataReader::libavformat;
bool FFMpegMetadataReader::LibOpen = false;
bool FFMpegMetadataReader::LibAllSyms = true;

#endif

FFMpegMetadataReader::FFMpegMetadataReader()
{
#ifdef USE_FFMPEG_METADATA_READER
    libavformat.reset(new QLibrary("avformat"));
    if (libavformat->load())
    {
        LibOpen = true;
    }
    else
    {
        LOG_FATAL("FFMpegMetadataReader: unable to load library libavformat.so");
    }

    if (LibOpen)
    {
        av_log_set_level = (av_log_set_level_func) libavformat->resolve("av_log_set_level");
        if (av_log_set_level)
        {
            av_log_set_level(-8);
        }
        else
        {
            LOG_FATAL("FFMpegMetadataReader: libavformat.so: av_log_set_level not resolved!");
            LOG_WARNING("ffmpeg might be a bit spammy and corrupt terminal formatting without silencing its output.");
        }

        avformat_alloc_context = (avformat_alloc_context_func) libavformat->resolve("avformat_alloc_context");
        if (!avformat_alloc_context)
        {
            LOG_FATAL("FFMpegMetadataReader: libavformat.so: avformat_alloc_context not resolved!");
            LibAllSyms = false;
        }

        avformat_open_input = (avformat_open_input_func) libavformat->resolve("avformat_open_input");
        if (!avformat_open_input)
        {
            LOG_FATAL("FFMpegMetadataReader: libavformat.so: avformat_open_input not resolved!");
            LibAllSyms = false;
        }

        avformat_free_context = (avformat_free_context_func) libavformat->resolve("avformat_free_context");
        if (!avformat_free_context)
        {
            LOG_FATAL("FFMpegMetadataReader: libavformat.so: avformat_free_context not resolved!");
            LibAllSyms = false;
        }

        avformat_find_stream_info = (avformat_find_stream_info_func) libavformat->resolve("avformat_find_stream_info");
        if (!avformat_find_stream_info)
        {
            LOG_FATAL("FFMpegMetadataReader: libavformat.so: avformat_find_stream_info not resolved!");
            LibAllSyms = false;
        }

        av_dict_get = (av_dict_get_func) libavformat->resolve("av_dict_get");
        if (!av_dict_get)
        {
            LOG_FATAL("FFMpegMetadataReader: libavformat.so: av_dict_get_func not resolved!");
            LibAllSyms = false;
        }

        if (!LibAllSyms)
        {
            LOG_FATAL("FFMpegMetadataReader: libavformat.so: some symbols could not be resolved! Empty metadata will be returned.");
        }
    }
#endif
}

FFMpegMetadataReader::~FFMpegMetadataReader()
{
#ifdef USE_FFMPEG_METADATA_READER
    libavformat->unload();
#endif
}

const Metadata FFMpegMetadataReader::ReadMetadataFromFile(const String &file) const
{
#ifdef USE_FFMPEG_METADATA_READER
    if (LibOpen && LibAllSyms) {
#endif

    avformatcontext_t *ctx = avformat_alloc_context();
    if (avformat_open_input(&ctx, *file, nullptr, nullptr) < 0)
    {
        LOG_ERROR("FFMpegMetadataReader: avformat_open_input(ctx=0x%x, file=\"%s\") failed.", ctx, file);
        avformat_free_context(ctx);
        return Metadata{};
    }

    if (avformat_find_stream_info(ctx, nullptr) < 0)
    {
        // file has no metadata
        avformat_free_context(ctx);
        return Metadata{};
    }

    Metadata metadata;
    GetString(static_cast<AVFormatContext*>(ctx)->metadata, metadata.artist, "artist");
    GetString(static_cast<AVFormatContext*>(ctx)->metadata, metadata.album_artist, "album_artist");
    GetString(static_cast<AVFormatContext*>(ctx)->metadata, metadata.album, "album");
    GetString(static_cast<AVFormatContext*>(ctx)->metadata, metadata.title, "title");
    GetString(static_cast<AVFormatContext*>(ctx)->metadata, metadata.genre, "genre");
    GetNumber(static_cast<AVFormatContext*>(ctx)->metadata, metadata.track, "track");
    GetNumber(static_cast<AVFormatContext*>(ctx)->metadata, metadata.total_tracks, "totaltracks");

    avformat_free_context(ctx);

    return metadata;

#ifdef USE_FFMPEG_METADATA_READER
    }
#endif
    return Metadata{};
}

void FFMpegMetadataReader::GetString(avdictionary_t *dict, String &target, const char *field_name) const
{
    AVDictionaryEntry *tag = static_cast<AVDictionaryEntry*>(av_dict_get(dict, field_name, nullptr, 2));
    if (tag)
    {
        target = String(tag->value);
    }
}

void FFMpegMetadataReader::GetNumber(avdictionary_t *dict, quint32 &target, const char *field_name) const
{
    AVDictionaryEntry *tag = static_cast<AVDictionaryEntry*>(av_dict_get(dict, field_name, nullptr, 2));
    if (tag)
    {
        target = static_cast<quint32>(std::atoi(tag->value));
    }
}

#endif // USE_FFMPEG_METADATA_READER
