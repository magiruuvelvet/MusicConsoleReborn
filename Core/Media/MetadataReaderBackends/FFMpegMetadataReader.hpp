#ifndef FFMPEGMETADATAREADER_HPP
#define FFMPEGMETADATAREADER_HPP

#if defined(USE_FFMPEG_METADATA_READER) || defined(USE_LINKED_FFMPEG_METADATA_READER)

#include "../MetadataReader.hpp"

#include <memory>

#ifdef USE_LINKED_FFMPEG_METADATA_READER
extern "C" {
    #include <libavformat/avformat.h>
}
#endif

class QLibrary;

class FFMpegMetadataReader final : public MetadataReader
{
public:
    FFMpegMetadataReader();
    ~FFMpegMetadataReader() override;

    const Metadata ReadMetadataFromFile(const String &file) const override;

private:
#ifdef USE_FFMPEG_METADATA_READER
    static std::unique_ptr<QLibrary> libavformat;
    static bool LibOpen;
    static bool LibAllSyms;

    using avformatcontext_t = void;
    using avdictionary_t = void;

    // prototype: void av_log_set_level(int level);
    // #define AV_LOG_QUIET    -8
    typedef void (*av_log_set_level_func)(int);
    av_log_set_level_func av_log_set_level;

    // prototype: AVFormatContext *avformat_alloc_context(void);
    typedef void *(*avformat_alloc_context_func)();
    avformat_alloc_context_func avformat_alloc_context;

    // prototype: int avformat_open_input(AVFormatContext **ps, const char *url, AVInputFormat *fmt, AVDictionary **options);
    typedef int (*avformat_open_input_func)(void**, const char*, void*, void**);
    avformat_open_input_func avformat_open_input;

    // prototype: void avformat_free_context(AVFormatContext *s);
    typedef void (*avformat_free_context_func)(void*);
    avformat_free_context_func avformat_free_context;

    // prototype: int avformat_find_stream_info(AVFormatContext *ic, AVDictionary **options);
    typedef int (*avformat_find_stream_info_func)(void*, void**);
    avformat_find_stream_info_func avformat_find_stream_info;

    // prototype: AVDictionaryEntry *av_dict_get(const AVDictionary *m, const char *key, const AVDictionaryEntry *prev, int flags);
    // #define AV_DICT_IGNORE_SUFFIX   2
    typedef void *(*av_dict_get_func)(const void*, const char*, const void*, int);
    av_dict_get_func av_dict_get;
#else
    using avformatcontext_t = AVFormatContext;
    using avdictionary_t = AVDictionary;
#endif

    void GetString(avdictionary_t *dict, String &target, const char *field_name) const;
    void GetNumber(avdictionary_t *dict, quint32 &target, const char *field_name) const;
};

#endif // USE_FFMPEG_METADATA_READER

#endif // FFMPEGMETADATAREADER_HPP
