#ifdef USE_STUB_METADATA_READER

#include "StubMetadataReader.hpp"

StubMetadataReader::StubMetadataReader()
{
}

const Metadata StubMetadataReader::ReadMetadataFromFile(const String &) const
{
    return Metadata{};
}

#endif // USE_STUB_METADATA_READER
