#ifndef MEDIAFILEFACTORY_HPP
#define MEDIAFILEFACTORY_HPP

#include "MediaFile.hpp"
#include "MetadataReader.hpp"
#include "Cache/MediaCacheObject.hpp"

#include <array>
#include <memory>

class SearchTermGen;

class MediaFileFactory final
{
    MediaFileFactory() = delete;

public:

    static void InitMetadataReader();
    static void DeinitMetadataReader();

    // reads and setups metadata, generates search terms
    // returns newly created MediaFile object or nullptr on error
    static std::shared_ptr<MediaFile> CreateMediaFile(const QFileInfo &fileinfo, const MediaType &type);

    // initializes a MediaFile object from the cache
    // returns nullptr on error
    static std::shared_ptr<MediaFile> CreateFromCache(const MediaCacheObject *cache_id, bool *outdated);

private:
    static const std::array<std::unique_ptr<SearchTermGen>, 3> _searchTermGens;
    static std::unique_ptr<MetadataReader> _metadataReader;
};

#endif // MEDIAFILEFACTORY_HPP
