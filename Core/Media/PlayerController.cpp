#include "PlayerController.hpp"
#include <Core/Logger.hpp>

#include <Core/MusicConsoleReborn.hpp>
#include <Media/MediaLibraryModel.hpp>
#include <Utils/AppearanceController.hpp>

std::map<MediaType, Player> PlayerController::_players;
std::map<String, UserPlayer> PlayerController::_userPlayers;

#include <unistd.h>
#include <csignal>
#include <sys/types.h>
#include <sys/wait.h>

#if defined(OS_BSD) || defined(OS_MACOS)
using signal_func = sig_t;
#else
using signal_func = sighandler_t;
#endif

namespace {
    static signal_func orig_int;
    static signal_func orig_term;
    static signal_func orig_quit;

    static int run_process_internal(const String &command, const StringVector &arguments)
    {
        // don't terminate {THIS} process on Ctrl+C
        orig_int  = std::signal(SIGINT, SIG_IGN);
        orig_term = std::signal(SIGTERM, SIG_IGN);
        orig_quit = std::signal(SIGQUIT, SIG_IGN);

        const auto &pid = vfork();
//        std::signal(SIGTTOU, SIG_IGN);
        if (pid != 0)
        {
//            setsid();
//            setpgid(pid, pid);
//            tcsetpgrp(0, pid);
//            tcsetpgrp(1, pid);
//            tcsetpgrp(2, pid);
            return pid;
        }

        // convert command and arguments into C types
        std::vector<char*> exec_args;
        exec_args.push_back(strdup(*command));
        for (auto&& arg : arguments)
            exec_args.push_back(strdup(*arg));
        exec_args.push_back(nullptr);

        // create process group and set FDs (handle signals in the child process)
//        setsid();
//        setpgid(0, 0);
//        tcsetpgrp(0, getpgrp());
//        tcsetpgrp(1, getpgrp());
//        tcsetpgrp(2, getpgrp());

        // execute new process
        const auto &status = execvp(command.data(), exec_args.data());

        // free alloc'ed strings
        for (auto *a : exec_args)
            std::free(a);

        return status;
    }
    static int run_process(const String &command, const StringVector &arguments)
    {
        const auto &cpid = run_process_internal(command, arguments);
        if (cpid == -1)
        {
            // Failed to fork
            return -1;
        }

        // wait for child process to finish
        int status;
        waitpid(cpid, &status, 0);
        if (!WIFEXITED(status))
        {
            return -3;
        }

        // return child process real exit status
        status = WEXITSTATUS(status);

        // child process is done with its work, restore Ctrl+C
        std::signal(SIGINT,  orig_int);
        std::signal(SIGTERM, orig_term);
        std::signal(SIGQUIT, orig_quit);

        return status;
    }
}

void PlayerController::DeregisterAll()
{
    _players.clear();
    _userPlayers.clear();
}

void PlayerController::RegisterPlayer(const Player &player)
{
    _players.insert({player.fmt, player});
}

void PlayerController::RegisterPlayerOverride(const UserPlayer &player, bool override)
{
    if (!override && _userPlayers.count(player.fmt))
    {
        LOG_WARNING("PlayerController: %s: already registered", player.fmt);
        return;
    }

    _userPlayers.insert({player.fmt, player});
}

void PlayerController::Play(const MediaFile *mf, bool forceplayer, const MediaType &forcetype)
{
    if (!mf)
        return;

    // select player for given MediaFile object
    String cmd;
    StringVector args;
    if (forceplayer)
        SelectPlayer(forcetype, mf->fileextension, cmd, args);
    else
        SelectPlayer(mf->type, mf->fileextension, cmd, args);

    if (cmd.isEmpty())
    {
        LOG_FATAL("PlayerController: command is empty. check your configuration file!");
        return;
    }

    // replace "%f" in arguments with filename, or append it if missing
    auto f = args.index_of("%f");
    if (f != decltype(args)::npos)
    {
        args.replace(f, mf->fileinfo.absoluteFilePath().toUtf8().constData());
    }
    else
    {
        args.append(mf->fileinfo.absoluteFilePath().toUtf8().constData());
    }

    AppearanceController::Print(mf);
    MusicConsoleReborn::library()->setLastPlayed(mf);

    // execute player
    const auto &ret = run_process(cmd, args);
    ShowPlayerError(cmd, ret);
}

void PlayerController::Play(const QList<const MediaFile*> &mf)
{
    if (mf.isEmpty())
        return;

    // select _allplayer for crossfade
    String cmd;
    StringVector args;
    SelectPlayer(NONE, "", cmd, args);

    if (cmd.isEmpty())
    {
        LOG_FATAL("PlayerController: command is empty. check your configuration file!");
        return;
    }

    // replace "%f" in arguments with filenames, or append them if missing
    auto f = args.index_of("%f");
    if (f != decltype(args)::npos)
    {
        args.remove_at(f);
        for (auto&& m : mf)
        {
            args.insert(f, m->fileinfo.absoluteFilePath());
            f++;
        }
    }
    else
    {
        for (auto&& m : mf)
            args.append(m->fileinfo.absoluteFilePath());
    }

    LOG_HINT("Playing crossfade:");
    for (auto&& m : mf)
        AppearanceController::Print(m);
    MusicConsoleReborn::library()->setLastPlayed(mf.at(0)); // list can't be empty here

    // execute player
    const auto &ret = run_process(cmd, args);
    ShowPlayerError(cmd, ret);
}

void PlayerController::SelectPlayer(const MediaType &type, const String &fileExtension,
                                    String &command, StringVector &arguments)
{
    if (_userPlayers.count(fileExtension))
    {
        const auto &player = _userPlayers.at(fileExtension);
        command = player.cmd;
        arguments = player.args;
    }
    else
    {
        const auto &player = _players.at(type);
        command = player.cmd;
        arguments = player.args;
    }
}

void PlayerController::ShowPlayerError(const String &cmd, const int &ret)
{
    // ret -1, failed to fork()
    // ret -3, failed to waitpid()
    if (ret == -1)
    {
        LOG_FATAL("\nPlayerController: failed to fork process! command was: %s", cmd);
    }
    else if (ret == -3)
    {
        LOG_FATAL("\nPlayerController: failed to wait for child process to terminate! command was: %s", cmd);
    }
}
