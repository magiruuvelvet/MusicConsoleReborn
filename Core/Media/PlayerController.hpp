#ifndef PLAYERCONTROLLER_HPP
#define PLAYERCONTROLLER_HPP

#include "MediaFile.hpp"

#include <map>

struct UserPlayer {
    String fmt;
    String cmd;
    StringVector args;
};

struct Player {
    MediaType fmt;
    String cmd;
    StringVector args;
};

class PlayerController final
{
    PlayerController() = delete;

public:
    static void DeregisterAll();

    static void RegisterPlayer(const Player &player);
    static void RegisterPlayerOverride(const UserPlayer &player, bool override = false);

    static void Play(const MediaFile *mf, bool forceplayer = false, const MediaType &forcetype = NONE);
    // FIXME: last Qt type in this class
    static void Play(const QList<const MediaFile*> &mf);

private:
    static void SelectPlayer(const MediaType &type, const String &fileExtension,
                             String &command, StringVector &arguments);

    static void ShowPlayerError(const String &cmd, const int &ret);

    static std::map<MediaType, Player> _players;
    static std::map<String, UserPlayer> _userPlayers;
};

#endif // PLAYERCONTROLLER_HPP
