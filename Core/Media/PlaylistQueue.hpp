#ifndef PLAYLISTQUEUE_HPP
#define PLAYLISTQUEUE_HPP

#include "MediaFile.hpp"

#include <initializer_list>
#include <queue>

class PlaylistQueue final
{
public:
    PlaylistQueue();
    PlaylistQueue(const std::list<const MediaFile*> &list);
    PlaylistQueue(const std::initializer_list<const MediaFile*> &list);
    ~PlaylistQueue();

    struct PlaylistQueueEntry
    {
        MediaFile *mf = nullptr;
        MediaType player_override;
    };

    // enqueues a new MediaFile object
    void enqueue(const MediaFile *mf, const MediaType &player_override);

    // returns the next PlaylistQueueEntry in the queue and removes it
    // if there are no more PlaylistQueueEntries in the queue, a nullptr
    // MediaFile will be returned
    //  > can be used for while loops
    const PlaylistQueueEntry next();

    // removes all remaining MediaFiles from the playlist
    void clear();

private:
    std::queue<PlaylistQueueEntry, std::list<PlaylistQueueEntry>> _queue;
};

#endif // PLAYLISTQUEUE_HPP
