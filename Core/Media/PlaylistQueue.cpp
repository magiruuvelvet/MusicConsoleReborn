#include "PlaylistQueue.hpp"

PlaylistQueue::PlaylistQueue()
{
}

PlaylistQueue::PlaylistQueue(const std::list<const MediaFile*> &list)
{
    for (auto&& elem : list)
        _queue.push(PlaylistQueueEntry{const_cast<MediaFile*>(elem), elem->type});
}

PlaylistQueue::PlaylistQueue(const std::initializer_list<const MediaFile*> &list)
{
    for (auto&& elem : list)
        _queue.push(PlaylistQueueEntry{const_cast<MediaFile*>(elem), elem->type});
}

PlaylistQueue::~PlaylistQueue()
{
    _queue = {};
}

void PlaylistQueue::enqueue(const MediaFile *mf, const MediaType &player_override)
{
    _queue.push(PlaylistQueueEntry{const_cast<MediaFile*>(mf), player_override});
}

const PlaylistQueue::PlaylistQueueEntry PlaylistQueue::next()
{
    if (!_queue.empty())
    {
        const auto &f = _queue.front();
        _queue.pop();
        return f;
    }
    return PlaylistQueueEntry{nullptr, NONE};
}

void PlaylistQueue::clear()
{
    _queue = {};
}
