#include "MediaFileFactory.hpp"
#include "Cache/MediaCache.hpp"

#include "MetadataReaderBackends/FFMpegMetadataReader.hpp"
#include "MetadataReaderBackends/StubMetadataReader.hpp"

#include <Utils/PrefixRemover.hpp>
#include <Utils/SearchTermGen.hpp>
#include <Utils/SearchTermGens/UnicodeWhitespaceFixer.hpp>
#include <Utils/SearchTermGens/UnicodeLatinGen.hpp>
#include <Utils/SearchTermGens/UniversalJapaneseKanaGen.hpp>

#include <Types/IteratorUtils.hpp>

#include <Types/StrFmt.hpp>

#include <algorithm>

const std::array<std::unique_ptr<SearchTermGen>, 3> MediaFileFactory::_searchTermGens = {
    std::make_unique<SearchTermGens::UnicodeWhitespaceFixer>(),
    std::make_unique<SearchTermGens::UnicodeLatinGen>(),
    std::make_unique<SearchTermGens::UniversalJapaneseKanaGen>(),
};

std::unique_ptr<MetadataReader> MediaFileFactory::_metadataReader;

void MediaFileFactory::InitMetadataReader()
{
#if defined(USE_FFMPEG_METADATA_READER) || defined(USE_LINKED_FFMPEG_METADATA_READER)
    _metadataReader = std::make_unique<FFMpegMetadataReader>();
#else
    _metadataReader = std::make_unique<StubMetadataReader>();
#endif
}

void MediaFileFactory::DeinitMetadataReader()
{
    _metadataReader.reset();
}

std::shared_ptr<MediaFile> MediaFileFactory::CreateMediaFile(const QFileInfo &fileinfo, const MediaType &type)
{
    auto mf = std::make_shared<MediaFile>(fileinfo, type);
    mf->hash = MediaCache::HashHelper(fileinfo);

    if (!mf->isValid())
    {
        mf.reset();
        return nullptr;
    }

    // read metadata
    if (_metadataReader)
        mf->metadata = _metadataReader->ReadMetadataFromFile(fileinfo.absoluteFilePath().toUtf8());

    // generate search terms
    const auto &cleanPath = PrefixRemover::RemovePrefix(mf->fileinfo);
    QByteArrayList searchTerms;
    for (auto&& gen : _searchTermGens)
        searchTerms.append(gen->processString(cleanPath));

    // remove duplicates without sorting the list
    IteratorUtils::RemoveDuplicates(searchTerms);

    // prepend clean path (must be at the top)
    searchTerms.prepend(cleanPath);

    // add metadata to search terms as they can differ from filenames
    searchTerms.append(String(                                              // FIXME: operator QByteArray() is used here!
        mf->metadata.artist + ' ' +
        mf->metadata.album_artist + ' ' +
        mf->metadata.album + ' ' +
        (mf->metadata.track != 0 ? fmt::format("%02i", mf->metadata.track) : "") + ' ' +
        // ignore total tracks
        mf->metadata.title + ' ' +
        mf->metadata.album + ' ' // add album twice for more flexible lookup
    ).simplified());

    mf->searchTerms = searchTerms;

    return mf;
}

std::shared_ptr<MediaFile> MediaFileFactory::CreateFromCache(const MediaCacheObject *cache_id, bool *outdated)
{
    const auto &mf = cache_id->read(outdated);
    if (*outdated)
        return nullptr;
    if (mf->isValid())
        return mf;
    return nullptr;
}
