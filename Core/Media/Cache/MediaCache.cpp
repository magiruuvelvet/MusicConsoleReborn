#include "MediaCache.hpp"

#include <Core/Logger.hpp>

#include <QDir>
#include <QCryptographicHash>

inline uint qHash(const QFileInfo &fileInfo, uint seed = 0)
{
    return qHash(fileInfo.absoluteFilePath(), seed);
}

QByteArray MediaCache::_path;
bool MediaCache::_isInit = false;
bool MediaCache::_readonly = false;

QHash<QFileInfo, std::shared_ptr<MediaCacheObject>> MediaCache::_storage;

void MediaCache::Init(const QByteArray &path)
{
    _path = path;

    // check if configuration directory exists
    QDir dir(_path);
    if (!dir.exists())
    {
        // attempt to create config directory
        dir = QDir::root();
        if (dir.mkpath(_path))
        {
            LOG_NOTICE("Successfully created: %s", _path);
            _isInit = true;
        }
        else
        {
            LOG_FATAL("Failed to create: %s", _path);
            _isInit = false;
        }
    }

    // validate read/write permissions
    else
    {
        if (!QFileInfo(_path).isReadable())
        {
            LOG_FATAL("Failed to access: %s", _path);
            _isInit = false;
        }
        else if (!QFileInfo(_path).isWritable())
        {
            LOG_WARNING("Cache directory is not writable. You won't be able to enjoy fast startups.\n%s", _path);
            _isInit = true;
            _readonly = true;
        }
    }
}

bool MediaCache::hasFile(const QFileInfo &fileinfo)
{
    if (_storage.contains(fileinfo))
        return true;

    const auto &hash = HashHelper(fileinfo);
    const auto &cache_path = HashedFilePathHelper2(hash);

    if (QFileInfo(cache_path).exists())
    {
        _storage.insert(fileinfo, std::make_shared<MediaCacheObject>(cache_path, hash));
        return true;
    }

    return false;
}

const MediaCacheObject *MediaCache::GetObject(const QFileInfo &fileinfo)
{
    return _storage[fileinfo].get();
}

void MediaCache::addFile(const std::shared_ptr<MediaFile> &file)
{
    if (!_readonly)
    {
        const auto &hash = HashHelper(file->fileinfo);
        auto cache_obj = std::make_shared<MediaCacheObject>(HashedFilePathHelper2(hash), hash);
        cache_obj->write(file);
        _storage.insert(file->fileinfo, cache_obj);
    }
    else
    {
        LOG_WARNING("MediaCache: add file: cache is readonly!");
    }
}

const QByteArray MediaCache::HashHelper(const QFileInfo &fileinfo)
{
    return
        QCryptographicHash::hash(fileinfo.absoluteFilePath().toUtf8(),
                                 QCryptographicHash::Md5).toHex()
      + '_'
      + QCryptographicHash::hash(fileinfo.baseName().toUtf8(),
                                 QCryptographicHash::Md4).toHex();
}
