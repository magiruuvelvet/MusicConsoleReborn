#ifndef MEDIACACHE_HPP
#define MEDIACACHE_HPP

#include <QHash>

#include "../MediaFile.hpp"
#include "MediaCacheObject.hpp"

#include <memory>

class MediaCache final
{
    MediaCache() = delete;

    friend class MediaFileFactory;

public:
    static void Init(const QByteArray &path);

    static bool hasFile(const QFileInfo &fileinfo);

    static const MediaCacheObject *GetObject(const QFileInfo &fileinfo);

    static void addFile(const std::shared_ptr<MediaFile> &file);

private:
    static QByteArray _path;
    static bool _isInit;
    static bool _readonly;

    static QHash<QFileInfo, std::shared_ptr<MediaCacheObject>> _storage;

    static const QByteArray HashHelper(const QFileInfo &fileinfo);
    inline static const QByteArray HashedFilePathHelper(const QFileInfo &fileinfo)
    { return _path + '/' + HashHelper(fileinfo) + ".obj"; }
    inline static const QByteArray HashedFilePathHelper2(const QByteArray &filename)
    { return _path + '/' + filename + ".obj"; }
};

#endif // MEDIACACHE_HPP
