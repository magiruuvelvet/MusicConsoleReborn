#include "MediaCacheObject.hpp"

#include <QFile>
#include <QDataStream>

#include <memory>

const quint64 MediaCacheObject::REVISION = 0x00001227ULL;

QDataStream &operator<< (QDataStream &out, const Metadata &md)
{
    out << *md.artist;
    out << *md.album_artist;
    out << *md.title;
    out << *md.album;
    out << *md.genre;
    out << md.track;
    out << md.total_tracks;
    return out;
}

QDataStream &operator>> (QDataStream &in, Metadata &md)
{
    char *data;
    in >> data; md.artist = String(data);
    in >> data; md.album_artist = String(data);
    in >> data; md.title = String(data);
    in >> data; md.album = String(data);
    in >> data; md.genre = String(data);
    std::free(data);

    in >> md.track;
    in >> md.total_tracks;

    return in;
}

QDataStream &operator<< (QDataStream &out, const MediaFile &mf)
{
    out << mf.fileinfo.lastModified();
    out << mf.fileinfo.absoluteFilePath().toUtf8().constData();
    out << mf.hash.constData();
    out << static_cast<quint32>(mf.type);
    out << mf.metadata;

    out << mf.searchTerms.size();
    for (auto&& term : mf.searchTerms)
        out << term.constData();

    return out;
}

QDataStream &operator>> (QDataStream &in, MediaFile &mf)
{
    in >> mf.mtime;

    char *data;
    in >> data; mf.fileinfo = QFileInfo(QByteArray(data));
    in >> data; mf.hash = QByteArray(data);

    quint32 dataInt;
    in >> dataInt; mf.type = static_cast<MediaType>(dataInt);

    in >> mf.metadata;

    int size;
    in >> size;
    for (auto i = 0; i < size; i++)
    {
        in >> data; mf.searchTerms.append(QByteArray(data));
    }

    std::free(data);
    return in;
}

MediaCacheObject::MediaCacheObject()
{
}

MediaCacheObject::MediaCacheObject(const QByteArray &cache_path, const QByteArray &hash)
{
    _cachePath = cache_path;
    _hash = hash;
}

MediaCacheObject::~MediaCacheObject()
{
    _cachePath.clear();
    _hash.clear();
}

const std::shared_ptr<MediaFile> MediaCacheObject::read(bool *outdated) const
{
    QFile f(_cachePath);
    f.open(QIODevice::ReadOnly);
    QDataStream fstr(&f);
    fstr.setByteOrder(QDataStream::LittleEndian);

    MediaFile mf;

    quint64 rev;
    fstr >> rev;
    fstr >> mf;
    f.close();

    if (rev != REVISION || mf.fileinfo.lastModified() != mf.mtime)
    {
        *outdated = true;
        return nullptr;
    }
    else
    {
        *outdated = false;
    }

    return std::make_shared<MediaFile>(mf);
}

void MediaCacheObject::write(const std::shared_ptr<MediaFile> &file)
{
    QFile f(_cachePath);
    if (f.exists())
        f.remove();
    f.open(QIODevice::WriteOnly);
    QDataStream fstr(&f);
    fstr.setByteOrder(QDataStream::LittleEndian);

    fstr << REVISION;
    fstr << (*file.get());
    // set cache file to readonly for owner
    f.setPermissions(QFileDevice::ReadOwner);
    f.close();
}
