#ifndef MEDIACACHEOBJECT_HPP
#define MEDIACACHEOBJECT_HPP

#include <QByteArray>
#include "../MediaFile.hpp"

#include <memory>

class MediaCacheObject final
{
public:
    MediaCacheObject();
    MediaCacheObject(const QByteArray &cache_path, const QByteArray &hash);
    ~MediaCacheObject();

    inline const QByteArray &cachePath() const
    { return _cachePath; }
    inline const QByteArray &hash() const
    { return _hash; }

    const std::shared_ptr<MediaFile> read(bool *outdated) const;
    void write(const std::shared_ptr<MediaFile> &file);

private:
    QByteArray _cachePath;
    QByteArray _hash;

    static const quint64 REVISION;
};

#endif // MEDIACACHEOBJECT_HPP
