#ifndef MEDIAFILE_HPP
#define MEDIAFILE_HPP

#include <QByteArrayList>
#include <QFileInfo>
#include <QDateTime>

#include <Types/String.hpp>
#include <Types/Vector.hpp>

enum MediaType
{
    NONE   = 0x0000,
    AUDIO  = 0x0001,
    VIDEO  = 0x0010,
    MOD    = 0x0100,
};

struct Metadata final
{
    ~Metadata();

    bool isEmpty() const;
    void clear();

    String artist;
    String album_artist;

    String album;
    String title;

    String genre;

    quint32 track = 0;
    quint32 total_tracks = 0;

    // copy operator
    inline void operator= (const Metadata &other)
    {
        artist = other.artist;
        album_artist = other.album_artist;
        album = other.album;
        title = other.title;
        genre = other.genre;
        track = other.track;
        total_tracks = other.total_tracks;
    }

    // compare operator
    inline bool operator== (const Metadata &other)
    {
        return (
            artist == other.artist &&
            album_artist == other.album_artist &&
            album == other.album &&
            title == other.title &&
            genre == other.genre &&
            track == other.track &&
            total_tracks == other.total_tracks
        );
    }
};

struct MediaFile final
{
    // all those need read/write access to the internal hash
    // don't make the hash public for everything else
    friend class MediaLibraryModel;
    friend class MediaFileFactory;
    friend class MediaCache;
    friend class MediaCacheObject;
    friend QDataStream &operator<< (QDataStream &out, const MediaFile &mf);
    friend QDataStream &operator>> (QDataStream &in, MediaFile &mf);

private:
    MediaFile();
public:
    MediaFile(const MediaFile &other);
    MediaFile(const QFileInfo &file, const MediaType &type);
    MediaFile(const QFileInfo &file, const MediaType &type, const Metadata &metadata);
    ~MediaFile();

    bool isValid() const;
    bool hasMetadata() const;

    // relative path in the model, not suitable for file operations
    inline const QByteArray &prettyPath() const
    { return searchTerms.at(0); }

    QFileInfo fileinfo;
    MediaType type;
    Metadata metadata;

    QByteArrayList searchTerms;

    // buffer to avoid calling QFileInfo::suffix() all the time
    String fileextension;

    inline bool operator== (const MediaFile &other)
    {
        return (
            isValid() == other.isValid() &&
            fileinfo == other.fileinfo &&
            type == other.type &&
            metadata == other.metadata &&
            hash == other.hash
            // don't compare against search terms for performance reasons
            // don't compare against fileextension (redundant)
        );
    }

private:
    // disable copy
    void operator= (const MediaFile &other) = delete;

    QByteArray hash;
    QDateTime mtime;
};

#endif // MEDIAFILE_HPP
