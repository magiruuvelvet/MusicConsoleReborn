#include "MediaFile.hpp"

Metadata::~Metadata()
{
    this->clear();
}

bool Metadata::isEmpty() const
{
    return (
        artist.isEmpty() &&
        album_artist.isEmpty() &&
        album.isEmpty() &&
        title.isEmpty() &&
        track == 0 &&
        total_tracks == 0
    );
}

void Metadata::clear()
{
    artist.clear();
    album_artist.clear();

    album.clear();
    title.clear();

    track = {};
    total_tracks = {};
}

MediaFile::MediaFile()
{
}

MediaFile::MediaFile(const MediaFile &other)
{
    this->fileinfo = other.fileinfo;
    this->fileextension = other.fileinfo.suffix().toUtf8().constData();
    this->hash = other.hash;
    this->type = other.type;
    this->metadata = other.metadata;
    this->searchTerms = other.searchTerms;
}

MediaFile::MediaFile(const QFileInfo &file, const MediaType &type)
    : MediaFile()
{
    this->fileinfo = file;
    this->fileextension = file.suffix().toUtf8().constData();
    this->type = type;
}

MediaFile::MediaFile(const QFileInfo &file, const MediaType &type, const Metadata &metadata)
    : MediaFile(file, type)
{
    this->metadata = metadata;
}

MediaFile::~MediaFile()
{
    type = NONE;
    metadata.clear();
    hash.clear();
}

bool MediaFile::isValid() const
{
    return fileinfo.exists() && fileinfo.isReadable() && hash.size() != 0;
}

bool MediaFile::hasMetadata() const
{
    return !metadata.isEmpty();
}
