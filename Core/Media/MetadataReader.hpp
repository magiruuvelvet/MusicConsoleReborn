#ifndef METADATAREADER_HPP
#define METADATAREADER_HPP

#include "MediaFile.hpp"

class MetadataReader
{
public:
    MetadataReader();
    virtual ~MetadataReader();

    virtual const Metadata ReadMetadataFromFile(const String &file) const = 0;
};

#endif // METADATAREADER_HPP
