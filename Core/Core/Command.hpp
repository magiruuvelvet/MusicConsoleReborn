#ifndef COMMAND_HPP
#define COMMAND_HPP

#include "Private/CommandBase.hpp"
#include <Media/MediaFile.hpp>

/**
 * template class for MusicConsoleReborn commands
 */
class Command : public CommandBase
{
public:
    Command(const QString &name);
    virtual ~Command();

    // sets the active arguments for the command
    void setArguments(const QString &input);
    void setArguments(const QStringList &args)
    { _arguments = args; }
    void setArguments(const QByteArrayList &args);

    // returns a copy of all arguments merged
    const QString mergeArgs() const
    { return _arguments.join(' '); }

    // clears the active arguments
    void clearArguments() const
    { _arguments.clear(); }

    // execute the command (must be implemented in subclass)
    virtual void exec() const = 0;

protected:
    // shifts command arguments and returns the extracted MediaType
    MediaType ApplyFilter() const;

    // shifts command arguments
    void shift() const;

private:
    QString _name;
};

#endif // COMMAND_HPP
