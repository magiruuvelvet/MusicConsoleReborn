#ifndef CONFIGMANAGER_HPP
#define CONFIGMANAGER_HPP

#include <Media/MediaFile.hpp>
#include <Media/PlayerController.hpp>

#include <Utils/AppearanceController.hpp>

#include <Types/UnicodeString.hpp>
#include <Types/RegularExpression.hpp>

#include <memory>

class ConfigManager final
{
    ConfigManager();
    friend __attribute__((always_inline)) inline std::unique_ptr<ConfigManager> _MakeConfigManager();

public:
    static ConfigManager *instance();
    ~ConfigManager();

    void LoadConfigFromDisk();
    bool SaveConfigToDisk();

    const QString GetServerListeningAddress() const;
    quint16 GetServerListeningPort() const;
    const QString GetServerStaticAssetsPath() const;
    const QString GetServerTemplatesPath() const;

    // already escaped <TerminalEscapeSequences>
    const String GetPrompt() const;
    void SetPrompt(const String &prompt);

    double GetKbhitSleepInterval() const;
    void SetKbhitSleepInterval(const double &kbhit_sleep_interval);

    bool GetHintsEnabled() const;
    void SetHintsEnabled(const bool &hints_enabled);

    uint GetHintRows() const;
    void SetHintRows(const uint &hint_rows);

    const String GetHintColor() const;
    void SetHintColor(const String &hint_color);

    bool GetHighlighterEnabled() const;
    void SetHighlighterEnabled(const bool &highlighter_enabled);

    const List<RegularExpression> GetHistIgnorePatterns() const;

    enum class Command {
        audio,
        video,
        mod,
        all,
        search,
        random,
        shuffle,
        repeat,
        history,
        statistics,
        rescan,
        playlist,
        exit,
        clear,
        config,
    };
    const QString GetCommand(const Command &cmd) const;
    const QString GetDefaultCommand() const;
    const QString GetFilterCommand(const MediaType &type) const;

    const QString GetPlaylistFileLoadCommand() const;
    const QString GetPlaylistCrossfadeCommand() const;

    const QByteArray GetLibraryRootPath(bool expand = true) const;
    void SetLibraryRootPath(const QByteArray &rootpath);

    const StringList GetFiletypes(const MediaType &type) const;

    const QByteArrayList GetLibraryPlaylistPaths() const;
    const QByteArrayList GetPrefixDeletionPatterns() const;
    const QByteArrayList GetMoveToBottomPhrases() const;

    const QStringList GetMetadataTitleSplitList() const;

    quint64 GetRandomizerHistorySize() const;
    void SetRandomizerHistorySize(const quint64 &historysize);

    const AppearanceSettings GetAppearanceSettings() const;

    static const QByteArray &GetConfigDirectory();
    static const QByteArray &GetCacheDirectory();
    static const QByteArray &GetSettingsFileLocation();
    static const QByteArray &GetPlayerConfigFileLocation();

    const QList<Player> GetPlayers() const;
    const QList<UserPlayer> GetUserPlayers() const;

private:
    bool _isInit = false;
    bool _readonly = false;

    enum ConfigFileStatus {
        IsDirectory,
        Missing,
        Permission,
        OK,
    };
    ConfigFileStatus ProbeForConfigFile();
    void InitDefaultConfiguration(bool save = true);
    void ValidateConfiguration();
    void ValidatePlayerConfiguration();
    bool LoadConfigFromDiskInternal();
    bool LoadPlayerConfigFromDiskInternal();
    bool SaveConfigToDiskInternal();
};

inline ConfigManager *Config()
{ return ConfigManager::instance(); }

#endif // CONFIGMANAGER_HPP
