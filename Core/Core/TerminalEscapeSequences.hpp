#ifndef TERMINALESCAPESEQUENCES_HPP
#define TERMINALESCAPESEQUENCES_HPP

#include <Types/String.hpp>

class TerminalEscapeSequences final
{
    TerminalEscapeSequences() = delete;

public:
    // evaluates escape sequences and returns the escaped raw string
    static const String Eval(const String &s);
};

#endif // TERMINALESCAPESEQUENCES_HPP
