#ifndef MUSICCONSOLEREBORN_HPP
#define MUSICCONSOLEREBORN_HPP

#include <QCoreApplication>
#include <QProcessEnvironment>
#include <QByteArrayList>
#include <QString>

#include <memory>

class MediaLibraryModel;

class MusicConsoleReborn final : public QCoreApplication
{
    Q_OBJECT

public:
    explicit MusicConsoleReborn(const int *argc, const char **argv);
    ~MusicConsoleReborn();

    static MusicConsoleReborn *instance()
    { return static_cast<MusicConsoleReborn*>(QCoreApplication::instance()); }

    static bool isRunning();

    static void setApplicationDisplayName(const QString &displayName);
    static const QString &applicationDisplayName();

    static QProcessEnvironment &processEnvironment();

    static MediaLibraryModel *library();

    const QByteArrayList &arguments() const;
    inline bool hasArgs() const
    { return !_args.isEmpty(); }

    void emitReady();

signals:
    void ready();

private:
    QByteArrayList _args;
    static QString __applicationDisplayName;
    static QProcessEnvironment __processEnvironment;
    static QString _pidFileLocation;
    static bool _isRunning;
    static std::unique_ptr<MediaLibraryModel> _lib;
};

#endif // MUSICCONSOLEREBORN_HPP
