#include "ConfigManager.hpp"

#include <Core/Logger.hpp>

#include <Core/TerminalEscapeSequences.hpp>
#include <Utils/ExpandEnvironment.hpp>

#include <QStandardPaths>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QTextStream>

#include <iostream>
#include <fstream>
#include <valijson/adapters/rapidjson_adapter.hpp>
#include <valijson/utils/rapidjson_utils.hpp>
#include <valijson/schema.hpp>
#include <valijson/schema_parser.hpp>
#include <valijson/validator.hpp>

#include <rapidjson/document.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/ostreamwrapper.h>
#include <rapidjson/pointer.h>
#include <rapidjson/error/error.h>
#include <rapidjson/error/en.h>

#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>

using valijson::Schema;
using valijson::SchemaParser;
using valijson::Validator;
using valijson::ValidationResults;
using valijson::adapters::RapidJsonAdapter;

namespace {
    namespace ConfigManagerInternal {
        std::unique_ptr<rapidjson::Document> rapidjson_settings;
        std::unique_ptr<rapidjson::Document> rapidjson_players;
    }
}

namespace {
    static bool rmtree(const char *path)
    {
        std::size_t path_len;
        char *full_path;
        DIR *dir;
        struct stat stat_path, stat_entry;
        struct dirent *entry;

        // stat for the path
        stat(path, &stat_path);

        // if path does not exists or is not dir
        if (S_ISDIR(stat_path.st_mode) == 0) {
            std::fprintf(stderr, "%s: %s\n", "Is not directory", path);
            return false;
        }

        // if not possible to read the directory for this user
        if ((dir = opendir(path)) == nullptr) {
            std::fprintf(stderr, "%s: %s\n", "Can`t open directory", path);
            return false;
        }

        // the length of the path
        path_len = strlen(path);

        // iteration through entries in the directory
        while ((entry = readdir(dir)) != nullptr) {

            // skip entries "." and ".."
            if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, ".."))
                continue;

            // determinate a full path of an entry
            full_path = std::move<char*>(static_cast<char*>((std::calloc(path_len + strlen(entry->d_name) + 1, sizeof(char)))));
            strcpy(full_path, path);
            strcat(full_path, "/");
            strcat(full_path, entry->d_name);

            // stat for the entry
            stat(full_path, &stat_entry);

            // recursively remove a nested directory
            if (S_ISDIR(stat_entry.st_mode) != 0) {
                rmtree(full_path);
                continue;
            }

            // remove a file object
            if (unlink(full_path) == 0)
                printf("Removed a file: %s\n", full_path);
            else
                printf("Can`t remove a file: %s\n", full_path);
        }

        // remove the devastated directory and close the object of it
        bool status;
        if (rmdir(path) == 0)
        {
            printf("Removed a directory: %s\n", path);
            status = true;
        }
        else
        {
            printf("Can`t remove a directory: %s\n", path);
            status = false;
        }

        closedir(dir);
        return status;
    }
}

#define JSONDOC (*ConfigManagerInternal::rapidjson_settings.get())
#define JSONDOC_PLAYERS (*ConfigManagerInternal::rapidjson_players.get())

__attribute__((always_inline))
inline std::unique_ptr<ConfigManager> _MakeConfigManager()
{
    return std::unique_ptr<ConfigManager>(new ConfigManager());
}

ConfigManager *ConfigManager::instance()
{
    static auto _instance = _MakeConfigManager();
    return _instance.get();
}

ConfigManager::ConfigManager()
{
    // check if configuration directory exists
    QDir dir(GetConfigDirectory());
    if (!dir.exists())
    {
        // attempt to create config directory
        dir = QDir::root();
        if (dir.mkpath(GetConfigDirectory()))
        {
            LOG_NOTICE("Successfully created: %s", GetConfigDirectory());
            _isInit = true;
        }
        else
        {
            LOG_FATAL("Failed to create: %s", GetConfigDirectory());
            _isInit = false;
        }
    }

    // validate read/write permissions
    else
    {
        if (!QFileInfo(GetConfigDirectory()).isReadable())
        {
            LOG_FATAL("Failed to access: %s", GetConfigDirectory());
            _isInit = false;
        }
        else if (!QFileInfo(GetConfigDirectory()).isWritable())
        {
            LOG_WARNING("Config directory is not writable. You won't be able to change settings.\n%s", GetConfigDirectory());
            _isInit = true;
            _readonly = true;
        }
    }
}

ConfigManager::~ConfigManager()
{
}

void ConfigManager::LoadConfigFromDisk()
{
    const auto &status = ProbeForConfigFile();
    bool res;

    switch (status)
    {
        case OK:
            if (!LoadConfigFromDiskInternal())
            {
                LOG_ERROR("ConfigManager: Error loading config!\n"
                          "Check if the file exists and you have permissions to read it.\n"
                          "%s", GetSettingsFileLocation());
            }
            else
            {
                LoadPlayerConfigFromDiskInternal();
            }
            break;

        case IsDirectory:
            res = rmtree(GetSettingsFileLocation().constData());
            // try once again on success (init default config)
            if (res)
                LoadConfigFromDisk();
            // abort on failure
            else
                LOG_FATAL("ConfigManager: unable to unlink directory! Manual user intervention required.");
            break;

        case Permission:
            LOG_ERROR("ConfigManager: unable to read: %s", GetSettingsFileLocation());
            LOG_NOTICE("ConfigManager: temporarily using default configuration.\n"
                       "Please fix your config file permissions.");
            InitDefaultConfiguration(false);
            break;

        case Missing:
            // Initialize default settings
            LOG_NOTICE("ConfigManager: No config file found. Initializing default settings...");
            InitDefaultConfiguration();
            LoadPlayerConfigFromDiskInternal();
            break;
    }
}

bool ConfigManager::SaveConfigToDisk()
{
    return SaveConfigToDiskInternal();
}

const QString ConfigManager::GetServerListeningAddress() const
{
    const auto &address = JSONDOC["webserver"]["interface"];
    return QString::fromUtf8(address.GetString()).simplified();
}

quint16 ConfigManager::GetServerListeningPort() const
{
    const auto &port = JSONDOC["webserver"]["port"];
    return static_cast<quint16>(port.GetUint());
}

const QString ConfigManager::GetServerStaticAssetsPath() const
{
    const auto &path = JSONDOC["webserver"]["static_assets"];
    return QString::fromUtf8(ExpandEnvironment::ExpandEnvironmentVariables(path.GetString()));
}

const QString ConfigManager::GetServerTemplatesPath() const
{
    const auto &path = JSONDOC["webserver"]["templates"];
    return QString::fromUtf8(ExpandEnvironment::ExpandEnvironmentVariables(path.GetString()));
}

const String ConfigManager::GetPrompt() const
{
    const auto &prompt = JSONDOC["prompt"]["line"];
    const auto &translated = TerminalEscapeSequences::Eval(prompt.GetString());
    return translated;
}

void ConfigManager::SetPrompt(const String &prompt)
{
    // make sure to copy the string data because it runs out-of-scope here and corrupts saving afterwards
    JSONDOC["prompt"]["line"].SetString(prompt.c_str(), prompt.size(), JSONDOC.GetAllocator());
}

double ConfigManager::GetKbhitSleepInterval() const
{
    const auto &kbhit_sleep_interval = JSONDOC["prompt"]["kbhit_sleep_interval"];
    return kbhit_sleep_interval.GetDouble();
}

void ConfigManager::SetKbhitSleepInterval(const double &kbhit_sleep_interval)
{
    JSONDOC["prompt"]["kbhit_sleep_interval"].SetDouble(kbhit_sleep_interval);
}

bool ConfigManager::GetHintsEnabled() const
{
    const auto &hints_enabled = JSONDOC["prompt"]["hints_enabled"];
    return hints_enabled.GetBool();
}

void ConfigManager::SetHintsEnabled(const bool &hints_enabled)
{
    JSONDOC["prompt"]["hints_enabled"].SetBool(hints_enabled);
}

uint ConfigManager::GetHintRows() const
{
    const auto &hint_rows = JSONDOC["prompt"]["hint_rows"];
    return hint_rows.GetUint();
}

void ConfigManager::SetHintRows(const uint &hint_rows)
{
    JSONDOC["prompt"]["hint_rows"].SetUint(hint_rows);
}

const String ConfigManager::GetHintColor() const
{
    const auto &hint_color = JSONDOC["prompt"]["hint_color"];
    return hint_color.GetString();
}

void ConfigManager::SetHintColor(const String &hint_color)
{
    JSONDOC["prompt"]["hint_color"].SetString(hint_color.c_str(), hint_color.size(), JSONDOC.GetAllocator());
}

bool ConfigManager::GetHighlighterEnabled() const
{
    const auto &highlighter_enabled = JSONDOC["prompt"]["highlighter_enabled"];
    return highlighter_enabled.GetBool();
}

void ConfigManager::SetHighlighterEnabled(const bool &highlighter_enabled)
{
    JSONDOC["prompt"]["highlighter_enabled"].SetBool(highlighter_enabled);
}

const List<RegularExpression> ConfigManager::GetHistIgnorePatterns() const
{
    const auto &patterns = JSONDOC["prompt"]["histignore"];
    List<RegularExpression> p;
    for (auto&& pattern : patterns.GetArray())
        p.append(RegularExpression(UnicodeString(pattern.GetString())));
    return p;
}

const QString ConfigManager::GetCommand(const Command &cmd) const
{
    const auto &commands = JSONDOC["commands"];
    switch (cmd)
    {
        case Command::audio:      return QString::fromUtf8(commands["audio"].GetString()).simplified();
        case Command::video:      return QString::fromUtf8(commands["video"].GetString()).simplified();
        case Command::mod:        return QString::fromUtf8(commands["mod"].GetString()).simplified();
        case Command::all:        return QString::fromUtf8(commands["all"].GetString()).simplified();
        case Command::search:     return QString::fromUtf8(commands["search"].GetString()).simplified();
        case Command::random:     return QString::fromUtf8(commands["random"].GetString()).simplified();
        case Command::shuffle:    return QString::fromUtf8(commands["shuffle"].GetString()).simplified();
        case Command::repeat:     return QString::fromUtf8(commands["repeat"].GetString()).simplified();
        case Command::history:    return QString::fromUtf8(commands["history"].GetString()).simplified();
        case Command::statistics: return QString::fromUtf8(commands["statistics"].GetString()).simplified();
        case Command::rescan:     return QString::fromUtf8(commands["rescan"].GetString()).simplified();
        case Command::playlist:   return QString::fromUtf8(commands["playlist"].GetString()).simplified();
        case Command::exit:       return QString::fromUtf8(commands["exit"].GetString()).simplified();
        case Command::clear:      return QString::fromUtf8(commands["clear"].GetString()).simplified();
        case Command::config:     return QString::fromUtf8(commands["config"].GetString()).simplified();
    }
    return QString();
}

const QString ConfigManager::GetDefaultCommand() const
{
    const auto &defcmd = JSONDOC["commands"]["_defaultcommand"];
    return QString::fromUtf8(defcmd.GetString()).simplified();
}

const QString ConfigManager::GetFilterCommand(const MediaType &type) const
{
    const auto &filtercmds = JSONDOC["commands"]["filter_commands"];
    switch (type)
    {
        case AUDIO: return QString::fromUtf8(filtercmds["audio"].GetString()).simplified();
        case VIDEO: return QString::fromUtf8(filtercmds["video"].GetString()).simplified();
        case MOD:   return QString::fromUtf8(filtercmds["mod"].GetString()).simplified();
        case NONE: break;
    }
    return QString();
}

const QString ConfigManager::GetPlaylistFileLoadCommand() const
{
    const auto &file_load = JSONDOC["commands"]["playlist_commands"]["file_load"];
    return QString::fromUtf8(file_load.GetString()).simplified();
}

const QString ConfigManager::GetPlaylistCrossfadeCommand() const
{
    const auto &crossfade = JSONDOC["commands"]["playlist_commands"]["crossfade"];
    return QString::fromUtf8(crossfade.GetString()).simplified();
}

const QByteArray ConfigManager::GetLibraryRootPath(bool expand) const
{
    const auto &path = JSONDOC["library"]["rootpath"];
    return expand ? ExpandEnvironment::ExpandEnvironmentVariables(path.GetString()) :
                    QByteArray(path.GetString());
}

void ConfigManager::SetLibraryRootPath(const QByteArray &rootpath)
{
    JSONDOC["library"]["rootpath"].SetString(rootpath.constData(), rootpath.size(), JSONDOC.GetAllocator());
}

const StringList ConfigManager::GetFiletypes(const MediaType &type) const
{
    const rapidjson::Value *filters = nullptr;

    switch (type)
    {
        case AUDIO: filters = rapidjson::GetValueByPointer(JSONDOC, "/library/audioformats"); break;
        case VIDEO: filters = rapidjson::GetValueByPointer(JSONDOC, "/library/videoformats"); break;
        case MOD:   filters = rapidjson::GetValueByPointer(JSONDOC, "/library/moduleformats"); break;
        case NONE: break;
    }

    if (filters)
    {
        if (filters->Empty())
            return {};

        StringList f;
        for (auto&& ext : filters->GetArray())
            f.append(ext.GetString());
        return f;
    }

    return {};
}

const QByteArrayList ConfigManager::GetLibraryPlaylistPaths() const
{
    const auto &paths = JSONDOC["library"]["playlist_paths"];
    QByteArrayList p;
    for (auto&& path : paths.GetArray())
        p.append(ExpandEnvironment::ExpandEnvironmentVariables(
                     path.GetString()));
    return p;
}

const QByteArrayList ConfigManager::GetPrefixDeletionPatterns() const
{
    const auto &patterns = JSONDOC["library"]["prefixdeletionpatterns"];
    QByteArrayList p;
    for (auto&& pattern : patterns.GetArray())
        p.append(ExpandEnvironment::ExpandEnvironmentVariables(
                     pattern.GetString()));
    return p;
}

const QByteArrayList ConfigManager::GetMoveToBottomPhrases() const
{
    const auto &phrases = JSONDOC["library"]["movetobottomphrases"];
    QByteArrayList p;
    for (auto&& ph : phrases.GetArray())
        p.append(ph.GetString());
    return p;
}

const QStringList ConfigManager::GetMetadataTitleSplitList() const
{
    const auto &list = JSONDOC["prompt"]["metadata_title_split"];
    QStringList l;
    for (auto&& i : list.GetArray())
        l.append(QString::fromUtf8(i.GetString()));
    return l;
}

quint64 ConfigManager::GetRandomizerHistorySize() const
{
    const auto &randhistsize = JSONDOC["randomizer"]["historysize"];
    return randhistsize.GetUint64();
}

void ConfigManager::SetRandomizerHistorySize(const quint64 &historysize)
{
    JSONDOC["randomizer"]["historysize"].SetUint64(historysize);
}

const AppearanceSettings ConfigManager::GetAppearanceSettings() const
{
    const auto &appearance = JSONDOC["appearance"].GetObject();

    AppearanceSettings settings;
    for (auto&& keyR : appearance)
    {
        const auto &key = String(keyR.name.GetString());
        const auto &value = String(keyR.value.GetString());
        const auto &evaled = TerminalEscapeSequences::Eval(value);

             if (key == "artist")       settings.artist = evaled;
        else if (key == "album")        settings.album = evaled;
        else if (key == "albmartist")   settings.albmartist = evaled;
        else if (key == "title")        settings.title = evaled;
        else if (key == "genre")        settings.genre = evaled;
        else if (key == "track")        settings.track = evaled;
        else if (key == "total_tracks") settings.total_tracks = evaled;
        else if (key == "extension")    settings.extension = evaled;
        else if (key == "path")         settings.path = evaled;
        else if (key == "print_tagged") settings.print_tagged = evaled;
        else if (key == "print_plain")  settings.print_plain = evaled;
    }
    return settings;
}

const QByteArray &ConfigManager::GetConfigDirectory()
{
    static const QByteArray _configDirectory = ([]{
        return QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation).toUtf8();
    })();
    return _configDirectory;
}

const QByteArray &ConfigManager::GetCacheDirectory()
{
    static const QByteArray _cacheDirectory = ([]{
        return GetConfigDirectory() + "/cache";
    })();
    return _cacheDirectory;
}

const QByteArray &ConfigManager::GetSettingsFileLocation()
{
    static const QByteArray _settingsFileLoc = ([]{
        return GetConfigDirectory() + "/settings.json";
    })();
    return _settingsFileLoc;
}

const QByteArray &ConfigManager::GetPlayerConfigFileLocation()
{
    static const QByteArray _playerConfigFileLoc = ([]{
        return GetConfigDirectory() + "/players.json";
    })();
    return _playerConfigFileLoc;
}

const QList<Player> ConfigManager::GetPlayers() const
{
    const auto &player = JSONDOC["player"].GetObject();

    QList<Player> players;
    for (auto&& fmtR : player)
    {
        const auto &fmt = String(fmtR.name.GetString());
        const auto &current = player[*fmt].GetObject();

        Player pl;

        if (fmt == "audioplayer")
            pl.fmt = AUDIO;
        else if (fmt == "videoplayer")
            pl.fmt = VIDEO;
        else if (fmt == "modplayer")
            pl.fmt = MOD;
        else
            pl.fmt = NONE;

        pl.cmd = String(current["command"].GetString());
        for (auto&& arg : current["arguments"].GetArray())
            pl.args.append(String(arg.GetString()));
        players.append(pl);
    }
    return players;
}

const QList<UserPlayer> ConfigManager::GetUserPlayers() const
{
    if (ConfigManagerInternal::rapidjson_players)
    {
        QList<UserPlayer> players;
        for (auto&& extR : JSONDOC_PLAYERS.GetObject())
        {
            const auto &ext = String(extR.name.GetString());
            const auto &current = JSONDOC_PLAYERS[*ext].GetObject();

            UserPlayer player;
            player.fmt = ext;
            player.cmd = String(current["command"].GetString());
            for (auto&& arg : current["arguments"].GetArray())
                player.args.append(String(arg.GetString()));
            players.append(player);
        }
        return players;
    }
    return {};
}

ConfigManager::ConfigFileStatus ConfigManager::ProbeForConfigFile()
{
    QFileInfo i(GetSettingsFileLocation());
    if (i.exists() && i.isFile() && i.isReadable() && i.isWritable())
    {
        return OK;
    }
    else if (i.exists() && i.isFile())
    {
        LOG_ERROR("ConfigManager: Config file is not accessible. Fix your config file permissions.");
        return Permission;
    }
    else if (i.isDir())
    {
        LOG_ERROR("ConfigManager: You config file seems to be a directory 🤔");
        return IsDirectory;
    }
    else
    {
        return Missing;
    }
}

void ConfigManager::InitDefaultConfiguration(bool save)
{
    // load default configuration into RapidJSON document
    ConfigManagerInternal::rapidjson_settings = std::unique_ptr<rapidjson::Document>(new rapidjson::Document());
    QFile default_settings(":/settings_default.json");
    default_settings.open(QIODevice::ReadOnly | QIODevice::Text);
    ConfigManagerInternal::rapidjson_settings->Parse(default_settings.readAll().constData());
    default_settings.close();

    LOG_NOTICE("ConfigManager: default settings initialized.");

    // save to disk
    if (save)
        SaveConfigToDiskInternal();
}

void ConfigManager::ValidateConfiguration()
{
    // don't try to validate a null pointer
    if (!ConfigManagerInternal::rapidjson_settings)
        return;

    auto configHasErrors = false;

    // Load embedded JSON validation schema file
    // https://easy-json-schema.github.io/
    rapidjson::ParseResult res;
    static const rapidjson::Document schemaDoc = ([](rapidjson::ParseResult *res){
        QFile json(":/validation_schema.json");
        json.open(QIODevice::ReadOnly | QIODevice::Text);
        rapidjson::Document d;
        rapidjson::ParseResult r = d.Parse(json.readAll().constData());
        *res = r;
        return d;
    })(&res);

    if (res)
    {
        try {
            // Parse JSON validation schema
            Schema schema;
            SchemaParser parser;
            RapidJsonAdapter rapidSchemaAdapter(schemaDoc);
            parser.populateSchema(rapidSchemaAdapter, schema);

            // Validate loaded user settings
            Validator validator;
            ValidationResults results;
            RapidJsonAdapter rapidTargetAdapter(JSONDOC);
            if (!validator.validate(schema, rapidTargetAdapter, &results))
            {
                LOG_CRITICAL("ConfigManager: Configuration file validation failed!");
                configHasErrors = true;

                // Iterate over all errors to help the user finding the faulty parts
                ValidationResults::Error error;
                while (results.popError(error))
                {
                    LOG_ERROR("%s: %s", error.context, error.description);

                    // obtain key name from error message
                    // keep code for later
                    //   const auto &extractKeyName = QRegularExpression("'(.*?)'");
                    //   const auto &res = extractKeyName.match(error.description.c_str());
                    //   const auto &keyName = res.captured(1);
                }
            }
        } catch (...) {
            configHasErrors = true;
            LOG_FATAL("valijson: JSON schema file is not valid!");
        }
    }
    else
    {
        configHasErrors = true;
        LOG_FATAL("valijson: JSON schema file is not valid!: %s (offset=%u)",
                  rapidjson::GetParseError_En(res.Code()), res.Offset());
    }

    // temporarily use the default config and tell the user to fix the file
    if (configHasErrors)
    {
        LOG_NOTICE("ConfigManager: config file was not loaded due to errors.\n"
                   "Temporarily using the default configuration. Consider fixing your config file.");
        InitDefaultConfiguration(false);
    }
}

void ConfigManager::ValidatePlayerConfiguration()
{
    bool valid = true;
    for (auto&& keyR : JSONDOC_PLAYERS.GetObject())
    {
        const auto &key = QString::fromUtf8(keyR.name.GetString());
        const auto &player = JSONDOC_PLAYERS[key.toUtf8().constData()];

        // check if PLAYER object is an actual object
        if (!player.IsObject())
        {
            valid = false;
            break;
        }

        // get all PLAYER keys
        const auto &player_keysR = player.GetObject();
        QStringList player_keys = ([&]{
            QStringList keys;
            for (auto&& keyR : player_keysR)
                keys.append(QString::fromUtf8(keyR.name.GetString()));
            return keys;
        })();

        // check if PLAYER has a "command" key
        if (!(player_keys.contains("command")))
        {
            LOG_ERROR("%s: missing command", key);
            valid = false;
            break;
        }
        // check if the command key is a string
        if (!(player.GetObject()["command"].IsString()))
        {
            LOG_ERROR("%s: field command is not a string", key);
            valid = false;
            break;
        }
        // check if the command is empty
        if (strlen(player.GetObject()["command"].GetString()) == 0)
        {
            LOG_ERROR("%s: command must not be empty.", key);
            valid = false;
            break;
        }

        // check if PLAYER has an "arguments" key
        if (!(player_keys.contains("arguments")))
        {
            LOG_ERROR("%s: missing arguments. can be empty but must be present.", key);
            valid = false;
            break;
        }
        // check if the arguments key is an array
        if (!(player.GetObject()["arguments"].IsArray()))
        {
            LOG_ERROR("%s: field arguments is not an array", key);
            valid = false;
            break;
        }
        // check if all argument values are strings
        const auto &args = player.GetObject()["arguments"].GetArray();
        QStringList arguments;
        for (auto i = 0U; i < args.Size(); i++)
        {
            if (!args[i].IsString())
            {
                LOG_ERROR("%s: argument @pos=%i is not a string. all args must be a string.", key, i);
                valid = false;
                break;
            }
            arguments.append(QString::fromUtf8(args[i].GetString()));
        }
    }

    if (!valid)
    {
        LOG_FATAL("ConfigManager: players.json is not valid, must be in the format of "
                  "\"player.*\" (\"command\": string, \"arguments\": []) with file extension as object name.");
        ConfigManagerInternal::rapidjson_players.reset();
    }
}

bool ConfigManager::LoadConfigFromDiskInternal()
{
    QByteArray val;
    QFile file(GetSettingsFileLocation());
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        val = file.readAll();
        file.close();

        // load configuration into RapidJSON document
        ConfigManagerInternal::rapidjson_settings = std::unique_ptr<rapidjson::Document>(new rapidjson::Document());
        rapidjson::ParseResult res = ConfigManagerInternal::rapidjson_settings->Parse(val.constData());
        if (!res)
            LOG_ERROR("settings.json: %s (offset=%u)",
                      rapidjson::GetParseError_En(res.Code()), res.Offset());

        ValidateConfiguration();
        return true;
    }
    return false;
}

bool ConfigManager::LoadPlayerConfigFromDiskInternal()
{
    QByteArray val;
    QFile file(GetPlayerConfigFileLocation());
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        val = file.readAll();
        file.close();

        ConfigManagerInternal::rapidjson_players = std::unique_ptr<rapidjson::Document>(new rapidjson::Document());
        rapidjson::ParseResult res = ConfigManagerInternal::rapidjson_players->Parse(val.constData());
        if (!res)
        {
            if (res.Code() != rapidjson::kParseErrorDocumentEmpty)
                LOG_ERROR("players.json: %s (offset=%u)",
                          rapidjson::GetParseError_En(res.Code()), res.Offset());
            ConfigManagerInternal::rapidjson_players.reset();
            return false;
        }

        ValidatePlayerConfiguration();
        return true;
    }
    return false;
}

bool ConfigManager::SaveConfigToDiskInternal()
{
    // FIXME: handle permission and write errors

    // Write JSON to file
    std::ofstream stream(GetSettingsFileLocation().constData());
    rapidjson::OStreamWrapper wrapper(stream);
    rapidjson::PrettyWriter<rapidjson::OStreamWrapper> writer(wrapper);
    JSONDOC.Accept(writer);
    return true;
}
