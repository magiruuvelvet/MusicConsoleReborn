#include "Command.hpp"

#include <Core/ConfigManager.hpp>

extern const QStringList QByteArrayList2QStringList(const QByteArrayList &list);

Command::Command(const QString &name)
    : CommandBase()
{
    _name = name;
    _command = name;
}

Command::~Command()
{
    _name.clear();
}

void Command::setArguments(const QString &input)
{
    this->setDataWithoutCommand(input.toUtf8());
    _command = _name;
}

void Command::setArguments(const QByteArrayList &args)
{
    _arguments = QByteArrayList2QStringList(args);
}

MediaType Command::ApplyFilter() const
{
    if (this->hasArgs())
    {
        const auto &filter_cmd = this->arguments().at(0);
        if (filter_cmd.compare(Config()->GetFilterCommand(AUDIO)) == 0)
        {
            _arguments.removeFirst();
            return AUDIO;
        }
        else if (filter_cmd.compare(Config()->GetFilterCommand(VIDEO)) == 0)
        {
            _arguments.removeFirst();
            return VIDEO;
        }
        else if (filter_cmd.compare(Config()->GetFilterCommand(MOD)) == 0)
        {
            _arguments.removeFirst();
            return MOD;
        }
    }

    return NONE;
}

void Command::shift() const
{
    if (this->hasArgs())
        _arguments.removeFirst();
}
