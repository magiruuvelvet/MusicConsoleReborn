#ifndef COMMANDABSTRACTPRIVATE_HPP
#define COMMANDABSTRACTPRIVATE_HPP

#include <QByteArrayList>

class CommandAbstractPrivate final
{
    friend class CommandBase;
    friend class CommandParseHelper;

    CommandAbstractPrivate() = delete;

private:
    static const QByteArrayList ParseCommandString(const char *input);
    static inline const QByteArrayList ParseCommandString(const QByteArray &input)
    { return ParseCommandString(input.constData()); }
};

#endif // COMMANDABSTRACTPRIVATE_HPP
