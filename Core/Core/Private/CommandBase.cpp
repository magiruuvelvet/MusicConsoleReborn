#include "CommandBase.hpp"

#include "CommandAbstractPrivate.hpp"

const QStringList QByteArrayList2QStringList(const QByteArrayList &list)
{
    QStringList strlist;
    for (auto&& str : list)
        strlist.append(str);
    return strlist;
}

CommandBase::CommandBase()
{
}

CommandBase::~CommandBase()
{
    _command.clear();
    _arguments.clear();
}

void CommandBase::setData(const QByteArray &data)
{
    const auto &_data = CommandAbstractPrivate::ParseCommandString(data);
    setDataInternal(_data);
}

void CommandBase::setData(const char *data)
{
    const auto &_data = CommandAbstractPrivate::ParseCommandString(data);
    setDataInternal(_data);
}

void CommandBase::setDataWithoutCommand(const QByteArray &data)
{
    const auto &_data = CommandAbstractPrivate::ParseCommandString(data);
    setDataWithoutCommandInternal(_data);
}

void CommandBase::setDataWithoutCommand(const char *data)
{
    const auto &_data = CommandAbstractPrivate::ParseCommandString(data);
    setDataWithoutCommandInternal(_data);
}

void CommandBase::setDataInternal(const QByteArrayList &data)
{
    _command.clear();
    _arguments.clear();

    if (data.size() > 0)
        _command = QString::fromUtf8(data.at(0));
    if (data.size() > 1)
        _arguments = QByteArrayList2QStringList(data.mid(1));
}

void CommandBase::setDataWithoutCommandInternal(const QByteArrayList &data)
{
    _command.clear();
    _arguments.clear();

    if (data.size() > 0)
        _arguments = QByteArrayList2QStringList(data);
}
