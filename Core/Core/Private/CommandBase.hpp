#ifndef COMMANDBASE_HPP
#define COMMANDBASE_HPP

#include <QString>
#include <QStringList>

class CommandBase
{
    friend class Command;
    friend class CommandParseHelper;

public:
    CommandBase();
    virtual ~CommandBase();

    inline const QString &command() const
    { return _command; }
    inline const QStringList &arguments() const
    { return _arguments; }

    // checks if the command is empty
    inline bool isEmpty() const
    { return _command.isEmpty(); }

    // checks if the command has arguments
    inline bool hasArgs() const
    { return !_arguments.isEmpty(); }

protected:
    void setData(const QByteArray &data);
    void setData(const char *data);

    void setDataWithoutCommand(const QByteArray &data);
    void setDataWithoutCommand(const char *data);

private:
    void setDataInternal(const QByteArrayList &data);
    void setDataWithoutCommandInternal(const QByteArrayList &data);
    QString _command;
    mutable QStringList _arguments;
};

#endif // COMMANDBASE_HPP
