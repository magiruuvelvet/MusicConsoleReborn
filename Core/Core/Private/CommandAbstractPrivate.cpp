#include "CommandAbstractPrivate.hpp"

#include <cstring>

extern "C" {
    #include "3rdParty/nargv/nargv.h"
}

const QByteArrayList CommandAbstractPrivate::ParseCommandString(const char *input)
{
    // don't pass through empty data to silence some errors
    if (!input || std::strcmp(input, "") == 0)
        return QByteArrayList();

    // parse user input using nargv
    auto *ret = nargv_parse(input);

    // parse error
    if (ret->error_code)
    {
        std::fprintf(stderr, "nargv parse error: %i: %s: at input column %i\n",
                    ret->error_code, ret->error_message, ret->error_index);

        // free and return empty list
        nargv_free(ret);
        return QByteArrayList();
    }

    // convert parsed arguments to Qt types
    QByteArrayList args;
    for (auto i = 0; i < ret->argc; i++)
            args.append(QByteArray(ret->argv[i]));

    // free nargv reserved memory
    nargv_free(ret);

    // return Qt data
    return args;
}
