#include "CommandParseHelper.hpp"
#include "Private/CommandAbstractPrivate.hpp"

const CommandBase CommandParseHelper::Parse(const QString &input)
{
    CommandBase cmd;
    cmd.setData(input.toUtf8());
    return cmd;
}

const QByteArrayList CommandParseHelper::ParseSimple(const QByteArray &input)
{
    return CommandAbstractPrivate::ParseCommandString(input);
}
