#ifndef LOGGER_HPP
#define LOGGER_HPP

#include <iostream>
#include <vector>
#include <string>

#include <QString>
#include <QStringList>
#include <QByteArray>
#include <QFileInfo>
#include <QDir>

#include <Types/StrFmt.hpp>

#include <Core/ConfigManager.hpp>
#include <Core/Private/CommandBase.hpp>
#include <Core/Command.hpp>
#include <Media/MediaFile.hpp>
#include <Media/Cache/MediaCacheObject.hpp>
#include <Media/PlayerController.hpp>
#include <Media/Playlist/PlaylistParser.hpp>
#include <Utils/SearchTerms.hpp>
#include <Types/StringUtils.hpp>
#include <Types/String.hpp>
#include <Types/UnicodeString.hpp>
#include <Types/FileEntry.hpp>

// taken from this code, won't work in this file for some reason so define a macro here
//   static SAFE_BUFFERS FORCE_INLINE const T& get_object(u64 arg)
//   { return *reinterpret_cast<const T*>(static_cast<std::uintptr_t>(arg)); }
#define __strfmt_get_object__(T) (*reinterpret_cast<const T*>(static_cast<std::uintptr_t>(arg)))

// add support for <QString> to "StrFmt", format == %s
template <>
inline void fmt_class_string<QString>::format(std::string& out, u64 arg)
{ out += __strfmt_get_object__(QString).toUtf8().constData(); }

// add support for <QByteArray> to "StrFmt", format == %s
template <>
inline void fmt_class_string<QByteArray>::format(std::string& out, u64 arg)
{ out += __strfmt_get_object__(QByteArray).constData(); }

// add support for <String> to "StrFmt", format == %s
template <>
inline void fmt_class_string<String>::format(std::string& out, u64 arg)
{ out += __strfmt_get_object__(String).to_std_string(); }

// add support for <UnicodeString> to "StrFmt", format == %s
template <>
inline void fmt_class_string<UnicodeString>::format(std::string& out, u64 arg)
{ out += __strfmt_get_object__(UnicodeString).to_utf8(); }

// add support for <Char> to "StrFmt", format == %s
template <>
inline void fmt_class_string<Char>::format(std::string& out, u64 arg)
{ out += __strfmt_get_object__(Char).utf8(); }

// add support for <QDir> to "StrFmt", format == %s
template <>
inline void fmt_class_string<QDir>::format(std::string& out, u64 arg)
{ out += __strfmt_get_object__(QDir).path().toUtf8().constData(); }

// generic list formatting helper macro
//  > avoid code redundancy, the implementation itself must be inline though
#define __strfmt_list_impl__(T, EMPTY_CHECK, FMT, SURROUNDING) \
    const auto &list = __strfmt_get_object__(T); \
    if (list.EMPTY_CHECK) \
    { \
        out += "()"; \
        return; \
    } \
    \
    out += '('; \
    for (auto&& item : list) \
        out += SURROUNDING + fmt::format(FMT, item) + SURROUNDING + ", "; \
    out.replace(out.size() - 2, 3, ""); \
    out += ')'

#define __strfmt_list_ptr_impl__(T, EMPTY_CHECK, FMT, SURROUNDING) \
    const auto &list = __strfmt_get_object__(T); \
    if (list.EMPTY_CHECK) \
    { \
        out += "()"; \
        return; \
    } \
    \
    out += '('; \
    for (auto&& item : list) \
        out += SURROUNDING + fmt::format(FMT, *item) + SURROUNDING + ", "; \
    out.replace(out.size() - 2, 3, ""); \
    out += ')'

#define strfmt_create_list_impl(T, EMPTY_CHECK, FMT, SURROUNDING) \
    template <> inline void fmt_class_string<T>::format(std::string& out, u64 arg) \
    { __strfmt_list_impl__(T, EMPTY_CHECK, FMT, SURROUNDING); } \
    template <> inline void fmt_class_string<const T>::format(std::string& out, u64 arg) \
    { __strfmt_list_impl__(T, EMPTY_CHECK, FMT, SURROUNDING); }

#define strfmt_create_list_ptr_impl(T, EMPTY_CHECK, FMT, SURROUNDING) \
    template <> inline void fmt_class_string<T>::format(std::string& out, u64 arg) \
    { __strfmt_list_ptr_impl__(T, EMPTY_CHECK, FMT, SURROUNDING); } \
    template <> inline void fmt_class_string<const T>::format(std::string& out, u64 arg) \
    { __strfmt_list_ptr_impl__(T, EMPTY_CHECK, FMT, SURROUNDING); }

#define strfmt_create_list_impl_tmpl(T, T2, EMPTY_CHECK, FMT, SURROUNDING) \
    template <> inline void fmt_class_string<T<T2>>::format(std::string& out, u64 arg) \
    { __strfmt_list_impl__(T<T2>, EMPTY_CHECK, FMT, SURROUNDING); } \
    /*template <> inline void fmt_class_string<const T<T2>>::format(std::string& out, u64 arg)*/ \
    /*{ __strfmt_list_impl__(const T<T2>, EMPTY_CHECK, FMT, SURROUNDING); }*/ \
    template <> inline void fmt_class_string<T<const T2>>::format(std::string& out, u64 arg) \
    { __strfmt_list_impl__(T<const T2>, EMPTY_CHECK, FMT, SURROUNDING); } \
    /*template <> inline void fmt_class_string<const T<const T2>>::format(std::string& out, u64 arg)*/ \
    /*{ __strfmt_list_impl__(const T<const T2>, EMPTY_CHECK, FMT, SURROUNDING); }*/ \
    template <> inline void fmt_class_string<T<T2*>>::format(std::string& out, u64 arg) \
    { __strfmt_list_ptr_impl__(T<T2*>, EMPTY_CHECK, FMT, SURROUNDING); } \
    /*template <> inline void fmt_class_string<const T<T2*>>::format(std::string& out, u64 arg)*/ \
    /*{ __strfmt_list_ptr_impl__(const T<T2*>, EMPTY_CHECK, FMT, SURROUNDING); }*/ \
    template <> inline void fmt_class_string<T<const T2*>>::format(std::string& out, u64 arg) \
    { __strfmt_list_ptr_impl__(T<const T2*>, EMPTY_CHECK, FMT, SURROUNDING); } \
    /*template <> inline void fmt_class_string<const T<const T2*>>::format(std::string& out, u64 arg)*/ \
    /*{ __strfmt_list_ptr_impl__(const T<const T2*>, EMPTY_CHECK, FMT, SURROUNDING); }*/

// add support for <std::vector<Char::utf16_t>> lists to "StrFmt", format == %s
strfmt_create_list_impl(std::vector<Char::utf16_t>, empty(), "%i", "")

// add support for <QStringList> lists to "StrFmt", format == %s
strfmt_create_list_impl(QStringList, isEmpty(), "%s", '"')

// add support for <QList<QByteArray>> lists to "StrFmt", format == %s
strfmt_create_list_impl(QList<QByteArray>, isEmpty(), "%s", '"')

// add support for <QList<char*> lists to "StrFmt", format == %s
strfmt_create_list_impl_tmpl(QList, char*, isEmpty(), "%s", '"')

// add support for <std::vector<char*>> lists to "StrFmt", format == %s
strfmt_create_list_impl_tmpl(std::vector, char*, empty(), "%s", '"')

// add support for <std::vector<std::string>> lists to "StrFmt", format == %s
strfmt_create_list_impl(std::vector<std::string>, empty(), "%s", '"')

// add support for <StringVector> lists to "StrFmt", format == %s
strfmt_create_list_impl(StringVector, empty(), "%s", '"')

// add support for <StringList> lists to "StrFmt", format == %s
strfmt_create_list_impl(StringList, empty(), "%s", '"')

// add support for <UnicodeStringVector> lists to "StrFmt", format == %s
strfmt_create_list_impl(UnicodeStringVector, empty(), "%s", '"')

// add support for <UnicodeStringList> lists to "StrFmt", format == %s
strfmt_create_list_impl(UnicodeStringList, empty(), "%s", '"')

// add support for <QList<int>> lists to "StrFmt", format == %s
strfmt_create_list_impl(QList<int>, isEmpty(), "%i", "")

// add support for <Vector<int>> lists to "StrFmt", format == %s
strfmt_create_list_impl(Vector<int>, empty(), "%i", "")

// add support for <QList<const MediaFile*>> to "StrFmt", format == %s
template <>
inline void fmt_class_string<QList<const MediaFile*>>::format(std::string& out, u64 arg)
{ __strfmt_list_ptr_impl__(QList<const MediaFile*>, isEmpty(), "%s", '"'); }

// add support for <QRegularExpression> to "StrFmt", format == %s
template <>
inline void fmt_class_string<QRegularExpression>::format(std::string& out, u64 arg)
{
    // simplified RegEx output, just the pattern
    const auto &regex = __strfmt_get_object__(QRegularExpression);
    out += fmt::format("QRegularExpression(\"%s\")", regex.pattern());
}
template <>
inline void fmt_class_string<QList<QRegularExpression>>::format(std::string& out, u64 arg)
{ __strfmt_list_impl__(QList<QRegularExpression>, isEmpty(), "%s", ""); }

// add support for <RegularExpression> to "StrFmt", format == %s
template <>
inline void fmt_class_string<RegularExpression>::format(std::string& out, u64 arg)
{
    // simplified RegEx output, just the pattern
    const auto &regex = __strfmt_get_object__(RegularExpression);
    out += fmt::format("RegularExpression(\"%s\")", regex.pattern());
}
template <>
inline void fmt_class_string<List<RegularExpression>>::format(std::string& out, u64 arg)
{ __strfmt_list_impl__(List<RegularExpression>, empty(), "%s", ""); }

// add support for <Player> struct and QList<Player> to "StrFmt", format == %s
template <>
inline void fmt_class_string<Player>::format(std::string& out, u64 arg)
{
    const auto &player = __strfmt_get_object__(Player);
    out += fmt::format("Player[fmt=%s, command=\"%s\", arguments=%s]",
                       player.fmt, player.cmd, player.args);
}
strfmt_create_list_impl(QList<Player>, isEmpty(), "%s", "");

// add support for <UserPlayer> struct and QList<UserPlayer> to "StrFmt", format == %s
template <>
inline void fmt_class_string<UserPlayer>::format(std::string& out, u64 arg)
{
    const auto &player = __strfmt_get_object__(UserPlayer);
    out += fmt::format("UserPlayer[fmt=%s, command=\"%s\", arguments=%s]",
                       player.fmt, player.cmd, player.args);
}
strfmt_create_list_impl(QList<UserPlayer>, isEmpty(), "%s", "")

// add support for <CommandBase> to "StrFmt", format == %s
template <>
inline void fmt_class_string<CommandBase>::format(std::string& out, u64 arg)
{
    const auto &cmd = __strfmt_get_object__(CommandBase);
    out += "Command{";
    if (cmd.isEmpty())
    {
        out += "empty}";
        return;
    }
    else
    {
        out += '"' + fmt::format("%s", cmd.command()) + '"';
    }

    if (cmd.hasArgs())
    {
        out += " args[" + std::to_string(cmd.arguments().size()) + "]=";
        out += fmt::format("%s", cmd.arguments());
        out += "}";
    }
    else
    {
        out += " no args}";
    }
}

// add support for <Command> to "StrFmt", format == %s
template <>
inline void fmt_class_string<Command>::format(std::string& out, u64 arg)
{ out += fmt::format("%s", static_cast<CommandBase>(__strfmt_get_object__(Command))); }

// add support for <ConfigManager::Command> enum to "StrFmt", format == %s
template <>
inline void fmt_class_string<ConfigManager::Command>::format(std::string& out, u64 arg)
{
    using Command = ConfigManager::Command;
    format_enum(out, arg, [](ConfigManager::Command value)
    {
        switch (value)
        {
        STR_CASE(Command::audio);
        STR_CASE(Command::video);
        STR_CASE(Command::mod);
        STR_CASE(Command::all);
        STR_CASE(Command::search);
        STR_CASE(Command::random);
        STR_CASE(Command::shuffle);
        STR_CASE(Command::repeat);
        STR_CASE(Command::history);
        STR_CASE(Command::statistics);
        STR_CASE(Command::rescan);
        STR_CASE(Command::playlist);
        STR_CASE(Command::exit);
        STR_CASE(Command::clear);
        STR_CASE(Command::config);
        }
        return unknown;
    });
}

// add support for <QFileInfo> to "StrFmt", format == %s
template <>
inline void fmt_class_string<QFileInfo>::format(std::string& out, u64 arg)
{
    const auto &finfo = __strfmt_get_object__(QFileInfo);
    out += "QFileInfo(" + fmt::format("%s", finfo.absoluteFilePath()) + ")";
}

// add support for <Metadata> to "StrFmt", format == %s
template <>
inline void fmt_class_string<Metadata>::format(std::string& out, u64 arg)
{
    const auto &mt = __strfmt_get_object__(Metadata);
    out += "Metadata{";
    if (mt.isEmpty())
    {
        out += "empty}";
    }
    else
    {
        out += "artist=\"" +       fmt::format("%s", mt.artist) + "\", ";
        out += "album_artist=\"" + fmt::format("%s", mt.album_artist) + "\", ";
        out += "album=\"" +        fmt::format("%s", mt.album) + "\", ";
        out += "title=\"" +        fmt::format("%s", mt.title) + "\", ";
        out += "genre=\"" +        fmt::format("%s", mt.genre) + "\", ";
        out += "track=\"" +        std::to_string(mt.track) + "\", ";
        out += "total_tracks=\"" + std::to_string(mt.total_tracks) + "\"}";
    }
}

// add support for <MediaType> enum to "StrFmt", format == %s
template <>
inline void fmt_class_string<MediaType>::format(std::string& out, u64 arg)
{
    format_enum(out, arg, [](MediaType value)
    {
        switch (value)
        {
        STR_CASE(NONE);
        STR_CASE(AUDIO);
        STR_CASE(VIDEO);
        STR_CASE(MOD);
        }
        return unknown;
    });
}

// add support for <MediaFile> to "StrFmt", format == %s
template <>
inline void fmt_class_string<MediaFile>::format(std::string& out, u64 arg)
{
    const auto &mf = __strfmt_get_object__(MediaFile);
    out += "MediaFile{";
    out += "path=\"" + fmt::format("%s", mf.fileinfo.absoluteFilePath()) + "\", ";
    out += "type=" + fmt::format("%s", mf.type) + ", ";
    out += "metadata=" + fmt::format("%s", mf.metadata) + ", ";
    out += "search_terms=" + fmt::format("%s", mf.searchTerms) + ", ";
    out += "valid=" + std::string((mf.isValid() ? "true" : "false"));
    out += "}";
}

// add support for <MediaCacheObject> to "StrFmt", format == %s
template <>
inline void fmt_class_string<MediaCacheObject>::format(std::string& out, u64 arg)
{
    const auto &obj = __strfmt_get_object__(MediaCacheObject);
    out += "MediaCacheObject(";
    out += "path=\"%s\", " + fmt::format("%s", obj.cachePath());
    out += "hash=\"%s\"" + fmt::format("%s", obj.hash());
    out += ")";
}

// add support for <SearchTerms> to "StrFmt", format == %s
template <>
inline void fmt_class_string<SearchTerms>::format(std::string& out, u64 arg)
{
    const auto &obj = __strfmt_get_object__(SearchTerms);
    out += "SearchTerms{";
    if (obj.isEmpty())
    {
        out += "empty}";
    }
    else
    {
        out += fmt::format("data=\"%s\", ", obj.data().pattern());
        out += fmt::format("raw=\"%s\", ", obj.rawData());
        out += fmt::format("valid=%s", obj.isValid());
        out += "}";

        const auto &filters = obj.GetAllSubFilters();

        if (!filters.isEmpty())
        {
            out += "(";
            for (auto&& filter : filters)
                out += fmt::format("%s", filter) + ",";
            out.replace(out.size() - 1, 1, ")");
        }
    }
}

// add support for <SearchTerms::Filter> to "StrFmt", format == %s
template <>
inline void fmt_class_string<SearchTerms::Filter>::format(std::string& out, u64 arg)
{
    const auto &filter = __strfmt_get_object__(SearchTerms::Filter);
    out += "Filter(";
    if (filter.isEmpty())
    {
        out += fmt::format("empty, type=%s)", filter.type);
    }
    else
    {
        out += fmt::format("data=\"%s\", ", filter.data.pattern());
        out += fmt::format("raw=\"%s\", ", filter.rawData);
        out += fmt::format("type=%s, ", filter.type);
        out += fmt::format("valid=%s", filter.data.isValid());
        out += ")";
    }
}

// add support for <SearchTerms::FilterType> to "StrFmt", format == %s
template <>
inline void fmt_class_string<SearchTerms::FilterType>::format(std::string& out, u64 arg)
{
    using FilterType = SearchTerms::FilterType;
    format_enum(out, arg, [](FilterType value)
    {
        switch (value)
        {
        STR_CASE(FilterType::MAIN);
        STR_CASE(FilterType::WITHOUT);
        STR_CASE(FilterType::WITHOUT_GENRE);
        }
        return unknown;
    });
}
strfmt_create_list_impl(QList<SearchTerms::FilterType>, isEmpty(), "%s", "")

// add support for <PlaylistParser::ParseResult> enum to "StrFmt", format == %s
template <>
inline void fmt_class_string<PlaylistParser::ParseResult>::format(std::string& out, u64 arg)
{
    using ParseResult = PlaylistParser::ParseResult;
    format_enum(out, arg, [](ParseResult value)
    {
        switch (value)
        {
        STR_CASE(ParseResult::Success);
        STR_CASE(ParseResult::FileNotFound);
        STR_CASE(ParseResult::FileOpenError);
        STR_CASE(ParseResult::NotAPlaylistFile);
        STR_CASE(ParseResult::Unknown);
        STR_CASE(ParseResult::InvalidPointer);
        }
        return unknown;
    });
}

// add support for <Timestamp> to "StrFmt", format == %s
template <>
inline void fmt_class_string<Timestamp>::format(std::string& out, u64 arg)
{
    const auto &ts = __strfmt_get_object__(Timestamp);
    // YYYY-MM-DD HH:MM:SS
    out += fmt::format("Timestamp(%04i-%02i-%02i %02i:%02i:%02i UTC)",
                       ts.year, ts.month, ts.day, ts.hours, ts.minutes, ts.seconds);
}

#include <Utils/TermColor.hpp>

class Logger final
{
private:
    Logger() = delete;

    using tc = TermColor;

#define DECLARE_FRIEND(LOG_CHANNEL) \
    template<typename... Args> \
    friend void LOG_CHANNEL(const char *, const Args&... args);

    DECLARE_FRIEND(LOG_CRITICAL)
    DECLARE_FRIEND(LOG_FATAL)
    DECLARE_FRIEND(LOG_ERROR)
    DECLARE_FRIEND(LOG_WARNING)
    DECLARE_FRIEND(LOG_SUCCESS)
    DECLARE_FRIEND(LOG_NOTICE)
    DECLARE_FRIEND(LOG_HINT)
    DECLARE_FRIEND(LOG_TODO)
    DECLARE_FRIEND(LOG_FIXME)
    DECLARE_FRIEND(LOG_INFO)

#undef DECLARE_FRIEND

    enum LogChannel {
        Critical,
        Fatal,
        Error,
        Warning,
        Success,
        Notice,
        Hint,
        Todo,
        Fixme,
        Info
    };

    enum class fd {
        in  = 0, // unused
        out = 1,
        err = 2,
    };

    template <typename... Args>
    inline static void Print(const LogChannel &c, const char *f, const Args&... args)
    {
        fd _fd = fd::out;
        switch (c)
        {
            case Critical: { std::cout << tc::fg({ 93, 41,102}, tc::Bold); _fd = fd::err; } break; // deep dark violet, bold
            case Fatal:    { std::cout << tc::fg({200,  0, 10}, tc::Bold); _fd = fd::err; } break; // deep red, bold
            case Error:    { std::cout << tc::fg({193, 29, 87});           _fd = fd::err; } break; // slightly pinkish/redish, normal
            case Warning:  { std::cout << tc::fg({145,138, 36}, tc::Bold); _fd = fd::err; } break; // dark yellow, bold
            case Success:  { std::cout << tc::fg({ 41,172, 46});           _fd = fd::out; } break; // green, normal
            case Notice:   { std::cout << tc::fg({ 45,131,185});           _fd = fd::out; } break; // middle blue, normal
            case Hint:     { std::cout << tc::fg({ 22, 91, 24}, tc::Bold); _fd = fd::out; } break; // dark green, bold
            case Todo:     { std::cout << tc::fg({ 33,145,141}, tc::Bold) + "{TODO} ";  _fd = fd::err; } break; // dark cyan, bold
            case Fixme:    { std::cout << tc::fg({214,133, 40}, tc::Bold) + "{FIXME} "; _fd = fd::err; } break; // orange, bold
            case Info:     break;                                         // terminal default
        }

        switch (_fd)
        {
            case fd::out: {
                std::cout << fmt::format(f, args...);
                // reset colors/formatting and flush screen
                std::cout << tc::reset() << std::endl;
            } break;

            case fd::err: {
                std::cerr << fmt::format(f, args...);
                // reset colors/formatting and flush screen
                std::cerr << tc::reset() << std::endl;
            } break;

            case fd::in: break;
        }
    }
};

#define IMPLEMENT_LOG_CHANNEL(NAME, ENUM) \
    template<typename... Args> \
    inline void NAME(const char *f, const Args&... args) \
    { Logger::Print(Logger::ENUM, f, args...); }

IMPLEMENT_LOG_CHANNEL(LOG_CRITICAL, Critical)
IMPLEMENT_LOG_CHANNEL(LOG_FATAL,    Fatal)
IMPLEMENT_LOG_CHANNEL(LOG_ERROR,    Error)
IMPLEMENT_LOG_CHANNEL(LOG_WARNING,  Warning)
IMPLEMENT_LOG_CHANNEL(LOG_SUCCESS,  Success)
IMPLEMENT_LOG_CHANNEL(LOG_NOTICE,   Notice)
IMPLEMENT_LOG_CHANNEL(LOG_HINT,     Hint)
IMPLEMENT_LOG_CHANNEL(LOG_INFO,     Info)

#ifdef MUSICCONSOLE_DEBUG
IMPLEMENT_LOG_CHANNEL(LOG_TODO,     Todo)
IMPLEMENT_LOG_CHANNEL(LOG_FIXME,    Fixme)
#else
#define LOG_TODO(...)
#define LOG_FIXME(...)
#endif

#undef IMPLEMENT_LOG_CHANNEL
#undef IMPLEMENT_DISABLED_LOG_CHANNEL

#endif // LOGGER_HPP
