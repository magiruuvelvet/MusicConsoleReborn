#include "TerminalEscapeSequences.hpp"

const String TerminalEscapeSequences::Eval(const String &s)
{
    String res = s;

    for (size_t i = 0; i < res.size(); i++)
    {
        if (res.at(i) == '\\')
        {
            if (i < res.size())
            {
                const auto &next = res.at(i+1);
                switch (next)
                {
                    // new line
                    case 'n': res.replace(i, 2, "\n"); i--; continue;

                    // tabulator
                    case 't': res.replace(i, 2, "\t"); i--; continue;

                    // x group -> supported: x1b
                    case 'x':
                        if (i+3 < res.size())
                        {
                            if (res.mid(i, 4) == "\\x1b")
                            {
                                res.replace(i, 4, "\x1b");
                                i--;
                            }
                        }
                        continue;

                    // zero group -> supported: single zero \0, \001, \002 (readline specific)
                    case '0':
                        if (i+3 < res.size() && res.at(i+2) == '0')
                        {
                            if (res.mid(i, 4) == "\\001")
                            {
                                res.replace(i, 4, "\001"); // explicitly required this way, otherwise it won't work
                                i--;
                            }
                            else if (res.mid(i, 4) == "\\002")
                            {
                                res.replace(i, 4, "\002");
                                i--;
                            }
                        }
                        else
                        {
                            res.replace(i, 2, "\0");
                            i--;
                        }
                        continue;
                }
            }
        }
    }
    return res;
}
