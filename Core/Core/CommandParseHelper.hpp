#ifndef COMMANDPARSEHELPER_HPP
#define COMMANDPARSEHELPER_HPP

#include "Private/CommandBase.hpp"

class CommandParseHelper final
{
    CommandParseHelper() = delete;

public:
    static const CommandBase Parse(const QString &input);
    static const QByteArrayList ParseSimple(const QByteArray &input);
};

#endif // COMMANDPARSEHELPER_HPP
