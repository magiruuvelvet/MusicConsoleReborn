#include "MusicConsoleReborn.hpp"
#include <Media/MediaLibraryModel.hpp>

#include <Core/Logger.hpp>

#include <unistd.h>
#include <sys/file.h>
#include <cerrno>

namespace {
    static int __argc__ = 0;
}

QProcessEnvironment MusicConsoleReborn::__processEnvironment = QProcessEnvironment::systemEnvironment();
QString MusicConsoleReborn::__applicationDisplayName;
QString MusicConsoleReborn::_pidFileLocation;
bool    MusicConsoleReborn::_isRunning;

std::unique_ptr<MediaLibraryModel> MusicConsoleReborn::_lib =
    std::unique_ptr<MediaLibraryModel>(new MediaLibraryModel());

MusicConsoleReborn::MusicConsoleReborn(const int *argc, const char **argv)
    // shadow command line arguments to disable Qt specific features in the app
    : QCoreApplication(__argc__, nullptr)
{
    // argc*, argv* must be valid; argc must be greater than 1 (without application name)
    if (argc && argv && *argc > 1)
    {
        for (auto i = 1; i < *argc; i++)
        {
            _args.append(argv[i]);
        }
    }

    // allow one release and debug build running at the same time
    //  > use at your own risk, may mess up config or cache
#ifdef MUSICCONSOLE_DEBUG
    _pidFileLocation = "/tmp/" + MusicConsoleReborn::applicationName() + ".debug_pid";
#else
    _pidFileLocation = "/tmp/" + MusicConsoleReborn::applicationName() + ".pid";
#endif

    // create pid file
    int pid_file = open(_pidFileLocation.toUtf8().constData(), O_CREAT | O_RDWR, 0666);
    int rc = flock(pid_file, LOCK_EX | LOCK_NB);
    if (rc)
    {
        if (EWOULDBLOCK == errno)
            _isRunning = true;
    }
    else
    {
        _isRunning = false;
    }
}

MusicConsoleReborn::~MusicConsoleReborn()
{
    _args.clear();
}

bool MusicConsoleReborn::isRunning()
{
    return _isRunning;
}

void MusicConsoleReborn::setApplicationDisplayName(const QString &displayName)
{ __applicationDisplayName = displayName; }
const QString &MusicConsoleReborn::applicationDisplayName()
{ return __applicationDisplayName; }

QProcessEnvironment &MusicConsoleReborn::processEnvironment()
{ return __processEnvironment; }

MediaLibraryModel *MusicConsoleReborn::library()
{ return _lib.get(); }

const QByteArrayList &MusicConsoleReborn::arguments() const
{ return _args; }

void MusicConsoleReborn::emitReady()
{
    emit ready();
}
