#include "String.hpp"

#include "Number.hpp"

#include <algorithm>
#include <sstream>

String::String(const char c, size_type n)
    : std::string()
{
    this->insert(begin(), n, c);
}

String::String(const char *cs)
    : std::string(cs)
{
}

String::String(const std::string &ss)
{
    dynamic_cast<std::string&>(*this) = ss;
}

String &String::operator=(const char *cs)
{
    dynamic_cast<std::string&>(*this) = cs;
    return *this;
}

String &String::operator=(const std::string &ss)
{
    dynamic_cast<std::string&>(*this) = ss;
    return *this;
}

String &String::fill(const char ch)
{
    for (auto&& c : *this)
        c = ch;
    return *this;
}

String &String::fill(const char ch, size_t size)
{
    resize(size, ch);
    return *this;
}

String &String::to_lower()
{
    std::transform(begin(), end(), begin(), [](unsigned char c) {
        return std::tolower(c);
    });
    return *this;
}

String String::lower() const
{
    String s(*this);
    s.to_lower();
    return s;
}

String &String::to_upper()
{
    std::transform(begin(), end(), begin(), [](unsigned char c) {
        return std::toupper(c);
    });
    return *this;
}

String String::upper() const
{
    String s(*this);
    s.to_upper();
    return s;
}

String &String::trim()
{
    this->trim_front();
    this->trim_back();
    return *this;
}

String &String::trim_front()
{
    while (length() > 0)
    {
        if (!is_whitespace(this->front()))
        {
            break;
        }
        this->erase(begin());
    }
    return *this;
}

String &String::trim_back()
{
    while (length() > 0)
    {
        if (!is_whitespace(this->back()))
        {
            break;
        }
        this->erase(--end());
    }
    return *this;
}

String String::trimmed() const
{
    String s(*this);
    return s.trim();
}

String String::trimmed_front() const
{
    String s(*this);
    return s.trim_front();
}

String String::trimmed_back() const
{
    String s(*this);
    return s.trim_back();
}

String &String::simplify()
{
    // do nothing if string is empty
    if (empty())
        return *this;

    // raw pointers of this string
    auto *ssrc = begin().base();
    auto *send = end().base();

    // temporary string (working copy)
    auto res = *this;

    // raw pointers of temporary string
    auto *dst = res.begin().base();
    auto *ptr = dst;

    bool unmodified = true;

    // simplify the temporary string
    while (true)
    {
        while (ssrc != send && is_whitespace(*ssrc))
            ++ssrc;
        while (ssrc != send && !is_whitespace(*ssrc))
            *ptr++ = *ssrc++;
        if (ssrc == send)
            break;
        if (*ssrc != 0x20)
            unmodified = false;
        *ptr++ = 0x20;
    }
    if (ptr != dst && ptr[-1] == 0x20)
        --ptr;

    // calculate new length
    size_type newlen = static_cast<size_type>(ptr - dst);

    // nothing happened, restore original string and return
    if (newlen == size() && unmodified)
    {
        *this = res;
        return *this;
    }

    // resize result and and copy it back into the string
    res.resize(newlen);
    *this = res;
    return *this;
}

String String::simplified() const
{
    String s(*this);
    return s.simplify();
}

bool String::starts_with(const String &ss, CaseSensitivity cs) const
{
    // if the given string is larger than the current string return false
    if (ss.size() > size())
        return false;

    // compare from begin to s.size()
    switch (cs)
    {
        case CaseSensitive: return compare(0, ss.size(), ss) == 0;
        case CaseInsensitive: {
            auto copy = String(*this).lower();
            return copy.compare(0, ss.size(), ss.lower()) == 0;
        }
    }
}

bool String::ends_with(const String &ss, CaseSensitivity cs) const
{
    // if the given string is larger than the current string return false
    if (ss.size() > size())
        return false;

    // compare from the position which matches the s.size() to the end
    switch (cs)
    {
        case CaseSensitive: return compare(size() - ss.size(), ss.size(), ss) == 0;
        case CaseInsensitive: {
            auto copy = String(*this).lower();
            return copy.compare(size() - ss.size(), ss.size(), ss.lower()) == 0;
        }
    }
}

std::size_t String::count(const char c, CaseSensitivity cs) const
{
    std::size_t count = 0;
    switch (cs)
    {
        case CaseSensitive: {
            for (auto&& ch : *this)
                ch == c ? count++ : count;
        } break;

        case CaseInsensitive: {
            auto copy = String(*this).lower();
            auto other = String(c).lower().at(0);
            for (auto&& ch : copy)
                ch == other ? count++ : count;
        } break;
    }

    return count;
}

std::size_t String::count(const String &substr, CaseSensitivity cs) const
{
    std::size_t count = 0;
    switch (cs)
    {
        case CaseSensitive: {
            for (std::size_t i = 0; i < this->size(); i++)
                if (this->substr(i, substr.size()) == substr)
                    count++;
        } break;

        case CaseInsensitive: {
            auto copy = String(*this).lower();
            auto other = substr.lower();
            for (std::size_t i = 0; i < copy.size(); i++)
                if (copy.substr(i, other.size()) == other)
                    count++;
        } break;
    }

    return count;
}

StringVector String::split(const char sep, SplitMode mode) const
{
    std::stringstream ss(*this);
    String item;
    StringVector tokens;
    while (std::getline(ss, item, sep))
    {
        if (mode == KeepEmptyParts)
        {
            tokens.emplace_back(item);
        }
        else if (mode == SkipEmptyParts)
        {
            if (!item.empty())
            {
                tokens.emplace_back(item);
            }
        }
        else
        {
            item.trim();
            if (!item.empty())
            {
                tokens.emplace_back(item);
            }
        }
    }
    return tokens;
}

StringVector String::split(const String &sep, SplitMode mode) const
{
    StringVector tokens;

    size_type start = 0;
    size_type pos = 0; // end
    while ((pos = find(sep, pos)) != String::npos)
    {
        auto len = pos - start;
        String item = substr(start, len);
        if (mode == KeepEmptyParts)
        {
            tokens.emplace_back(item);
        }
        else if (mode == SkipEmptyParts)
        {
            if (!item.empty())
            {
                tokens.emplace_back(item);
            }
        }
        else
        {
            item.trim();
            if (!item.empty())
            {
                tokens.emplace_back(item);
            }
        }
        start += len + sep.size();
        pos += sep.size();
    }

    // add last element
    if (start != size())
    {
        String item = substr(start);
        if (mode == KeepEmptyParts)
        {
            tokens.emplace_back(item);
        }
        else if (mode == SkipEmptyParts)
        {
            if (!item.empty())
            {
                tokens.emplace_back(item);
            }
        }
        else
        {
            item.trim();
            if (!item.empty())
            {
                tokens.emplace_back(item);
            }
        }
    }

    return tokens;
}

StringVector String::split(const RegExp &regex, SplitMode mode) const
{
    try {
        StringVector tokens;
        std::sregex_token_iterator iter(begin(), end(), regex.regex(), -1);
        std::sregex_token_iterator end;

        while (iter != end)
        {
            if (mode == KeepEmptyParts)
                tokens.emplace_back(*iter);
            else if (mode == SkipEmptyParts)
            {
                if (!(iter->str().empty()))
                    tokens.emplace_back(*iter);
            }
            else
            {
                const auto &str = String(iter->str()).trimmed();
                if (!str.empty())
                    tokens.emplace_back(str);
            }
            ++iter;
        }

        return tokens;

    // catch expression errors
    } catch (std::regex_error err) {
        std::printf("String::split(%s): %s\n", regex.pattern().data(), err.what());
        return {};
    }
}

int String::toInt(bool *ok) const
{
    using DataMethod = std::string (String::*)(void) const;
    // template<typename NumberType, ushort base, typename StringType, typename DataMethod>
    try {
        if (ok) *ok = true;
        return Number::toInt<int, 10, String, DataMethod>()(*this, &String::to_std_string);
    } catch (...) {
        if (ok) *ok = false;
        return 0;
    }
}

uint32_t String::toUInt(bool *ok) const
{
    using DataMethod = std::string (String::*)(void) const;
    // template<typename NumberType, ushort base, typename StringType, typename DataMethod>
    try {
        if (ok) *ok = true;
        return Number::toUInt<uint32_t, 10, String, DataMethod>()(*this, &String::to_std_string);
    } catch (...) {
        if (ok) *ok = false;
        return 0;
    }
}

int64_t String::toInt64(bool *ok) const
{
    using DataMethod = std::string (String::*)(void) const;
    // template<typename NumberType, ushort base, typename StringType, typename DataMethod>
    try {
        if (ok) *ok = true;
        return Number::toInt64<int64_t, 10, String, DataMethod>()(*this, &String::to_std_string);
    } catch (...) {
        if (ok) *ok = false;
        return 0;
    }
}

uint64_t String::toULongLong(bool *ok) const
{
    using DataMethod = std::string (String::*)(void) const;
    // template<typename NumberType, ushort base, typename StringType, typename DataMethod>
    try {
        if (ok) *ok = true;
        return Number::toUInt64<uint64_t, 10, String, DataMethod>()(*this, &String::to_std_string);
    } catch (...) {
        if (ok) *ok = false;
        return 0;
    }
}

float String::toFloat(bool *ok) const
{
    using DataMethod = std::string (String::*)(void) const;
    // template<typename NumberType, ushort base, typename StringType, typename DataMethod>
    try {
        if (ok) *ok = true;
        return Number::toFloat<float, String, DataMethod>()(*this, &String::to_std_string);
    } catch (...) {
        if (ok) *ok = false;
        return 0;
    }
}

double String::toDouble(bool *ok) const
{
    using DataMethod = std::string (String::*)(void) const;
    // template<typename NumberType, ushort base, typename StringType, typename DataMethod>
    try {
        if (ok) *ok = true;
        return Number::toDouble<double, String, DataMethod>()(*this, &String::to_std_string);
    } catch (...) {
        if (ok) *ok = false;
        return 0;
    }
}

long double String::toLongDouble(bool *ok) const
{
    using DataMethod = std::string (String::*)(void) const;
    // template<typename NumberType, ushort base, typename StringType, typename DataMethod>
    try {
        if (ok) *ok = true;
        return Number::toLongDouble<long double, String, DataMethod>()(*this, &String::to_std_string);
    } catch (...) {
        if (ok) *ok = false;
        return 0;
    }
}

void String::replace_helper(String &str, const String &from, const String &to)
{
    if (from.empty())
        return;

    size_t start_pos = 0;
    while ((start_pos = str.find(from, start_pos)) != String::npos)
    {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length();
    }
}

bool String::is_whitespace(char ch)
{
    return std::isspace(static_cast<unsigned char>(ch));
}
