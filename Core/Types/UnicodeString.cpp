#include "UnicodeString.hpp"
#include "RegularExpression.hpp"

// helper functions which aren't in ICU
#include "Private/utf8.h"

// number conversion templates
#include "Number.hpp"

// FIXME: try to improve overall performance
//        fix utf16 conversions (don't double convert!!)

UnicodeString::UnicodeString()
{
}

UnicodeString::UnicodeString(const UnicodeString &other)
{
    dd = other.dd;
}

UnicodeString::UnicodeString(const std::vector<Char> &utf32chars)
{
    dd = utf32chars;
}

UnicodeString::UnicodeString(const std::vector<Char::type> &utf32cp)
{
    for (auto&& cp : utf32cp)
        dd.emplace_back(cp);
}

UnicodeString::UnicodeString(const std::string &utf8)
{
    *this = from_utf8(utf8);
}

UnicodeString::UnicodeString(const char *utf8)
{
    *this = from_utf8(utf8);
}

UnicodeString::~UnicodeString()
{
    dd.clear();
}

UnicodeString &UnicodeString::operator= (const UnicodeString &other)
{
    dd = other.dd;
    return *this;
}

UnicodeString &UnicodeString::operator= (const std::vector<Char> &utf32chars)
{
    dd = utf32chars;
    return *this;
}

UnicodeString &UnicodeString::operator= (const std::vector<Char::type> &utf32cp)
{
    dd.clear();
    for (auto&& cp : utf32cp)
        dd.emplace_back(cp);
    return *this;
}

UnicodeString &UnicodeString::operator= (const std::string &utf8)
{
    *this = from_utf8(utf8);
    return *this;
}

UnicodeString &UnicodeString::operator= (const char *utf8)
{
    *this = from_utf8(utf8);
    return *this;
}

bool UnicodeString::compare(const UnicodeString &l, const UnicodeString &r, CaseSensitivity cs)
{
    const auto &left  = (cs == CaseSensitive ? l : l.lower());
    const auto &right = (cs == CaseSensitive ? r : r.lower());
    return left == right;
}

bool UnicodeString::operator< (const UnicodeString &other) const
{
    auto total_self = calculate_char_total();
    auto total_other = other.calculate_char_total();
    return total_self < total_other;
}

bool UnicodeString::operator<= (const UnicodeString &other) const
{
    auto total_self = calculate_char_total();
    auto total_other = other.calculate_char_total();
    return total_self <= total_other;
}

bool UnicodeString::operator> (const UnicodeString &other) const
{
    auto total_self = calculate_char_total();
    auto total_other = other.calculate_char_total();
    return total_self > total_other;
}

bool UnicodeString::operator>= (const UnicodeString &other) const
{
    auto total_self = calculate_char_total();
    auto total_other = other.calculate_char_total();
    return total_self >= total_other;
}

std::string UnicodeString::to_utf8() const
{
    if (dd.empty())
        return {};

    std::string buffer;
    const auto &data = internal_data();
    utf8::utf32to8(data.begin(), data.end(), std::back_inserter(buffer));
    return buffer;
}

std::vector<Char::utf16_t> UnicodeString::to_utf16() const
{
    if (dd.empty())
        return {};

    std::vector<Char::utf16_t> buffer;
    const auto &utf8 = to_utf8();

    utf8::utf8to16(utf8.begin(), utf8.end(), std::back_inserter(buffer));
    return buffer;
}

UnicodeString UnicodeString::from_utf8(const std::string &utf8)
{
    if (utf8.empty())
        return {};

    UnicodeString str;
    std::vector<Char::type> buffer;
    utf8::utf8to32(utf8.begin(), utf8.end(), std::back_inserter(buffer));
    for (auto&& c : buffer)
        str.dd.emplace_back(c);
    return str;
}

UnicodeString UnicodeString::from_utf16(const std::vector<Char::utf16_t> &utf16)
{
    if (utf16.empty())
        return {};

    std::string utf8;
    utf8::utf16to8(utf16.begin(), utf16.end(), std::back_inserter(utf8));
    return from_utf8(utf8);

//    void convert_utf16_to_utf32(const UTF16 *input,
//                                size_t input_size,
//                                UTF32 *output)
//    {
//        const UTF16 * const end = input + input_size;
//        while (input < end) {
//            const UTF16 uc = *input++;
//            if (!is_surrogate(uc)) {
//                *output++ = uc;
//            } else {
//                if (is_high_surrogate(uc) && input < end && is_low_surrogate(*input))
//                    *output++ = surrogate_to_utf32(uc, *input++);
//                else
//                    // ERROR
//            }
//        }
//    }

//    UnicodeString utf32;
//    const auto *input = utf16.data();
//    const auto &input_size = utf16.size();

//    const UChar *const end = input + input_size;
//    while (input < end)
//    {
//        const Char16 uc(*input++);

//        if (uc.is_surrogate())
//        {
//            utf32.append(Char32(uc));
//        }
//        else
//        {
//            if (uc.is_high_surrogate() && input < end && Char16::is_low_surrogate(*input))
//            {
//                utf32.append(Char32::surrogate_to_utf32(uc, *input++));
//            }
//        }
//    }

//    return utf32;
}

UnicodeString UnicodeString::from_utf16(const Char::utf16_t *utf16array, size_t len)
{
    if (len == 0)
        return {};
    return from_utf16(std::vector<Char::utf16_t>(utf16array, utf16array + len));
}

UnicodeString::index UnicodeString::find(const Char &ch, size_t from, CaseSensitivity cs) const
{
    if (from > size())
        return npos;

    if (cs == CaseSensitive)
    {
        return dd.index_of(ch, from);
    }
    else
    {
        const auto &self = this->lower();
        const auto &_ch = ch.to_lowercase();
        return self.dd.index_of(_ch, from);
    }
}

UnicodeString::index UnicodeString::find(const UnicodeString &substr, size_t from, CaseSensitivity cs) const
{
    if (substr.empty() || substr.size() > size() || from > size())
        return npos;

    const auto &self = (cs == CaseSensitive ? *this : this->lower());
    const auto &_substr = (cs == CaseSensitive ? substr : substr.lower());

    // find start index, first char of substring
    auto start = find(substr.front(), from, cs);

    while (start != npos)
    {
        // use compare_helper to check against the sub string
        if (self.compare_helper(start, _substr.size(), _substr, 0, _substr.size()))
            return start;
        start++;

        // find next start position
        start = find(substr.front(), start, cs);
    }
    return npos;
}

UnicodeString::index UnicodeString::find_last(const Char &ch, size_t from, CaseSensitivity cs) const
{
    if (from > size())
        return npos;

    if (cs == CaseSensitive)
    {
        return dd.last_index_of(ch, from);
    }
    else
    {
        const auto &self = this->lower();
        const auto &_ch = ch.to_lowercase();
        return self.dd.last_index_of(_ch, from);
    }
}

UnicodeString::index UnicodeString::find_last(const UnicodeString &substr, size_t from, CaseSensitivity cs) const
{
    if (substr.empty() || substr.size() > size() || from > size())
        return npos;

    const auto &self = (cs == CaseSensitive ? *this : this->lower());
    const auto &_substr = (cs == CaseSensitive ? substr : substr.lower());

    // find start index, first char of substring
    auto start = dd.last_index_of(_substr.front(), from);

    while (start != npos)
    {
        // use compare_helper to check against the sub string
        if (self.compare_helper(start, _substr.size(), _substr, 0, _substr.size()))
            return start;
        start--;

        // find next previous start position
        start = dd.__last_index_of(start, _substr.front());
    }
    return npos;
}

UnicodeString::index UnicodeString::index_of(const RegularExpression &regex, size_t from, RegularExpressionMatch *rmatch) const
{
    if (!regex.isValid()) {
        std::fprintf(stderr, "UnicodeString::index_of: invalid RegularExpression object");
        return -1;
    }

    RegularExpressionMatch match = regex.match(*this, from);
    if (match.hasMatch()) {
        const int ret = match.capturedStart();
        if (rmatch)
            *rmatch = std::move(match);
        return ret;
    }

    return npos;
}

UnicodeString::index UnicodeString::last_index_of(const RegularExpression &regex, size_t from, RegularExpressionMatch *rmatch) const
{
    if (!regex.isValid()) {
        std::fprintf(stderr, "UnicodeString::last_index_of: invalid RegularExpression object\n");
        return -1;
    }

    // glue code (signedness)
    int real_from = (from == 0 ? -1 : from);

    int endpos = (real_from < 0) ? (size() + real_from + 1) : (real_from + 1);
    RegularExpressionMatchIterator iterator = regex.globalMatch(*this);
    int lastIndex = -1;
    while (iterator.hasNext()) {
        RegularExpressionMatch match = iterator.next();
        int start = match.capturedStart();
        if (start < endpos) {
            lastIndex = start;
            if (rmatch)
                *rmatch = std::move(match);
        } else {
            break;
        }
    }

    return lastIndex;
}

UnicodeString &UnicodeString::fill(const Char &ch)
{
    for (auto &&c : dd)
        c = ch;
    return *this;
}

UnicodeString &UnicodeString::fill(const Char &ch, size_t size)
{
    resize(size);
    fill(ch);
    return *this;
}

UnicodeString &UnicodeString::to_lower()
{
    for (auto &&c : dd)
        c.to_lowercase();
    return *this;
}

UnicodeString UnicodeString::lower() const
{
    UnicodeString s(*this);
    return s.to_lower();
}

UnicodeString &UnicodeString::to_upper()
{
    for (auto &&c : dd)
        c.to_uppercase();
    return *this;
}

UnicodeString UnicodeString::upper() const
{
    UnicodeString s(*this);
    return s.to_upper();
}

UnicodeString &UnicodeString::trim()
{
    this->trim_front();
    this->trim_back();
    return *this;
}

UnicodeString &UnicodeString::trim_front()
{
    while (length() > 0)
    {
        if (!this->front().is_space())
        {
            break;
        }
        this->erase(begin());
    }
    return *this;
}

UnicodeString &UnicodeString::trim_back()
{
    while (length() > 0)
    {
        if (!this->back().is_space())
        {
            break;
        }
        this->erase(--end());
    }
    return *this;
}

UnicodeString UnicodeString::trimmed() const
{
    UnicodeString s(*this);
    return s.trim();
}

UnicodeString UnicodeString::trimmed_front() const
{
    UnicodeString s(*this);
    return s.trim_front();
}

UnicodeString UnicodeString::trimmed_back() const
{
    UnicodeString s(*this);
    return s.trim_back();
}

UnicodeString &UnicodeString::simplify()
{
    // do nothing if string is empty
    if (empty())
        return *this;

    // raw pointers of this string
    auto *ssrc = begin().base();
    auto *send = end().base();

    // temporary string (working copy)
    auto res = *this;

    // raw pointers of temporary string
    auto *dst = res.begin().base();
    auto *ptr = dst;

    bool unmodified = true;

    // simplify the temporary string
    while (true)
    {
        while (ssrc != send && ((*ssrc).is_space()))
            ++ssrc;
        while (ssrc != send && !((*ssrc).is_space()))
            *ptr++ = *ssrc++;
        if (ssrc == send)
            break;
        if (*ssrc != Char::Space)
            unmodified = false;
        *ptr++ = Char::Space;
    }
    if (ptr != dst && ptr[-1] == Char::Space)
        --ptr;

    // calculate new length
    size_type newlen = static_cast<size_type>(ptr - dst);

    // nothing happened, restore original string and return
    if (newlen == size() && unmodified)
    {
        *this = res;
        return *this;
    }

    // resize result and and copy it back into the string
    res.resize(newlen);
    *this = res;
    return *this;
}

UnicodeString UnicodeString::simplified() const
{
    UnicodeString s(*this);
    return s.simplify();
}

bool UnicodeString::starts_with(const UnicodeString &ss, CaseSensitivity cs) const
{
    switch (cs)
    {
        case CaseSensitive: {
            return compare_helper(0, ss.length(), ss, 0, ss.length());
        }
        case CaseInsensitive: {
            const auto &self = this->lower();
            const auto &other = ss.lower();
            return self.compare_helper(0, other.length(), other, 0, other.length());
        }
    }
}

bool UnicodeString::ends_with(const UnicodeString &ss, CaseSensitivity cs) const
{
    switch (cs)
    {
        case CaseSensitive: {
            return compare_helper(length() - ss.length(), ss.length(), ss, 0, ss.length());
        }
        case CaseInsensitive: {
            const auto &self = this->lower();
            const auto &other = ss.lower();
            return self.compare_helper(self.length() - other.length(), other.length(), other, 0, other.length());
        }
    }
}

UnicodeStringVector UnicodeString::split(const Char sep, SplitMode mode) const
{
    // TODO
    return {};
}

UnicodeStringVector UnicodeString::split(const UnicodeString &sep, SplitMode mode) const
{
    // TODO
    return {};
}

UnicodeStringVector UnicodeString::split(const RegularExpression &regex, SplitMode mode) const
{
    const auto &res = regex.split(*this);
    if (mode == KeepEmptyParts)
        return res;

    UnicodeStringVector tokens;
    for (auto&& str : res)
    {
        if (mode == SkipEmptyParts)
        {
            if (!str.empty())
                tokens.emplace_back(str);
        }
        else
        {
            const auto &str2 = str.trimmed();
            if (!str2.empty())
                tokens.emplace_back(str2);
        }
    }
    return tokens;
}

UnicodeString &UnicodeString::replace(size_type pos, size_type len, const UnicodeString &after)
{
    // calculate new length
    if (pos != 0)
        len = pos + len;

    // range check
    if (pos > size() || (pos + len) > size())
        throw std::out_of_range("std::out_of_range exception: position and/or length too high");

    // do replacement
    auto left = mid(0, pos);
    auto right = mid(len);
    *this = left + after + right;
    return *this;
}

UnicodeString UnicodeString::mid(size_type pos, index len) const
{
    // calculate new length
    if (len == npos && pos == 0)
        len = size();
    else if (len == npos)
        len = size() - pos;

    // range check
    if (pos > size() || (pos + len) > size())
        throw std::out_of_range("std::out_of_range exception: position and/or length too high");

    // get substring
    UnicodeString sub;
    for (auto i = pos; i < pos+len; i++)
        sub.append(at(i));
    return sub;
}

int UnicodeString::toInt(bool *ok) const
{
    using DataMethod = std::string (UnicodeString::*)(void) const;
    // template<typename NumberType, ushort base, typename StringType, typename DataMethod>
    try {
        if (ok) *ok = true;
        return Number::toInt<int, 10, UnicodeString, DataMethod>()(*this, &UnicodeString::to_utf8);
    } catch (...) {
        if (ok) *ok = false;
        return 0;
    }
}

uint32_t UnicodeString::toUInt(bool *ok) const
{
    using DataMethod = std::string (UnicodeString::*)(void) const;
    // template<typename NumberType, ushort base, typename StringType, typename DataMethod>
    try {
        if (ok) *ok = true;
        return Number::toUInt<uint32_t, 10, UnicodeString, DataMethod>()(*this, &UnicodeString::to_utf8);
    } catch (...) {
        if (ok) *ok = false;
        return 0;
    }
}

int64_t UnicodeString::toInt64(bool *ok) const
{
    using DataMethod = std::string (UnicodeString::*)(void) const;
    // template<typename NumberType, ushort base, typename StringType, typename DataMethod>
    try {
        if (ok) *ok = true;
        return Number::toInt64<int64_t, 10, UnicodeString, DataMethod>()(*this, &UnicodeString::to_utf8);
    } catch (...) {
        if (ok) *ok = false;
        return 0;
    }
}

uint64_t UnicodeString::toULongLong(bool *ok) const
{
    using DataMethod = std::string (UnicodeString::*)(void) const;
    // template<typename NumberType, ushort base, typename StringType, typename DataMethod>
    try {
        if (ok) *ok = true;
        return Number::toUInt64<uint64_t, 10, UnicodeString, DataMethod>()(*this, &UnicodeString::to_utf8);
    } catch (...) {
        if (ok) *ok = false;
        return 0;
    }
}

float UnicodeString::toFloat(bool *ok) const
{
    using DataMethod = std::string (UnicodeString::*)(void) const;
    // template<typename NumberType, ushort base, typename StringType, typename DataMethod>
    try {
        if (ok) *ok = true;
        return Number::toFloat<float, UnicodeString, DataMethod>()(*this, &UnicodeString::to_utf8);
    } catch (...) {
        if (ok) *ok = false;
        return 0;
    }
}

double UnicodeString::toDouble(bool *ok) const
{
    using DataMethod = std::string (UnicodeString::*)(void) const;
    // template<typename NumberType, ushort base, typename StringType, typename DataMethod>
    try {
        if (ok) *ok = true;
        return Number::toDouble<double, UnicodeString, DataMethod>()(*this, &UnicodeString::to_utf8);
    } catch (...) {
        if (ok) *ok = false;
        return 0;
    }
}

long double UnicodeString::toLongDouble(bool *ok) const
{
    using DataMethod = std::string (UnicodeString::*)(void) const;
    // template<typename NumberType, ushort base, typename StringType, typename DataMethod>
    try {
        if (ok) *ok = true;
        return Number::toLongDouble<long double, UnicodeString, DataMethod>()(*this, &UnicodeString::to_utf8);
    } catch (...) {
        if (ok) *ok = false;
        return 0;
    }
}

const UnicodeString::raw_type UnicodeString::internal_data() const
{
    raw_type data;
    for (auto&& c : dd)
        data.emplace_back(c());
    return data;
}

const UnicodeString::raw_type_pcre2 UnicodeString::internal_data_pcre2() const
{
    raw_type_pcre2 data;
    for (auto&& c : dd)
        data.emplace_back(static_cast<char_type_pcre2>(c()));
    return data;
}

bool UnicodeString::compare_data(const type &other) const
{
    if (dd.size() != other.size())
        return false;

    for (type::size_type i = 0; i < dd.size(); i++)
    {
        if (dd[i] != other[i])
        {
            return false;
        }
    }

    return true;
}

UnicodeString UnicodeString::from_uchar32_buffer_static(const uint32_t *buf, size_t len)
{
    UnicodeString str;
    str.from_uchar32_buffer(buf, len);
    return str;
}

UnicodeString &UnicodeString::from_uchar32_buffer(const uint32_t *buf, size_t len)
{
    dd.clear();
    for (auto i = 0U; i < len; i++)
        dd.emplace_back(char_type(static_cast<char_type::type>(buf[i])));
    return *this;
}

bool UnicodeString::compare_helper(size_t start,
                                   size_t length,
                                   const UnicodeString &srcText,
                                   size_t srcStart,
                                   size_t srcLength) const
{
    // FIXME: function may be private but still try to fix this before wrapping public methods around it
    //        index and length checks are wrong at the moment

//    if (start > size()-1 || length > size()-1 || start+length > size()-1)
//        return false;

//    if (srcStart > srcText.size()-1 || srcLength > srcText.size()-1 || srcStart+srcLength > srcText.size()-1)
//        return false;

    type::const_iterator _begin = dd.begin() + static_cast<type::difference_type>(start);
    type::const_iterator _end = _begin + static_cast<type::difference_type>(length);

    type::const_iterator _srcBegin = srcText.begin() + static_cast<type::difference_type>(srcStart);
    type::const_iterator _srcEnd = _srcBegin + static_cast<type::difference_type>(srcLength);

    for (; _begin != _end; ++_begin, ++_srcBegin)
    {
        if ((*_begin) != (*_srcBegin))
        {
            return false;
        }
    }

    return true;
}

void UnicodeString::replace_helper(UnicodeString &str, const UnicodeString &from, const UnicodeString &to)
{
    if (from.empty())
        return;

    index start_pos = 0;
    while ((start_pos = str.find(from, start_pos)) != UnicodeString::npos)
    {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length();
    }
}

UnicodeString::char_type::type UnicodeString::calculate_char_total() const
{
    char_type::type total = 0;
    for (auto&& c : *this)
        total += c();
    return total;
}
