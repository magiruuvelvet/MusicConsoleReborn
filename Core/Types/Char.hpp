#ifndef CHAR_HPP
#define CHAR_HPP

/** Unicode Character Class
 *
 * Convenient wrapper around the ICU C API to access the
 * Unicode Character Database.
 *
 * Every method comes with a static and non-static version
 * for universal usage.
 *
 * Inspired from the QChar class (Qt Framework)
 *
 */

#include <string>
#include <cinttypes>

// UTF-32 character
class Char final
{
public:
    // type of a single unicode code point
    using CodePoint = std::int32_t;
    using type = CodePoint;

    // type aliases
    using utf32_t = type;
    using utf16_t = unsigned short int;

    Char() {}
    Char(const Char &other)
    { this->cp = other.cp; }
    Char(const CodePoint &cp)
    { this->cp = cp; }

    // special character constants
    enum SpecialCharacter {
        Null = 0x0000,
        Tabulation = 0x0009,
        LineFeed = 0x000a,
        CarriageReturn = 0x000d,
        Space = 0x0020,
        Nbsp = 0x00a0,
        SoftHyphen = 0x00ad,
        ReplacementCharacter = 0xfffd,
        ObjectReplacementCharacter = 0xfffc,
        ByteOrderMark = 0xfeff,
        ByteOrderSwapped = 0xfffe,
        ParagraphSeparator = 0x2029,
        LineSeparator = 0x2028,
        LastValidCodePoint = 0x10ffff
    };

    // unicode scripts
    enum Script {
        USCRIPT_INVALID_CODE                  = -1,
        USCRIPT_COMMON                        =  0,
        USCRIPT_INHERITED                     =  1,
        USCRIPT_ARABIC                        =  2,
        USCRIPT_ARMENIAN                      =  3,
        USCRIPT_BENGALI                       =  4,
        USCRIPT_BOPOMOFO                      =  5,
        USCRIPT_CHEROKEE                      =  6,
        USCRIPT_COPTIC                        =  7,
        USCRIPT_CYRILLIC                      =  8,
        USCRIPT_DESERET                       =  9,
        USCRIPT_DEVANAGARI                    = 10,
        USCRIPT_ETHIOPIC                      = 11,
        USCRIPT_GEORGIAN                      = 12,
        USCRIPT_GOTHIC                        = 13,
        USCRIPT_GREEK                         = 14,
        USCRIPT_GUJARATI                      = 15,
        USCRIPT_GURMUKHI                      = 16,
        USCRIPT_HAN                           = 17,
        USCRIPT_HANGUL                        = 18,
        USCRIPT_HEBREW                        = 19,
        USCRIPT_HIRAGANA                      = 20,
        USCRIPT_KANNADA                       = 21,
        USCRIPT_KATAKANA                      = 22,
        USCRIPT_KHMER                         = 23,
        USCRIPT_LAO                           = 24,
        USCRIPT_LATIN                         = 25,
        USCRIPT_MALAYALAM                     = 26,
        USCRIPT_MONGOLIAN                     = 27,
        USCRIPT_MYANMAR                       = 28,
        USCRIPT_OGHAM                         = 29,
        USCRIPT_OLD_ITALIC                    = 30,
        USCRIPT_ORIYA                         = 31,
        USCRIPT_RUNIC                         = 32,
        USCRIPT_SINHALA                       = 33,
        USCRIPT_SYRIAC                        = 34,
        USCRIPT_TAMIL                         = 35,
        USCRIPT_TELUGU                        = 36,
        USCRIPT_THAANA                        = 37,
        USCRIPT_THAI                          = 38,
        USCRIPT_TIBETAN                       = 39,
        USCRIPT_CANADIAN_ABORIGINAL           = 40,
        USCRIPT_UCAS                          = USCRIPT_CANADIAN_ABORIGINAL,
        USCRIPT_YI                            = 41,
        USCRIPT_TAGALOG                       = 42,
        USCRIPT_HANUNOO                       = 43,
        USCRIPT_BUHID                         = 44,
        USCRIPT_TAGBANWA                      = 45,
        USCRIPT_BRAILLE                       = 46,
        USCRIPT_CYPRIOT                       = 47,
        USCRIPT_LIMBU                         = 48,
        USCRIPT_LINEAR_B                      = 49,
        USCRIPT_OSMANYA                       = 50,
        USCRIPT_SHAVIAN                       = 51,
        USCRIPT_TAI_LE                        = 52,
        USCRIPT_UGARITIC                      = 53,
        USCRIPT_KATAKANA_OR_HIRAGANA          = 54,
        USCRIPT_BUGINESE                      = 55,
        USCRIPT_GLAGOLITIC                    = 56,
        USCRIPT_KHAROSHTHI                    = 57,
        USCRIPT_SYLOTI_NAGRI                  = 58,
        USCRIPT_NEW_TAI_LUE                   = 59,
        USCRIPT_TIFINAGH                      = 60,
        USCRIPT_OLD_PERSIAN                   = 61,
        USCRIPT_BALINESE                      = 62,
        USCRIPT_BATAK                         = 63,
        USCRIPT_BLISSYMBOLS                   = 64,
        USCRIPT_BRAHMI                        = 65,
        USCRIPT_CHAM                          = 66,
        USCRIPT_CIRTH                         = 67,
        USCRIPT_OLD_CHURCH_SLAVONIC_CYRILLIC  = 68,
        USCRIPT_DEMOTIC_EGYPTIAN              = 69,
        USCRIPT_HIERATIC_EGYPTIAN             = 70,
        USCRIPT_EGYPTIAN_HIEROGLYPHS          = 71,
        USCRIPT_KHUTSURI                      = 72,
        USCRIPT_SIMPLIFIED_HAN                = 73,
        USCRIPT_TRADITIONAL_HAN               = 74,
        USCRIPT_PAHAWH_HMONG                  = 75,
        USCRIPT_OLD_HUNGARIAN                 = 76,
        USCRIPT_HARAPPAN_INDUS                = 77,
        USCRIPT_JAVANESE                      = 78,
        USCRIPT_KAYAH_LI                      = 79,
        USCRIPT_LATIN_FRAKTUR                 = 80,
        USCRIPT_LATIN_GAELIC                  = 81,
        USCRIPT_LEPCHA                        = 82,
        USCRIPT_LINEAR_A                      = 83,
        USCRIPT_MANDAIC                       = 84,
        USCRIPT_MANDAEAN                      = USCRIPT_MANDAIC,
        USCRIPT_MAYAN_HIEROGLYPHS             = 85,
        USCRIPT_MEROITIC_HIEROGLYPHS          = 86,
        USCRIPT_MEROITIC                      = USCRIPT_MEROITIC_HIEROGLYPHS,
        USCRIPT_NKO                           = 87,
        USCRIPT_ORKHON                        = 88,
        USCRIPT_OLD_PERMIC                    = 89,
        USCRIPT_PHAGS_PA                      = 90,
        USCRIPT_PHOENICIAN                    = 91,
        USCRIPT_MIAO                          = 92,
        USCRIPT_PHONETIC_POLLARD              = USCRIPT_MIAO,
        USCRIPT_RONGORONGO                    = 93,
        USCRIPT_SARATI                        = 94,
        USCRIPT_ESTRANGELO_SYRIAC             = 95,
        USCRIPT_WESTERN_SYRIAC                = 96,
        USCRIPT_EASTERN_SYRIAC                = 97,
        USCRIPT_TENGWAR                       = 98,
        USCRIPT_VAI                           = 99,
        USCRIPT_VISIBLE_SPEECH                = 100,
        USCRIPT_CUNEIFORM                     = 101,
        USCRIPT_UNWRITTEN_LANGUAGES           = 102,
        USCRIPT_UNKNOWN                       = 103,
        USCRIPT_CARIAN                        = 104,
        USCRIPT_JAPANESE                      = 105,
        USCRIPT_LANNA                         = 106,
        USCRIPT_LYCIAN                        = 107,
        USCRIPT_LYDIAN                        = 108,
        USCRIPT_OL_CHIKI                      = 109,
        USCRIPT_REJANG                        = 110,
        USCRIPT_SAURASHTRA                    = 111,
        USCRIPT_SIGN_WRITING                  = 112,
        USCRIPT_SUNDANESE                     = 113,
        USCRIPT_MOON                          = 114,
        USCRIPT_MEITEI_MAYEK                  = 115,
        USCRIPT_IMPERIAL_ARAMAIC              = 116,
        USCRIPT_AVESTAN                       = 117,
        USCRIPT_CHAKMA                        = 118,
        USCRIPT_KOREAN                        = 119,
        USCRIPT_KAITHI                        = 120,
        USCRIPT_MANICHAEAN                    = 121,
        USCRIPT_INSCRIPTIONAL_PAHLAVI         = 122,
        USCRIPT_PSALTER_PAHLAVI               = 123,
        USCRIPT_BOOK_PAHLAVI                  = 124,
        USCRIPT_INSCRIPTIONAL_PARTHIAN        = 125,
        USCRIPT_SAMARITAN                     = 126,
        USCRIPT_TAI_VIET                      = 127,
        USCRIPT_MATHEMATICAL_NOTATION         = 128,
        USCRIPT_SYMBOLS                       = 129,
        USCRIPT_BAMUM                         = 130,
        USCRIPT_LISU                          = 131,
        USCRIPT_NAKHI_GEBA                    = 132,
        USCRIPT_OLD_SOUTH_ARABIAN             = 133,
        USCRIPT_BASSA_VAH                     = 134,
        USCRIPT_DUPLOYAN                      = 135,
        USCRIPT_ELBASAN                       = 136,
        USCRIPT_GRANTHA                       = 137,
        USCRIPT_KPELLE                        = 138,
        USCRIPT_LOMA                          = 139,
        USCRIPT_MENDE                         = 140,
        USCRIPT_MEROITIC_CURSIVE              = 141,
        USCRIPT_OLD_NORTH_ARABIAN             = 142,
        USCRIPT_NABATAEAN                     = 143,
        USCRIPT_PALMYRENE                     = 144,
        USCRIPT_KHUDAWADI                     = 145,
        USCRIPT_SINDHI                        = USCRIPT_KHUDAWADI,
        USCRIPT_WARANG_CITI                   = 146,
        USCRIPT_AFAKA                         = 147,
        USCRIPT_JURCHEN                       = 148,
        USCRIPT_MRO                           = 149,
        USCRIPT_NUSHU                         = 150,
        USCRIPT_SHARADA                       = 151,
        USCRIPT_SORA_SOMPENG                  = 152,
        USCRIPT_TAKRI                         = 153,
        USCRIPT_TANGUT                        = 154,
        USCRIPT_WOLEAI                        = 155,
        USCRIPT_ANATOLIAN_HIEROGLYPHS         = 156,
        USCRIPT_KHOJKI                        = 157,
        USCRIPT_TIRHUTA                       = 158,
        USCRIPT_CAUCASIAN_ALBANIAN            = 159,
        USCRIPT_MAHAJANI                      = 160,
        USCRIPT_AHOM                          = 161,
        USCRIPT_HATRAN                        = 162,
        USCRIPT_MODI                          = 163,
        USCRIPT_MULTANI                       = 164,
        USCRIPT_PAU_CIN_HAU                   = 165,
        USCRIPT_SIDDHAM                       = 166,
        USCRIPT_ADLAM                         = 167,
        USCRIPT_BHAIKSUKI                     = 168,
        USCRIPT_MARCHEN                       = 169,
        USCRIPT_NEWA                          = 170,
        USCRIPT_OSAGE                         = 171,
        USCRIPT_HAN_WITH_BOPOMOFO             = 172,
        USCRIPT_JAMO                          = 173,
        USCRIPT_SYMBOLS_EMOJI                 = 174,
        USCRIPT_MASARAM_GONDI                 = 175,
        USCRIPT_SOYOMBO                       = 176,
        USCRIPT_ZANABAZAR_SQUARE              = 177,
    };

    // character categories
    enum Category {
        U_UNASSIGNED                          = 0,
        U_GENERAL_OTHER_TYPES                 = 0,
        U_UPPERCASE_LETTER                    = 1,
        U_LOWERCASE_LETTER                    = 2,
        U_TITLECASE_LETTER                    = 3,
        U_MODIFIER_LETTER                     = 4,
        U_OTHER_LETTER                        = 5,
        U_NON_SPACING_MARK                    = 6,
        U_ENCLOSING_MARK                      = 7,
        U_COMBINING_SPACING_MARK              = 8,
        U_DECIMAL_DIGIT_NUMBER                = 9,
        U_LETTER_NUMBER                       = 10,
        U_OTHER_NUMBER                        = 11,
        U_SPACE_SEPARATOR                     = 12,
        U_LINE_SEPARATOR                      = 13,
        U_PARAGRAPH_SEPARATOR                 = 14,
        U_CONTROL_CHAR                        = 15,
        U_FORMAT_CHAR                         = 16,
        U_PRIVATE_USE_CHAR                    = 17,
        U_SURROGATE                           = 18,
        U_DASH_PUNCTUATION                    = 19,
        U_START_PUNCTUATION                   = 20,
        U_END_PUNCTUATION                     = 21,
        U_CONNECTOR_PUNCTUATION               = 22,
        U_OTHER_PUNCTUATION                   = 23,
        U_MATH_SYMBOL                         = 24,
        U_CURRENCY_SYMBOL                     = 25,
        U_MODIFIER_SYMBOL                     = 26,
        U_OTHER_SYMBOL                        = 27,
        U_INITIAL_PUNCTUATION                 = 28,
        U_FINAL_PUNCTUATION                   = 29,
    };

    // unicode blocks
    enum Block {
        UBLOCK_NO_BLOCK                       = 0,
        UBLOCK_BASIC_LATIN                    = 1,
        UBLOCK_LATIN_1_SUPPLEMENT             = 2,
        UBLOCK_LATIN_EXTENDED_A               = 3,
        UBLOCK_LATIN_EXTENDED_B               = 4,
        UBLOCK_IPA_EXTENSIONS                 = 5,
        UBLOCK_SPACING_MODIFIER_LETTERS       = 6,
        UBLOCK_COMBINING_DIACRITICAL_MARKS    = 7,
        UBLOCK_GREEK                          = 8,
        UBLOCK_CYRILLIC                       = 9,
        UBLOCK_ARMENIAN                       = 10,
        UBLOCK_HEBREW                         = 11,
        UBLOCK_ARABIC                         = 12,
        UBLOCK_SYRIAC                         = 13,
        UBLOCK_THAANA                         = 14,
        UBLOCK_DEVANAGARI                     = 15,
        UBLOCK_BENGALI                        = 16,
        UBLOCK_GURMUKHI                       = 17,
        UBLOCK_GUJARATI                       = 18,
        UBLOCK_ORIYA                          = 19,
        UBLOCK_TAMIL                          = 20,
        UBLOCK_TELUGU                         = 21,
        UBLOCK_KANNADA                        = 22,
        UBLOCK_MALAYALAM                      = 23,
        UBLOCK_SINHALA                        = 24,
        UBLOCK_THAI                           = 25,
        UBLOCK_LAO                            = 26,
        UBLOCK_TIBETAN                        = 27,
        UBLOCK_MYANMAR                        = 28,
        UBLOCK_GEORGIAN                       = 29,
        UBLOCK_HANGUL_JAMO                    = 30,
        UBLOCK_ETHIOPIC                       = 31,
        UBLOCK_CHEROKEE                       = 32,
        UBLOCK_UNIFIED_CANADIAN_ABORIGINAL_SYLLABICS = 33,
        UBLOCK_OGHAM                          = 34,
        UBLOCK_RUNIC                          = 35,
        UBLOCK_KHMER                          = 36,
        UBLOCK_MONGOLIAN                      = 37,
        UBLOCK_LATIN_EXTENDED_ADDITIONAL      = 38,
        UBLOCK_GREEK_EXTENDED                 = 39,
        UBLOCK_GENERAL_PUNCTUATION            = 40,
        UBLOCK_SUPERSCRIPTS_AND_SUBSCRIPTS    = 41,
        UBLOCK_CURRENCY_SYMBOLS               = 42,
        UBLOCK_COMBINING_MARKS_FOR_SYMBOLS    = 43,
        UBLOCK_LETTERLIKE_SYMBOLS             = 44,
        UBLOCK_NUMBER_FORMS                   = 45,
        UBLOCK_ARROWS                         = 46,
        UBLOCK_MATHEMATICAL_OPERATORS         = 47,
        UBLOCK_MISCELLANEOUS_TECHNICAL        = 48,
        UBLOCK_CONTROL_PICTURES               = 49,
        UBLOCK_OPTICAL_CHARACTER_RECOGNITION  = 50,
        UBLOCK_ENCLOSED_ALPHANUMERICS         = 51,
        UBLOCK_BOX_DRAWING                    = 52,
        UBLOCK_BLOCK_ELEMENTS                 = 53,
        UBLOCK_GEOMETRIC_SHAPES               = 54,
        UBLOCK_MISCELLANEOUS_SYMBOLS          = 55,
        UBLOCK_DINGBATS                       = 56,
        UBLOCK_BRAILLE_PATTERNS               = 57,
        UBLOCK_CJK_RADICALS_SUPPLEMENT        = 58,
        UBLOCK_KANGXI_RADICALS                = 59,
        UBLOCK_IDEOGRAPHIC_DESCRIPTION_CHARACTERS = 60,
        UBLOCK_CJK_SYMBOLS_AND_PUNCTUATION    = 61,
        UBLOCK_HIRAGANA                       = 62,
        UBLOCK_KATAKANA                       = 63,
        UBLOCK_BOPOMOFO                       = 64,
        UBLOCK_HANGUL_COMPATIBILITY_JAMO      = 65,
        UBLOCK_KANBUN                         = 66,
        UBLOCK_BOPOMOFO_EXTENDED              = 67,
        UBLOCK_ENCLOSED_CJK_LETTERS_AND_MONTHS = 68,
        UBLOCK_CJK_COMPATIBILITY              = 69,
        UBLOCK_CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A = 70,
        UBLOCK_CJK_UNIFIED_IDEOGRAPHS         = 71,
        UBLOCK_YI_SYLLABLES                   = 72,
        UBLOCK_YI_RADICALS                    = 73,
        UBLOCK_HANGUL_SYLLABLES               = 74,
        UBLOCK_HIGH_SURROGATES                = 75,
        UBLOCK_HIGH_PRIVATE_USE_SURROGATES    = 76,
        UBLOCK_LOW_SURROGATES                 = 77,
        UBLOCK_PRIVATE_USE_AREA               = 78,
        UBLOCK_PRIVATE_USE = UBLOCK_PRIVATE_USE_AREA,
        UBLOCK_CJK_COMPATIBILITY_IDEOGRAPHS   = 79,
        UBLOCK_ALPHABETIC_PRESENTATION_FORMS  = 80,
        UBLOCK_ARABIC_PRESENTATION_FORMS_A    = 81,
        UBLOCK_COMBINING_HALF_MARKS           = 82,
        UBLOCK_CJK_COMPATIBILITY_FORMS        = 83,
        UBLOCK_SMALL_FORM_VARIANTS            = 84,
        UBLOCK_ARABIC_PRESENTATION_FORMS_B    = 85,
        UBLOCK_SPECIALS                       = 86,
        UBLOCK_HALFWIDTH_AND_FULLWIDTH_FORMS  = 87,
        UBLOCK_OLD_ITALIC                     = 88,
        UBLOCK_GOTHIC                         = 89,
        UBLOCK_DESERET                        = 90,
        UBLOCK_BYZANTINE_MUSICAL_SYMBOLS      = 91,
        UBLOCK_MUSICAL_SYMBOLS                = 92,
        UBLOCK_MATHEMATICAL_ALPHANUMERIC_SYMBOLS = 93,
        UBLOCK_CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B = 94,
        UBLOCK_CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT = 95,
        UBLOCK_TAGS                           = 96,
        UBLOCK_CYRILLIC_SUPPLEMENT            = 97,
        UBLOCK_CYRILLIC_SUPPLEMENTARY = UBLOCK_CYRILLIC_SUPPLEMENT,
        UBLOCK_TAGALOG                        = 98,
        UBLOCK_HANUNOO                        = 99,
        UBLOCK_BUHID                          = 100,
        UBLOCK_TAGBANWA                       = 101,
        UBLOCK_MISCELLANEOUS_MATHEMATICAL_SYMBOLS_A = 102,
        UBLOCK_SUPPLEMENTAL_ARROWS_A          = 103,
        UBLOCK_SUPPLEMENTAL_ARROWS_B          = 104,
        UBLOCK_MISCELLANEOUS_MATHEMATICAL_SYMBOLS_B = 105,
        UBLOCK_SUPPLEMENTAL_MATHEMATICAL_OPERATORS = 106,
        UBLOCK_KATAKANA_PHONETIC_EXTENSIONS   = 107,
        UBLOCK_VARIATION_SELECTORS            = 108,
        UBLOCK_SUPPLEMENTARY_PRIVATE_USE_AREA_A = 109,
        UBLOCK_SUPPLEMENTARY_PRIVATE_USE_AREA_B = 110,
        UBLOCK_LIMBU                          = 111,
        UBLOCK_TAI_LE                         = 112,
        UBLOCK_KHMER_SYMBOLS                  = 113,
        UBLOCK_PHONETIC_EXTENSIONS            = 114,
        UBLOCK_MISCELLANEOUS_SYMBOLS_AND_ARROWS = 115,
        UBLOCK_YIJING_HEXAGRAM_SYMBOLS        = 116,
        UBLOCK_LINEAR_B_SYLLABARY             = 117,
        UBLOCK_LINEAR_B_IDEOGRAMS             = 118,
        UBLOCK_AEGEAN_NUMBERS                 = 119,
        UBLOCK_UGARITIC                       = 120,
        UBLOCK_SHAVIAN                        = 121,
        UBLOCK_OSMANYA                        = 122,
        UBLOCK_CYPRIOT_SYLLABARY              = 123,
        UBLOCK_TAI_XUAN_JING_SYMBOLS          = 124,
        UBLOCK_VARIATION_SELECTORS_SUPPLEMENT = 125,
        UBLOCK_ANCIENT_GREEK_MUSICAL_NOTATION = 126,
        UBLOCK_ANCIENT_GREEK_NUMBERS          = 127,
        UBLOCK_ARABIC_SUPPLEMENT              = 128,
        UBLOCK_BUGINESE                       = 129,
        UBLOCK_CJK_STROKES                    = 130,
        UBLOCK_COMBINING_DIACRITICAL_MARKS_SUPPLEMENT = 131,
        UBLOCK_COPTIC                         = 132,
        UBLOCK_ETHIOPIC_EXTENDED              = 133,
        UBLOCK_ETHIOPIC_SUPPLEMENT            = 134,
        UBLOCK_GEORGIAN_SUPPLEMENT            = 135,
        UBLOCK_GLAGOLITIC                     = 136,
        UBLOCK_KHAROSHTHI                     = 137,
        UBLOCK_MODIFIER_TONE_LETTERS          = 138,
        UBLOCK_NEW_TAI_LUE                    = 139,
        UBLOCK_OLD_PERSIAN                    = 140,
        UBLOCK_PHONETIC_EXTENSIONS_SUPPLEMENT = 141,
        UBLOCK_SUPPLEMENTAL_PUNCTUATION       = 142,
        UBLOCK_SYLOTI_NAGRI                   = 143,
        UBLOCK_TIFINAGH                       = 144,
        UBLOCK_VERTICAL_FORMS                 = 145,
        UBLOCK_NKO                            = 146,
        UBLOCK_BALINESE                       = 147,
        UBLOCK_LATIN_EXTENDED_C               = 148,
        UBLOCK_LATIN_EXTENDED_D               = 149,
        UBLOCK_PHAGS_PA                       = 150,
        UBLOCK_PHOENICIAN                     = 151,
        UBLOCK_CUNEIFORM                      = 152,
        UBLOCK_CUNEIFORM_NUMBERS_AND_PUNCTUATION = 153,
        UBLOCK_COUNTING_ROD_NUMERALS          = 154,
        UBLOCK_SUNDANESE                      = 155,
        UBLOCK_LEPCHA                         = 156,
        UBLOCK_OL_CHIKI                       = 157,
        UBLOCK_CYRILLIC_EXTENDED_A            = 158,
        UBLOCK_VAI                            = 159,
        UBLOCK_CYRILLIC_EXTENDED_B            = 160,
        UBLOCK_SAURASHTRA                     = 161,
        UBLOCK_KAYAH_LI                       = 162,
        UBLOCK_REJANG                         = 163,
        UBLOCK_CHAM                           = 164,
        UBLOCK_ANCIENT_SYMBOLS                = 165,
        UBLOCK_PHAISTOS_DISC                  = 166,
        UBLOCK_LYCIAN                         = 167,
        UBLOCK_CARIAN                         = 168,
        UBLOCK_LYDIAN                         = 169,
        UBLOCK_MAHJONG_TILES                  = 170,
        UBLOCK_DOMINO_TILES                   = 171,
        UBLOCK_SAMARITAN                      = 172,
        UBLOCK_UNIFIED_CANADIAN_ABORIGINAL_SYLLABICS_EXTENDED = 173,
        UBLOCK_TAI_THAM                       = 174,
        UBLOCK_VEDIC_EXTENSIONS               = 175,
        UBLOCK_LISU                           = 176,
        UBLOCK_BAMUM                          = 177,
        UBLOCK_COMMON_INDIC_NUMBER_FORMS      = 178,
        UBLOCK_DEVANAGARI_EXTENDED            = 179,
        UBLOCK_HANGUL_JAMO_EXTENDED_A         = 180,
        UBLOCK_JAVANESE                       = 181,
        UBLOCK_MYANMAR_EXTENDED_A             = 182,
        UBLOCK_TAI_VIET                       = 183,
        UBLOCK_MEETEI_MAYEK                   = 184,
        UBLOCK_HANGUL_JAMO_EXTENDED_B         = 185,
        UBLOCK_IMPERIAL_ARAMAIC               = 186,
        UBLOCK_OLD_SOUTH_ARABIAN              = 187,
        UBLOCK_AVESTAN                        = 188,
        UBLOCK_INSCRIPTIONAL_PARTHIAN         = 189,
        UBLOCK_INSCRIPTIONAL_PAHLAVI          = 190,
        UBLOCK_OLD_TURKIC                     = 191,
        UBLOCK_RUMI_NUMERAL_SYMBOLS           = 192,
        UBLOCK_KAITHI                         = 193,
        UBLOCK_EGYPTIAN_HIEROGLYPHS           = 194,
        UBLOCK_ENCLOSED_ALPHANUMERIC_SUPPLEMENT = 195,
        UBLOCK_ENCLOSED_IDEOGRAPHIC_SUPPLEMENT = 196,
        UBLOCK_CJK_UNIFIED_IDEOGRAPHS_EXTENSION_C = 197,
        UBLOCK_MANDAIC                        = 198,
        UBLOCK_BATAK                          = 199,
        UBLOCK_ETHIOPIC_EXTENDED_A            = 200,
        UBLOCK_BRAHMI                         = 201,
        UBLOCK_BAMUM_SUPPLEMENT               = 202,
        UBLOCK_KANA_SUPPLEMENT                = 203,
        UBLOCK_PLAYING_CARDS                  = 204,
        UBLOCK_MISCELLANEOUS_SYMBOLS_AND_PICTOGRAPHS = 205,
        UBLOCK_EMOTICONS                      = 206,
        UBLOCK_TRANSPORT_AND_MAP_SYMBOLS      = 207,
        UBLOCK_ALCHEMICAL_SYMBOLS             = 208,
        UBLOCK_CJK_UNIFIED_IDEOGRAPHS_EXTENSION_D = 209,
        UBLOCK_ARABIC_EXTENDED_A              = 210,
        UBLOCK_ARABIC_MATHEMATICAL_ALPHABETIC_SYMBOLS = 211,
        UBLOCK_CHAKMA                         = 212,
        UBLOCK_MEETEI_MAYEK_EXTENSIONS        = 213,
        UBLOCK_MEROITIC_CURSIVE               = 214,
        UBLOCK_MEROITIC_HIEROGLYPHS           = 215,
        UBLOCK_MIAO                           = 216,
        UBLOCK_SHARADA                        = 217,
        UBLOCK_SORA_SOMPENG                   = 218,
        UBLOCK_SUNDANESE_SUPPLEMENT           = 219,
        UBLOCK_TAKRI                          = 220,
        UBLOCK_BASSA_VAH                      = 221,
        UBLOCK_CAUCASIAN_ALBANIAN             = 222,
        UBLOCK_COPTIC_EPACT_NUMBERS           = 223,
        UBLOCK_COMBINING_DIACRITICAL_MARKS_EXTENDED = 224,
        UBLOCK_DUPLOYAN                       = 225,
        UBLOCK_ELBASAN                        = 226,
        UBLOCK_GEOMETRIC_SHAPES_EXTENDED      = 227,
        UBLOCK_GRANTHA                        = 228,
        UBLOCK_KHOJKI                         = 229,
        UBLOCK_KHUDAWADI                      = 230,
        UBLOCK_LATIN_EXTENDED_E               = 231,
        UBLOCK_LINEAR_A                       = 232,
        UBLOCK_MAHAJANI                       = 233,
        UBLOCK_MANICHAEAN                     = 234,
        UBLOCK_MENDE_KIKAKUI                  = 235,
        UBLOCK_MODI                           = 236,
        UBLOCK_MRO                            = 237,
        UBLOCK_MYANMAR_EXTENDED_B             = 238,
        UBLOCK_NABATAEAN                      = 239,
        UBLOCK_OLD_NORTH_ARABIAN              = 240,
        UBLOCK_OLD_PERMIC                     = 241,
        UBLOCK_ORNAMENTAL_DINGBATS            = 242,
        UBLOCK_PAHAWH_HMONG                   = 243,
        UBLOCK_PALMYRENE                      = 244,
        UBLOCK_PAU_CIN_HAU                    = 245,
        UBLOCK_PSALTER_PAHLAVI                = 246,
        UBLOCK_SHORTHAND_FORMAT_CONTROLS      = 247,
        UBLOCK_SIDDHAM                        = 248,
        UBLOCK_SINHALA_ARCHAIC_NUMBERS        = 249,
        UBLOCK_SUPPLEMENTAL_ARROWS_C          = 250,
        UBLOCK_TIRHUTA                        = 251,
        UBLOCK_WARANG_CITI                    = 252,
        UBLOCK_AHOM                           = 253,
        UBLOCK_ANATOLIAN_HIEROGLYPHS          = 254,
        UBLOCK_CHEROKEE_SUPPLEMENT            = 255,
        UBLOCK_CJK_UNIFIED_IDEOGRAPHS_EXTENSION_E = 256,
        UBLOCK_EARLY_DYNASTIC_CUNEIFORM       = 257,
        UBLOCK_HATRAN                         = 258,
        UBLOCK_MULTANI                        = 259,
        UBLOCK_OLD_HUNGARIAN                  = 260,
        UBLOCK_SUPPLEMENTAL_SYMBOLS_AND_PICTOGRAPHS = 261,
        UBLOCK_SUTTON_SIGNWRITING             = 262,
        UBLOCK_ADLAM                          = 263,
        UBLOCK_BHAIKSUKI                      = 264,
        UBLOCK_CYRILLIC_EXTENDED_C            = 265,
        UBLOCK_GLAGOLITIC_SUPPLEMENT          = 266,
        UBLOCK_IDEOGRAPHIC_SYMBOLS_AND_PUNCTUATION = 267,
        UBLOCK_MARCHEN                        = 268,
        UBLOCK_MONGOLIAN_SUPPLEMENT           = 269,
        UBLOCK_NEWA                           = 270,
        UBLOCK_OSAGE                          = 271,
        UBLOCK_TANGUT                         = 272,
        UBLOCK_TANGUT_COMPONENTS              = 273,
        UBLOCK_CJK_UNIFIED_IDEOGRAPHS_EXTENSION_F = 274,
        UBLOCK_KANA_EXTENDED_A                = 275,
        UBLOCK_MASARAM_GONDI                  = 276,
        UBLOCK_NUSHU                          = 277,
        UBLOCK_SOYOMBO                        = 278,
        UBLOCK_SYRIAC_SUPPLEMENT              = 279,
        UBLOCK_ZANABAZAR_SQUARE               = 280,
        UBLOCK_INVALID_CODE                   = -1,
    };

    // writing direction
    enum Direction {
        U_LEFT_TO_RIGHT               = 0,
        U_RIGHT_TO_LEFT               = 1,
        U_EUROPEAN_NUMBER             = 2,
        U_EUROPEAN_NUMBER_SEPARATOR   = 3,
        U_EUROPEAN_NUMBER_TERMINATOR  = 4,
        U_ARABIC_NUMBER               = 5,
        U_COMMON_NUMBER_SEPARATOR     = 6,
        U_BLOCK_SEPARATOR             = 7,
        U_SEGMENT_SEPARATOR           = 8,
        U_WHITE_SPACE_NEUTRAL         = 9,
        U_OTHER_NEUTRAL               = 10,
        U_LEFT_TO_RIGHT_EMBEDDING     = 11,
        U_LEFT_TO_RIGHT_OVERRIDE      = 12,
        U_RIGHT_TO_LEFT_ARABIC        = 13,
        U_RIGHT_TO_LEFT_EMBEDDING     = 14,
        U_RIGHT_TO_LEFT_OVERRIDE      = 15,
        U_POP_DIRECTIONAL_FORMAT      = 16,
        U_DIR_NON_SPACING_MARK        = 17,
        U_BOUNDARY_NEUTRAL            = 18,
        U_FIRST_STRONG_ISOLATE        = 19,
        U_LEFT_TO_RIGHT_ISOLATE       = 20,
        U_RIGHT_TO_LEFT_ISOLATE       = 21,
        U_POP_DIRECTIONAL_ISOLATE     = 22,
    };

    // copy assignment
    inline void operator= (const Char &other)
    { cp = other.cp; }
    inline void operator= (const CodePoint &cp)
    { this->cp = cp; }

    // add/sub/mul/div
    inline Char operator+ (const Char &other) const
    { return cp + other.cp; }
    inline Char operator+ (const CodePoint &cp) const
    { return this->cp + cp; }
    inline Char operator- (const Char &other) const
    { return cp - other.cp; }
    inline Char operator- (const CodePoint &cp) const
    { return this->cp - cp; }
    inline Char operator* (const Char &other) const
    { return cp * other.cp; }
    inline Char operator* (const CodePoint &cp) const
    { return this->cp * cp; }
    inline Char operator/ (const Char &other) const
    { return cp / other.cp; }
    inline Char operator/ (const CodePoint &cp) const
    { return this->cp / cp; }

    // compare
    inline bool operator== (const Char &other) const
    { return cp == other.cp; }
    inline bool operator== (const CodePoint &cp) const
    { return this->cp == cp; }
    inline bool operator!= (const Char &other) const
    { return cp != other.cp; }
    inline bool operator!= (const CodePoint &cp) const
    { return this->cp != cp; }
    inline bool operator< (const Char &other) const
    { return cp < other.cp; }
    inline bool operator< (const CodePoint &cp) const
    { return this->cp < cp; }
    inline bool operator> (const Char &other) const
    { return cp > other.cp; }
    inline bool operator> (const CodePoint &cp) const
    { return this->cp > cp; }
    inline bool operator<= (const Char &other) const
    { return cp <= other.cp; }
    inline bool operator<= (const CodePoint &cp) const
    { return this->cp <= cp; }
    inline bool operator>= (const Char &other) const
    { return cp >= other.cp; }
    inline bool operator>= (const CodePoint &cp) const
    { return this->cp >= cp; }

    // increase/decrease code point value
    inline Char &operator++ (int) // post-increment (c++)
    { this->cp++; return *this; }
    inline Char &operator-- (int) // post-decrement (c--)
    { this->cp--; return *this; }
    inline Char &operator++ ()    // pre-increment (++c)
    { ++this->cp; return *this; }
    inline Char &operator-- ()    // pre-decrement (--c)
    { --this->cp; return *this; }
    inline Char &operator+= (const Char &other)
    { this->cp += other.cp; return *this; }
    inline Char &operator+= (const CodePoint &cp)
    { this->cp += cp; return *this; }
    inline Char &operator-= (const Char &other)
    { this->cp -= other.cp; return *this; }
    inline Char &operator-= (const CodePoint &cp)
    { this->cp -= cp; return *this; }
    inline Char &operator*= (const Char &other)
    { this->cp *= other.cp; return *this; }
    inline Char &operator*= (const CodePoint &cp)
    { this->cp *= cp; return *this; }
    inline Char &operator/= (const Char &other)
    { this->cp /= other.cp; return *this; }
    inline Char &operator/= (const CodePoint &cp)
    { this->cp /= cp; return *this; }

    // get internal code point
    inline operator CodePoint() const // automatic casting on function call, data type operator
    { return cp; }
    inline operator const CodePoint&() const // automatic casting on function call, data type operator
    { return cp; }
    inline const CodePoint &operator() () const // operator()
    { return cp; }

    static bool is_space(const CodePoint &ch);
    inline bool is_space() const { return is_space(cp); }

    static bool is_uppercase(const CodePoint &ch);
    inline bool is_uppercase() const { return is_uppercase(cp); }

    static bool is_lowercase(const CodePoint &ch);
    inline bool is_lowercase() const { return is_lowercase(cp); }

    static bool is_titlecase(const CodePoint &ch);
    inline bool is_titlecase() const { return is_titlecase(cp); }

    static bool is_alphabetic(const CodePoint &ch);
    inline bool is_alphabetic() const { return is_alphabetic(cp); }

    static bool is_alphanumeric(const CodePoint &ch);
    inline bool is_alphanumeric() const { return is_alphanumeric(cp); }

    static bool is_control_character(const CodePoint &ch);
    inline bool is_control_character() const { return is_control_character(cp); }

    static bool is_blank(const CodePoint &ch);
    inline bool is_blank() const { return is_blank(cp); }

    static bool is_punctuation(const CodePoint &ch);
    inline bool is_punctuation() const { return is_punctuation(cp); }

    static bool is_printable(const CodePoint &ch);
    inline bool is_printable() const { return is_printable(cp); }

    static bool is_digit(const CodePoint &ch);
    inline bool is_digit() const { return is_digit(cp); }

    static bool is_non_character(const CodePoint &ch);
    inline bool is_non_character() const { return is_non_character(cp); }

    static bool is_high_surrogate(const CodePoint &ch);
    inline bool is_high_surrogate() const { return is_high_surrogate(cp); }

    static bool is_low_surrogate(const CodePoint &ch);
    inline bool is_low_surrogate() const { return is_low_surrogate(cp); }

    static bool is_surrogate(const CodePoint &ch);
    inline bool is_surrogate() const { return is_surrogate(cp); }

    static bool requires_surrogates(const CodePoint &ch);
    inline bool requires_surrogates() const { return requires_surrogates(cp); }

    // transform code point to high surrogate
    static Char to_high_surrogate(const CodePoint &ch);
    inline Char &to_high_surrogate() { cp = to_high_surrogate(cp)(); return *this; }
    inline Char to_high_surrogate() const { return to_high_surrogate(cp); }

    // transform code point to low surrogate
    static Char to_low_surrogate(const CodePoint &ch);
    inline Char &to_low_surrogate() { cp = to_low_surrogate(cp)(); return *this; }
    inline Char to_low_surrogate() const { return to_low_surrogate(cp); }

    // get digit value of code point, returns -1 on error
    // supports radix, supports only a limited set of code points
    static int32_t digit_basic(const Char &ch, int8_t radix = 10);
    static int32_t digit_basic(const CodePoint &ch, int8_t radix = 10);
    inline int32_t digit_basic(int8_t radix = 10) const { return digit_basic(cp, radix); }

    // get digit value of code point, returns -1 on error
    // supports everything which has a digit value defined in the unicode database
    static int32_t digit(const Char &ch);
    static int32_t digit(const CodePoint &ch);
    inline int32_t digit() const { return digit(cp); }

    // get numeric value of code point, to check for errors pass a bool pointer as second argument (optimal)
    // [ok] is set to false when an error occurred or the code point doesn't have a numeric value
    // on error the return value is always 0x0
    static double numeric(const Char &ch, bool *ok = nullptr);
    static inline double numeric(const CodePoint &ch, bool *ok = nullptr)
    { return numeric(Char(ch), ok); }
    inline double numeric(bool *ok = nullptr) const
    { return numeric(cp, ok); }

    // returns the upper case variant of the given code point
    static Char to_uppercase(const Char &ch);
    static Char to_uppercase(const CodePoint &ch);
    inline Char &to_uppercase() { cp = to_uppercase(cp)(); return *this; }
    inline Char to_uppercase() const { return to_uppercase(cp); }

    // returns the lower case variant of the given code point
    static Char to_lowercase(const Char &ch);
    static Char to_lowercase(const CodePoint &ch);
    inline Char &to_lowercase() { cp = to_lowercase(cp)(); return *this; }
    inline Char to_lowercase() const { return to_lowercase(cp); }

    // returns the title case variant of the given code point
    static Char to_titlecase(const Char &ch);
    static Char to_titlecase(const CodePoint &ch);
    inline Char &to_titlecase() { cp = to_titlecase(cp)(); return *this; }
    inline Char to_titlecase() const { return to_titlecase(cp); }

    // fold case character
    // operates on a single code point, if possible operate on the entire string instead for better results
    static Char fold_case(const Char &ch);
    static Char fold_case(const CodePoint &ch);
    inline Char &fold_case() { cp = fold_case(cp)(); return *this; }
    inline Char fold_case() const { return fold_case(cp); }

    // is character script cased (is case sensitivity supported)
    static bool is_cased(const CodePoint &ch);
    inline bool is_cased() const { return is_cased(cp); }

    // script name (enum)
    static Script script(const CodePoint &ch);
    inline Script script() const { return script(cp); }

    // character type (enum)
    static Category category(const CodePoint &ch);
    inline Category category() const { return category(cp); }

    // block (enum)
    static Block block(const CodePoint &ch);
    inline Block block() const { return block(cp); }

    // writing direction (enum)
    static Direction direction(const CodePoint &ch);
    inline Direction direction() const { return direction(cp); }

    // is left to right
    static inline bool is_ltr(const CodePoint &ch)
    { return direction(ch) == U_LEFT_TO_RIGHT; }
    inline bool is_ltr() const
    { return is_ltr(cp); }

    // is right to left
    static inline bool is_rtl(const CodePoint &ch)
    { return direction(ch) == U_RIGHT_TO_LEFT; }
    inline bool is_rtl() const
    { return is_rtl(cp); }

    // script name (string)
    static const std::string script_name(const CodePoint &ch);
    inline const std::string script_name() const { return script_name(cp); }

    // script short name (4-letter code)
    static const std::string script_shortname(const CodePoint &ch);
    inline const std::string script_shortname() const { return script_name(cp); }

    // character name (string)
    static const std::string name(const CodePoint &ch);
    inline const std::string name() const { return name(cp); }

    // get UTF-8 buffer (for printing)
    const std::string utf8() const;
    static inline std::string utf8(const Char &other)
    { return other.utf8(); }
    static inline std::string utf8(const CodePoint &cp)
    { return Char(cp).utf8(); }

    // convert surrogate to UTF-32, takes UTF-16 input
    static Char surrogate_to_utf32(utf16_t high, utf16_t low);

private:
    // internal code point
    CodePoint cp = {0};
};

// left handed: add/sub/mul/div
inline Char operator+ (const Char::CodePoint &lhs, const Char &rhs)
{ return lhs + rhs(); }
inline Char operator- (const Char::CodePoint &lhs, const Char &rhs)
{ return lhs - rhs(); }
inline Char operator* (const Char::CodePoint &lhs, const Char &rhs)
{ return lhs * rhs(); }
inline Char operator/ (const Char::CodePoint &lhs, const Char &rhs)
{ return lhs / rhs(); }

// left handed: compare
inline bool operator== (const Char::CodePoint &lhs, const Char &rhs)
{ return lhs == rhs(); }
inline bool operator!= (const Char::CodePoint &lhs, const Char &rhs)
{ return lhs != rhs(); }
inline bool operator< (const Char::CodePoint &lhs, const Char &rhs)
{ return lhs < rhs(); }
inline bool operator> (const Char::CodePoint &lhs, const Char &rhs)
{ return lhs > rhs(); }
inline bool operator<= (const Char::CodePoint &lhs, const Char &rhs)
{ return lhs <= rhs(); }
inline bool operator>= (const Char::CodePoint &lhs, const Char &rhs)
{ return lhs >= rhs(); }

#endif // CHAR_HPP
