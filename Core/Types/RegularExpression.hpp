#ifndef REGULAREXPRESSION_HPP
#define REGULAREXPRESSION_HPP

#include "BitwiseFlags.hpp"

#include "UnicodeString.hpp"

class RegularExpressionMatch;
class RegularExpressionMatchIterator;
struct RegularExpressionPrivate;
class RegularExpression;

class RegularExpression
{
public:
    enum PatternOption {
        NoPatternOption                 = 0x0000,
        CaseInsensitiveOption           = 0x0001,
        DotMatchesEverythingOption      = 0x0002,
        MultilineOption                 = 0x0004,
        ExtendedPatternSyntaxOption     = 0x0008,
        InvertedGreedinessOption        = 0x0010,
        DontCaptureOption               = 0x0020,
        UseUnicodePropertiesOption      = 0x0040,
    };
    DECLARE_BITWISE_FLAGS(PatternOptions, PatternOption)

    PatternOptions patternOptions() const;
    void setPatternOptions(PatternOptions options);

    RegularExpression();
    explicit RegularExpression(const UnicodeString &pattern, PatternOptions options = NoPatternOption);
    RegularExpression(const RegularExpression &re);
    ~RegularExpression();
    RegularExpression &operator=(const RegularExpression &re);
    RegularExpression &operator=(const UnicodeString &re);

    UnicodeString pattern() const;
    void setPattern(const UnicodeString &pattern);

    bool isValid() const;
    int patternErrorOffset() const;
    UnicodeString errorString() const;

    int captureCount() const;
    UnicodeStringVector namedCaptureGroups() const;

    enum MatchType {
        NormalMatch = 0,
        PartialPreferCompleteMatch,
        PartialPreferFirstMatch,
        NoMatch
    };

    enum MatchOption {
        NoMatchOption                     = 0x0000,
        AnchoredMatchOption               = 0x0001,
        DontCheckSubjectStringMatchOption = 0x0002
    };
    DECLARE_BITWISE_FLAGS(MatchOptions, MatchOption)

    RegularExpressionMatch match(const UnicodeString &subject,
                                 int offset                = 0,
                                 MatchType matchType       = NormalMatch,
                                 MatchOptions matchOptions = NoMatchOption) const;

    RegularExpressionMatchIterator globalMatch(const UnicodeString &subject,
                                               int offset                = 0,
                                               MatchType matchType       = NormalMatch,
                                               MatchOptions matchOptions = NoMatchOption) const;

    UnicodeStringVector split(const UnicodeString &subject) const;

    static UnicodeString escape(const UnicodeString &str);

    bool operator==(const RegularExpression &re) const;
    inline bool operator!=(const RegularExpression &re) const { return !operator==(re); }

private:
    friend struct RegularExpressionPrivate;
    friend class RegularExpressionMatch;
    friend struct RegularExpressionMatchPrivate;
    friend class RegularExpressionMatchIterator;

    RegularExpression(RegularExpressionPrivate &dd);
    RegularExpressionPrivate *d = nullptr;
};

DECLARE_OPERATORS_FOR_BITWISE_FLAGS(RegularExpression::PatternOptions)
DECLARE_OPERATORS_FOR_BITWISE_FLAGS(RegularExpression::MatchOptions)

struct RegularExpressionMatchPrivate;
class RegularExpressionMatch
{
public:
    RegularExpressionMatch();
    ~RegularExpressionMatch();
    RegularExpressionMatch(const RegularExpressionMatch &match);
    RegularExpressionMatch &operator=(const RegularExpressionMatch &match);

    RegularExpression regularExpression() const;
    RegularExpression::MatchType matchType() const;
    RegularExpression::MatchOptions matchOptions() const;

    bool hasMatch() const;
    bool hasPartialMatch() const;

    bool isValid() const;

    int lastCapturedIndex() const;

    UnicodeString captured(int nth = 0) const;
    UnicodeString captured(const UnicodeString &name) const;
    UnicodeStringVector capturedTexts() const;

    int capturedStart(int nth = 0) const;
    int capturedLength(int nth = 0) const;
    int capturedEnd(int nth = 0) const;

    int capturedStart(const UnicodeString &name) const;
    int capturedLength(const UnicodeString &name) const;
    int capturedEnd(const UnicodeString &name) const;

private:
    friend class RegularExpression;
    friend struct RegularExpressionMatchPrivate;
    friend class RegularExpressionMatchIterator;

    RegularExpressionMatch(RegularExpressionMatchPrivate &dd);
    RegularExpressionMatchPrivate *d = nullptr;
};

struct RegularExpressionMatchIteratorPrivate;
class RegularExpressionMatchIterator
{
public:
    RegularExpressionMatchIterator();
    ~RegularExpressionMatchIterator();
    RegularExpressionMatchIterator(const RegularExpressionMatchIterator &iterator);
    RegularExpressionMatchIterator &operator=(const RegularExpressionMatchIterator &iterator);

    bool isValid() const;

    bool hasNext() const;
    RegularExpressionMatch next();
    RegularExpressionMatch peekNext() const;

    RegularExpression regularExpression() const;
    RegularExpression::MatchType matchType() const;
    RegularExpression::MatchOptions matchOptions() const;

private:
    friend class RegularExpression;

    RegularExpressionMatchIterator(RegularExpressionMatchIteratorPrivate &dd);
    RegularExpressionMatchIteratorPrivate *d = nullptr;
};

#endif // REGULAREXPRESSION_HPP
