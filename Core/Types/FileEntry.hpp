#ifndef FILEENTRY_HPP
#define FILEENTRY_HPP

#include "String.hpp"

#include <sys/stat.h>
#include <cinttypes>

class FileEntry;
class Directory;

using FileEntryList = Vector<FileEntry>;

// UTC Timestamp
struct Timestamp final
{
    // converts from UNIX epoch time
    static Timestamp from_timespec(const struct timespec &_timespec);
    static Timestamp from_time_t(const std::time_t &_time_t);

    // constructs a Timestamp object from the current system time (UTC)
    static Timestamp current();

    // initialize a Timestamp object from the given calendar date and time
    static Timestamp make_timestamp(const std::uint16_t &year,
                                    const std::uint8_t &month,
                                    const std::uint8_t &day,
                                    const std::uint8_t &hours,
                                    const std::uint8_t &minutes,
                                    const std::uint8_t &seconds);

    // constructs an empty timestamp (every field set to zero)
    static Timestamp make_null();

    // copy constructor
    Timestamp(const Timestamp &other);

    // calendar date and time
    const std::uint16_t year;
    const std::uint8_t  month;
    const std::uint8_t  day;
    const std::uint8_t  hours;
    const std::uint8_t  minutes;
    const std::uint8_t  seconds;

    // UNIX epoch time
    const std::uint64_t total_seconds;

    // comparision operators
    inline bool operator== (const Timestamp &other) const
    { return total_seconds == other.total_seconds; }
    inline bool operator!= (const Timestamp &other) const
    { return total_seconds != other.total_seconds; }
    inline bool operator< (const Timestamp &other) const
    { return total_seconds < other.total_seconds; }
    inline bool operator> (const Timestamp &other) const
    { return total_seconds > other.total_seconds; }
    inline bool operator<= (const Timestamp &other) const
    { return total_seconds <= other.total_seconds; }
    inline bool operator>= (const Timestamp &other) const
    { return total_seconds >= other.total_seconds; }

private:
    friend class FileEntry;

    Timestamp();
    Timestamp(const std::uint16_t &year,
              const std::uint8_t &month,
              const std::uint8_t &day,
              const std::uint8_t &hours,
              const std::uint8_t &minutes,
              const std::uint8_t &seconds,
              const std::uint64_t &total_seconds);
};

class FileEntry final
{
    // disable copy
    //auto operator= (const FileEntry&) = delete;
    //FileEntry(const FileEntry&) = delete;

public:
    FileEntry(); // constructs an invalid FileEntry object
    FileEntry(const String &file);
    FileEntry(const Directory &dir);
    FileEntry(const Directory &dir, const String &file);
    ~FileEntry();

    void refresh(); // runs stat() again, results are cached in the object

    const String &name() const; // file name
    const String &path() const; // file path

    const String &full_path() const; // path + name

    // is FileEntry object valid
    inline bool is_valid() const
    { return _valid; }

    // file exists
    inline bool exists() const
    { return _exists; }

    // is regular file
    bool is_file() const;

    // is directory
    bool is_directory() const;

    // permissions for current user
    bool is_readable() const;
    bool is_writable() const;
    bool is_executable() const;
    bool is_rw() const; // read+write
    bool is_rwx() const; // all permissions

    // permissions for owner
    bool is_owner_readable() const;
    bool is_owner_writable() const;
    bool is_owner_executable() const;
    bool is_owner_rw() const; // read+write
    bool is_owner_rwx() const; // all permissions

    // permissions for user group
    bool is_group_readable() const;
    bool is_group_writable() const;
    bool is_group_executable() const;
    bool is_group_rw() const; // read+write
    bool is_group_rwx() const; // all permissions

    // permissions for others
    bool is_other_readable() const;
    bool is_other_writable() const;
    bool is_other_executable() const;
    bool is_other_rw() const; // read+write
    bool is_other_rwx() const; // all permissions

    // are file permissions 0777 (handy alias)
    bool is_0777() const;

    // SUID and GUID flags
    bool has_suid() const;
    bool has_guid() const;

    // file size in bytes
    off_t filesize() const;

    // inode number
    ino_t inode() const;
    // number of hard links pointing to that file
    nlink_t hard_links() const;

    // ownership ids
    uid_t uid() const;
    gid_t gid() const;

    // ownership names
    const String uid_name() const;
    const String gid_name() const;

    // timestamps
    const Timestamp birthtime() const; // file creation time (*BSD only, uses ctime() on POSIX)
    const Timestamp atime() const;     // last access time
    const Timestamp mtime() const;     // last data modification time
    const Timestamp ctime() const;     // last file status change time

private:
    String _file;
    struct stat _sb;
    bool _valid = false;
    bool _exists = false;

    // buffers to avoid constant processing on many calls
    String _name, _path;
};

#endif // FILEENTRY_HPP
