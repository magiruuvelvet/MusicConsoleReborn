#include "RegExp.hpp"

RegExp::RegExp()
{
}

RegExp::RegExp(const std::string &regex)
{
    try {
        _regex = std::regex(regex);
    } catch (std::regex_error err) {
        std::printf("RegExp(%s): %s\n", regex.data(), err.what());
        _regex = std::regex();
    }
    _pattern = regex;
}

RegExp::RegExp(const char *regex)
    : RegExp(std::string(regex))
{
}
