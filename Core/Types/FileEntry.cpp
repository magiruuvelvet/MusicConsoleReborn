#include "FileEntry.hpp"

#include <cstdio>
#include <cstdlib>
#include <ctime>

#include <unistd.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>

#include "Directory.hpp"

// copy constructor
Timestamp::Timestamp(const Timestamp &other)
    : year(other.year),
      month(other.month),
      day(other.day),
      hours(other.hours),
      minutes(other.minutes),
      seconds(other.seconds),
      total_seconds(other.total_seconds)
{ }

// construct invalid timestamp
Timestamp::Timestamp()
    : year(0),
      month(0),
      day(0),
      hours(0),
      minutes(0),
      seconds(0),
      total_seconds(0)
{ }

// private initialization constructor (total_seconds must match)
Timestamp::Timestamp(
          const std::uint16_t &year,
          const std::uint8_t &month,
          const std::uint8_t &day,
          const std::uint8_t &hours,
          const std::uint8_t &minutes,
          const std::uint8_t &seconds,
          const std::uint64_t &total_seconds)
    : year(year),
      month(month),
      day(day),
      hours(hours),
      minutes(minutes),
      seconds(seconds),
      total_seconds(total_seconds)
{ }

Timestamp Timestamp::from_timespec(const struct timespec &_timespec)
{
    const std::time_t epoch = _timespec.tv_sec;
    struct std::tm results;

    (void) gmtime_r(&epoch, &results);

    Timestamp ts(
        (std::uint16_t) results.tm_year + 1900, // since 1900, convert to real year
        (std::uint8_t)  results.tm_mon + 1,     // [1-12]
        (std::uint8_t)  results.tm_mday,        // month day [1-31]
        (std::uint8_t)  results.tm_hour,        // [0-23]
        (std::uint8_t)  results.tm_min,         // [0-59]
        (std::uint8_t)  results.tm_sec,         // [0-60] (1 leap second)
        (std::uint64_t) _timespec.tv_sec);
    return ts;
}

Timestamp Timestamp::from_time_t(const std::time_t &_time_t)
{
    timespec ts{_time_t, 0};
    return from_timespec(ts);
}

Timestamp Timestamp::current()
{
    auto now = std::time(nullptr);
    return from_time_t(now);
}

Timestamp Timestamp::make_timestamp(const std::uint16_t &year,
                                    const std::uint8_t &month,
                                    const std::uint8_t &day,
                                    const std::uint8_t &hours,
                                    const std::uint8_t &minutes,
                                    const std::uint8_t &seconds)
{
    struct tm t = {}; // Initalize all values to 0
    t.tm_year = year - 1900;
    t.tm_mon = month - 1;
    t.tm_mday = day;
    t.tm_hour = hours;
    t.tm_min = minutes;
    t.tm_sec = seconds;
    std::time_t epoch = timegm(&t);
    return from_time_t(epoch);
}

Timestamp Timestamp::make_null()
{
    return Timestamp(0,0,0,0,0,0,0);
}

FileEntry::FileEntry()
{
}

FileEntry::FileEntry(const String &file)
{
    _file = Directory::normalize(file);
    _name = _file.mid(_file.find_last_of('/') + 1);
    _path = _file.mid(0, _file.find_last_of('/'));
    _exists = stat(*_file, &_sb) == 0;
    _valid = true;
}

FileEntry::FileEntry(const Directory &dir)
    : FileEntry(dir.dir())
{
}

FileEntry::FileEntry(const Directory &dir, const String &file)
    : FileEntry(dir.dir() + '/' + file)
{
}

FileEntry::~FileEntry()
{
    _file.clear();
    _name.clear();
    _path.clear();
}

void FileEntry::refresh()
{
    _exists = stat(*_file, &_sb) == 0;
}

const String &FileEntry::name() const
{
    return _name;
}

const String &FileEntry::path() const
{
    return _path;
}

const String &FileEntry::full_path() const
{
    return _file;
}

bool FileEntry::is_file() const
{
    return _valid && _exists ? S_ISREG(_sb.st_mode) : false;
}

bool FileEntry::is_directory() const
{
    return _valid && _exists ? S_ISDIR(_sb.st_mode) : false;
}

// permissions for current user
bool FileEntry::is_readable() const
{ return access(*_file, R_OK) == 0; }
bool FileEntry::is_writable() const
{ return access(*_file, W_OK) == 0; }
bool FileEntry::is_executable() const
{ return access(*_file, X_OK) == 0; }
bool FileEntry::is_rw() const
{ return is_readable() && is_writable(); }
bool FileEntry::is_rwx() const
{ return is_readable() && is_writable() && is_executable(); }

// owner permissions
bool FileEntry::is_owner_readable() const
{ return _valid && _exists ? _sb.st_mode & S_IRUSR : false; }
bool FileEntry::is_owner_writable() const
{ return _valid && _exists ? _sb.st_mode & S_IWUSR : false; }
bool FileEntry::is_owner_executable() const
{ return _valid && _exists ? _sb.st_mode & S_IXUSR : false; }
bool FileEntry::is_owner_rw() const
{ return is_owner_readable() && is_owner_writable(); }
bool FileEntry::is_owner_rwx() const
{ return _valid && _exists ? _sb.st_mode & S_IRWXU : false; }

// group permissions
bool FileEntry::is_group_readable() const
{ return _valid && _exists ? _sb.st_mode & S_IRGRP : false; }
bool FileEntry::is_group_writable() const
{ return _valid && _exists ? _sb.st_mode & S_IWGRP : false; }
bool FileEntry::is_group_executable() const
{ return _valid && _exists ? _sb.st_mode & S_IXGRP : false; }
bool FileEntry::is_group_rw() const
{ return is_group_readable() && is_group_writable(); }
bool FileEntry::is_group_rwx() const
{ return _valid && _exists ? _sb.st_mode & S_IRWXG : false;}

// other permissions
bool FileEntry::is_other_readable() const
{ return _valid && _exists ? _sb.st_mode & S_IROTH : false; }
bool FileEntry::is_other_writable() const
{ return _valid && _exists ? _sb.st_mode & S_IWOTH : false; }
bool FileEntry::is_other_executable() const
{ return _valid && _exists ? _sb.st_mode & S_IXOTH : false; }
bool FileEntry::is_other_rw() const
{ return is_other_readable() && is_other_writable(); }
bool FileEntry::is_other_rwx() const
{ return _valid && _exists ? _sb.st_mode & S_IRWXO : false; }

bool FileEntry::is_0777() const
{
    return _valid && _exists ? _sb.st_mode & ACCESSPERMS : false;
}

// SUID and GUID flags
bool FileEntry::has_suid() const
{ return _valid && _exists ? _sb.st_mode & S_ISUID : false; }
bool FileEntry::has_guid() const
{ return _valid && _exists ? _sb.st_mode & S_ISGID : false; }

off_t FileEntry::filesize() const
{
    return _valid && _exists ? _sb.st_size : 0;
}

ino_t FileEntry::inode() const
{
    return _valid && _exists ? _sb.st_ino : 0;
}

nlink_t FileEntry::hard_links() const
{
    return _valid && _exists ? _sb.st_nlink : 0;
}

uid_t FileEntry::uid() const
{
    return _valid && _exists ? _sb.st_uid : 0;
}

gid_t FileEntry::gid() const
{
    return _valid && _exists ? _sb.st_gid : 0;
}

const String FileEntry::uid_name() const
{
    struct passwd *pw = getpwuid(uid());
    if (pw)
        return String(pw->pw_name);
    return {};
}

const String FileEntry::gid_name() const
{
    struct group *gw = getgrgid(gid());
    if (gw)
        return String(gw->gr_name);
    return {};
}

// file creation time
const Timestamp FileEntry::birthtime() const
{
#if defined(OS_BSD) || defined(OS_MACOS)
    // use actual birth time on *BSD compatible operating systems
    return _valid && _exists ?
        Timestamp::from_timespec(_sb.st_birthtim) : Timestamp();
#else
    if (!(_valid && _exists))
        return Timestamp();

    // find oldest timestamp on systems which don't support birthtime in stat()
    struct timespec time_to_use;
    if (_sb.st_atime < _sb.st_mtime && _sb.st_atime < _sb.st_ctime)
        time_to_use = _sb.st_atim;
    else if (_sb.st_mtime < _sb.st_ctime)
        time_to_use = _sb.st_mtim;
    else
        time_to_use = _sb.st_ctim;

    return Timestamp::from_timespec(time_to_use);
#endif
}

// last access time
const Timestamp FileEntry::atime() const
{
    return _valid && _exists ?
        Timestamp::from_timespec(_sb.st_atim) : Timestamp();
}

// last data modification time
const Timestamp FileEntry::mtime() const
{
    return _valid && _exists ?
        Timestamp::from_timespec(_sb.st_mtim) : Timestamp();
}

// last file status change time
const Timestamp FileEntry::ctime() const
{
    return _valid && _exists ?
        Timestamp::from_timespec(_sb.st_ctim) : Timestamp();
}
