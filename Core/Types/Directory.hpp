#ifndef DIRECTORY_HPP
#define DIRECTORY_HPP

#include "String.hpp"
#include "FileEntry.hpp"
#include "BitwiseFlags.hpp"

class Directory final
{
    // disable copy
    //auto operator= (const Directory&) = delete;
    //Directory(const Directory&) = delete;

public:
    Directory(); // constructs and invalid Directory object
    Directory(const String &dir);
    Directory(const FileEntry &fe);
    ~Directory();

    enum SortingFlag {
        NoSortingFlags        = 0x00, // no sorting applied (default)
        DirectoriesFirst      = 0x01, // directories are placed on top
        FilesFirst            = 0x02, // files are placed on top
        AlphabeticallyAscii   = 0x04, // sorts list alphabetically (ASCII only)
        AlphabeticallyUnicode = 0x08, // sorts list alphabetically in any language (slower than ASCII version)
    };
    DECLARE_BITWISE_FLAGS(SortingFlags, SortingFlag)

    enum FilterFlag {
        NoFilterFlags       = 0x00, // no filters applied (default)
        DirectoriesOnly     = 0x01, // only adds directories
        FilesOnly           = 0x02, // only adds files
        NoDotAndDotDot      = 0x04, // removes "." and ".." from the entry lists
        NoHiddenFiles       = 0x08, // removes entries starting with a dot
    };
    DECLARE_BITWISE_FLAGS(FilterFlags, FilterFlag)

    // check for directory existence, returns false on files
    static bool exists(const String &dir);
    inline bool exists() const
    { return exists(_dir); }

    bool cd(const String &path);
    bool cdUp();

    // returns the directory as string
    inline const String &dir() const
    { return this->_dir; }

    // variadic cd() method
    template<typename... Path>
    bool cd(Path... path)
    {
        const StringList paths = {path...};
        String new_path;
        for (auto&& p : paths)
            new_path += p + '/';
        new_path.erase(--new_path.end());
        return cd(new_path);
    }

    // attempts to create a directory, returns true on success or when dir already exists
    bool mkdir(const String &name) const;

    // attempts to remove a directory, returns true on success,
    // if dir doesn't exists anymore false is returned, use exists() beforehand
    bool rmdir(const String &name) const;

    // recursive mkdir(), returns true on success or when dir already exists
    bool mkpath(const String &path) const;

    // recursive rmdir(), returns true on success,
    // if dir doesn't exists anymore false is returned, use exists() beforehand
    bool rmpath(const String &path) const;

    // attempts to remove a file, returns true on success
    bool remove(const String &filename) const;

    // initialize a directory object from the current working directory
    static const Directory cwd();
    static const String cwdStr();

    // initialize a directory object from the root directory
    static const Directory root();
    static const String rootStr();

    // remove redundant directory separators
    // path may or may not be valid for this operation
    // takes a relative or absolute path
    // supports files too
    static const String normalize(const String &path);

    // canonicalizes the path, removes redundant dir separators, resolves symlinks and ".", ".."
    // path must be valid, returns empty string on failure, optimal error checking with [ok]
    // relative paths are relative to the current working directory
    // supports files too
    static const String canonicalize(const String &path, bool *ok = nullptr);

    // is directory object valid
    static bool valid(const String &path);
    inline bool valid() const
    { return valid(_dir); }

    static bool is_absolute(const String &path);
    inline bool is_absolute() const
    { return is_absolute(_dir); }

    static bool is_relative(const String &path);
    inline bool is_relative() const
    { return is_relative(_dir); }

    // readable by current user
    static bool is_readable(const String &path);
    static bool is_readable(const Directory &dir);
    inline bool is_readable() const
    { return is_readable(*this); }

    // writable by current user
    static bool is_writable(const String &path);
    static bool is_writable(const Directory &dir);
    inline bool is_writable() const
    { return is_writable(*this); }

    // receive all current directory entries, supports several sorting and filter methods
    const StringList entries(const SortingFlags &sorting = NoSortingFlags, const FilterFlags &filter = NoFilterFlags) const;
    const FileEntryList file_entries(const SortingFlags &sorting = NoSortingFlags, const FilterFlags &filter = NoFilterFlags) const;

    // constructs a FileEntry object from current path
    const FileEntry self_entry() const;

private:
    String _dir;

    bool mkpath_internal(const char *path) const;
    bool rmpath_internal(const char *path) const;
};

DECLARE_OPERATORS_FOR_BITWISE_FLAGS(Directory::SortingFlags)
DECLARE_OPERATORS_FOR_BITWISE_FLAGS(Directory::FilterFlags)

#endif // DIRECTORY_HPP
