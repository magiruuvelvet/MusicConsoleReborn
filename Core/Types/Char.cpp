#include "Char.hpp"

// note: UCS-4 == UTF-32 (identical code point values)

#include <string>
#include <vector>
#include <memory>

#include <unicode/uchar.h>
#include <unicode/uscript.h>
#include <unicode/uclean.h>

// helper functions which aren't in ICU
#include "Private/utf8.h"

// silence <urename.h> warnings about recursive macros
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdisabled-macro-expansion"
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdisabled-macro-expansion"

bool Char::is_space(const CodePoint &ch)
{ return u_isUWhiteSpace(ch); }

bool Char::is_uppercase(const CodePoint &ch)
{ return u_isUUppercase(ch); }

bool Char::is_lowercase(const CodePoint &ch)
{ return u_isULowercase(ch); }

bool Char::is_titlecase(const CodePoint &ch)
{ return u_istitle(ch); }

bool Char::is_alphabetic(const CodePoint &ch)
{ return u_isUAlphabetic(ch); }

bool Char::is_alphanumeric(const CodePoint &ch)
{ return u_isalnum(ch); }

bool Char::is_control_character(const CodePoint &ch)
{ return u_iscntrl(ch); }

bool Char::is_blank(const CodePoint &ch)
{ return u_isblank(ch); }

bool Char::is_punctuation(const CodePoint &ch)
{ return u_ispunct(ch); }

bool Char::is_printable(const CodePoint &ch)
{ return u_isprint(ch); }

bool Char::is_digit(const CodePoint &ch)
{ return u_isdigit(ch); }

bool Char::is_non_character(const CodePoint &ch)
{ return ch >= 0xfdd0 && (ch <= 0xfdef || (ch & 0xfffe) == 0xfffe); }

bool Char::is_high_surrogate(const CodePoint &ch)
{ return ((ch & 0xfffffc00) == 0xd800); }

bool Char::is_low_surrogate(const CodePoint &ch)
{ return ((ch & 0xfffffc00) == 0xdc00); }

bool Char::is_surrogate(const CodePoint &ch)
{ return (ch - 0xd800u < 2048u); }

bool Char::requires_surrogates(const CodePoint &ch)
{ return (ch >= 0x10000); }

Char Char::to_high_surrogate(const CodePoint &ch)
{ return Char( (ch >> 10) + 0xd7c0 ); }

Char Char::to_low_surrogate(const CodePoint &ch)
{ return Char( ch % 0x400 + 0xdc00 ); }

int32_t Char::digit_basic(const Char &ch, int8_t radix)
{ return u_digit(ch(), radix); }
int32_t Char::digit_basic(const CodePoint &ch, int8_t radix)
{ return u_digit(ch, radix); }

int32_t Char::digit(const Char &ch)
{ return u_charDigitValue(ch()); }
int32_t Char::digit(const CodePoint &ch)
{ return u_charDigitValue(ch); }

double Char::numeric(const Char &ch, bool *ok)
{
    const auto &res = u_getNumericValue(ch());
    // error
    if (res == U_NO_NUMERIC_VALUE)
    {
        if (ok) *ok = false;
        return double{0};
    }
    if (ok) *ok = true;
    return res;
}

Char Char::to_uppercase(const Char &ch)
{ return u_toupper(ch()); }
Char Char::to_uppercase(const CodePoint &ch)
{ return u_toupper(ch); }

Char Char::to_lowercase(const Char &ch)
{ return u_tolower(ch()); }
Char Char::to_lowercase(const CodePoint &ch)
{ return u_tolower(ch); }

Char Char::to_titlecase(const Char &ch)
{ return u_totitle(ch()); }
Char Char::to_titlecase(const CodePoint &ch)
{ return u_totitle(ch); }

Char Char::fold_case(const Char &ch)
{ return u_foldCase(ch(), U_FOLD_CASE_DEFAULT); }
Char Char::fold_case(const CodePoint &ch)
{ return u_foldCase(ch, U_FOLD_CASE_DEFAULT); }

bool Char::is_cased(const CodePoint &ch)
{
    return uscript_isCased(static_cast<UScriptCode>(script(ch)));
}

Char::Script Char::script(const CodePoint &ch)
{
    UErrorCode error = U_ZERO_ERROR;
    u_init(&error);
    return static_cast<Script>(uscript_getScript(ch, &error));
}

Char::Category Char::category(const CodePoint &ch)
{
    return static_cast<Category>(u_charType(ch));
}

Char::Block Char::block(const CodePoint &ch)
{
    return static_cast<Block>(ublock_getCode(ch));
}

Char::Direction Char::direction(const CodePoint &ch)
{
    return static_cast<Direction>(u_charDirection(ch));
}

const std::string Char::script_name(const CodePoint &ch)
{
    const auto *scr = uscript_getName(static_cast<UScriptCode>(script(ch)));
    return scr ? std::string(scr) : std::string();
}

const std::string Char::script_shortname(const CodePoint &ch)
{
    const auto *scr = uscript_getShortName(static_cast<UScriptCode>(script(ch)));
    return scr ? std::string(scr) : std::string();
}

const std::string Char::name(const CodePoint &ch)
{
    UErrorCode error = U_ZERO_ERROR;
    u_init(&error);
    std::unique_ptr<char> buffer(new char[512]);
    u_charName(ch, U_UNICODE_CHAR_NAME, buffer.get(), 512, &error);
    return U_SUCCESS(error) ? std::string(buffer.get()) : std::string();
}

const std::string Char::utf8() const
{
    std::string buffer;
    std::vector<UChar32> src{this->cp};
    utf8::utf32to8(src.begin(), src.end(), std::back_inserter(buffer));
    return buffer;
}

Char Char::surrogate_to_utf32(utf16_t high, utf16_t low)
{ return (high << 10) + low - 0x35fdc00; }

#pragma clang diagnostic pop
#pragma GCC diagnostic pop
