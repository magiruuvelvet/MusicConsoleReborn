#ifndef SYSTEMINFO_HPP
#define SYSTEMINFO_HPP

#include "String.hpp"

#include <array>
#include <cstdint>

class SystemInfo final
{
    SystemInfo() = delete;

public:
    using u8  = std::uint8_t;
    using u16 = std::uint16_t;
    using u32 = std::uint32_t;
    using u64 = std::uint64_t;

    using s8  = std::int8_t;
    using s16 = std::int16_t;
    using s32 = std::int32_t;
    using s64 = std::int64_t;

    static inline std::array<u32, 4> get_cpuid(u32 func, u32 subfunc)
    {
        int regs[4];
        __asm__ volatile("cpuid" : "=a" (regs[0]), "=b" (regs[1]), "=c" (regs[2]), "=d" (regs[3]) : "a" (func), "c" (subfunc));
        return {0u+regs[0], 0u+regs[1], 0u+regs[2], 0u+regs[3]};
    }

    static inline u64 get_xgetbv(u32 xcr)
    {
        u32 eax, edx;
        __asm__ volatile( "xgetbv" : "=a"(eax), "=d"(edx) : "c"(xcr));
        return eax | (u64(edx) << 32);
    }

    static bool has_sse();
    static bool has_sse2();
    static bool has_ssse3();
    static bool has_sse41();
    static bool has_sse42();
    static bool has_htt();
    static bool has_avx();
    static bool has_avx2();
    static bool has_rtm();
    static bool has_512();
    static bool has_xop();

    static String get_system_info();
    static String get_os_name();
};

#endif // SYSTEMINFO_HPP
