#ifndef REGEXP_HPP
#define REGEXP_HPP

#include <regex>

class RegExp
{
public:
    // explicit required to avoid a conflict with the String class
    explicit RegExp();
    explicit RegExp(const std::string &regex);
    explicit RegExp(const char *regex);

    // receive the regular expression object
    inline std::regex &regex()
    { return _regex; }
    inline operator std::regex() &
    { return _regex; }

    inline const std::regex &regex() const
    { return _regex; }
    inline operator const std::regex() const&
    { return _regex; }

    // receive the original pattern of the regular expression
    inline const std::string &pattern() const
    { return _pattern; }

private:
    std::regex _regex;
    std::string _pattern;
};

#endif // REGEXP_HPP
