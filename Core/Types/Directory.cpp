#include "Directory.hpp"

#include "UnicodeString.hpp"

#include <cstdio>
#include <cstdlib>
#include <climits>
#include <memory>

#include <unistd.h>
#include <ftw.h>            // file tree walk
#include <dirent.h>
#include <sys/stat.h>
#if defined(OS_BSD) || defined(OS_MACOS)
#include <sys/dirent.h>
#endif

Directory::Directory()
{
}

Directory::Directory(const String &_dir)
{
    if (valid(_dir))
        this->_dir = normalize(_dir);
}

Directory::Directory(const FileEntry &fe)
{
    if (valid(fe.full_path()))
        this->_dir = normalize(fe.full_path());
}

Directory::~Directory()
{
    _dir.clear();
}

bool Directory::exists(const String &dir)
{
    return valid(dir);
}

bool Directory::cd(const String &path)
{
    if (path == ".")
        return true;
    else if (path == "..")
        return cdUp();

    if (is_absolute(path) && valid(path))
    {
        const auto &p = path.split('/', String::SkipEmptyParts);
        _dir = '/' + p.join("/");
        return true;
    }
    else if (is_relative(path))
    {
        if (!valid() || !valid(_dir + '/' + path))
            return false;

        const auto &p = path.split('/', String::SkipEmptyParts);
        _dir += '/' + p.join("/");
        return true;
    }
    return false;
}

bool Directory::cdUp()
{
    auto pos = _dir.find_last_of('/');
    if (pos == String::npos || _dir.size() <= 1)
        return false;
    _dir = _dir.mid(0, pos);
    return true;
}

bool Directory::mkdir(const String &name) const
{
    if (!valid() || name.empty() || name.contains('/'))
        return false;

    String realpath;
    if (is_relative())
        realpath = canonicalize(cwdStr() + '/' + dir()) + '/' + normalize(name);
    else
        realpath = canonicalize(dir()) + '/' + normalize(name);

    if (realpath.empty())
        return false;

    // return true if directory already exists
    if (exists(realpath))
        return true;

    return ::mkdir(*realpath, 0755) == 0; // rwxr-xr-x
}

bool Directory::rmdir(const String &name) const
{
    if (!valid() || name.empty() || name.contains('/'))
        return false;

    String realpath;
    if (is_relative())
        realpath = canonicalize(cwdStr() + '/' + dir()) + '/' + normalize(name);
    else
        realpath = canonicalize(dir()) + '/' + normalize(name);

    if (realpath.empty() || !FileEntry(realpath).is_directory())
        return false;

    return ::rmdir(*realpath) == 0;
}

bool Directory::mkpath(const String &path) const
{
    if (!valid() || path.empty())
        return false;

    String rpath;
    if (is_relative())
        rpath = canonicalize(cwdStr() + '/' + dir()) + '/' + normalize(path);
    else
        rpath = canonicalize(dir()) + '/' + normalize(path);

    if (rpath.empty())
        return false;

    return mkpath_internal(*rpath);
}

bool Directory::mkpath_internal(const char *path) const
{
    const size_t len = strlen(path);
    char _path[PATH_MAX];
    char *p;

    errno = 0;

    // Copy string so its mutable
    if (len > sizeof(_path) - 1)
    {
        errno = ENAMETOOLONG;
        return false;
    }
    strcpy(_path, path);

    // Iterate the string
    for (p = _path + 1; *p; p++)
    {
        if (*p == '/')
        {
            // Temporarily truncate
            *p = '\0';

            if (::mkdir(_path, 0755) != 0)
            {
                if (errno != EEXIST)
                    return false;
            }

            *p = '/';
        }
    }

    if (::mkdir(_path, 0755) != 0)
    {
        if (errno != EEXIST)
            return false;
    }

    return true;
}

bool Directory::rmpath(const String &path) const
{
    if (!valid() || path.empty())
        return false;

    String rpath;
    if (is_relative())
        rpath = canonicalize(cwdStr() + '/' + dir() + '/' + normalize(path));
    else
        rpath = canonicalize(dir() + '/' + normalize(path));

    if (rpath.empty() || !FileEntry(rpath).is_directory())
        return false;

    return rmpath_internal(*rpath);
}

bool Directory::rmpath_internal(const char *path) const
{
    std::size_t path_len;
    char *full_path;
    DIR *dir;
    struct stat stat_path, stat_entry;
    struct dirent *entry;

    // stat for the path
    stat(path, &stat_path);

    // if path does not exists or is not dir
    if (S_ISDIR(stat_path.st_mode) == 0)
        return false;

    // if not possible to read the directory for this user
    if ((dir = opendir(path)) == nullptr)
        return false;

    // the length of the path
    path_len = strlen(path);

    // iteration through entries in the directory
    while ((entry = readdir(dir)) != nullptr) {

        // skip entries "." and ".."
        if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, ".."))
            continue;

        // determinate a full path of an entry
        full_path = std::move<char*>(static_cast<char*>((std::calloc(path_len + strlen(entry->d_name) + 1, sizeof(char)))));
        strcpy(full_path, path);
        strcat(full_path, "/");
        strcat(full_path, entry->d_name);

        // stat for the entry
        stat(full_path, &stat_entry);

        // recursively remove a nested directory
        if (S_ISDIR(stat_entry.st_mode) != 0) {
            if (rmpath_internal(full_path))
                continue;
            else
                return false;
        }

        // remove a file object
        bool status = unlink(full_path) == 0;
        if (!status)
            return false;
    }

    // remove the devastated directory and close the object of it
    bool status;
    if (::rmdir(path) == 0)
        status = true;
    else
        status = false;

    closedir(dir);
    return status;
}

bool Directory::remove(const String &filename) const
{
    if (!valid() || filename.empty())
        return false;

    String rpath;
    if (is_relative())
        rpath = canonicalize(cwdStr() + '/' + dir() + '/' + normalize(filename));
    else
        rpath = canonicalize(dir() + '/' + normalize(filename));

    if (rpath.empty())
        return false;

    return ::remove(*rpath) == 0;
}

const Directory Directory::cwd()
{
    char buf[PATH_MAX + 1];
    if (!getcwd(buf, sizeof(buf)))
        return {};
    return Directory(buf);
}

const String Directory::cwdStr()
{
    return cwd().dir();
}

const Directory Directory::root()
{
    return Directory("/");
}

const String Directory::rootStr()
{
    return root().dir();
}

const String Directory::normalize(const String &path)
{
    auto is_abs = is_absolute(path);
    const auto &p = path.split('/', String::SkipEmptyParts);
    const auto &normalized = p.join("/");
    return is_abs ? String('/' + normalized) : normalized;
}

const String Directory::canonicalize(const String &path, bool *ok)
{
    if (path.empty() || !FileEntry(path).exists())
    {
        if (ok) *ok = false;
        return {};
    }

    char buf[PATH_MAX + 1];
    char *res = realpath(*path, buf);
    if (res)
    {
        if (ok) *ok = true;
        return String(buf);
    }

    if (ok) *ok = false;
    return {};
}

bool Directory::valid(const String &path)
{
    if (path.empty())
        return false;

    FileEntry p(path);
    return p.exists() && p.is_directory();
}

bool Directory::is_absolute(const String &path)
{
    return path.starts_with('/');
}

bool Directory::is_relative(const String &path)
{
    return !path.starts_with('/');
}

bool Directory::is_readable(const String &path)
{
    return is_readable(Directory(path));
}

bool Directory::is_readable(const Directory &dir)
{
    const auto &self_e = dir.self_entry();
    return self_e.is_readable() &&
           self_e.is_executable(); // directory enter permission
}

bool Directory::is_writable(const String &path)
{
    return is_writable(Directory(path));
}

bool Directory::is_writable(const Directory &dir)
{
    const auto &self_e = dir.self_entry();
    return self_e.is_writable() &&
           self_e.is_executable(); // directory enter permission
}

const StringList Directory::entries(const SortingFlags &sorting, const FilterFlags &filter) const
{
    const auto &entryList = file_entries(sorting, filter);
    StringList strList;
    for (auto&& entry : entryList)
        strList.append(entry.full_path());
    return strList;
}

const FileEntryList Directory::file_entries(const SortingFlags &sorting, const FilterFlags &filter) const
{
    if (!valid())
        return {};

    DIR *d = nullptr;
    struct dirent *e = nullptr;

    // open directory
    d = opendir(*_dir);
    if (!d)
        return {};

    FileEntryList entryList;

    // receive directory entries
    while ((e = readdir(d)))
    {
        if (!e)
        {
            closedir(d);
            return {};
        }

        // create FileEntry object for current entry
        FileEntry entry(*this, e->d_name);

        // add when no filter is set
        if (filter.testFlag(NoFilterFlags))
        {
            entryList.append(entry);
            continue;
        }

        // check for hidden entry
        if (filter.testFlag(NoHiddenFiles))
            if (entry.name().starts_with("."))
                continue;

        // check for . and ..
        if (filter.testFlag(NoDotAndDotDot))
            if (entry.name() == "." || entry.name() == "..")
                continue;

        // directory and file filter
        auto add = true;
        if (filter.testFlag(DirectoriesOnly))
        {
            add = entry.is_directory();
        }
        else if (filter.testFlag(FilesOnly))
        {
            add = entry.is_file();
        }

        if (add)
        {
            entryList.append(entry);
        }
    }

    // close directory
    closedir(d);

    // do sorting
    if (sorting.testFlag(NoSortingFlags))
        return entryList;

    // TODO
    //    DirectoriesFirst
    //    FilesFirst
    //    AlphabeticallyAscii
    //    AlphabeticallyUnicode


    return entryList;
}

const FileEntry Directory::self_entry() const
{
    return FileEntry(*this);
}
