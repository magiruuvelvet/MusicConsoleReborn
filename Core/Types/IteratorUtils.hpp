#ifndef ITERATORUTILS_HPP
#define ITERATORUTILS_HPP

#include <algorithm>

namespace IteratorUtils {

// remove duplicates without sorting the list, preserving the order of elements
// container type must be STL compatible
// usage: list.erase(remove_duplicates(list.begin(), list.end()), list.end());
template <typename ForwardIterator>
inline ForwardIterator remove_duplicates(ForwardIterator first, ForwardIterator last)
{
    auto new_last = first;

    for (auto current = first; current != last; ++current)
    {
        if (std::find(first, new_last, *current) == new_last)
        {
            if (new_last != current) *new_last = *current;
            ++new_last;
        }
    }

    return new_last;
}

// convenience function for the above iterator
template <typename T>
inline void RemoveDuplicates(T &list)
{
    list.erase(remove_duplicates(list.begin(), list.end()), list.end());
}

// remove duplicates and sort the list at the same time
// container type must be STL compatible
// container value type must have the operators < and == implemented
template <typename ContainerType, typename ContainerValueType>
inline void remove_duplicates_and_sort(ContainerType &container)
{
    // sort list
    std::sort(container.begin(), container.end(),
              [](const ContainerValueType &value1, const ContainerValueType &value2) {
        return value1 < value2;
    });

    // remove duplicates
    typename ContainerType::iterator it = std::unique(container.begin(), container.end(),
                                                      [](const ContainerValueType &value1,
                                                         const ContainerValueType &value2) {
        return value1 == value2;
    });

    // remove remaining element which is a duplicate
    container.erase(it, container.end());
}

} // namespace

#endif // ITERATORUTILS_HPP
