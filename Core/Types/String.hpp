#ifndef STRING_HPP
#define STRING_HPP

#include <string>
#include <algorithm>
#include <type_traits>

#include "RegExp.hpp"
#include "Vector.hpp"
#include "List.hpp"

/// FIXME
/// temporary: remove as soon as possible
#include <QString>
#include <QByteArray>
/// -------------------------------------

class String;

using StringVector = Vector<String>;
using StringList = List<String>;

// std::string wrapper with additional methods
// supports basic case sensitivity manipulations (ASCII only)
class String : public std::string
{
public:
    // forward std::string constructors
    using std::string::string;

    // override constructors
    String() = default;
    String(const char c, size_type n = 1);
    String(const char *cs);
    String(const std::string &ss);
    String(const String&) = default;

    // copy assignment operators
    String &operator=(const String&) = default;
    String &operator=(const char *cs);
    String &operator=(const std::string &ss);

    // operator*, direct access to raw data -> *myString
    inline const char *operator*() const
    { return this->c_str(); }

    // ASCII only
    enum CaseSensitivity {
        CaseSensitive,       // default
        CaseInsensitive,
    };

    enum SplitMode {
        KeepEmptyParts,         // default, keeps all parts
        SkipEmptyParts,         // skips empty() parts
        TrimAndSkipEmptyParts,  // trims substring before empty() check
    };

    // explicit std::string conversion (shouldn't be needed in most cases)
    inline std::string to_std_string() const
    { return std::string(c_str()); }

    /// FIXME
    /// temporary: remove as soon as possible
    // auto cast to QByteArray wherever possible
    // makes transitioning away from Qt a bit easier,
    // until all parts of the code base are ported
    inline operator QByteArray() const
    { return QByteArray(c_str()); }
    inline operator QString() const
    { return QString::fromUtf8(c_str()); }

    // construct String from QByteArray
    String(const QByteArray &bytearray)
        : std::string(bytearray.constData())
    { }
    String(const QString &qstring)
        : std::string(qstring.toUtf8().constData())
    { }
    /// -------------------------------------

    // fill entire string with a character
    String &fill(const char ch);

    // resize and fill entire string with a character
    String &fill(const char ch, size_t size);

    //
    // manipulate case sensitivity (ASCII only)
    //
    String &to_lower();
    String lower() const;
    String &to_upper();
    String upper() const;

    //
    // trim whitespaces from string (ASCII only)
    //
    String &trim();
    String &trim_front();
    String &trim_back();
    String trimmed() const;
    String trimmed_front() const;
    String trimmed_back() const;

    //
    // simplify string: trim and remove inner repeating whitespaces (ASCII only)
    //
    String &simplify();
    String simplified() const;

    // case insensitive compare (ASCII only)
    // for case sensitive compare use operator==()
    inline bool case_insensitive_compare(const String &other) const
    { return this->lower() == other.lower(); }

    //
    // starts with substring
    //
    bool starts_with(const String &ss, CaseSensitivity cs = CaseSensitive) const;
    bool starts_with(const std::string &ss, CaseSensitivity cs = CaseSensitive) const
    { return this->starts_with(String(ss), cs); }
    inline bool starts_with(const char *ss, CaseSensitivity cs = CaseSensitive) const
    { return this->starts_with(String(ss), cs); }

    //
    // not starts with substring
    //
    inline bool not_starts_with(const String &ss, CaseSensitivity cs = CaseSensitive) const
    { return !this->starts_with(ss, cs); }
    inline bool not_starts_with(const std::string &ss, CaseSensitivity cs = CaseSensitive) const
    { return !this->starts_with(String(ss), cs); }
    inline bool not_starts_with(const char *ss, CaseSensitivity cs = CaseSensitive) const
    { return !this->starts_with(ss, cs); }

    //
    // ends with substring
    //
    bool ends_with(const String &ss, CaseSensitivity cs = CaseSensitive) const;
    inline bool ends_with(const std::string &ss, CaseSensitivity cs = CaseSensitive) const
    { return this->ends_with(String(ss), cs); }
    inline bool ends_with(const char *ss, CaseSensitivity cs = CaseSensitive) const
    { return this->ends_with(String(ss), cs); }

    //
    // not ends with substring
    //
    inline bool not_ends_with(const String &ss, CaseSensitivity cs = CaseSensitive) const
    { return !this->ends_with(ss, cs); }
    inline bool not_ends_with(const std::string &ss, CaseSensitivity cs = CaseSensitive) const
    { return !this->ends_with(String(ss), cs); }
    inline bool not_ends_with(const char *ss, CaseSensitivity cs = CaseSensitive) const
    { return !this->ends_with(String(ss), cs); }

    // count characters
    std::size_t count(const char c, CaseSensitivity cs = CaseSensitive) const;

    // count substrings
    std::size_t count(const String &substr, CaseSensitivity cs = CaseSensitive) const;
    inline std::size_t count(const std::string &substr, CaseSensitivity cs = CaseSensitive) const
    { return this->count(String(substr), cs); }
    inline std::size_t count(const char *substr, CaseSensitivity cs = CaseSensitive) const
    { return this->count(String(substr), cs); }

    //
    // split string at delimiter
    //
    StringVector split(const char sep, SplitMode = KeepEmptyParts) const;
    StringVector split(const String &sep, SplitMode = KeepEmptyParts) const;
    StringVector split(const RegExp &regex, SplitMode = KeepEmptyParts) const;

    //
    // string contains character or substring
    //
    inline bool contains(const char c) const
    { return this->find(c) != String::npos; }
    inline bool contains(const String &substr) const
    { return this->find(substr) != String::npos; }
    inline bool contains(const std::string &substr) const
    { return this->find(String(substr)) != String::npos; }
    inline bool contains(const char *substr) const
    { return this->find(substr) != String::npos; }

    //
    // index methods
    //
    inline size_type index_of(const char c, size_type n = 0) const
    { return this->find_first_of(c, n); }
    inline size_type index_of(const String &s, size_type n = 0) const
    { return this->find_first_of(s, n); }

    //
    // mid (alias for substr)
    //
    inline String mid(size_type pos, size_type n = npos) const
    { return this->substr(pos, n); }

    //
    // prepend
    //
    inline String &prepend(size_type n, char c)
    { this->insert(0, n, c); return *this; }
    inline String &prepend(const String &str)
    { this->insert(0, str); return *this; }
    inline String &prepend(const String &str, size_type pos, size_type n)
    { this->insert(0, str, pos, n); return *this; }

    //
    // proper replace methods
    //
    inline String &replace(size_type i, size_type len, char after)
    { std::string::replace(i, len, std::string() + after); return *this; }
    inline String &replace(size_type i, size_type len, const String &after)
    { std::string::replace(i, len, after.to_std_string()); return *this; }
    inline String &replace(char before, char after)
    { replace_helper(*this, String(before), String(after)); return *this; }
    inline String &replace(const String &before, const String &after)
    { replace_helper(*this, before, after); return *this; }
    inline String &replace(char before, const String &after)
    { replace_helper(*this, String(before), after); return *this; }
    inline String &replace(const String &before, char after)
    { replace_helper(*this, before, String(after)); return *this; }

    //
    // remove methods
    //
    inline String &remove(size_type i, size_type len)
    { return this->replace(i, len, String()); }
    inline String &remove(char c)
    { return this->replace(c, String()); }
    inline String &remove(const String &s)
    { return this->replace(s, String()); }

    //
    // convert from number
    // only supports literal types (uses type_traits)
    //
    template<typename _NumberType, typename = std::enable_if<std::is_literal_type<_NumberType>::value>>
    static inline String number(const _NumberType &num)
    { return String(std::to_string(num)); }

    //
    // number conversion methods
    //
    int toInt(bool *ok = nullptr) const;
    inline int32_t toInt32(bool *ok = nullptr) const
    { return toInt(ok); }

    uint32_t toUInt(bool *ok = nullptr) const;
    inline uint32_t toUInt32(bool *ok = nullptr) const
    { return toUInt(ok); }

    int64_t toInt64(bool *ok = nullptr) const;

    uint64_t toULongLong(bool *ok = nullptr) const;
    inline uint64_t toUInt64(bool *ok = nullptr) const
    { return toULongLong(ok); }

    float toFloat(bool *ok = nullptr) const;
    double toDouble(bool *ok = nullptr) const;
    long double toLongDouble(bool *ok = nullptr) const;

    //
    // more alias methods
    //
    inline bool isEmpty() const
    { return empty(); }

private:
    static void replace_helper(String &str, const String &from, const String &to);
    static bool is_whitespace(char ch);
};

#endif // STRING_HPP
