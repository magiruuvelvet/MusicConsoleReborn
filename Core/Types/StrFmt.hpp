#ifndef STRFMT_HPP
#define STRFMT_HPP

// 3rd party String Formatting library
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#pragma GCC diagnostic ignored "-Wpessimizing-move"
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wsign-compare"
#pragma clang diagnostic ignored "-Wmissing-field-initializers"
#pragma clang diagnostic ignored "-Wpessimizing-move"
#include "Private/StrFmt/StrFmt.hpp"
#pragma clang diagnostic pop
#pragma GCC diagnostic pop

#endif // STRFMT_HPP
