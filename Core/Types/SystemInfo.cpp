#include "SystemInfo.hpp"

#include <unistd.h>
#include <sys/utsname.h>

#include "StrFmt.hpp"

// 1<<bit == register

bool SystemInfo::has_sse()
{
    static const bool g_value = get_cpuid(0, 0)[0] >= 0x1 && get_cpuid(1, 0)[2] & 0x2000000;
    return g_value;
}

bool SystemInfo::has_sse2()
{
    static const bool g_value = get_cpuid(0, 0)[0] >= 0x1 && get_cpuid(1, 0)[2] & 0x4000000;
    return g_value;
}

bool SystemInfo::has_ssse3()
{
    static const bool g_value = get_cpuid(0, 0)[0] >= 0x1 && get_cpuid(1, 0)[2] & 0x200;
    return g_value;
}

bool SystemInfo::has_sse41()
{
    static const bool g_value = get_cpuid(0, 0)[0] >= 0x1 && get_cpuid(1, 0)[2] & 0x80000;
    return g_value;
}

bool SystemInfo::has_sse42()
{
    static const bool g_value = get_cpuid(0, 0)[0] >= 0x1 && get_cpuid(1, 0)[2] & 0x100000;
    return g_value;
}

bool SystemInfo::has_htt()
{
    static const bool g_value = get_cpuid(0, 0)[0] >= 0x1 && get_cpuid(1, 0)[2] & 0x10000000;
    return g_value;
}

bool SystemInfo::has_avx()
{
    static const bool g_value = get_cpuid(0, 0)[0] >= 0x1 && get_cpuid(1, 0)[2] & 0x10000000 && (get_cpuid(1, 0)[2] & 0x0C000000) == 0x0C000000 && (get_xgetbv(0) & 0x6) == 0x6;
    return g_value;
}

bool SystemInfo::has_avx2()
{
    static const bool g_value = get_cpuid(0, 0)[0] >= 0x7 && get_cpuid(7, 0)[1] & 0x20 && (get_cpuid(1, 0)[2] & 0x0C000000) == 0x0C000000 && (get_xgetbv(0) & 0x6) == 0x6;
    return g_value;
}

bool SystemInfo::has_rtm()
{
    // Check RTM and MPX extensions in order to filter out TSX on Haswell CPUs
    static const bool g_value = get_cpuid(0, 0)[0] >= 0x7 && (get_cpuid(7, 0)[1] & 0x4800) == 0x4800;
    return g_value;
}

bool SystemInfo::has_512()
{
    // Check AVX512F, AVX512CD, AVX512DQ, AVX512BW, AVX512VL extensions (Skylake-X level support)
    static const bool g_value = get_cpuid(0, 0)[0] >= 0x7 && (get_cpuid(7, 0)[1] & 0xd0030000) == 0xd0030000 && (get_cpuid(1,0)[2] & 0x0C000000) == 0x0C000000 && (get_xgetbv(0) & 0xe6) == 0xe6;
    return g_value;
}

bool SystemInfo::has_xop()
{
    static const bool g_value = has_avx() && get_cpuid(0x80000001, 0)[2] & 0x800;
    return g_value;
}

String SystemInfo::get_system_info()
{
    std::string result;
    std::string brand;

    if (get_cpuid(0x80000000, 0)[0] >= 0x80000004)
    {
        for (u32 i = 0; i < 3; i++)
        {
            brand.append(reinterpret_cast<const char*>(get_cpuid(0x80000002 + i, 0).data()), 16);
        }
    }
    else
    {
        brand = "Unknown CPU";
    }

    brand.erase(0, brand.find_first_not_of(' '));
    brand.erase(brand.find_last_not_of(' ') + 1);

    while (auto found = brand.find("  ") + 1)
    {
        brand.erase(brand.begin() + found);
    }

    const u32 num_proc = ::sysconf(_SC_NPROCESSORS_ONLN);
    const u64 mem_total = ::sysconf(_SC_PHYS_PAGES) * ::sysconf(_SC_PAGE_SIZE);

    fmt::append(result, "%s | %d Threads | %.2f GiB RAM", brand, num_proc, mem_total / (1024.0f * 1024 * 1024));

    if (has_avx())
    {
        result += " | AVX";

        if (has_avx2())
        {
            result += '+';
        }

        if (has_512())
        {
            result += '+';
        }

        if (has_xop())
        {
            result += 'x';
        }
    }

    if (has_rtm())
    {
        result += " | TSX";
    }

    return result;
}

String SystemInfo::get_os_name()
{
    struct utsname result;
    if (uname(&result) != 0)
        return {};

    std::string name;
    fmt::append(name, "%s %s [%s]", result.sysname, result.release, result.machine);
    return name;
}
