#ifndef STRINGUTILS_HPP
#define STRINGUTILS_HPP

#include "UnicodeString.hpp"

#include <string>
#include <sstream>
#include <iomanip>

// FIXME: remove
#include <QString>

class StringUtils final
{
    StringUtils() = delete;

public:

    // FIXME: remove
    static const QString ltrim(const QString &string)
    {
        for (auto i = 0; i < string.size(); i++)
        {
            if (!string.at(i).isSpace())
            {
                return string.mid(i);
            }
        }
        return {};
    }

    // convert integer into a hexadecimal string
    template<typename T>
    static const std::string int_to_hex(const T &i)
    {
        std::stringstream stream;
        stream << "0x"
               << std::setfill('0') << std::setw(sizeof(T)*2)
               << std::hex << i;
        return stream.str();
    }
};

#endif // STRINGUTILS_HPP
