#ifndef BITWISEFLAGS_HPP
#define BITWISEFLAGS_HPP

#include <ostream>
#include <initializer_list>

class BitwiseFlag
{
    int i;

public:
    constexpr inline BitwiseFlag(int value) noexcept : i(value) {}
    constexpr inline operator int() const noexcept { return i; }

    constexpr inline BitwiseFlag(uint value) noexcept : i(int(value)) {}
    constexpr inline BitwiseFlag(short value) noexcept : i(int(value)) {}
    constexpr inline BitwiseFlag(ushort value) noexcept : i(int(uint(value))) {}
    constexpr inline operator uint() const noexcept { return uint(i); }
};

class IncompatibleBitwiseFlag
{
    int i;

public:
    constexpr inline explicit IncompatibleBitwiseFlag(int i) noexcept;
    constexpr inline operator int() const noexcept { return i; }
};

constexpr inline IncompatibleBitwiseFlag::IncompatibleBitwiseFlag(int value) noexcept : i(value) {}

template<typename Enum>
class BitwiseFlags
{
    static_assert((sizeof(Enum) <= sizeof(int)),
                      "BitwiseFlags uses an int as storage, so an enum with underlying "
                      "long long will overflow.");
    static_assert((std::is_enum<Enum>::value), "BitwiseFlags is only usable on enumeration types.");

    struct Private;
    typedef int (Private::*Zero);
    template <typename E> friend std::ostream &operator>>(std::ostream&, BitwiseFlags<E>&);
    template <typename E> friend std::ostream &operator<<(std::ostream&, BitwiseFlags<E>);
public:
    typedef typename std::conditional<
            std::is_unsigned<typename std::underlying_type<Enum>::type>::value,
            unsigned int,
            signed int
        >::type Int;

    typedef Enum enum_type;
    // compiler-generated copy/move ctor/assignment operators are fine!
    constexpr inline BitwiseFlags(Enum flags) noexcept : i(Int(flags)) {}
    constexpr inline BitwiseFlags(Zero = nullptr) noexcept : i(0) {}
    constexpr inline BitwiseFlags(BitwiseFlag flag) noexcept : i(flag) {}

    constexpr inline BitwiseFlags(std::initializer_list<Enum> flags) noexcept
        : i(initializer_list_helper(flags.begin(), flags.end())) {}

    constexpr inline BitwiseFlags &operator&=(int mask) noexcept { i &= mask; return *this; }
    constexpr inline BitwiseFlags &operator&=(uint mask) noexcept { i &= mask; return *this; }
    constexpr inline BitwiseFlags &operator&=(Enum mask) noexcept { i &= Int(mask); return *this; }
    constexpr inline BitwiseFlags &operator|=(BitwiseFlags other) noexcept { i |= other.i; return *this; }
    constexpr inline BitwiseFlags &operator|=(Enum other) noexcept { i |= Int(other); return *this; }
    constexpr inline BitwiseFlags &operator^=(BitwiseFlags other) noexcept { i ^= other.i; return *this; }
    constexpr inline BitwiseFlags &operator^=(Enum other) noexcept { i ^= Int(other); return *this; }

    constexpr inline operator Int() const noexcept { return i; }

    constexpr inline BitwiseFlags operator|(BitwiseFlags other) const noexcept { return BitwiseFlags(BitwiseFlag(i | other.i)); }
    constexpr inline BitwiseFlags operator|(Enum other) const noexcept { return BitwiseFlags(BitwiseFlag(i | Int(other))); }
    constexpr inline BitwiseFlags operator^(BitwiseFlags other) const noexcept { return BitwiseFlags(BitwiseFlag(i ^ other.i)); }
    constexpr inline BitwiseFlags operator^(Enum other) const noexcept { return BitwiseFlags(BitwiseFlag(i ^ Int(other))); }
    constexpr inline BitwiseFlags operator&(int mask) const noexcept { return BitwiseFlags(BitwiseFlag(i & mask)); }
    constexpr inline BitwiseFlags operator&(uint mask) const noexcept { return BitwiseFlags(BitwiseFlag(i & mask)); }
    constexpr inline BitwiseFlags operator&(Enum other) const noexcept { return BitwiseFlags(BitwiseFlag(i & Int(other))); }
    constexpr inline BitwiseFlags operator~() const noexcept { return BitwiseFlags(BitwiseFlag(~i)); }

    constexpr inline bool operator!() const noexcept { return !i; }

    constexpr inline bool testFlag(Enum flag) const noexcept { return (i & Int(flag)) == Int(flag) && (Int(flag) != 0 || i == Int(flag) ); }
    constexpr inline BitwiseFlags &setFlag(Enum flag, bool on = true) noexcept
    {
        return on ? (*this |= flag) : (*this &= ~Int(flag));
    }

private:
    constexpr static inline Int initializer_list_helper(typename std::initializer_list<Enum>::const_iterator it,
                                                        typename std::initializer_list<Enum>::const_iterator end)
    noexcept
    {
        return (it == end ? Int(0) : (Int(*it) | initializer_list_helper(it + 1, end)));
    }

    Int i;
};

#define DECLARE_BITWISE_FLAGS(Flags, Enum) \
using Flags = BitwiseFlags<Enum>;

#define DECLARE_INCOMPATIBLE_BITWISE_FLAGS(Flags) \
constexpr inline IncompatibleBitwiseFlag operator|(Flags::enum_type f1, int f2) noexcept \
{ return IncompatibleBitwiseFlag(int(f1) | f2); }

#define DECLARE_OPERATORS_FOR_BITWISE_FLAGS(Flags) \
constexpr inline BitwiseFlags<Flags::enum_type> operator|(Flags::enum_type f1, Flags::enum_type f2) noexcept \
{ return BitwiseFlags<Flags::enum_type>(f1) | f2; } \
constexpr inline BitwiseFlags<Flags::enum_type> operator|(Flags::enum_type f1, BitwiseFlags<Flags::enum_type> f2) noexcept \
{ return f2 | f1; } DECLARE_INCOMPATIBLE_BITWISE_FLAGS(Flags)

#endif // BITWISEFLAGS_HPP
