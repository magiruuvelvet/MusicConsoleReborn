#include "RegularExpression.hpp"

#include "StrFmt.hpp"

#include <memory>

// implementation is inspired by Qt
// https://code.woboq.org/qt5/qtbase/src/corelib/tools/qregularexpression.cpp

// use UTF-32 perl compatible regular expressions
#define PCRE2_CODE_UNIT_WIDTH 32
#include <pcre2.h>

// uncomment this for debugging
//#define REGULAREXPRESSION_DEBUG

static uint32_t convertToPcreOptions(RegularExpression::PatternOptions patternOptions)
{
    uint32_t options = 0;

    if (patternOptions & RegularExpression::CaseInsensitiveOption)
        options |= PCRE2_CASELESS;
    if (patternOptions & RegularExpression::DotMatchesEverythingOption)
        options |= PCRE2_DOTALL;
    if (patternOptions & RegularExpression::MultilineOption)
        options |= PCRE2_MULTILINE;
    if (patternOptions & RegularExpression::ExtendedPatternSyntaxOption)
        options |= PCRE2_EXTENDED;
    if (patternOptions & RegularExpression::InvertedGreedinessOption)
        options |= PCRE2_UNGREEDY;
    if (patternOptions & RegularExpression::DontCaptureOption)
        options |= PCRE2_NO_AUTO_CAPTURE;
    if (patternOptions & RegularExpression::UseUnicodePropertiesOption)
        options |= PCRE2_UCP;

    return options;
}

static uint32_t convertToPcreOptions(RegularExpression::MatchOptions matchOptions)
{
    uint32_t options = 0;

    if (matchOptions & RegularExpression::AnchoredMatchOption)
        options |= PCRE2_ANCHORED;
    if (matchOptions & RegularExpression::DontCheckSubjectStringMatchOption)
        options |= PCRE2_NO_UTF_CHECK;

    return options;
}

struct RegularExpressionPrivate
{
    RegularExpressionPrivate();
    ~RegularExpressionPrivate();
    RegularExpressionPrivate(const RegularExpressionPrivate &other);

    void cleanCompiledPattern();
    void compilePattern();
    void getPatternInfo();

    enum CheckSubjectStringOption {
        CheckSubjectString,
        DontCheckSubjectString
    };

    RegularExpressionMatchPrivate *doMatch(const UnicodeString &subject,
                                           int subjectStartPos,
                                           int subjectLength,
                                           int offset,
                                           RegularExpression::MatchType matchType,
                                           RegularExpression::MatchOptions matchOptions,
                                           CheckSubjectStringOption checkSubjectStringOption = CheckSubjectString,
                                           const RegularExpressionMatchPrivate *previous = nullptr) const;

    int captureIndexForName(const UnicodeString &name) const;

    RegularExpression::PatternOptions patternOptions;
    UnicodeString pattern;

    pcre2_code_32 *compiledPattern;
    int errorCode;
    int errorOffset;
    int capturingCount;
    unsigned int usedCount;
    bool usingCrLfNewlines;
    bool isDirty;
};

struct RegularExpressionMatchPrivate
{
    RegularExpressionMatchPrivate(const RegularExpression &re,
                                  const UnicodeString &subject,
                                  int subjectStart,
                                  int subjectLength,
                                  RegularExpression::MatchType matchType,
                                  RegularExpression::MatchOptions matchOptions);

    RegularExpressionMatch nextMatch() const;

    const RegularExpression regularExpression;
    const UnicodeString subject;
    // the capturedOffsets vector contains pairs of (start, end) positions
    // for each captured substring
    Vector<int> capturedOffsets;

    const int subjectStart;
    const int subjectLength;

    const RegularExpression::MatchType matchType;
    const RegularExpression::MatchOptions matchOptions;

    int capturedCount;

    bool hasMatch;
    bool hasPartialMatch;
    bool isValid;
};

struct RegularExpressionMatchIteratorPrivate
{
    RegularExpressionMatchIteratorPrivate(const RegularExpression &re,
                                          RegularExpression::MatchType matchType,
                                          RegularExpression::MatchOptions matchOptions,
                                          const RegularExpressionMatch &next);

    bool hasNext() const;
    RegularExpressionMatch next;
    const RegularExpression regularExpression;
    const RegularExpression::MatchType matchType;
    const RegularExpression::MatchOptions matchOptions;
};

RegularExpression::RegularExpression(RegularExpressionPrivate &dd)
    : d(&dd)
{
}

RegularExpressionPrivate::RegularExpressionPrivate()
    : patternOptions(RegularExpression::NoPatternOption),
      pattern(),
      compiledPattern(nullptr),
      errorCode(0),
      errorOffset(-1),
      capturingCount(0),
      usedCount(0),
      usingCrLfNewlines(false),
      isDirty(true)
{
}

RegularExpressionPrivate::~RegularExpressionPrivate()
{
    cleanCompiledPattern();
}

RegularExpressionPrivate::RegularExpressionPrivate(const RegularExpressionPrivate &other)
    : patternOptions(other.patternOptions),
      pattern(other.pattern),
      compiledPattern(nullptr),
      errorCode(0),
      errorOffset(-1),
      capturingCount(0),
      usedCount(0),
      usingCrLfNewlines(false),
      isDirty(true)
{
}

void RegularExpressionPrivate::cleanCompiledPattern()
{
    pcre2_code_free_32(compiledPattern);
    compiledPattern = nullptr;
    errorCode = 0;
    errorOffset = -1;
    capturingCount = 0;
    usedCount = 0;
    usingCrLfNewlines = false;
}

void RegularExpressionPrivate::compilePattern()
{
    if (!isDirty)
        return;

    isDirty = false;
    cleanCompiledPattern();

    uint32_t options = convertToPcreOptions(patternOptions);
    options |= PCRE2_UTF;

    PCRE2_SIZE patternErrorOffset;
    compiledPattern = pcre2_compile_32(pattern.internal_data_pcre2().data(),
                                       pattern.length(),
                                       options,
                                       &errorCode,
                                       &patternErrorOffset,
                                       nullptr);

    if (!compiledPattern) {
        errorOffset = static_cast<int>(patternErrorOffset);
        return;
    } else {
        // ignore whatever PCRE2 wrote into errorCode -- leave it to 0 to mean "no error"
        errorCode = 0;
    }

    getPatternInfo();
}

void RegularExpressionPrivate::getPatternInfo()
{
    if (!compiledPattern)
        return;

    pcre2_pattern_info_32(compiledPattern, PCRE2_INFO_CAPTURECOUNT, &capturingCount);

    // detect the settings for the newline
    unsigned int patternNewlineSetting;
    if (pcre2_pattern_info_32(compiledPattern, PCRE2_INFO_NEWLINE, &patternNewlineSetting) != 0) {
        // no option was specified in the regexp, grab PCRE build defaults
        pcre2_config_32(PCRE2_CONFIG_NEWLINE, &patternNewlineSetting);
    }

    usingCrLfNewlines = (patternNewlineSetting == PCRE2_NEWLINE_CRLF) ||
            (patternNewlineSetting == PCRE2_NEWLINE_ANY) ||
            (patternNewlineSetting == PCRE2_NEWLINE_ANYCRLF);

    unsigned int hasJOptionChanged;
    pcre2_pattern_info_32(compiledPattern, PCRE2_INFO_JCHANGED, &hasJOptionChanged);
    if (hasJOptionChanged) {
        std::fprintf(stderr, "RegularExpressionPrivate::getPatternInfo(): the pattern '%s'\n"
                             "    is using the (?J) option; duplicate capturing group names are not supported",
                     pattern.to_utf8().data());
    }
}

int RegularExpressionPrivate::captureIndexForName(const UnicodeString &name) const
{
    if (name.isEmpty())
        return -1;

    if (!compiledPattern)
        return -1;

    int index = pcre2_substring_number_from_name_32(compiledPattern, name.internal_data_pcre2().data());
    if (index >= 0)
        return index;

    return -1;
}

RegularExpressionMatchPrivate *RegularExpressionPrivate::doMatch(const UnicodeString &subject,
                                                                 int subjectStart,
                                                                 int subjectLength,
                                                                 int offset,
                                                                 RegularExpression::MatchType matchType,
                                                                 RegularExpression::MatchOptions matchOptions,
                                                                 CheckSubjectStringOption checkSubjectStringOption,
                                                                 const RegularExpressionMatchPrivate *previous) const
{
    if (offset < 0)
        offset += subjectLength;

    RegularExpression re(*const_cast<RegularExpressionPrivate*>(this));

    RegularExpressionMatchPrivate *priv = new RegularExpressionMatchPrivate(re, subject,
                                                                            subjectStart, subjectLength,
                                                                            matchType, matchOptions);

    if (offset < 0 || offset > subjectLength)
        return priv;

    if (!compiledPattern) {
        std::fprintf(stderr, "RegularExpressionPrivate::doMatch(): called on an invalid RegularExpression object\n");
        return priv;
    }

    // skip optimizing and doing the actual matching if NoMatch type was requested
    if (matchType == RegularExpression::NoMatch) {
        priv->isValid = true;
        return priv;
    }

    uint32_t pcreOptions = convertToPcreOptions(matchOptions);

    if (matchType == RegularExpression::PartialPreferCompleteMatch)
        pcreOptions |= PCRE2_PARTIAL_SOFT;
    else if (matchType == RegularExpression::PartialPreferFirstMatch)
        pcreOptions |= PCRE2_PARTIAL_HARD;

    if (checkSubjectStringOption == DontCheckSubjectString)
        pcreOptions |= PCRE2_NO_UTF_CHECK;

    bool previousMatchWasEmpty = false;
    if (previous && previous->hasMatch &&
            (previous->capturedOffsets.at(0) == previous->capturedOffsets.at(1))) {
        previousMatchWasEmpty = true;
    }

    pcre2_match_context_32 *matchContext = pcre2_match_context_create_32(nullptr);
    pcre2_match_data_32 *matchData = pcre2_match_data_create_from_pattern_32(compiledPattern, nullptr);

    // copy internal buffer (reason: temporary memory / return)
    const auto subjectUtf32_p = subject.internal_data_pcre2();
    const uint32_t *const subjectUtf32 = subjectUtf32_p.data() + subjectStart;

    int result;

    if (!previousMatchWasEmpty) {
        result = pcre2_match_32(compiledPattern,
                                subjectUtf32, subjectLength,
                                offset, pcreOptions,
                                matchData, matchContext);
    } else {
        result = pcre2_match_32(compiledPattern,
                                subjectUtf32, subjectLength,
                                offset, pcreOptions | PCRE2_NOTEMPTY_ATSTART | PCRE2_ANCHORED,
                                matchData, matchContext);

        if (result == PCRE2_ERROR_NOMATCH) {
            ++offset;

            if (usingCrLfNewlines
                    && offset < subjectLength
                    && subjectUtf32[offset - 1] == '\r'
                    && subjectUtf32[offset] == '\n') {
                ++offset;
            } else if (offset < subjectLength
                       && Char::is_low_surrogate(static_cast<Char::type>(subjectUtf32[offset]))) {
                ++offset;
            }

            result = pcre2_match_32(compiledPattern,
                                    subjectUtf32, subjectLength,
                                    offset, pcreOptions,
                                    matchData, matchContext);
        }
    }

#ifdef REGULAREXPRESSION_DEBUG
    std::printf("Matching R(  %s  ) against \"%s\" "
                "starting at: >%i<, len: >%i< "
                "offset: >%i<, previousMatchWasEmpty: >%i< "
                "result: >%i<\n",
                pattern.to_utf8().data(), subject.to_utf8().data(),
                subjectStart, subjectLength,
                offset, previousMatchWasEmpty,
                result);
#endif

    if (result > 0) {
        // full match
        priv->isValid = true;
        priv->hasMatch = true;
        priv->capturedCount = result;
        priv->capturedOffsets.resize(result * 2);
    } else {
        // no match, partial match or error
        priv->hasPartialMatch = (result == PCRE2_ERROR_PARTIAL);
        priv->isValid = (result == PCRE2_ERROR_NOMATCH || result == PCRE2_ERROR_PARTIAL);

        if (result == PCRE2_ERROR_PARTIAL) {
            // partial match:
            // leave the start and end capture offsets (i.e. cap(0))
            priv->capturedCount = 1;
            priv->capturedOffsets.resize(2);
        } else {
            // no match or error
            priv->capturedCount = 0;
            priv->capturedOffsets.clear();
        }
    }

    // copy the captured substrings offsets, if any
    if (priv->capturedCount) {
        PCRE2_SIZE *ovector = pcre2_get_ovector_pointer_32(matchData);
        int * const capturedOffsets = priv->capturedOffsets.data();

        for (int i = 0; i < priv->capturedCount * 2; ++i)
            capturedOffsets[i] = static_cast<int>(ovector[i]);

        if (result == PCRE2_ERROR_PARTIAL) {
            unsigned int maximumLookBehind;
            pcre2_pattern_info_32(compiledPattern, PCRE2_INFO_MAXLOOKBEHIND, &maximumLookBehind);
            capturedOffsets[0] -= maximumLookBehind;
        }
    }

    pcre2_match_data_free_32(matchData);
    pcre2_match_context_free_32(matchContext);

    return priv;
}

RegularExpressionMatchPrivate::RegularExpressionMatchPrivate(const RegularExpression &re,
                                                             const UnicodeString &subject,
                                                             int subjectStart,
                                                             int subjectLength,
                                                             RegularExpression::MatchType matchType,
                                                             RegularExpression::MatchOptions matchOptions)
    : regularExpression(re), subject(subject),
      subjectStart(subjectStart), subjectLength(subjectLength),
      matchType(matchType), matchOptions(matchOptions),
      capturedCount(0),
      hasMatch(false), hasPartialMatch(false), isValid(false)
{
}

RegularExpressionMatch RegularExpressionMatchPrivate::nextMatch() const
{
    RegularExpressionMatchPrivate *nextPrivate = regularExpression.d->doMatch(subject,
                                                                              subjectStart,
                                                                              subjectLength,
                                                                              capturedOffsets.at(1),
                                                                              matchType,
                                                                              matchOptions,
                                                                              RegularExpressionPrivate::DontCheckSubjectString,
                                                                              this);
    return RegularExpressionMatch(*nextPrivate);
}

RegularExpressionMatchIteratorPrivate::RegularExpressionMatchIteratorPrivate(const RegularExpression &re,
                                                                             RegularExpression::MatchType matchType,
                                                                             RegularExpression::MatchOptions matchOptions,
                                                                             const RegularExpressionMatch &next)
    : next(next),
      regularExpression(re),
      matchType(matchType), matchOptions(matchOptions)
{
}

bool RegularExpressionMatchIteratorPrivate::hasNext() const
{
    return next.isValid() && (next.hasMatch() || next.hasPartialMatch());
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

RegularExpression::RegularExpression()
    : d(new RegularExpressionPrivate)
{
}

RegularExpression::RegularExpression(const UnicodeString &pattern, PatternOptions options)
    : d(new RegularExpressionPrivate)
{
    d->pattern = pattern;
    d->patternOptions = options;
}

RegularExpression::RegularExpression(const RegularExpression &re)
    : d(re.d)
{
}

RegularExpression::~RegularExpression()
{
    // [d] is automatically deleted
}

RegularExpression &RegularExpression::operator=(const RegularExpression &re)
{
    d = re.d;
    return *this;
}

RegularExpression &RegularExpression::operator=(const UnicodeString &re)
{
    *this = RegularExpression(re);
    return *this;
}

UnicodeString RegularExpression::pattern() const
{
    return d->pattern;
}

void RegularExpression::setPattern(const UnicodeString &pattern)
{
    d->isDirty = true;
    d->pattern = pattern;
}

RegularExpression::PatternOptions RegularExpression::patternOptions() const
{
    return d->patternOptions;
}

void RegularExpression::setPatternOptions(PatternOptions options)
{
    d->isDirty = true;
    d->patternOptions = options;
}

int RegularExpression::captureCount() const
{
    if (!isValid()) // will compile the pattern
        return -1;
    return d->capturingCount;
}

UnicodeStringVector RegularExpression::namedCaptureGroups() const
{
    if (!isValid()) // isValid() will compile the pattern
        return UnicodeStringVector();

    // namedCapturingTable will point to a table of
    // namedCapturingTableEntryCount entries, each one of which
    // contains one Char32::type followed by the name, NUL terminated.
    // The Char32::type is the numerical index of the name in the pattern.
    // The length of each entry is namedCapturingTableEntrySize.
    PCRE2_SPTR32 *namedCapturingTable;
    unsigned int namedCapturingTableEntryCount;
    unsigned int namedCapturingTableEntrySize;

    pcre2_pattern_info_32(d->compiledPattern, PCRE2_INFO_NAMETABLE, &namedCapturingTable);
    pcre2_pattern_info_32(d->compiledPattern, PCRE2_INFO_NAMECOUNT, &namedCapturingTableEntryCount);
    pcre2_pattern_info_32(d->compiledPattern, PCRE2_INFO_NAMEENTRYSIZE, &namedCapturingTableEntrySize);

    UnicodeStringVector result;

    result.reserve(d->capturingCount + 1);
    for (int i = 0; i < d->capturingCount + 1; ++i)
        result.append(UnicodeString());

    for (unsigned int i = 0; i < namedCapturingTableEntryCount; ++i) {
        const uint32_t *const currentNamedCapturingTableRow =
                reinterpret_cast<const uint32_t*>(namedCapturingTable) + namedCapturingTableEntrySize * i;

        const unsigned int index = *currentNamedCapturingTableRow;
        result[index] = UnicodeString::from_uchar32_buffer_static(currentNamedCapturingTableRow + 1, namedCapturingTableEntrySize);
    }

    return result;
}

bool RegularExpression::isValid() const
{
    d->compilePattern();
    return d->compiledPattern;
}

UnicodeString RegularExpression::errorString() const
{
    d->compilePattern();
    if (d->errorCode) {
        UnicodeString errorString;
        int errorStringLength;

        PCRE2_UCHAR32 msg[1024];
        errorStringLength = pcre2_get_error_message_32(d->errorCode, msg, 1024);

        errorString.from_uchar32_buffer(msg, static_cast<size_t>(errorStringLength));
        errorString.prepend("RegularExpression: ");                      // prepend class name
        errorString.append(fmt::format("! offset=%lu", d->errorOffset)); // append error offset
        return errorString;
    }
    return UnicodeString("RegularExpression: pattern successfully compiled!");
}

int RegularExpression::patternErrorOffset() const
{
    d->compilePattern();
    return d->errorOffset;
}

RegularExpressionMatch RegularExpression::match(const UnicodeString &subject,
                                                int offset,
                                                MatchType matchType,
                                                MatchOptions matchOptions) const
{
    d->compilePattern();

    RegularExpressionMatchPrivate *priv = d->doMatch(subject, 0, subject.length(), offset, matchType, matchOptions);
    return RegularExpressionMatch(*priv);
}

RegularExpressionMatchIterator RegularExpression::globalMatch(const UnicodeString &subject,
                                                              int offset,
                                                              MatchType matchType,
                                                              MatchOptions matchOptions) const
{
    RegularExpressionMatchIteratorPrivate *priv =
        new RegularExpressionMatchIteratorPrivate(*this,
                                                  matchType,
                                                  matchOptions,
                                                  match(subject, offset, matchType, matchOptions));

    return RegularExpressionMatchIterator(*priv);
}

namespace {
template<class ResultList, typename MidMethod>
static inline ResultList splitString(const UnicodeString &source, MidMethod mid, const RegularExpression &re,
                                     UnicodeString::SplitMode mode)
{
    ResultList list;
    if (!re.isValid()) {
        return list;
    }

    int start = 0;
    int end = 0;
    RegularExpressionMatchIterator iterator = re.globalMatch(source);
    while (iterator.hasNext()) {
        RegularExpressionMatch match = iterator.next();
        end = match.capturedStart();
        if (start != end || mode == UnicodeString::KeepEmptyParts)
            list.append((source.*mid)(start, end - start));
        start = match.capturedEnd();
    }

    if (start != source.size() || mode == UnicodeString::KeepEmptyParts)
        list.append((source.*mid)(start, -1));

    return list;
}
} // namespace

UnicodeStringVector RegularExpression::split(const UnicodeString &subject) const
{
    return splitString<UnicodeStringVector>(subject, &UnicodeString::mid, *this, UnicodeString::KeepEmptyParts);
}

bool RegularExpression::operator==(const RegularExpression &re) const
{
    return (d == re.d) ||
           (d->pattern == re.d->pattern && d->patternOptions == re.d->patternOptions);
}

UnicodeString RegularExpression::escape(const UnicodeString &str)
{
    UnicodeString result;
    const auto count = str.size();

    // everything but [a-zA-Z0-9_] gets escaped,
    // cf. perldoc -f quotemeta
    for (auto i = 0U; i < count; ++i) {
        const Char current = str.at(i);

        if (current == Char::Null) {
            // unlike Perl, a literal NUL must be escaped with
            // "\\0" (backslash + 0) and not "\\\0" (backslash + NUL),
            // because pcre32_compile uses a NUL-terminated string
            result.append('\\');
            result.append('0');
        } else if ( (current < 'a' || current > 'z') &&
                    (current < 'A' || current > 'Z') &&
                    (current < '0' || current > '9') &&
                     current != '_' )
        {
            result.append('\\');
            result.append(current);
            if (current.is_high_surrogate() && i < (count - 1))
                result.append(str.at(++i));
        } else {
            result.append(current);
        }
    }

    return result;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

RegularExpressionMatch::RegularExpressionMatch()
    : d(new RegularExpressionMatchPrivate(RegularExpression(),
                                          UnicodeString(),
                                          0,
                                          0,
                                          RegularExpression::NoMatch,
                                          RegularExpression::NoMatchOption))
{
    d->isValid = true;
}

RegularExpressionMatch::~RegularExpressionMatch()
{
    // [d] is automatically deleted
}

RegularExpressionMatch::RegularExpressionMatch(const RegularExpressionMatch &match)
    : d(match.d)
{
}

RegularExpressionMatch &RegularExpressionMatch::operator=(const RegularExpressionMatch &match)
{
    d = match.d;
    return *this;
}

RegularExpressionMatch::RegularExpressionMatch(RegularExpressionMatchPrivate &dd)
    : d(&dd)
{
}

RegularExpression RegularExpressionMatch::regularExpression() const
{
    return d->regularExpression;
}

RegularExpression::MatchType RegularExpressionMatch::matchType() const
{
    return d->matchType;
}

RegularExpression::MatchOptions RegularExpressionMatch::matchOptions() const
{
    return d->matchOptions;
}

int RegularExpressionMatch::lastCapturedIndex() const
{
    return d->capturedCount - 1;
}

UnicodeString RegularExpressionMatch::captured(int nth) const
{
    if (nth < 0 || nth > lastCapturedIndex())
        return UnicodeString();

    int start = capturedStart(nth);

    if (start == -1) // didn't capture
        return UnicodeString();

    return d->subject.mid(start + d->subjectStart, capturedLength(nth));
}

UnicodeString RegularExpressionMatch::captured(const UnicodeString &name) const
{
    if (name.isEmpty()) {
        std::fprintf(stderr, "RegularExpressionMatch::captured: empty capturing group name passed\n");
        return UnicodeString();
    }
    int nth = d->regularExpression.d->captureIndexForName(name);
    if (nth == -1)
        return UnicodeString();
    return captured(nth);
}

UnicodeStringVector RegularExpressionMatch::capturedTexts() const
{
    UnicodeStringVector texts;
    texts.reserve(d->capturedCount);
    for (int i = 0; i < d->capturedCount; ++i)
        texts << captured(i);
    return texts;
}

int RegularExpressionMatch::capturedStart(int nth) const
{
    if (nth < 0 || nth > lastCapturedIndex())
        return -1;

    return d->capturedOffsets.at(nth * 2);
}

int RegularExpressionMatch::capturedLength(int nth) const
{
    // bound checking performed by these two functions
    return capturedEnd(nth) - capturedStart(nth);
}

int RegularExpressionMatch::capturedEnd(int nth) const
{
    if (nth < 0 || nth > lastCapturedIndex())
        return -1;

    return d->capturedOffsets.at(nth * 2 + 1);
}

int RegularExpressionMatch::capturedStart(const UnicodeString &name) const
{
    if (name.isEmpty()) {
        std::fprintf(stderr, "RegularExpressionMatch::capturedStart: empty capturing group name passed\n");
        return -1;
    }
    int nth = d->regularExpression.d->captureIndexForName(name);
    if (nth == -1)
        return -1;
    return capturedStart(nth);
}

int RegularExpressionMatch::capturedLength(const UnicodeString &name) const
{
    if (name.isEmpty()) {
        std::fprintf(stderr, "RegularExpressionMatch::capturedLength: empty capturing group name passed\n");
        return 0;
    }
    int nth = d->regularExpression.d->captureIndexForName(name);
    if (nth == -1)
        return 0;
    return capturedLength(nth);
}

int RegularExpressionMatch::capturedEnd(const UnicodeString &name) const
{
    if (name.isEmpty()) {
        std::fprintf(stderr, "RegularExpressionMatch::capturedEnd: empty capturing group name passed\n");
        return -1;
    }
    int nth = d->regularExpression.d->captureIndexForName(name);
    if (nth == -1)
        return -1;
    return capturedEnd(nth);
}

bool RegularExpressionMatch::hasMatch() const
{
    return d->hasMatch;
}

bool RegularExpressionMatch::hasPartialMatch() const
{
    return d->hasPartialMatch;
}

bool RegularExpressionMatch::isValid() const
{
    return d->isValid;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

RegularExpressionMatchIterator::RegularExpressionMatchIterator(RegularExpressionMatchIteratorPrivate &dd)
    : d(&dd)
{
}

RegularExpressionMatchIterator::RegularExpressionMatchIterator()
    : d(new RegularExpressionMatchIteratorPrivate(RegularExpression(),
                                                  RegularExpression::NoMatch,
                                                  RegularExpression::NoMatchOption,
                                                  RegularExpressionMatch()))
{
}

RegularExpressionMatchIterator::~RegularExpressionMatchIterator()
{
    // [d] is automatically deleted
}

RegularExpressionMatchIterator::RegularExpressionMatchIterator(const RegularExpressionMatchIterator &iterator)
    : d(iterator.d)
{
}

RegularExpressionMatchIterator &RegularExpressionMatchIterator::operator=(const RegularExpressionMatchIterator &iterator)
{
    d = iterator.d;
    return *this;
}

bool RegularExpressionMatchIterator::isValid() const
{
    return d->next.isValid();
}

bool RegularExpressionMatchIterator::hasNext() const
{
    return d->hasNext();
}

RegularExpressionMatch RegularExpressionMatchIterator::peekNext() const
{
    if (!hasNext())
        std::fprintf(stderr, "RegularExpressionMatchIterator::peekNext() called on an iterator already at end\n");

    return d->next;
}

RegularExpressionMatch RegularExpressionMatchIterator::next()
{
    if (!hasNext()) {
        std::fprintf(stderr, "RegularExpressionMatchIterator::next() called on an iterator already at end\n");
        return d->next;
    }

    RegularExpressionMatch current = d->next;
    d->next = d->next.d->nextMatch();
    return current;
}

RegularExpression RegularExpressionMatchIterator::regularExpression() const
{
    return d->regularExpression;
}

RegularExpression::MatchType RegularExpressionMatchIterator::matchType() const
{
    return d->matchType;
}

RegularExpression::MatchOptions RegularExpressionMatchIterator::matchOptions() const
{
    return d->matchOptions;
}
