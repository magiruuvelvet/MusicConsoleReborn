#ifndef REQUESTMAPPER_HPP
#define REQUESTMAPPER_HPP

#include <HttpServer/HttpRequestHandler.hpp>

#include <memory>

class MusicConsoleServer;

// Note: QtWebApp creates a unique copy in its own thread for each request
class RequestMapper : public QtWebApp::HttpServer::HttpRequestHandler
{
    Q_OBJECT
    Q_DISABLE_COPY(RequestMapper)

public:
    RequestMapper(MusicConsoleServer *server, QObject *parent = nullptr);
    ~RequestMapper();

    void service(QtWebApp::HttpServer::HttpRequest &request, QtWebApp::HttpServer::HttpResponse &response);

signals:
    void shutdown();
    void reload();

private:
    MusicConsoleServer *server;

    QByteArray path;
    QByteArray method;
    QtWebApp::HttpServer::HttpRequest *request;
    QtWebApp::HttpServer::HttpResponse *response;
};

#endif // REQUESTMAPPER_HPP
