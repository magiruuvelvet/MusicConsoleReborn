#include <Core/MusicConsoleReborn.hpp>
#include <Media/MediaLibraryModel.hpp>
#include "MusicConsoleServer.hpp"

#include <I18n/I18n.hpp>

#include <Core/ConfigManager.hpp>
#include <Core/Logger.hpp>

#include <Utils/ProcessUptime.hpp>

#include <memory>
#include <csignal>

#ifdef INCLUDE_GIT_VERSION_DATA
namespace {
#include <GitVersion.hpp>
static const QByteArray GIT_VERSION =
    QByteArray("+git-") + GitVersion::GIT_VERSION_NUMBER +
  + "/" + GitVersion::GIT_BRANCH_NAME;
}
#else
namespace {
static const QByteArray GIT_VERSION;
}
#endif

// application exit function for signal handlers
__attribute__((noreturn))
static void quit_app(int)
{ std::exit(10); }

int main(int argc, char **argv)
{
    I18n::setToActiveLocale();

    MusicConsoleReborn::setApplicationName("musicconsole-reborn-server");
    MusicConsoleReborn::setApplicationDisplayName("Music Console Reborn Server");
    MusicConsoleReborn::setApplicationVersion("v0.1-1" + GIT_VERSION);
    MusicConsoleReborn::setOrganizationName("マギルゥーベルベット");

    // create application instance
    MusicConsoleReborn instance(&argc, std::move<const char**>(const_cast<const char**>(argv)));

    // print header
    LOG_INFO("%s%s%s %s",
             TermColor::code(TermColor::Bold),
             MusicConsoleReborn::applicationDisplayName(),
             TermColor::code(TermColor::Reset),
             MusicConsoleReborn::applicationVersion());

    // check if application is already running
    if (instance.isRunning())
    {
        static constexpr const auto reset = TermColor::code(TermColor::Reset);
        static constexpr const auto italic = TermColor::code(TermColor::Italic);
        static const auto &notice_fmt = TermColor::fg({166, 74, 0}, TermColor::Bold);
        LOG_INFO(*I18n::get("%sNOTICE:%s %sonly one instance is allowed!%s"),
                 notice_fmt, reset, italic, reset);
        return 5;
    }

    // start process uptime timer (has its own event loop)
    ProcessUptime::Start();

    // register unix signals
    std::signal(SIGINT,  quit_app);
    std::signal(SIGTERM, quit_app);
    std::signal(SIGQUIT, quit_app);

    // load configuration
    Config()->LoadConfigFromDisk();

    // register local environment variables
    MusicConsoleReborn::processEnvironment().insert("MUSICCONSOLE_CONFIG_DIR", ConfigManager::GetConfigDirectory());
    MusicConsoleReborn::processEnvironment().insert("MUSICCONSOLE_LIB_DIR", Config()->GetLibraryRootPath());
    MusicConsoleReborn::processEnvironment().insert("MUSICCONSOLE_APP_DIR", MusicConsoleReborn::applicationDirPath());

    // setup media library
    MusicConsoleReborn::library()->setFilters(AUDIO, Config()->GetFiletypes(AUDIO));
    MusicConsoleReborn::library()->setFilters(VIDEO, Config()->GetFiletypes(VIDEO));
    MusicConsoleReborn::library()->setFilters(MOD, Config()->GetFiletypes(MOD));
    MusicConsoleReborn::library()->setDirectory(Config()->GetLibraryRootPath());
    if (!MusicConsoleReborn::library()->isDirectoryValid())
    {
        LOG_ERROR(*I18n::get("Server: Invalid media directory: %s"), MusicConsoleReborn::library()->pathString());
        return 1;
    }
    MusicConsoleReborn::processEnvironment().insert("MUSICCONSOLE_LIB_DIR", Config()->GetLibraryRootPath());
    LOG_INFO(*I18n::get("Scanning library, please wait..."));
    MusicConsoleReborn::library()->scan();
    LOG_INFO(*I18n::get("Scan complete."));

    // initialize server
    auto address = Config()->GetServerListeningAddress();
    auto port = Config()->GetServerListeningPort();
    auto server = std::make_unique<MusicConsoleServer>(address, port);

    // install qt signals
    QObject::connect(server.get(), &MusicConsoleServer::stopped, server.get(), &MusicConsoleServer::deleteLater);
    QObject::connect(server.get(), &MusicConsoleServer::stopped, &instance, &QCoreApplication::quit);

    // start server
    QTimer::singleShot(0, server.get(), &MusicConsoleServer::start);

    return instance.exec();
}
