#ifndef ROUTE_HPP
#define ROUTE_HPP

#include <HttpServer/HttpRequestHandler.hpp>
#include <TemplateEngine/Template.hpp>

class MusicConsoleServer;

namespace Routes {

using HttpRequest = QtWebApp::HttpServer::HttpRequest;
using HttpResponse = QtWebApp::HttpServer::HttpResponse;

enum ResultCode {
    Success = 0,
    Error = 1,

    NoMatchingRoute = 0xFF,
};

/**
 * Abstract base routing class.
 */
class Route
{
public:
    Route();
    virtual ~Route() = 0;

    static QtWebApp::TemplateEngine::Template applicationLayout(const QByteArray &title, MusicConsoleServer *server);

    /**
     * Checks if the given path matches the current route.
     * Note that query strings are not part of the path.
     * QtWebApp handles this already.
     *
     * Pure virtual method; must be implemented!
     */
    virtual bool matches(const QByteArray &path) = 0;

    /**
     * The current route should be handled as a controller.
     * The match string is considered as a prefix and removed
     * from the new internal controller path.
     *
     * Examples:
     *  path == /media
     *
     *  /media == true,     new path = /
     *  /media/ == true,    new path = /
     *  /media/sub == true, new path = /sub
     *  /media1 == false
     *  /mediasomethingelse == false
     */
    static bool matchesController(const QByteArray &reqPath, const QByteArray &controllerName,
                                  QByteArray &newPath);

    /**
     * Executes the current route.
     * The route should END the response and no further HTTP
     * methods should be called after the execution.
     *
     * This method should also set the Content-Type!
     * The global default is plain text.
     *
     * The HttpRequest and HttpResponse must be given
     * to any routing subclass.
     *
     * The MusicConsoleServer is optimal and defaults to nullptr.
     */
    virtual ResultCode exec(HttpRequest *request, HttpResponse *response, MusicConsoleServer *server = nullptr) = 0;

    /**
     * Content-Disposition HTTP header value
     */
    struct ContentDisposition {

        // content behavior
        enum {
            Attachment,      // force download
            Inline,          // display the content inline, if possible
        } type;

        // preferred content file name
        QByteArray fileName;

        const QByteArray header() const
        {
            if (this->fileName.isEmpty())
            {
                return QByteArray();
            }

            switch (this->type)
            {
                case Attachment:
                    return "attachment; filename=\"" + this->fileName + "\"";
                    break;

                case Inline:
                    return "inline; filename=\"" + this->fileName + "\"";
                    break;
            }
        }
    };

    // several Content-Type helpers to avoid typos
    static void setPlainText(HttpResponse *response);
    static void setHtml(HttpResponse *response);
    static void setBinary(HttpResponse *response, const ContentDisposition &contentDisposition = {});

    // several audio mime types
    // TODO: add more types here
    static void setFLAC(HttpResponse *response, const QByteArray &fileName);

    // cache control helpers
    static void disableCache(HttpResponse *response);
    static void cacheStaticAssets(HttpResponse *response);
};

} // namespace Routes

#endif // ROUTE_HPP
