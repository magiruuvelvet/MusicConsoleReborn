#include "Route.hpp"

#include <MusicConsoleServer.hpp>

namespace Routes {

Route::Route()
{
}

Route::~Route()
{
}

QtWebApp::TemplateEngine::Template Route::applicationLayout(const QByteArray &title, MusicConsoleServer *server)
{
    auto tpl = server->templateCache()->getTemplate("layout");
    tpl.setCondition("hasTitle", !title.isEmpty());
    if (!title.isEmpty())
    {
        tpl.setVariable("title", title);
    }
    return QtWebApp::TemplateEngine::Template(tpl.toUtf8(), "layout");
}

bool Route::matchesController(const QByteArray &reqPath, const QByteArray &controllerName, QByteArray &newPath)
{
    newPath.clear();

    if (reqPath.startsWith(controllerName))
    {
        newPath = reqPath.mid(controllerName.size());
        if (newPath.isEmpty())
        {
            newPath = "/";
        }
        if (!newPath.startsWith("/"))
        {
            return false;
        }
        return true;
    }

    return false;
}

void Route::setPlainText(HttpResponse *response)
{
    response->setHeader("Content-Type", "text/plain; charset=UTF-8");
}

void Route::setHtml(HttpResponse *response)
{
    response->setHeader("Content-Type", "text/html; charset=UTF-8");
}

void Route::setBinary(HttpResponse *response, const ContentDisposition &contentDisposition)
{
    response->setHeader("Content-Type", "application/octet-stream");

    auto contentDispositionHeader = contentDisposition.header();
    if (!contentDispositionHeader.isEmpty())
    {
        response->setHeader("Content-Disposition", contentDispositionHeader);
    }
}

void Route::setFLAC(HttpResponse *response, const QByteArray &fileName)
{
    response->setHeader("Content-Type", "audio/flac");
    response->setHeader("Content-Disposition", ContentDisposition{
        ContentDisposition::Inline,
        fileName,
    }.header());
}

void Route::disableCache(HttpResponse *response)
{
    // disable caching and force revalidation (for use with dynamic content)
    response->setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
}

void Route::cacheStaticAssets(HttpResponse *response)
{
#ifdef MUSICCONSOLE_DEBUG
    Route::disableCache(response);
#else
    // cache static assets for 1 year
    response->setHeader("Cache-Control", "public, max-age=31536000");
#endif
}

} // namespace Routes
