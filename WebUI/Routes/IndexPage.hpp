#ifndef INDEXPAGE_HPP
#define INDEXPAGE_HPP

#include "Route.hpp"

namespace Routes {

class IndexPage final : public Route
{
public:
    IndexPage();

    bool matches(const QByteArray &path) override;

    ResultCode exec(HttpRequest *request, HttpResponse *response, MusicConsoleServer *server = nullptr) override;
};

} // namespace Routes

#endif // INDEXPAGE_HPP
