#ifndef MEDIACONTROLLER_HPP
#define MEDIACONTROLLER_HPP

#include "Route.hpp"

namespace Routes {

class MediaController final : public Route
{
public:
    MediaController();

    bool matches(const QByteArray &path) override;

    ResultCode exec(HttpRequest *request, HttpResponse *response, MusicConsoleServer *server = nullptr) override;

private:
    QByteArray path;
};

} // namespace Routes

#endif // MEDIACONTROLLER_HPP
