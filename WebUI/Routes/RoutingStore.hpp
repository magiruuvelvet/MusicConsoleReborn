#ifndef ROUTINGSTORE_HPP
#define ROUTINGSTORE_HPP

#include <list>
#include <memory>

#include "Route.hpp"

class RoutingStore final
{
    RoutingStore();
    ~RoutingStore();

public:
    using RouteList = std::list<std::shared_ptr<Routes::Route>>;

    static void clear();

    static void registerRoute(const std::shared_ptr<Routes::Route> &route);

    inline static const RouteList &routes()
    { return RoutingStore::_routes; }

private:
    static RouteList _routes;
};

#endif // ROUTINGSTORE_HPP
