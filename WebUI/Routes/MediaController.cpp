#include "MediaController.hpp"

#include <MusicConsoleServer.hpp>

namespace Routes {

MediaController::MediaController()
    : Route()
{
}

bool MediaController::matches(const QByteArray &path)
{
    return Route::matchesController(path, "/media", this->path);
}

ResultCode MediaController::exec(HttpRequest *request, HttpResponse *response, MusicConsoleServer *server)
{
    Route::setPlainText(response);
    response->setStatus(200);

    (void) request;
    (void) server;

    response->write(this->path, true);
    return Success;
}

} // namespace Routes
