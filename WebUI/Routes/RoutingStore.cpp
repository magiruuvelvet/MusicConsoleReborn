#include "RoutingStore.hpp"

RoutingStore::RouteList RoutingStore::_routes;

void RoutingStore::clear()
{
    RoutingStore::_routes.clear();
}

void RoutingStore::registerRoute(const std::shared_ptr<Routes::Route> &route)
{
    RoutingStore::_routes.emplace_back(route);
}
