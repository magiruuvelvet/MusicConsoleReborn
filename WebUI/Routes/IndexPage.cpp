#include "IndexPage.hpp"

#include <MusicConsoleServer.hpp>

namespace Routes {

IndexPage::IndexPage()
    : Route()
{
}

bool IndexPage::matches(const QByteArray &path)
{
    return (path.isEmpty() || path == "/");
}

ResultCode IndexPage::exec(HttpRequest *request, HttpResponse *response, MusicConsoleServer *server)
{
    Route::setHtml(response);
    response->setStatus(200);

    auto layout = Route::applicationLayout("", server);

    auto tpl = server->templateCache()->getTemplate("index/test", request->getHeader("Accept-Language"));
    tpl.setVariable("path", request->getPath());

    auto headers = request->getHeaderMap();
    tpl.loop("header", headers.size());
    auto i = 0;
    for (auto kv = headers.begin(); kv != headers.end(); ++kv)
    {
        tpl.setVariable(QString("header%1.name").arg(i), QString(kv.key()));
        tpl.setVariable(QString("header%1.value").arg(i), QString(kv.value()));
        ++i;
    }

    auto params = request->getParameterMap();
    tpl.loop("parameter", params.size());
    i = 0;
    for (auto kv = params.begin(); kv != params.end(); ++kv)
    {
        tpl.setVariable(QString("parameter%1.name").arg(i), QString(kv.key()));
        tpl.setVariable(QString("parameter%1.value").arg(i), QString(kv.value()));
        ++i;
    }

    layout.setVariable("body", tpl.toUtf8());
    response->write(layout.toUtf8(), true);
    return Success;
}

} // namespace Routes
