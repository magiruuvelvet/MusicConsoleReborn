#include "TemplateLoader.hpp"
#include <QFile>
#include <QFileInfo>
#include <QStringList>
#include <QDir>
#include <QSet>

QTWEBAPP_TEMPLATEENGINE_NAMESPACE_BEGIN

TemplateLoader::TemplateLoader(const QString &path, QObject* parent)
    : QObject(parent)
{
    templatePath = path;
    fileNameSuffix = ".tpl";
    textCodec = QTextCodec::codecForName("utf-8");
}

TemplateLoader::~TemplateLoader()
{}

QString TemplateLoader::tryFile(const QString &localizedName)
{
    QString fileName = templatePath + "/" + localizedName + fileNameSuffix;
    qDebug("TemplateCache: trying file %s", qPrintable(fileName));

    QFile file(fileName);
    if (file.exists())
    {
        file.open(QIODevice::ReadOnly);
        QString document = textCodec->toUnicode(file.readAll());
        file.close();
        if (file.error())
        {
            qCritical("TemplateLoader: cannot load file %s, %s", qPrintable(fileName), qPrintable(file.errorString()));
            return "";
        }
        else
        {
            return document;
        }
    }

    return "";
}

Template TemplateLoader::getTemplate(const QString &templateName, const QString &locales)
{
    QSet<QString> tried; // used to suppress duplicate attempts
    QStringList locs = locales.split(',', QString::SkipEmptyParts);

    // Search for exact match
    for (auto&& loc : locs)
    {
        loc.replace(QRegExp(";.*"), "");
        loc.replace('-', '_');
        QString localizedName = templateName + "-" + loc.trimmed();
        if (!tried.contains(localizedName))
        {
            QString document = tryFile(localizedName);
            if (!document.isEmpty())
            {
                return Template(document, localizedName);
            }
            tried.insert(localizedName);
        }
    }

    // Search for correct language but any country
    for (auto&& loc : locs)
    {
        loc.replace(QRegExp("[;_-].*"), "");
        QString localizedName = templateName + "-" + loc.trimmed();
        if (!tried.contains(localizedName))
        {
            QString document = tryFile(localizedName);
            if (!document.isEmpty())
            {
                return Template(document, localizedName);
            }
            tried.insert(localizedName);
        }
    }

    // Search for default file
    QString document = tryFile(templateName);
    if (!document.isEmpty())
    {
        return Template(document, templateName);
    }

    qCritical("TemplateCache: cannot find template %s", qPrintable(templateName));
    return Template("", templateName);
}

QTWEBAPP_TEMPLATEENGINE_NAMESPACE_END
