#include "TemplateCache.hpp"
#include <QDateTime>
#include <QStringList>
#include <QSet>

QTWEBAPP_TEMPLATEENGINE_NAMESPACE_BEGIN

TemplateCache::TemplateCache(const QString &path, QObject *parent)
    :TemplateLoader(path, parent)
{
    cache.setMaxCost(1000000);
    cacheTimeout = 60000;
}

QString TemplateCache::tryFile(const QString &localizedName)
{
    qint64 now = QDateTime::currentMSecsSinceEpoch();
    mutex.lock();

    // search in cache
    qDebug("TemplateCache: trying cached %s", qPrintable(localizedName));
    CacheEntry *entry = cache.object(localizedName);
    if (entry && (cacheTimeout == 0 || entry->created > now - cacheTimeout))
    {
        mutex.unlock();
        return entry->document;
    }

    // search on filesystem
    entry = new CacheEntry();
    entry->created = now;
    entry->document = TemplateLoader::tryFile(localizedName);

    // Store in cache even when the file did not exist, to remember that there is no such file
    cache.insert(localizedName, entry, entry->document.size());
    mutex.unlock();
    return entry->document;
}

QTWEBAPP_TEMPLATEENGINE_NAMESPACE_END
