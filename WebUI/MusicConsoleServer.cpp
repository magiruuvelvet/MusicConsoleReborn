#include "MusicConsoleServer.hpp"

#include <I18n/I18n.hpp>
#include <Core/Logger.hpp>

#include <random>
#include <chrono>

MusicConsoleServer::MusicConsoleServer(const QString &listeningAddress, const quint16 &listeningPort, QObject *parent)
    : QObject(parent)
{
    this->m_httpServerSettings.reset(new QtWebApp::HttpServerSettings);
    this->m_sessionStoreSettings.reset(new QtWebApp::HttpSessionStoreSettings);
    this->m_listeningAddress = listeningAddress;
    this->m_listeningPort = listeningPort;
    this->m_httpServerSettings->host = listeningAddress;
    this->m_httpServerSettings->port = listeningPort;

    this->m_sessionStore = std::make_unique<QtWebApp::HttpServer::HttpSessionStore>(this->m_sessionStoreSettings.get());
    this->m_templateCache = std::make_unique<QtWebApp::TemplateEngine::TemplateCache>(Config()->GetServerTemplatesPath());
}

MusicConsoleServer::~MusicConsoleServer()
{
    this->m_httpServerSettings.reset();
    this->m_sessionStoreSettings.reset();
    this->m_requestMapper.reset();
    this->m_httpListener.reset();
    this->m_sessionStore.reset();
    this->m_templateCache.reset();
}

bool MusicConsoleServer::isPortAvailable(const quint16 &port)
{
    static QMutex mutex;
    mutex.lock();

    QTcpSocket socket;

    if (socket.bind(port))
    {
        socket.close();
        mutex.unlock();
        return true;
    }

    mutex.unlock();
    return false;
}

const bool &MusicConsoleServer::isRunning() const
{
    return this->m_isRunning;
}

const QString &MusicConsoleServer::listeningAddress() const
{
    return this->m_listeningAddress;
}

const quint16 &MusicConsoleServer::listeningPort() const
{
    return this->m_listeningPort;
}

void MusicConsoleServer::setListeningAddress(const QString &address)
{
    if (address.size() != 0)
    {
        this->m_mutex.lock();
        this->m_listeningAddress = address;
        this->m_httpServerSettings->host = address;
        this->m_mutex.unlock();
        LOG_NOTICE(*I18n::get("Server: set listening address to '%s'"), address);
        emit listeningAddressChanged();
    }

    else
    {
        LOG_ERROR(*I18n::get("Server: '%s' is not a valid listening address."), address);
        LOG_NOTICE(*I18n::get("Server: listening address unchanged."));
    }
}

void MusicConsoleServer::setListeningPort(const quint16 &port)
{
    // do nothing if port is unchanged
    if (port == this->m_listeningPort && port == this->m_httpServerSettings->port)
    {
        LOG_NOTICE(*I18n::get("Server: listening port is already %i"), port);
        return;
    }

    if (/*port > 0 && port <= 65535 && */this->isPortAvailable(port))
    {
        this->m_mutex.lock();
        this->setPortInternal(port);
        this->m_mutex.unlock();
        emit listeningPortChanged();
    }

    else if (port == 0)
    {
        static std::random_device rd;
        static std::mt19937 mt(rd());
        static std::uniform_int_distribution<quint16> dist(1, 65535);

        this->m_mutex.lock();
        quint16 random_port = dist(mt);

        if (this->isPortAvailable(random_port))
        {
            this->setPortInternal(random_port);
            this->m_mutex.unlock();
            emit listeningPortChanged();
        }
    }

    else
    {
        LOG_ERROR(*I18n::get("Server: %s = invalid listening port or port is already taken"), port);
        LOG_NOTICE(*I18n::get("Server: listening port unchanged."));
    }
}

void MusicConsoleServer::start()
{
    if (!this->m_isRunning)
    {
        if (this->m_listeningAddress.size() == 0 || this->m_listeningPort == 0)
        {
            LOG_ERROR("Server: can not start server, please specify a valid address and port number");
            emit error(InvalidAddressOrPort);
            return;
        }

        this->startInternal();
        emit started();
    }

    else
    {
        LOG_WARNING("Server: server is already running on %s:%i", this->m_listeningAddress, this->m_listeningPort);
    }
}

void MusicConsoleServer::stop()
{
    if (this->m_isRunning)
    {
        this->stopInternal();
        emit stopped();
    }

    else
    {
        LOG_WARNING("Server: server already stopped.");
    }
}

void MusicConsoleServer::halt()
{
    if (this->m_isRunning)
    {
        this->stopInternal();
        LOG_NOTICE("Server: server halted.");
        emit halted();
    }

    else
    {
        LOG_WARNING("Server: server not running.");
    }
}

void MusicConsoleServer::restart()
{
    this->stopInternal();
    this->startInternal();
    emit restarted();
}

QtWebApp::HttpServer::HttpSessionStore *MusicConsoleServer::sessionStore() const
{
    return this->m_sessionStore.get();
}

QtWebApp::TemplateEngine::TemplateCache *MusicConsoleServer::templateCache() const
{
    return this->m_templateCache.get();
}

void MusicConsoleServer::setPortInternal(const quint16 &port)
{
    this->m_listeningPort = port;
    this->m_httpServerSettings->port = port;
    LOG_NOTICE(*I18n::get("Server: listening port changed to %i"), port);
}

void MusicConsoleServer::startInternal()
{
    LOG_NOTICE(*I18n::get("Server: starting up..."));

    if (!this->m_httpListener && !this->m_requestMapper)
    {
        this->m_requestMapper.reset(new RequestMapper(this, this));
        this->m_httpListener.reset(new QtWebApp::HttpServer::HttpListener(this->m_httpServerSettings.get(), this->m_requestMapper.get(), this));
        QObject::connect(this->m_requestMapper.get(), &RequestMapper::shutdown, this, &MusicConsoleServer::stop);
        QObject::connect(this->m_requestMapper.get(), &RequestMapper::reload, this, &MusicConsoleServer::restart);
        this->m_isRunning = true;
        LOG_NOTICE(*I18n::get("Server: started."));
    }
}

void MusicConsoleServer::stopInternal()
{
    LOG_NOTICE(*I18n::get("Server: shutting down..."));

    if (this->m_httpListener && this->m_requestMapper)
    {
        QObject::disconnect(this->m_requestMapper.get(), &RequestMapper::shutdown, this, &MusicConsoleServer::stop);
        QObject::disconnect(this->m_requestMapper.get(), &RequestMapper::reload, this, &MusicConsoleServer::restart);
        this->m_requestMapper.reset();

        this->m_httpListener->close();
        this->m_httpListener.reset();

        this->m_isRunning = false;
        LOG_NOTICE(*I18n::get("Server: stopped."));
    }
}
