<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="description" content="Music Console Reborn Server">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    {if hasTitle}
      <title>{title} ─ Music Console Reborn</title>
    {else hasTitle}
      <title>Music Console Reborn</title>
    {end hasTitle}
  </head>
  <body>
    {body}
  </body>
</html>
