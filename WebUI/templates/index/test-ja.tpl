<p>あなたはパスをリクエストしました: {path}</p>
<p>「要求ヘッダー」</p>
<ul>
  {loop header}
    <li><code>{header.name}: {header.value}</code></li>
  {end header}
</ul>
<p>Query string parameters:</p>
<ul>
  {loop parameter}
    <li><code>{parameter.name}: {parameter.value}</code></li>
  {end parameter}
</ul>
