<p>You requested the path: {path}</p>
<p>Request headers:</p>
<ul>
  {loop header}
    <li><code>{header.name}: {header.value}</code></li>
  {end header}
</ul>
<p>Query string parameters:</p>
<ul>
  {loop parameter}
    <li><code>{parameter.name}: {parameter.value}</code></li>
  {end parameter}
</ul>
