#include "RequestMapper.hpp"

#include "MusicConsoleServer.hpp"
#include "Routes/RoutingStore.hpp"

#include "Routes/IndexPage.hpp"
#include "Routes/MediaController.hpp"

#include <Core/Logger.hpp>

RequestMapper::RequestMapper(MusicConsoleServer *server, QObject *parent)
    : HttpRequestHandler(parent)
{
    this->server = server;

    // register all available routes
    RoutingStore::clear();
    RoutingStore::registerRoute(std::make_shared<Routes::IndexPage>());
    RoutingStore::registerRoute(std::make_shared<Routes::MediaController>());
}

RequestMapper::~RequestMapper()
{
    this->server = nullptr;
    RoutingStore::clear();
}

void RequestMapper::service(QtWebApp::HttpServer::HttpRequest &request, QtWebApp::HttpServer::HttpResponse &response)
{
    this->request = &request;
    this->response = &response;

    this->path = this->request->getPath();
    this->method = this->request->getMethod();

#ifdef MUSICCONSOLE_DEBUG
    LOG_NOTICE("[HTTP %s] RequestMapper: (path=%s)", this->method.constData(), this->path.constData());
#endif

    // set default content-type to plain text
    response.setHeader("Content-Type", "text/plain; charset=UTF-8");

    // disable caching by default (overwrite in routes and controllers as needed)
    Routes::Route::disableCache(this->response);

    // result code from routing store
    Routes::ResultCode result = Routes::NoMatchingRoute;

    // iterate over all registered routes
    for (auto&& route : RoutingStore::routes())
    {
        if (route->matches(path))
        {
            result = route->exec(this->request, this->response, this->server);
            break;
        }
    }

    // 404 error; no matching route found
    if (result == Routes::NoMatchingRoute)
    {
        // TODO: 404 html error page
        response.setStatus(404);
        response.write(QString::fromUtf8("No handler for URL '%1' found").arg(this->path.constData()).toUtf8(), true);
    }

    // reset data and pointers
    this->path.clear();
    this->method.clear();
    this->request = nullptr;
    this->response = nullptr;
}
