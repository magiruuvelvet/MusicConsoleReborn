#ifndef MUSICCONSOLESERVER_HPP
#define MUSICCONSOLESERVER_HPP

#include <QObject>
#include <QMutex>

#include <HttpServer/HttpServerSettings.hpp>
#include <HttpServer/HttpListener.hpp>
#include <HttpServer/HttpSessionStore.hpp>
#include <TemplateEngine/TemplateCache.hpp>
#include "RequestMapper.hpp"

#include <memory>

class MusicConsoleServer final : public QObject
{
    Q_OBJECT

public:
    MusicConsoleServer(const QString &listeningAddress, const quint16 &listeningPort, QObject *parent = nullptr);
    ~MusicConsoleServer();

    enum ErrorCode : quint8 {
        InvalidAddressOrPort,
    };

    static bool isPortAvailable(const quint16 &port);
    const bool &isRunning() const;

    const QString &listeningAddress() const;
    const quint16 &listeningPort() const;

    void setListeningAddress(const QString &address);
    void setListeningPort(const quint16 &port);

    void start();
    void stop();
    void halt();
    void restart();

    QtWebApp::HttpServer::HttpSessionStore *sessionStore() const;
    QtWebApp::TemplateEngine::TemplateCache *templateCache() const;

signals:
    void listeningAddressChanged();
    void listeningPortChanged();

    void started();
    void stopped();
    void halted();
    void restarted();

    void error(ErrorCode);

private:
    QMutex m_mutex;

    bool m_isRunning = false;

    std::unique_ptr<QtWebApp::HttpServerSettings> m_httpServerSettings;
    std::unique_ptr<QtWebApp::HttpSessionStoreSettings> m_sessionStoreSettings;
    std::unique_ptr<QtWebApp::HttpServer::HttpListener> m_httpListener;
    std::unique_ptr<QtWebApp::HttpServer::HttpSessionStore> m_sessionStore;
    std::unique_ptr<QtWebApp::TemplateEngine::TemplateCache> m_templateCache;
    std::unique_ptr<RequestMapper> m_requestMapper;

    QString m_listeningAddress = "127.0.0.1";
    quint16 m_listeningPort = 3823;

    void setPortInternal(const quint16 &port);
    void startInternal();
    void stopInternal();
};

#endif // MUSICCONSOLESERVER_HPP
