#ifndef CONSOLE_HPP
#define CONSOLE_HPP

#include <QObject>
#include <QList>

#include <Core/Command.hpp>

class QRegularExpression;
class RegularExpression;

class Console final
{
public:
    Console();
    ~Console();

    // ANSI escape sequences to control the terminal screen and cursor
    static void ResetStyle();
    static void ClearTerminal();
    static void ClearCursorLine();
    static void ClearEverythingBelowCursor();
    static void MoveCursorUp(uint lines = 1);
    static void MoveCursorDown(uint lines = 1);
    static void SaveCursor();
    static void RestoreCursor();

    inline static const QRegularExpression &split_command()
    { return SPLIT_COMMAND; }

    void RegisterCommands();
    void RegisterPlayers();

    static const StringList GetCommandCompletions();
    static const StringList GetFilterCommandCompletions();
    static const StringList GetSubCommandCompletions();
    static const StringList GetFilterCompletions();

    void InitConfig();
    void UpdateConfig(bool firststart, bool *libpathchanged = nullptr);
    void ExecReloadConfigCommand();
    void DeinitConfig();

    int enter();

private:
    inline bool compare(const QString &s1, const QString &s2)
    { return QString::compare(s1, s2, Qt::CaseInsensitive) == 0; }
    inline bool compare(const CommandBase &cmd, const QString &s)
    { return compare(cmd.command(), s); }
    inline bool compare(const Command *cmd1, const CommandBase &cmd2)
    { return cmd1 ? compare(cmd1->command(), cmd2.command()) : false; }
    inline bool compare(const CommandBase &cmd1, const Command *cmd2)
    { return compare(cmd2, cmd1); }
    inline bool compare(const Command *cmd1, const Command *cmd2)
    { return cmd1 && cmd2 ? compare(cmd1->command(), cmd2->command()) : false; }

    bool _configIsInit;

    static const QRegularExpression SPLIT_COMMAND;
    static const RegularExpression SPLIT_COMMAND2;
};

#endif // CONSOLE_HPP
