#ifndef COMMANDSTORE_HPP
#define COMMANDSTORE_HPP

#include <QList>

#include <memory>

class Command;

class CommandStore final
{
    CommandStore() = delete;

public:

    // registers a new command in the store
    static bool RegisterCommand(const std::shared_ptr<Command> &cmd);

    // sets a given command by name as default command in the store
    static void SetDefaultCommand(const QString &name);
    static void SetDefaultCommand(std::nullptr_t);

    // return const reference to command list, the commands itself are non-const
    static const QList<std::shared_ptr<Command>> &GetCommands();

    // return pointer to default command, nullptr if no default command was set
    static Command *GetDefaultCommand();

    // returns the number of commands in the store
    static int Count();

    // deletes all commands
    static void Deinitialize();

private:
    static QList<std::shared_ptr<Command>> _commands;
    static Command *_defaultCommand;

    static bool ContainsCommand(Command *cmd);
    struct _Result {
        bool found = false;
        Command *cmd = nullptr;
    };
    static _Result ContainsCommand(const QString &name, std::nullptr_t);
};

#endif // COMMANDSTORE_HPP
