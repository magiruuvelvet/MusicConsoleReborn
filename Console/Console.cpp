#include "Console.hpp"
#include "Replxx.hpp"

#include <I18n/I18n.hpp>

#include <Core/MusicConsoleReborn.hpp>
#include <Core/ConfigManager.hpp>
#include <Core/Logger.hpp>

#include <Utils/Randomizer.hpp>
#include <Utils/PrefixRemover.hpp>
#include <Utils/SearchTerms.hpp>
#include <Utils/KbHit.hpp>
#include <Utils/TermColor.hpp>

#include <Media/MediaLibraryModel.hpp>

#include <Types/RegularExpression.hpp>

#include <Core/CommandParseHelper.hpp>
#include "CommandStore.hpp"

#ifdef MUSICCONSOLE_DEBUG
#include "Commands/DevCommands.hpp"
#endif
#include "Commands/StandardFilter.hpp"
#include "Commands/Search.hpp"
#include "Commands/RandomShuffleRepeat.hpp"
#include "Commands/History.hpp"
#include "Commands/Stats.hpp"
#include "Commands/LibMiscCommands.hpp"
#include "Commands/Playlist.hpp"
#include "Commands/Clear.hpp"
#include "Commands/Config.hpp"

const QRegularExpression Console::SPLIT_COMMAND = QRegularExpression(R"((?<!["'])(&&)(?!["']))");
const RegularExpression Console::SPLIT_COMMAND2 = RegularExpression(R"((?<!["'])(&&)(?!["']))");

Console::Console()
{
    _configIsInit = false;
}

Console::~Console()
{
    _configIsInit = false;
}

#include <unistd.h>

// replxx requires that we use ::write (unistd) to write ANSI escape sequences to the stdout
// if using std::printf() or anything else, than the escape sequences are not processed immediately
// until the Enter key was pressed, this is because replxx does some weird things to the tty itself

namespace {
    struct sequence_t {
        const void *seq = nullptr;
        const std::size_t size = 0;
        void write() const
        { (void) ::write(1, seq, size); }
    };

    static inline const sequence_t create_sequence(const char *seq)
    {
        return{seq, std::strlen(seq)};
    }
}

void Console::ResetStyle()
{
    static const auto &seq = create_sequence(TermColor::code(TermColor::Reset));
    seq.write();
}

void Console::ClearTerminal()
{
    // clear terminal AND scrollback
    // replxx's own clear terminal sequence keeps the scrollback
    static const auto &seq = create_sequence("\033c\033[3J");
    seq.write();
}

void Console::ClearCursorLine()
{
    static const auto &seq = create_sequence("\033[2K");
    seq.write();
}

void Console::ClearEverythingBelowCursor()
{
    static const auto &seq = create_sequence("\033[J");
    seq.write();
}

void Console::MoveCursorUp(uint lines)
{
    const auto &seq = create_sequence(fmt::format("\033%iA", lines).c_str());
    seq.write();
}

void Console::MoveCursorDown(uint lines)
{
    const auto &seq = create_sequence(fmt::format("\033%iB", lines).c_str());
    seq.write();
}

void Console::SaveCursor()
{
    static const auto &seq = create_sequence("\0337");
    seq.write();
}

void Console::RestoreCursor()
{
    static const auto &seq = create_sequence("\0338");
    seq.write();
}

void Console::RegisterCommands()
{
    CommandStore::RegisterCommand(std::make_shared<Commands::StandardFilter>(Config()->GetCommand(ConfigManager::Command::audio), AUDIO));
    CommandStore::RegisterCommand(std::make_shared<Commands::StandardFilter>(Config()->GetCommand(ConfigManager::Command::video), VIDEO));
    CommandStore::RegisterCommand(std::make_shared<Commands::StandardFilter>(Config()->GetCommand(ConfigManager::Command::mod), MOD));
    CommandStore::RegisterCommand(std::make_shared<Commands::StandardFilter>(Config()->GetCommand(ConfigManager::Command::all), NONE));
    CommandStore::RegisterCommand(std::make_shared<Commands::Search>(Config()->GetCommand(ConfigManager::Command::search)));
    CommandStore::RegisterCommand(std::make_shared<Commands::RandomShuffleRepeat>(Config()->GetCommand(ConfigManager::Command::random),
                                      Commands::RandomShuffleRepeat::Random));
    CommandStore::RegisterCommand(std::make_shared<Commands::RandomShuffleRepeat>(Config()->GetCommand(ConfigManager::Command::shuffle),
                                      Commands::RandomShuffleRepeat::Shuffle));
    CommandStore::RegisterCommand(std::make_shared<Commands::RandomShuffleRepeat>(Config()->GetCommand(ConfigManager::Command::repeat),
                                      Commands::RandomShuffleRepeat::Repeat));
    CommandStore::RegisterCommand(std::make_shared<Commands::History>(Config()->GetCommand(ConfigManager::Command::history)));
    CommandStore::RegisterCommand(std::make_shared<Commands::Stats>(Config()->GetCommand(ConfigManager::Command::statistics)));
    CommandStore::RegisterCommand(std::make_shared<Commands::Rescan>(Config()->GetCommand(ConfigManager::Command::rescan)));
    CommandStore::RegisterCommand(std::make_shared<Commands::Playlist>(Config()->GetCommand(ConfigManager::Command::playlist)));
    CommandStore::RegisterCommand(std::make_shared<Commands::Clear>(Config()->GetCommand(ConfigManager::Command::clear)));
    CommandStore::RegisterCommand(std::make_shared<Commands::Config>(Config()->GetCommand(ConfigManager::Command::config)));

#ifdef MUSICCONSOLE_DEBUG
    CommandStore::RegisterCommand(std::make_shared<DevCommands::ConfigTest>("dev:config"));
    CommandStore::RegisterCommand(std::make_shared<DevCommands::TagReaderTest>("dev:tag"));
    CommandStore::RegisterCommand(std::make_shared<DevCommands::ScanTest>("dev:scan"));
    CommandStore::RegisterCommand(std::make_shared<DevCommands::SearchTermTest>("dev:s"));
    CommandStore::RegisterCommand(std::make_shared<DevCommands::RandTest>("dev:rand"));
    CommandStore::RegisterCommand(std::make_shared<DevCommands::PlayerTest>("dev:player"));
    CommandStore::RegisterCommand(std::make_shared<DevCommands::LastPlayedTest>("dev:lastplayed"));
    CommandStore::RegisterCommand(std::make_shared<DevCommands::ThreadingTests>("dev:thr"));
    CommandStore::RegisterCommand(std::make_shared<DevCommands::KbHitTest>("dev:kbhit"));
    CommandStore::RegisterCommand(std::make_shared<DevCommands::PlaylistTest>("dev:pl"));
    CommandStore::RegisterCommand(std::make_shared<DevCommands::RegExPlayground>("dev:regex"));
    CommandStore::RegisterCommand(std::make_shared<DevCommands::NumberTest>("dev:num"));
    CommandStore::RegisterCommand(std::make_shared<DevCommands::TypesTest>("dev:types"));
#endif

    // set default command
    const auto &defcmd = Config()->GetDefaultCommand();
    if (!defcmd.isEmpty())
        CommandStore::SetDefaultCommand(defcmd);
}

void Console::RegisterPlayers()
{
    const auto &players = Config()->GetPlayers();
    const auto &userPlayers = Config()->GetUserPlayers();

    for (auto&& p : players)
        PlayerController::RegisterPlayer(p);
    for (auto&& p : userPlayers)
        PlayerController::RegisterPlayerOverride(p, true);
}

const StringList Console::GetCommandCompletions()
{
    // command completions
    StringList commandCompletions;
    for (auto&& c : CommandStore::GetCommands())
        commandCompletions.append(c->command().toUtf8());
    return commandCompletions;
}

const StringList Console::GetFilterCommandCompletions()
{
    StringList filterCommandCompletions;

    // filter commands
    filterCommandCompletions.append(Config()->GetFilterCommand(AUDIO).toUtf8());
    filterCommandCompletions.append(Config()->GetFilterCommand(VIDEO).toUtf8());
    filterCommandCompletions.append(Config()->GetFilterCommand(MOD).toUtf8());

    return filterCommandCompletions;
}

const StringList Console::GetSubCommandCompletions()
{
    StringList subCommandCompletions;

    // sub commands
    subCommandCompletions.append(Config()->GetPlaylistFileLoadCommand().toUtf8());
    subCommandCompletions.append(Config()->GetPlaylistCrossfadeCommand().toUtf8());
    //subCommandCompletions.append(Commands::Config::GetSubCommands());

    return subCommandCompletions;
}

const StringList Console::GetFilterCompletions()
{
    StringList filterCompletions;

    // special filter completions
    filterCompletions.append("sp:bottomphrases");
    filterCompletions.append("sp:empty");
    filterCompletions.append("wo:");
    filterCompletions.append("wo:sp:bottomphrases");
    filterCompletions.append("wo:sp:empty");
    filterCompletions.append("wg:");
    filterCompletions.append("wg:sp:bottomphrases");
    filterCompletions.append("wg:sp:empty");

    return filterCompletions;
}

void Console::InitConfig()
{
#ifdef MUSICCONSOLE_DEBUG
    Replxx::SetHistoryFileLocation((ConfigManager::GetConfigDirectory() + "/history.debug").constData());
#else
    Replxx::SetHistoryFileLocation((ConfigManager::GetConfigDirectory() + "/history").constData());
#endif
}

void Console::UpdateConfig(bool firststart, bool *libpathchanged)
{
    // deinitialize old config
    DeinitConfig();

    if (!_configIsInit)
    {
        // set library root path
        const auto &current_lib_path = MusicConsoleReborn::library()->pathString();
        MusicConsoleReborn::library()->setDirectory(Config()->GetLibraryRootPath());
        MusicConsoleReborn::processEnvironment().insert("MUSICCONSOLE_LIB_DIR", Config()->GetLibraryRootPath());

        // validate library root path
        if (!MusicConsoleReborn::library()->isDirectoryValid())
        {
            LOG_CRITICAL("Library path is not valid: %s", MusicConsoleReborn::library()->pathString());
            if (!firststart)
            {
                LOG_NOTICE("Reverting to old path: %s", current_lib_path);
                MusicConsoleReborn::library()->setDirectory(current_lib_path);
                MusicConsoleReborn::processEnvironment().insert("MUSICCONSOLE_LIB_DIR", current_lib_path);
            }
        }
        if (libpathchanged && !firststart &&
            current_lib_path != MusicConsoleReborn::library()->pathString())
        {
            *libpathchanged = true;
        }

        // register commands
        RegisterCommands();

        // register players
        RegisterPlayers();

        // set replxx prompt
        Replxx::SetPrompt(Config()->GetPrompt());

        // set appearance settings
        AppearanceController::SetAppearanceSettings(Config()->GetAppearanceSettings());

        // set move to bottom phrases
        SearchTerms::SetMoveToBottomPhrases(Config()->GetMoveToBottomPhrases());

        // set history ignore patterns
        Replxx::SetHistIgnore(Config()->GetHistIgnorePatterns());

        // set prefix deletion patterns
        PrefixRemover::SetPrefixDeletionPatterns(Config()->GetPrefixDeletionPatterns());

        // set randomizer history size
        Randomizer::SetHistorySize(Config()->GetRandomizerHistorySize());

        // set kbhit sleep interval
        KbHit::SetSleepInterval(Config()->GetKbhitSleepInterval());

        // set file extension filters
        MusicConsoleReborn::library()->setFilters(AUDIO, Config()->GetFiletypes(AUDIO));
        MusicConsoleReborn::library()->setFilters(VIDEO, Config()->GetFiletypes(VIDEO));
        MusicConsoleReborn::library()->setFilters(MOD, Config()->GetFiletypes(MOD));

        // do initial library scan
        SaveCursor();
        LOG_INFO(*I18n::get("Scanning library, please wait..."));
        MusicConsoleReborn::library()->scan();
        RestoreCursor();
        ClearCursorLine();
        ClearEverythingBelowCursor();

        // set replxx autocompletion
        const auto &commandCompletions = GetCommandCompletions();
        const auto &filterCommandCompletions = GetFilterCommandCompletions();
        const auto &subCommandCompletions = GetSubCommandCompletions();
        const auto &filterCompletions = GetFilterCompletions();

        Replxx::SetAutocompletionAndHints(commandCompletions +
                                          filterCommandCompletions +
                                          subCommandCompletions +
                                          filterCompletions +
                                          MusicConsoleReborn::library()->GenerateReplxxAutocompletionList());

        // set input hints properties
        const auto &hints_enabled = Config()->GetHintsEnabled();
        Replxx::SetHintsEnabled(hints_enabled);
        if (hints_enabled)
        {
            Replxx::SetHintRows(Config()->GetHintRows());
            Replxx::SetHintColor(Config()->GetHintColor());
        }

        // syntax highlighter
        const auto &highlighter_enabled = Config()->GetHighlighterEnabled();
        Replxx::SetHighlighterEnabled(highlighter_enabled);
        if (highlighter_enabled)
        {
            Replxx::UpdateHighlighter(
                {commandCompletions,       "RED"},         // commands
                {filterCommandCompletions, "BROWN"},       // filter commands
                {subCommandCompletions,    "GREEN"},       // sub commands
                {filterCompletions,        "CYAN"},        // sp: filters
                {{SPLIT_COMMAND2},         "LIGHTGRAY"});  // separator
        }

        _configIsInit = true;
    }
}

void Console::ExecReloadConfigCommand()
{
    LOG_NOTICE("Reloading configuration...");
    Config()->LoadConfigFromDisk();
    bool libpathchanged = false;
    UpdateConfig(false, &libpathchanged);
    LOG_NOTICE("Configuration updated!");
    if (libpathchanged)
        LOG_HINT("Library path changed. Please do a manual rescan with \"%s\".",
            Config()->GetCommand(ConfigManager::Command::rescan));
}

void Console::DeinitConfig()
{
    if (_configIsInit)
    {
        // deinitialize commands
        CommandStore::Deinitialize();

        // deregister all players
        PlayerController::DeregisterAll();

        _configIsInit = false;
    }
}

int Console::enter()
{
    // initialize replxx components
    Replxx::Init();

    // one-time initialization
    InitConfig();

    // update config
    UpdateConfig(true);

    // FIXME: entirely remove QString and replace with UnicodeString
    while (true)
    {
        // get user input and parse it
        // notice: unparsed raw data is still pushed to replxx history as-is for convenience
        const auto &input = QString::fromUtf8(Replxx::Get().to_utf8().c_str());

        // do nothing when command is empty
        if (input.isEmpty())
            continue;

        // split commands at '&&' outside of quotes (shell style)
        const auto &commands = input.split(SPLIT_COMMAND, QString::SkipEmptyParts);
        LOG_FIXME("qt:          %s", commands);
        LOG_FIXME("replacement: %s", UnicodeString(input.toUtf8().constData()).split(SPLIT_COMMAND2, UnicodeString::TrimAndSkipEmptyParts));
        bool _break = false;
        for (auto&& c : commands)
        {
            const auto &splitcmd = CommandParseHelper::Parse(c);

            // do nothing when split command is empty
            if (splitcmd.isEmpty())
                continue;

            // only exit when the command "exit" has no arguments
            if (compare(splitcmd, Config()->GetCommand(ConfigManager::Command::exit)) && !splitcmd.hasArgs())
            {
                // set final break mark
                _break = true;
                break;
            }

            // update configuration command (must be placed here to avoid memory corruption)
            else if (compare(splitcmd, Config()->GetCommand(ConfigManager::Command::config)))
            {
                if (splitcmd.hasArgs() && splitcmd.arguments().at(0).compare(Commands::Config::GetReloadCommand(), Qt::CaseInsensitive) == 0)
                {
                    ExecReloadConfigCommand();
                    continue;
                }
                // pass through all other arguments to the Config command class
            }

            bool commandExecuted = false;
            for (auto&& command : CommandStore::GetCommands())
            {
                // check if current command matches the input command
                if (compare(command.get(), splitcmd))
                {
                    // set command arguments
                    command->setArguments(splitcmd.arguments());

                    // execute the command's routine
                    command->exec();

                    // clear command arguments
                    command->clearArguments();

                    commandExecuted = true;
                }
            }

            // no command was executed yet, run default command
            if (!commandExecuted)
            {
                auto *defaultCommand = CommandStore::GetDefaultCommand();
                if (defaultCommand)
                {
                    // execute default command with entire input interpreted as arguments
                    const auto &a = QStringList{splitcmd.command()} + splitcmd.arguments();
                    defaultCommand->setArguments(a);
                    defaultCommand->exec();
                    defaultCommand->clearArguments();
                }
                else
                {
                    LOG_NOTICE(*I18n::get("Console: unknown command: %s"), splitcmd.command());
                }
            }
        }

        // final break on exit
        if (_break)
            break;
    }

    // deinitialize config
    DeinitConfig();

    // deinitialize replxx components
    Replxx::Deinit();

    // exit with status of 0
    return 0;
}
