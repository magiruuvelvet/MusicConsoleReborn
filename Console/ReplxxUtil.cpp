#define REPLXX_MAIN_HEADER_INCLUDED
#include "ReplxxUtil.hpp"

#include <Core/ConfigManager.hpp>
#include <Core/Logger.hpp>

#include <Media/Playlist/PlaylistParser.hpp>

#include "Console.hpp"
#include "Commands/Config.hpp"

#include <Types/StringUtils.hpp>

ReplxxUtil::RxColor ReplxxUtil::hint_color = RxColor::DEFAULT;
ReplxxUtil::HighlightPatterns ReplxxUtil::highlight_patterns;
StringList ReplxxUtil::_autocompletionAndHintList;

ReplxxUtil::RxColor ReplxxUtil::ParseColorName(const String &color_name)
{
    const auto &_cn = color_name;
    RxColor color;

#define COMPARE_COLOR(COLOR_NAME, ENUM_VALUE) \
    if (_cn.case_insensitive_compare(COLOR_NAME)) \
        color = RxColor::ENUM_VALUE

         COMPARE_COLOR("BLACK",         BLACK);
    else COMPARE_COLOR("RED",           RED);
    else COMPARE_COLOR("GREEN",         GREEN);
    else COMPARE_COLOR("BROWN",         BROWN);
    else COMPARE_COLOR("BLUE",          BLUE);
    else COMPARE_COLOR("MAGENTA",       MAGENTA);
    else COMPARE_COLOR("CYAN",          CYAN);
    else COMPARE_COLOR("LIGHTGRAY",     LIGHTGRAY);
    else COMPARE_COLOR("GRAY",          GRAY);
    else COMPARE_COLOR("BRIGHTRED",     BRIGHTRED);
    else COMPARE_COLOR("BRIGHTGREEN",   BRIGHTGREEN);
    else COMPARE_COLOR("YELLOW",        YELLOW);
    else COMPARE_COLOR("BRIGHTBLUE",    BRIGHTBLUE);
    else COMPARE_COLOR("BRIGHTMAGENTA", BRIGHTMAGENTA);
    else COMPARE_COLOR("BRIGHTCYAN",    BRIGHTCYAN);
    else COMPARE_COLOR("WHITE",         WHITE);
    else COMPARE_COLOR("DEFAULT",       DEFAULT);
    else COMPARE_COLOR("",              DEFAULT); // no warning on empty

    else
    {
        LOG_WARNING("Replxx: unknown color: %s", color_name);
        color = RxColor::DEFAULT;
    }

    return color;
}

const String ReplxxUtil::EscapeStringForReplxx(const String &str)
{
    String escaped = str;

    for (String::size_type i = 0; i < escaped.size(); i++)
    {
        if (escaped.at(i) == ' ')
        {
            escaped.replace(i, 1, "\\ ");
            i++;
        }
        else if (escaped.at(i) == '"')
        {
            escaped.replace(i, 1, "\\\"");
            i++;
        }
        else if (escaped.at(i) == '\'')
        {
            escaped.replace(i, 1, "\\'");
            i++;
        }
    }

    return escaped;
}

const QByteArray ReplxxUtil::EscapeByteArrayForReplxx(const QByteArray &ba)
{
    QByteArray escaped = ba;

    for (auto i = 0; i < escaped.size(); i++)
    {
        if (escaped.at(i) == ' ')
        {
            escaped.replace(i, 1, "\\ ");
            i++;
        }
        else if (escaped.at(i) == '"')
        {
            escaped.replace(i, 1, "\\\"");
            i++;
        }
        else if (escaped.at(i) == '\'')
        {
            escaped.replace(i, 1, "\\'");
            i++;
        }
    }

    return escaped;
}

inline const ReplxxUtil::current_context_t ReplxxUtil::get_current_context(const QString &input)
{
    static const auto &empty = current_context_t{-1, QString(), QString()};

    const auto &current = input.split(Console::split_command(), QString::KeepEmptyParts);
    const auto &currentS = current.last().simplified();
    const auto &currentStartIndex = input.lastIndexOf(current.last());

    return current.isEmpty() ? empty : (
        current.last().isEmpty() ? empty : current_context_t{currentStartIndex, StringUtils::ltrim(current.last()), currentS});
}

ReplxxUtil::HookFuncContainer ReplxxUtil::completion_hook(const std::string &context, int index, void *user_data)
{
    const auto&& list = static_cast<StringList*>(user_data);
    HookFuncContainer completions;
    static RxColor stub;
    hook_internal(completions, context, index, stub, list, AUTOCOMPLETION);
    return completions;
}

ReplxxUtil::HookFuncContainer ReplxxUtil::hint_hook(const std::string &context, int index, RxColor &color, void *user_data)
{
    const auto&& list = static_cast<StringList*>(user_data);
    HookFuncContainer hints;
    hook_internal(hints, context, index, color, list, HINT);
    return hints;
}

inline void ReplxxUtil::hook_internal(HookFuncContainer &c,
                                      const std::string &context, const int &index, RxColor &color, const StringList *list,
                                      const HookFuncMode &mode)
{
    // convert utf-8 encoded std::string into a QString
    const auto &utf8context = QString::fromUtf8(context.c_str());

    // set hint color
    color = hint_color;

    // get current context, command split at '&&'
    const auto &current = get_current_context(utf8context);

    // playlist files
    const auto &playlist_file_seq = cmp_string(
        Config()->GetCommand(ConfigManager::Command::playlist) + " " + Config()->GetPlaylistFileLoadCommand(), mode);
    if (current.input_simplified.startsWith(playlist_file_seq, Qt::CaseInsensitive))
    {
        for (auto&& playlist : PlaylistParser::GetAllPlaylists())
        {
            const QString _item(EscapeByteArrayForReplxx(playlist));
            const auto &name = current.input.mid(current.input.lastIndexOf(' ')).trimmed();
            if (_item.startsWith(name, Qt::CaseInsensitive))
            {
                emplace_back(c, _item, name.size(), mode);
            }
        }
    }

    // commands without library specific arguments
    else if (current.input_simplified.startsWith(Config()->GetCommand(ConfigManager::Command::history)) ||
             current.input_simplified.startsWith(Config()->GetCommand(ConfigManager::Command::statistics)) ||
             current.input_simplified.startsWith(Config()->GetCommand(ConfigManager::Command::rescan)) ||
             current.input_simplified.startsWith(Config()->GetCommand(ConfigManager::Command::clear)))
    { }

    // config command
    else if (current.input_simplified.startsWith(cmp_string(Config()->GetCommand(ConfigManager::Command::config), mode)))
    {
        const auto &cmds = current.input_simplified.mid(Config()->GetCommand(ConfigManager::Command::config).size())
                                                   .split(QRegularExpression("[\\s]"), QString::SkipEmptyParts);

        if (cmds.size() <= 1 && (
                !cmds.isEmpty() ? !Commands::Config::GetSubCommands().contains(cmds.at(0).toUtf8()) : true
            ))
        {
            iterate_config_commands(c, current, mode, Commands::Config::GetSubCommands());
        }

        // FIXME: size check isn't correct, one too far ahead
        else if (cmds.size() == 1 || cmds.size() == 2)
        {
            iterate_config_command(c, current, mode, cmds.at(0), Commands::Config::commands, Commands::Config::GetCommandCommands());
            iterate_config_command(c, current, mode, cmds.at(0), Commands::Config::randomizer, Commands::Config::GetRandomizerCommands());
        }
    }

    // library
    else
    {
        const auto &prefix = utf8context.mid(index);

        // only show hints if the prefix is at least 'n' chars long
        // does nothing in autocompletion mode
        if (mode == HINT && !(prefix.size() >= 2))
        {
            return;
        }

        for (auto&& item : *list)
        {
            const QString _item(item);
            if (_item.startsWith(prefix, Qt::CaseInsensitive))
            {
                emplace_back(c, _item, prefix.size(), mode);
            }
        }
    }
}

void ReplxxUtil::highlight_hook(const std::string &context, Replxx::colors_t &colors, void *user_data)
{
    const auto&& patterns = static_cast<HighlightPatterns*>(user_data);
    const auto &utf8context = UnicodeString(context);

    // iterate over all regular expressions
    for (auto&& pattern : *patterns)
    {
        auto matches = pattern.first.globalMatch(utf8context);
        while (matches.hasNext())
        {
            const auto &match = matches.next();
            for (auto i = match.capturedStart(0); i < match.capturedEnd(0); i++)
            {
                // set all indices from the match positions to the current color
                colors.at(i) = pattern.second;
            }
        }
    }
}
