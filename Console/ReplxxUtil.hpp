#ifndef REPLXX_MAIN_HEADER_INCLUDED
#error "Do not include this header directly! Use <Replxx.hpp> instead."
#else
#ifndef REPLXXHOOKS_HPP
#define REPLXXHOOKS_HPP

#include "Commands/Config.hpp"

#include <vector>
#include <string>
#include <map>

#include <Types/RegularExpression.hpp>

#include <replxx.hxx>

class ReplxxUtil final
{
    friend class Replxx;

    ReplxxUtil() = delete;

    using Replxx = replxx::Replxx;
    using RxColor = Replxx::Color;
    using HighlightPatterns = std::vector<std::pair<RegularExpression, RxColor>>;

    using HookFuncContainer = std::vector<std::string>;
    enum HookFuncMode {
        AUTOCOMPLETION,
        HINT
    };

    static RxColor hint_color;
    static HighlightPatterns highlight_patterns;
    static StringList _autocompletionAndHintList;

    // parses Replxx color names and converts it to the matching enum value
    static RxColor ParseColorName(const String &color_name);

    // escapes reserved characters in a byte array to add support for Replxx
    static const String EscapeStringForReplxx(const String &str);
    // FIXME: remove this !
    static const QByteArray EscapeByteArrayForReplxx(const QByteArray &ba);

    // finds the current input context (command split at '&&') and
    // its start position and returns a struct with the data
    struct current_context_t {
        const int index;
        const QString input;
        const QString input_simplified;
    };
    static inline const current_context_t get_current_context(const QString &input);

    static inline void emplace_back(HookFuncContainer &c, const QString &item, const int &size, const HookFuncMode &mode)
    {
        switch (mode)
        {
            case AUTOCOMPLETION: c.emplace_back(item.toUtf8()); break;
            case HINT:           c.emplace_back(item.mid(size).toUtf8()); break;
        }
    }

    static inline void iterate_config_commands(HookFuncContainer &c, const current_context_t &current, const HookFuncMode &mode,
                                               const QByteArrayList &cmds)
    {
        for (auto&& cmd : cmds)
        {
            const QString _item(cmd);
            const auto &name = current.input.mid(current.input.lastIndexOf(' ')).trimmed();
            if (_item.startsWith(name, Qt::CaseInsensitive))
            {
                emplace_back(c, _item, name.size(), mode);
            }
        }
    }

    static inline void iterate_config_command(HookFuncContainer &c, const current_context_t &current, const HookFuncMode &mode,
                                              const QString &condition, const Commands::Config::SubCommand &cmd, const QByteArrayList &list)
    {
        if (condition == Commands::Config::GetSubCommands().at(cmd))
            iterate_config_commands(c, current, mode, list);
    }

    // Attention: index counts the Unicode code points, NOT the bytes
    static HookFuncContainer completion_hook(const std::string &context, int index, void *user_data);
    static HookFuncContainer hint_hook(const std::string &context, int index, RxColor &color, void *user_data);
    static inline void hook_internal(HookFuncContainer &c,
                                     const std::string &context, const int &index, RxColor &color, const StringList *list,
                                     const HookFuncMode &mode);
    static void highlight_hook(const std::string &context, Replxx::colors_t &colors, void *user_data);

    static inline const QString cmp_string(const QString &in, const HookFuncMode &mode)
    {
        switch (mode)
        {
            case AUTOCOMPLETION: return in;
            case HINT:           return in + " ";
        }
    }
};

#endif // REPLXXHOOKS_HPP
#endif
