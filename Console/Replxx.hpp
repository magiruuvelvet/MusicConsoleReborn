#ifndef REPLXX_HPP
#define REPLXX_HPP

#define REPLXX_MAIN_HEADER_INCLUDED

#include <Types/String.hpp>
#include <Types/UnicodeString.hpp>
#include <Types/RegularExpression.hpp>

#include <memory>

// forward replxx to avoid including it in the entire Console
namespace replxx { class Replxx; }

class Replxx final
{
    Replxx() = delete;

public:
    static void Init();
    static void Deinit();

    // sets the replxx prompt, default is empty
    static void SetPrompt(const String &prompt);

    // sets the history file location
    static void SetHistoryFileLocation(const String &history);

    // set ignore patterns for the history
    static void SetHistIgnore(const List<RegularExpression> &ignorePatterns);

    // set replxx autocompletion and hints list
    static void SetAutocompletionAndHints(const StringList &hints);
    static const StringList &CurrentAutocompletionAndHints();
    static void ClearAutocompletionAndHints();

    // input hints
    static void SetHintsEnabled(const bool &hints_enabled);
    static void SetHintRows(const uint &hint_rows);
    static void SetHintColor(const String &color_name);

    // command highlighting
    static void SetHighlighterEnabled(const bool &highlighter_enabled);
    // regex + highlight color
    using HighlightListColorPair = std::pair<const StringList, const String>;
    using HighlightListColorPairR = std::pair<const List<RegularExpression>, const String>;
    static void UpdateHighlighter(const HighlightListColorPair &commands,        // command names
                                  const HighlightListColorPair &filter_commands, // filter commands
                                  const HighlightListColorPair &sub_commands,    // sub commands like ::file
                                  const HighlightListColorPair &sp_filters,      // special SearchTerm filters
                                  const HighlightListColorPairR &separators);    // command separators

    // get user input, pushes input to replxx history
    static const UnicodeString Get();

    // get current history
    static const StringVector GetHistory();

private:
    static bool _isInit;

    static std::unique_ptr<replxx::Replxx> rx;

    // append new line to history
    static void append_history(const UnicodeString &line, const bool &starts_with_space);

    // location of history file
    static String _historyFile;

    // replxx prompt
    static String _replxxPrompt;

    // ignore patterns
    // FIXME: use ICU regex
    static List<RegularExpression> _ignorePatterns;
};

#endif // REPLXX_HPP
