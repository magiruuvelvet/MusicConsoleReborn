#ifndef CONFIG_HPP
#define CONFIG_HPP

#include <Core/Command.hpp>

#include <map>

namespace Commands {

class Config final : public Command
{
public:
    Config(const QString &name)
        : Command(name)
    { }

    struct Option {
        const QByteArray name;
        const QByteArray desc;
    };

    enum SubCommand {
        list,
        reload,         // out of scope command, see Console.cpp
        commands,
        librarypath,
        playlistpaths,
        prefixdeletionpatterns,
        movetobottomphrases,
        audioformats,
        videoformats,
        moduleformats,
        player,
        prompt,
        appearance,
        randomizer,
    };

    enum class CommandsCommand {
        audio,
        video,
        mod,
        all,
        search,
        random,
        shuffle,
        repeat,
        history,
        statistics,
        rescan,
        playlist,
        exit,
        clear,
        config,
        defaultcommand,
        filter_audio,
        filter_video,
        filter_mod,
        playlist_file_load,
        playlist_crossfade,
    };

    enum class RandomizerCommand {
        historysize,
    };

    static const QByteArrayList &GetSubCommands();
    static const QByteArrayList &GetCommandCommands();
    static const QByteArrayList &GetRandomizerCommands();
    static const QByteArray &GetReloadCommand();

    void exec() const override;
    inline void exec_from_command_line() const
    { started_from_command_line = true; exec(); }

    inline int lastStatus() const
    { return _lastStatus; }

private:
    static const std::map<SubCommand, Option> SubCommands;
    static const std::map<CommandsCommand, Option> CommandsCommands;
    static const std::map<RandomizerCommand, Option> RandomizerCommands;
    mutable int _lastStatus = 0;

    mutable bool started_from_command_line = false;

    static inline bool compare(const QString &text, const SubCommand &command)
    { return text.compare(SubCommands.at(command).name, Qt::CaseInsensitive) == 0; }

    static inline bool compare(const QString &text, const CommandsCommand &command)
    { return text.compare(CommandsCommands.at(command).name, Qt::CaseInsensitive) == 0; }

    static inline bool compare(const QString &text, const RandomizerCommand &command)
    { return text.compare(RandomizerCommands.at(command).name, Qt::CaseInsensitive) == 0; }

    static inline const QByteArrayList map_to_list(const void *command_map)
    {
        enum stub {};
        const auto *map = static_cast<const std::map<stub, Option>*>(command_map);

        QByteArrayList _list;
        for (auto&& cmd : *map)
            _list.append(cmd.second.name);
        return _list;
    }

    static quint16 longest_option_name();
    static void print_options(const void *command_map);
};

}

#endif // CONFIG_HPP
