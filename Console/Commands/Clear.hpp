#ifndef CLEAR_HPP
#define CLEAR_HPP

#include <Core/Command.hpp>

namespace Commands {

class Clear final : public Command
{
public:
    Clear(const QString &name)
        : Command(name)
    { }

    void exec() const override;
};

}

#endif // CLEAR_HPP
