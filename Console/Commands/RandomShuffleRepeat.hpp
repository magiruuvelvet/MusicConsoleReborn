#ifndef RANDOMSHUFFLEREPEAT_HPP
#define RANDOMSHUFFLEREPEAT_HPP

#include <Core/Command.hpp>

class SearchTerms;

namespace Commands {

class RandomShuffleRepeat final : public Command
{
public:
    enum Mode {
        Random,
        Shuffle,
        Repeat
    };

    RandomShuffleRepeat(const QString &name, const Mode &mode)
        : Command(name)
    { this->mode = mode; }

    void exec() const override;

private:
    Mode mode;

    void shuffle(const SearchTerms &st, const MediaType &type) const;
    void repeat(const SearchTerms &st, const MediaType &type) const;
    void repeat(const MediaFile *mf, const MediaType &type) const;
};

}

#endif // RANDOMSHUFFLEREPEAT_HPP
