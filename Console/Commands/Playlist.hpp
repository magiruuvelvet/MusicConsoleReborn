#ifndef PLAYLIST_HPP
#define PLAYLIST_HPP

#include <Core/Command.hpp>

namespace Commands {

class Playlist final : public Command
{
public:
    Playlist(const QString &name)
        : Command(name)
    { }

    void exec() const override;

private:
    void in_memory_playlist() const;
};

}

#endif // PLAYLIST_HPP
