#include "StandardFilter.hpp"

#include <I18n/I18n.hpp>

#include <Core/MusicConsoleReborn.hpp>
#include <Media/MediaLibraryModel.hpp>
#include <Media/PlayerController.hpp>
#include <Core/Logger.hpp>

#include <Utils/AppearanceController.hpp>
#include <Utils/SimpleTimeBenchmark.hpp>

void Commands::StandardFilter::exec() const
{
    if (!this->hasArgs())
    {
        LOG_ERROR(*I18n::get("%s: requires search terms"), this->command());
        return;
    }

    const auto &result = MusicConsoleReborn::library()->find(SearchTerms(this->mergeArgs()), this->type);

    if (result)
    {
        PlayerController::Play(result, true, this->type);
    }
    else
    {
        LOG_ERROR(*I18n::get("nothing found"));
    }
}
