#include "Stats.hpp"

#include <Core/Logger.hpp>
#include <Console/Console.hpp>
#include <Core/MusicConsoleReborn.hpp>
#include <Media/MediaLibraryModel.hpp>

#include <Utils/ProcessUptime.hpp>
#include <Utils/TermColor.hpp>

#include <Types/SystemInfo.hpp>

void Commands::Stats::exec() const
{
    static constexpr const auto *BOLD = TermColor::code(TermColor::Bold);
    static constexpr const auto *RESET = TermColor::code(TermColor::Reset);

    LOG_INFO("%s%s%s %s\n",
             BOLD, MusicConsoleReborn::applicationDisplayName(), RESET,
             MusicConsoleReborn::applicationVersion());

    LOG_HINT("System Info:         %s", SystemInfo::get_system_info());
    LOG_INFO("  > OS:              %s", SystemInfo::get_os_name());
    LOG_INFO("  > Hyper-threading: %s", SystemInfo::has_htt());
    LOG_INFO("  > SSE:             %s", SystemInfo::has_sse());
    LOG_INFO("  > SSE2:            %s", SystemInfo::has_sse2());
    LOG_INFO("  > SSSE3:           %s", SystemInfo::has_ssse3());
    LOG_INFO("  > SSE4.1:          %s", SystemInfo::has_sse41());
    LOG_INFO("  > SSE4.2:          %s", SystemInfo::has_sse42());
    LOG_INFO("  > AVX:             %s", SystemInfo::has_avx());
    LOG_INFO("  > AVX2:            %s", SystemInfo::has_avx2());
    LOG_INFO("  > AVX512*:         %s", SystemInfo::has_512());
    LOG_INFO("  > AVX xop:         %s", SystemInfo::has_xop());
    LOG_INFO("  > TSX:             %s", SystemInfo::has_rtm());
    LOG_INFO("One or more of these CPU extensions may be used for acceleration when possible.");
    LOG_INFO("\n");

    LOG_HINT("Bundled 3rd Party Libraries:");
    LOG_INFO("  > New Argument Vectors: %shypersoft%s, OPENWARE", BOLD, RESET);
    LOG_INFO("  > replxx:               %sCopyright (c) 2017-2018, Marcin Konarski (amok at codestation.org)%s", BOLD, RESET);
    LOG_INFO("  > StrFmt:               %sRPCS3 Contributors%s, GPLv2 License", BOLD, RESET);
    LOG_INFO("  > valijson:             %stristanpenman%s, Simplified BSD License", BOLD, RESET);
    LOG_INFO("  > rapidjson:            %sCopyright (C) 2015 THL A29 Limited, a Tencent company, and Milo Yip.%s", BOLD, RESET);
    LOG_INFO("\n");

    LOG_HINT("Total media files:   %i", MusicConsoleReborn::library()->count());
    LOG_INFO("  > Audio:           %i", MusicConsoleReborn::library()->count(AUDIO));
    LOG_INFO("  > Video:           %i", MusicConsoleReborn::library()->count(VIDEO));
    LOG_INFO("  > Module tracker:  %i", MusicConsoleReborn::library()->count(MOD));
    LOG_INFO("\n");

    LOG_HINT("Uptime:              %s", ProcessUptime::ElapsedHMS());
    LOG_INFO("\n");
}
