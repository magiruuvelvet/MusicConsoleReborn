#include "Playlist.hpp"

#include <Core/MusicConsoleReborn.hpp>
#include <Media/MediaLibraryModel.hpp>
#include <Media/PlayerController.hpp>
#include <Media/Playlist/PlaylistParser.hpp>
#include <Media/PlaylistQueue.hpp>
#include <Core/Logger.hpp>
#include <Utils/KbHit.hpp>

void Commands::Playlist::exec() const
{
    if (!this->hasArgs())
    {
        LOG_ERROR("%s: requires search terms and optimally a type filter or a playlist name", this->command());
        return;
    }

    const auto &file_load = Config()->GetPlaylistFileLoadCommand();
    const auto &crossfade = Config()->GetPlaylistCrossfadeCommand();

    if (this->arguments().at(0).compare(file_load, Qt::CaseInsensitive) == 0)
    {
        if (this->arguments().size() < 2)
        {
            LOG_ERROR("%s: [%s] requires a playlist name", this->command(), file_load);
        }
        else
        {
            // merge remaining arguments
            const auto &playlist_file = this->arguments().mid(1).join(' ').toUtf8();

            PlaylistQueue playlist;
            const auto &res = PlaylistParser::Parse(playlist_file, &playlist);

            switch (res)
            {
                case PlaylistParser::Success: {
                        // point to the first entry in the playlist
                        PlaylistQueue::PlaylistQueueEntry current = playlist.next();
                        // start kbhit loop
                        KBHIT_NOT_INFINITE(current.mf == nullptr)
                            PlayerController::Play(current.mf, true, current.player_override);
                            current = playlist.next(); // advance queue
                        KBHIT_NOT_INFINITE_END
                        playlist.clear();
                    }
                    break;
                case PlaylistParser::FileNotFound:
                    LOG_ERROR("%s: no playlist under the name \"%s\" found.",
                              this->command(), playlist_file);
                    break;
                case PlaylistParser::FileOpenError:
                    LOG_FATAL("%s: failed to read playlist \"%s\"! file i/o error or permission problem",
                              this->command(), playlist_file);
                    break;
                case PlaylistParser::NotAPlaylistFile:
                    LOG_ERROR("%s: \"%s\" is not a playlist file!",
                              this->command(), playlist_file);
                    break;

                // silence compiler warnings
                case PlaylistParser::Unknown: break;
                case PlaylistParser::InvalidPointer: break;
            }
        }
    }
    else if (this->arguments().at(0).compare(crossfade, Qt::CaseInsensitive) == 0)
    {
        if (this->arguments().size() < 2)
        {
            LOG_ERROR("%s: [%s] requires search terms and optimally a type filter", this->command(), crossfade);
        }
        else
        {
            // remove sub command
            this->shift();
            // apply type filter
            const auto &type = this->ApplyFilter();

            const auto &results = MusicConsoleReborn::library()->findMultiple(SearchTerms(this->mergeArgs()), type);

            if (results.isEmpty())
            {
                LOG_ERROR("nothing found");
                return;
            }
            else
            {
                PlayerController::Play(results);
            }
        }
    }
    else
    {
        in_memory_playlist();
    }
}

void Commands::Playlist::in_memory_playlist() const
{
    const auto &type = ApplyFilter();

    auto results = MusicConsoleReborn::library()->findMultiple(SearchTerms(this->mergeArgs()), type);

    if (results.isEmpty())
    {
        LOG_ERROR("nothing found");
    }
    else
    {
        KBHIT_NOT_INFINITE(results.isEmpty())
            PlayerController::Play(results.first(), true, type);
            results.removeFirst();
        KBHIT_NOT_INFINITE_END
    }
}
