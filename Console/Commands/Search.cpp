#include "Search.hpp"

#include <Core/MusicConsoleReborn.hpp>
#include <Media/MediaLibraryModel.hpp>
#include <Core/Logger.hpp>

#include <Utils/AppearanceController.hpp>
#include <Utils/SimpleTimeBenchmark.hpp>

void Commands::Search::exec() const
{
    if (!this->hasArgs())
    {
        LOG_ERROR("%s: requires search terms and optimally a type filter", this->command());
        return;
    }

    const auto &type = ApplyFilter();

    const auto &results = MusicConsoleReborn::library()->findMultiple(SearchTerms(this->mergeArgs()), type);

    if (results.isEmpty())
    {
        LOG_ERROR("nothing found");
    }
    else
    {
        for (auto&& res : results)
        {
            AppearanceController::Print(res);
        }
    }
}
