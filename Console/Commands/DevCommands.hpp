#ifdef MUSICCONSOLE_DEBUG
#ifndef TESTCOMMAND_HPP
#define TESTCOMMAND_HPP

#include <Core/Command.hpp>

namespace DevCommands {

#define DEFINE_DEV_COMMAND(NAME) \
    class NAME final : public Command \
    { \
    public: \
        NAME(const QString &name) \
            : Command(name) \
        { } \
    \
        void exec() const override; \
    }

DEFINE_DEV_COMMAND(ConfigTest);
DEFINE_DEV_COMMAND(TagReaderTest);
DEFINE_DEV_COMMAND(ScanTest);
DEFINE_DEV_COMMAND(SearchTermTest);
DEFINE_DEV_COMMAND(RandTest);
DEFINE_DEV_COMMAND(PlayerTest);
DEFINE_DEV_COMMAND(LastPlayedTest);
DEFINE_DEV_COMMAND(ThreadingTests);
DEFINE_DEV_COMMAND(KbHitTest);
DEFINE_DEV_COMMAND(PlaylistTest);
DEFINE_DEV_COMMAND(RegExPlayground);
DEFINE_DEV_COMMAND(NumberTest);
DEFINE_DEV_COMMAND(TypesTest);

}

#endif // TESTCOMMAND_HPP
#endif // MUSICCONSOLE_DEBUG
