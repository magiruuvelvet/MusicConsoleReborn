#ifndef HISTORY_HPP
#define HISTORY_HPP

#include <Core/Command.hpp>

namespace Commands {

class History final : public Command
{
public:
    History(const QString &name)
        : Command(name)
    { }

    void exec() const override;
};

}

#endif // HISTORY_HPP
