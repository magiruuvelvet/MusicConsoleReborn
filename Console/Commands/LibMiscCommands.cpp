#include "LibMiscCommands.hpp"

#include <Core/MusicConsoleReborn.hpp>
#include <Media/MediaLibraryModel.hpp>
#include <Console.hpp>
#include <Console/Replxx.hpp>
#include <Core/Logger.hpp>

void Commands::Rescan::exec() const
{
    Console::SaveCursor();
    LOG_INFO("Rescanning library, please wait...");
    MusicConsoleReborn::library()->scan();

    Replxx::SetAutocompletionAndHints(Console::GetCommandCompletions() +
                                      Console::GetFilterCommandCompletions() +
                                      Console::GetSubCommandCompletions() +
                                      Console::GetFilterCompletions() +
                                      MusicConsoleReborn::library()->GenerateReplxxAutocompletionList());

    Console::RestoreCursor();
    Console::ClearCursorLine();
    Console::ClearEverythingBelowCursor();
}
