#ifndef STANDARDFILTER_HPP
#define STANDARDFILTER_HPP

#include <Core/Command.hpp>

namespace Commands {

class StandardFilter final : public Command
{
public:
    StandardFilter(const QString &name, const MediaType &type)
        : Command(name)
    { this->type = type; }

    void exec() const override;

private:
    MediaType type;
};

}

#endif // STANDARDFILTER_HPP
