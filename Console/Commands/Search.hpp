#ifndef SEARCH_HPP
#define SEARCH_HPP

#include <Core/Command.hpp>

namespace Commands {

class Search final : public Command
{
public:
    Search(const QString &name)
        : Command(name)
    { }

    void exec() const override;
};

}

#endif // SEARCH_HPP
