#include "Clear.hpp"

#include <Console/Console.hpp>

void Commands::Clear::exec() const
{
    Console::ClearTerminal();
}
