#ifndef STATS_HPP
#define STATS_HPP

#include <Core/Command.hpp>

namespace Commands {

class Stats final : public Command
{
public:
    Stats(const QString &name)
        : Command(name)
    { }

    void exec() const override;
};

}

#endif // STATS_HPP
