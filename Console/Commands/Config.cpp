#include "Config.hpp"

#include <Core/Logger.hpp>
#include <Core/ConfigManager.hpp>
#include <Core/MusicConsoleReborn.hpp>
#include <Media/MediaLibraryModel.hpp>
#include <Console/Console.hpp>

using SubCommand = Commands::Config::SubCommand;
using CommandsCommand = Commands::Config::CommandsCommand;
using RandomizerCommand = Commands::Config::RandomizerCommand;
using Option = Commands::Config::Option;

const std::map<SubCommand, Option> Commands::Config::SubCommands = {
    {SubCommand::list,                   {"list",                    "Lists all available options."}},
    {SubCommand::reload,                 {"reload",                  "Reloads the configuration at runtime."}},
    {SubCommand::commands,               {"command",                 "Show/Set console commands."}},
    {SubCommand::librarypath,            {"library-path",            "Show/Set the library root path."}},
    {SubCommand::playlistpaths,          {"playlist-paths",          "Show/Set playlist paths."}},
    {SubCommand::prefixdeletionpatterns, {"prefixdel-patterns",      "Show/Set prefix deletion patterns."}},
    {SubCommand::movetobottomphrases,    {"movetobottom-phrases",    "Show/Set move to bottom phrases."}},
    {SubCommand::audioformats,           {"audio-formats",           "Show/Set audio formats."}},
    {SubCommand::videoformats,           {"video-formats",           "Show/Set video formats."}},
    {SubCommand::moduleformats,          {"module-formats",          "Show/Set module tracker formats."}},
    {SubCommand::player,                 {"player",                  "Show/Set players and player overrides."}},
    {SubCommand::prompt,                 {"prompt",                  "Show/Set the command prompt."}},
    {SubCommand::appearance,             {"appearance",              "Show/Set appearance settings."}},
    {SubCommand::randomizer,             {"randomizer",              "Show/Set the randomizer properties."}},
};

const std::map<CommandsCommand, Option> Commands::Config::CommandsCommands = {
    {CommandsCommand::audio,             {"audio",                   "Play: Audio format filter"}},
    {CommandsCommand::video,             {"video",                   "Play: Video format filter"}},
    {CommandsCommand::mod,               {"mod",                     "Play: Module tracker format filter"}},
    {CommandsCommand::all,               {"all",                     "Play: All-in-one (Audio, Video, Module)"}},
    {CommandsCommand::search,            {"search",                  "Search for matching files"}},
    {CommandsCommand::random,            {"random",                  "Play a random file"}},
    {CommandsCommand::shuffle,           {"shuffle",                 "Shuffle through your entire library"}},
    {CommandsCommand::repeat,            {"repeat",                  "Repeat the given file infinitely"}},
    {CommandsCommand::history,           {"history",                 "Show the command line history"}},
    {CommandsCommand::statistics,        {"statistics",              "Print application statistics"}},
    {CommandsCommand::rescan,            {"rescan",                  "Rescan the library for changes"}},
    {CommandsCommand::playlist,          {"playlist",                "Generate a playlist dynamically"}},
    {CommandsCommand::exit,              {"exit",                    "Exit the application (absolute match)"}},
    {CommandsCommand::clear,             {"clear",                   "Clears the terminal screen"}},
    {CommandsCommand::config,            {"config",                  "Configuration Manager"}},
    {CommandsCommand::defaultcommand,    {"default-command",         "Command which is executed by default"}},
    {CommandsCommand::filter_audio,      {"filter-audio",            "Sub filter for audio files"}},
    {CommandsCommand::filter_video,      {"filter-video",            "Sub filter for video files"}},
    {CommandsCommand::filter_mod,        {"filter-mod",              "Sub filter for module tracker files"}},
    {CommandsCommand::playlist_file_load,{"playlist-file-load",      "Load a playlist from a file"}},
    {CommandsCommand::playlist_crossfade,{"playlist-crossfade",      "Play a crossfade album"}},
};

const std::map<RandomizerCommand, Option> Commands::Config::RandomizerCommands = {
    {RandomizerCommand::historysize,     {"historysize",             "Show/Set the randomizer history size."}},
};

const QByteArrayList &Commands::Config::GetSubCommands()
{
    static const auto &_commands = map_to_list(&SubCommands);
    return _commands;
}

const QByteArrayList &Commands::Config::GetCommandCommands()
{
    static const auto &_commands = map_to_list(&CommandsCommands);
    return _commands;
}

const QByteArrayList &Commands::Config::GetRandomizerCommands()
{
    static const auto &_commands = map_to_list(&RandomizerCommands);
    return _commands;
}

const QByteArray &Commands::Config::GetReloadCommand()
{ return SubCommands.at(reload).name; }

void Commands::Config::exec() const
{
    // make sure the status is always 0
    _lastStatus = 0;

    // debug output
    LOG_INFO("config> args: %s", this->arguments());

    if (this->hasArgs())
    {
        // extract option name and shift arguments
        const auto &cmd = this->arguments().at(0).simplified();
        this->shift();

        bool changed = false;

        ///
        /// list
        ///
        if (compare(cmd, list))
        {
            LOG_HINT("Available options are:");
            print_options(&SubCommands);
        }

        ///
        /// commands
        ///
        else if (compare(cmd, commands))
        {
            if (this->hasArgs())
            {

            }
            else
            {
                print_options(&CommandsCommands);
            }
        }

        ///
        /// librarypath
        ///
        else if (compare(cmd, librarypath))
        {
            if (this->hasArgs())
            {
                ::Config()->SetLibraryRootPath(this->arguments().at(0).toUtf8());
                LOG_INFO("Changed library root path to: %s", ::Config()->GetLibraryRootPath(false));
                changed = true;
            }
            else
            {
                LOG_INFO("Library root path: %s", ::Config()->GetLibraryRootPath(false));
            }
        }

        ///
        /// playlistpaths
        ///
        else if (compare(cmd, playlistpaths))
        {

        }

        ///
        /// prefixdeletionpatterns
        ///
        else if (compare(cmd, prefixdeletionpatterns))
        {

        }

        ///
        /// movetobottomphrases
        ///
        else if (compare(cmd, movetobottomphrases))
        {

        }

        ///
        /// audioformats
        ///
        else if (compare(cmd, audioformats))
        {

        }

        ///
        /// videoformats
        ///
        else if (compare(cmd, videoformats))
        {

        }

        ///
        /// moduleformats
        ///
        else if (compare(cmd, moduleformats))
        {

        }

        ///
        /// player
        ///
        else if (compare(cmd, player))
        {

        }

        ///
        /// prompt
        ///
        else if (compare(cmd, prompt))
        {

        }

        ///
        /// appearance
        ///
        else if (compare(cmd, appearance))
        {

        }

        ///
        /// randomizer
        ///
        else if (compare(cmd, randomizer))
        {
            if (this->hasArgs())
            {
                const auto &cmd = this->arguments().at(0).simplified();
                this->shift();

                if (compare(cmd, RandomizerCommand::historysize))
                {
                    if (this->arguments().size() > 0)
                    {
                        bool ok;
                        const auto &res = this->arguments().at(0).toULongLong(&ok);
                        if (ok)
                        {
                            ::Config()->SetRandomizerHistorySize(res);
                            LOG_INFO("Changed randomizer history size to: %llu", ::Config()->GetRandomizerHistorySize());
                            changed = true;
                        }
                        else
                        {
                            LOG_ERROR("Not a valid number: %s", this->arguments().at(0));
                        }
                    }
                    else
                    {
                        LOG_INFO("Randomizer history size: %llu", ::Config()->GetRandomizerHistorySize());
                    }
                }
                else
                {
                    LOG_ERROR("Unknown property: '%s'!", cmd);
                }
            }
            else
            {
                print_options(&RandomizerCommands);
            }
        }

        ///
        /// (unknown option)
        ///
        else
        {
            LOG_ERROR("Unknown option '%s'!", cmd);
        }

        // only save when values has changed
        if (changed)
        {
            ::Config()->SaveConfigToDisk();
            if (!started_from_command_line)
                LOG_HINT("Configuration updated, please run '%s %s' for changes to take effect.", this->command(), GetReloadCommand());
        }
    }
    else
    {
        LOG_ERROR("No arguments provided. Use 'list' for a list of config options.");
        _lastStatus = 3;
    }
}

quint16 Commands::Config::longest_option_name()
{
    quint16 size = 0;
    for (auto&& o : SubCommands)
    {
        const auto &osize = static_cast<quint16>(o.second.name.size());
        if (osize > size)
        {
            size = osize;
        }
    }
    return size;
}

void Commands::Config::print_options(const void *command_map)
{
    enum stub {};
    const auto *map = static_cast<const std::map<stub, Option>*>(command_map);

    const auto &name_size = longest_option_name() + 15;
    for (auto&& c : *map)
    {
        auto display_name = c.second.name;
        display_name.append(name_size - display_name.size(), ' ');
        LOG_INFO("%s %s", display_name, c.second.desc);
    }
}
