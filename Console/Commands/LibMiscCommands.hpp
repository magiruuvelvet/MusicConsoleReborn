#ifndef LIBMISCCOMMANDS_HPP
#define LIBMISCCOMMANDS_HPP

#include <Core/Command.hpp>

namespace Commands {

class Rescan final : public Command
{
public:
    Rescan(const QString &name)
        : Command(name)
    { }

    void exec() const override;
};

}

#endif // LIBMISCCOMMANDS_HPP
