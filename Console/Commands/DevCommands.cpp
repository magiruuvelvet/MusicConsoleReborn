#ifdef MUSICCONSOLE_DEBUG
#include "DevCommands.hpp"

#include <Types/Char.hpp>
#include <Types/String.hpp>
#include <Types/UnicodeString.hpp>
#include <Types/RegularExpression.hpp>
#include <Types/Directory.hpp>

#include <Core/MusicConsoleReborn.hpp>
#include <Core/ConfigManager.hpp>
#include <Core/Logger.hpp>
#include <Console/Replxx.hpp>

#include <Media/MetadataReaderBackends/FFMpegMetadataReader.hpp>
#include <Media/MetadataReaderBackends/StubMetadataReader.hpp>

#include <Media/MediaLibraryModel.hpp>
#include <Media/PlaylistQueue.hpp>
#include <Media/Playlist/PlaylistParser.hpp>

#include <Utils/SearchTerms.hpp>
#include <Utils/Randomizer.hpp>
#include <Utils/AppearanceController.hpp>
#include <Utils/SimpleTimeBenchmark.hpp>
#include <Utils/KbHit.hpp>

#include <Types/Number.hpp>
#include <Types/StringUtils.hpp>

#include <QThread>
#include <QThreadPool>
#include <QThreadStorage>

#include <QVector>
#include <QList>

namespace DevCommands {

void ConfigTest::exec() const
{
    if (this->hasArgs())
    {
        if (this->arguments().at(0) == "load")
        {
            LOG_INFO("Loading settings from disk...");
            Config()->LoadConfigFromDisk();
        }
        else if (this->arguments().at(0) == "save")
        {
            LOG_INFO("Saving configuration to dis...");
            bool b = Config()->SaveConfigToDisk();
            LOG_WARNING("%s", b);
        }
        // test getters
        else if (this->arguments().at(0) == "testgetter")
        {
            LOG_INFO("Prompt:     %s", Config()->GetPrompt());
            LOG_INFO("KbHit:      %f", Config()->GetKbhitSleepInterval());
            LOG_INFO("HistIgnore: %s", Config()->GetHistIgnorePatterns());
            for (auto&& cmd : {
                ConfigManager::Command::audio,
                ConfigManager::Command::video,
                ConfigManager::Command::mod,
                ConfigManager::Command::all,
                ConfigManager::Command::search,
                ConfigManager::Command::random,
                ConfigManager::Command::shuffle,
                ConfigManager::Command::repeat,
                ConfigManager::Command::history,
                ConfigManager::Command::statistics,
                ConfigManager::Command::rescan,
                ConfigManager::Command::playlist,
                ConfigManager::Command::exit,
                ConfigManager::Command::clear,
                ConfigManager::Command::config,})
            {
                LOG_INFO("Cmd[%s]: %s", cmd, Config()->GetCommand(cmd));
            }
            LOG_INFO("DefCmd:     %s", Config()->GetDefaultCommand());
            for (auto&& cmd : {AUDIO, VIDEO, MOD, NONE})
                LOG_INFO("FilterCMD[%s]: %s", cmd, Config()->GetFilterCommand(cmd));
            LOG_INFO("LibPath:    %s", Config()->GetLibraryRootPath());
            for (auto&& cmd : {AUDIO, VIDEO, MOD, NONE})
                LOG_INFO("Filetypes[%s]: %s", cmd, Config()->GetFiletypes(cmd));
            LOG_INFO("Playlist Paths:    %s", Config()->GetLibraryPlaylistPaths());
            LOG_INFO("PrefixDelPatterns: %s", Config()->GetPrefixDeletionPatterns());
            LOG_INFO("MoveToBottomPh.:   %s", Config()->GetMoveToBottomPhrases());
            LOG_INFO("RandHistSize:      %llu", Config()->GetRandomizerHistorySize());

            const auto &appearanceSettings = Config()->GetAppearanceSettings();
            LOG_INFO("appearance[artist]: %s", appearanceSettings.artist);
            LOG_INFO("appearance[album]: %s", appearanceSettings.album);
            LOG_INFO("appearance[albmartist]: %s", appearanceSettings.albmartist);
            LOG_INFO("appearance[title]: %s", appearanceSettings.title);
            LOG_INFO("appearance[genre]: %s", appearanceSettings.genre);
            LOG_INFO("appearance[track]: %s", appearanceSettings.track);
            LOG_INFO("appearance[total_tracks]: %s", appearanceSettings.total_tracks);
            LOG_INFO("appearance[extension]: %s", appearanceSettings.extension);
            LOG_INFO("appearance[path]: %s", appearanceSettings.path);
            LOG_INFO("appearance[print_plain]: %s", appearanceSettings.print_plain);
            LOG_INFO("appearance[print_tagged]: %s", appearanceSettings.print_tagged);

            LOG_INFO("Players:     %s", Config()->GetPlayers());
            LOG_INFO("UserPlayers: %s", Config()->GetUserPlayers());
        }
        else if (this->arguments().at(0) == "set")
        {
            if (!(this->arguments().size() > 2))
            {
                LOG_ERROR("set requires a key name and a value");
                LOG_HINT("prompt, kbhit");
                return;
            }

            if (this->arguments().at(1) == "prompt")
            {
                Config()->SetPrompt(this->arguments().at(2).toUtf8());
                Replxx::SetPrompt(Config()->GetPrompt());
                LOG_HINT("prompt changed to \"%s\"", this->arguments().at(2));
            }
            else if (this->arguments().at(1) == "kbhit")
            {
                const auto &interval = this->arguments().at(2).toDouble();
                Config()->SetKbhitSleepInterval(interval);
                KbHit::SetSleepInterval(Config()->GetKbhitSleepInterval());
                LOG_HINT("kbhit changed to \"%f\"", interval);
            }
        }
    }
    else
    {
        LOG_ERROR("needs at least one argument");
        LOG_HINT("load, save, testgetter, set");
    }
}

void TagReaderTest::exec() const
{
    LOG_INFO("Tag Reader test");

    if (!this->hasArgs())
    {
        LOG_INFO("Filename required");
        return;
    }

#if defined(USE_FFMPEG_METADATA_READER) || defined(USE_LINKED_FFMPEG_METADATA_READER)
    const auto &tagreader = std::make_unique<FFMpegMetadataReader>();
    LOG_WARNING("%s", tagreader->ReadMetadataFromFile(this->arguments().at(0).toUtf8()));
#elif USE_STUB_METADATA_READER
    const auto &tagreader = std::make_unique<StubMetadataReader>();
    LOG_WARNING("%s", tagreader->ReadMetadataFromFile(this->arguments().at(0).toUtf8()));
#else
    LOG_ERROR("No metadata reader backend found!");
#endif
}

void ScanTest::exec() const
{
    if (this->hasArgs())
    {
        if (this->arguments().at(0) == "setpath" && this->arguments().size() > 1)
        {
            MusicConsoleReborn::library()->setDirectory(this->arguments().at(1).toUtf8());
            MusicConsoleReborn::processEnvironment().insert("MUSICCONSOLE_LIB_DIR", this->arguments().at(1));

            LOG_NOTICE("Changed directory of model to \"%s\"", this->arguments().at(1));
        }
        else if (this->arguments().at(0) == "get" && this->arguments().size() > 1)
        {
            const auto &mf = MusicConsoleReborn::library()->at(this->arguments().at(1).toUtf8());
            if (mf)
            {
                LOG_INFO("%s", *mf);
            }
            else
            {
                LOG_ERROR("mf: addr=nullptr");
            }
        }
        else
        {
            LOG_WARNING("Unknown argument or length failure.");
        }
    }
    else
    {
        MusicConsoleReborn::library()->scan();
        Replxx::SetAutocompletionAndHints(MusicConsoleReborn::library()->GenerateReplxxAutocompletionList());
    }
}

void SearchTermTest::exec() const
{
    if (this->hasArgs())
    {
        LOG_NOTICE("%s", SearchTerms(this->mergeArgs()));
    }

    SearchTerms t1(this->mergeArgs());
    SearchTerms t2(t1);
    SearchTerms t3("test");

    LOG_INFO("t1 == t2: %s", t1 == t2);
    LOG_INFO("t1 != t2: %s", t1 != t2);
    LOG_INFO("t1 == t3: %s", t1 == t3);
    LOG_INFO("t2 == t3: %s", t2 == t3);

    LOG_HINT("qHash of t1: %i", qHash(t1, 9090));
    LOG_HINT("qHash of t2: %i", qHash(t2, 9090));
    LOG_HINT("qHash of t3: %i", qHash(t3, 9090));

    LOG_ERROR("qHash of CacheEntry{t1, NONE, 1}: %i", qHash(MediaLibraryModel::CacheEntry{t1, NONE, 1}), 2929);
    LOG_ERROR("qHash of CacheEntry{t2, AUDIO, -1}: %i", qHash(MediaLibraryModel::CacheEntry{t2, AUDIO, -1}), 2929);
    LOG_ERROR("qHash of CacheEntry{t2, VIDEO, -1}: %i", qHash(MediaLibraryModel::CacheEntry{t2, AUDIO, -1}), 2929);
    LOG_ERROR("qHash of CacheEntry{t2, NONE, 1}: %i", qHash(MediaLibraryModel::CacheEntry{t2, NONE, 1}), 2929);
    LOG_ERROR("qHash of CacheEntry{t3, NONE, 1}: %i", qHash(MediaLibraryModel::CacheEntry{t3, NONE, 1}), 2929);
    LOG_ERROR("qHash of CacheEntry{t3, VIDEO, 1}: %i", qHash(MediaLibraryModel::CacheEntry{t3, VIDEO, 1}), 2929);
    LOG_ERROR("qHash of CacheEntry{t3, NONE, -1}: %i", qHash(MediaLibraryModel::CacheEntry{t3, NONE, -1}), 2929);
    LOG_ERROR("qHash of CacheEntry{t3, VIDEO, -1}: %i", qHash(MediaLibraryModel::CacheEntry{t3, VIDEO, -1}), 2929);
}

void RandTest::exec() const
{
    LOG_ERROR("------------- unranged tests");
    for (auto i = 0; i < 40; i++)
    {
        LOG_HINT("%llu", Randomizer::GetRandomNumber());
    }

    LOG_ERROR("------------- ranged tests");
    LOG_HINT("     0 ~     30: %llu", Randomizer::GetRandomNumber(0, 30));
    LOG_HINT("     1 ~     45: %llu", Randomizer::GetRandomNumber(1, 45));
    LOG_HINT("   124 ~    383: %llu", Randomizer::GetRandomNumber(124, 383));
    LOG_HINT("    24 ~    482: %llu", Randomizer::GetRandomNumber(24, 482));
    LOG_HINT("     0 ~   1000: %llu", Randomizer::GetRandomNumber(0, 1000));
    LOG_HINT("     0 ~   5000: %llu", Randomizer::GetRandomNumber(0, 5000));
    LOG_HINT("     0 ~   4333: %llu", Randomizer::GetRandomNumber(0, 4333));
    LOG_HINT("     0 ~   4383: %llu", Randomizer::GetRandomNumber(0, 4383));
    LOG_HINT("     0 ~ 383893: %llu", Randomizer::GetRandomNumber(0, 383893));
    LOG_HINT("     0 ~ 298323: %llu", Randomizer::GetRandomNumber(0, 298323));
    LOG_HINT("     0 ~  28332: %llu", Randomizer::GetRandomNumber(0, 28332));
    LOG_HINT("  2343 ~  28332: %llu", Randomizer::GetRandomNumber(2343, 234324));
    LOG_HINT("   234 ~   2432: %llu", Randomizer::GetRandomNumber(234, 2432));
    LOG_HINT("   123 ~   3283: %llu", Randomizer::GetRandomNumber(123, 3283));
    LOG_HINT("   143 ~   2553: %llu", Randomizer::GetRandomNumber(143, 2553));
    LOG_HINT("    53 ~   3382: %llu", Randomizer::GetRandomNumber(53, 3382));

    LOG_ERROR("------------- history test");
    Randomizer::SetHistorySize(25);
    for (auto i = 0; i < 50; i++)
    {
        LOG_HINT("%llu", Randomizer::GetRandomNumber(0, 50));
    }

    Randomizer::SetHistorySize(0);
    for (auto i = 0; i < 50; i++)
    {
        LOG_HINT("%llu", Randomizer::GetRandomNumber(0, 50));
    }

    LOG_ERROR("------------- test edge cases");
    LOG_HINT("%llu", Randomizer::GetRandomNumber(0, 0));
    LOG_HINT("%llu", Randomizer::GetRandomNumber(0, 1));
    LOG_HINT("%llu", Randomizer::GetRandomNumber(1, 0));
    LOG_HINT("%llu", Randomizer::GetRandomNumber(0, 2));
    LOG_HINT("%llu", Randomizer::GetRandomNumber(2, 2));
    LOG_HINT("%llu", Randomizer::GetRandomNumber(1, 1));
    LOG_ERROR("> enable history of 5");
    Randomizer::SetHistorySize(5);
    LOG_HINT("%llu", Randomizer::GetRandomNumber(0, 0));
    LOG_HINT("%llu", Randomizer::GetRandomNumber(0, 1));
    LOG_HINT("%llu", Randomizer::GetRandomNumber(1, 0));
    LOG_HINT("%llu", Randomizer::GetRandomNumber(0, 2));
    LOG_HINT("%llu", Randomizer::GetRandomNumber(2, 2));
    LOG_HINT("%llu", Randomizer::GetRandomNumber(1, 1));

    Randomizer::SetHistorySize(0);

    LOG_ERROR("------------- test template<> functions");
    LOG_HINT("%i", Randomizer::GetRandomInt<int>());
    LOG_HINT("%i", Randomizer::GetRandomInt<short>());
    LOG_HINT("%i", Randomizer::GetRandomInt<short>(25, 30000));
    LOG_HINT("%i", Randomizer::GetRandomInt<char>());
    LOG_HINT("%i", Randomizer::GetRandomUInt<uint>());
    LOG_HINT("%f", Randomizer::GetRandomFloat<float>());
    LOG_HINT("%f", Randomizer::GetRandomDouble<double>());
    //LOG_HINT("%f", Randomizer::GetRandomDouble<long double>());
}

void PlayerTest::exec() const
{
    const auto &players = Config()->GetPlayers();
    const auto &userPlayers = Config()->GetUserPlayers();

    LOG_INFO("Players=%s", players);
    LOG_INFO("UserPlayers=%s", userPlayers);

    for (auto&& p : players)
        PlayerController::RegisterPlayer(p);
    for (auto&& p : userPlayers)
        PlayerController::RegisterPlayerOverride(p, true);

    if (this->hasArgs())
    {
        const auto &mf = MusicConsoleReborn::library()->at(this->arguments().at(0).toUtf8());
        if (mf)
        {
            LOG_INFO("%s", *mf);
            PlayerController::Play(mf);
        }
        else
        {
            LOG_ERROR("mf: addr=nullptr");
        }
    }
}

void LastPlayedTest::exec() const
{
    const auto *last = MusicConsoleReborn::library()->lastPlayed();
    if (last)
        LOG_INFO("lastPlayed = %s", *last);
    else
        LOG_ERROR("lastPlayed = nullptr");
}

void ThreadingTests::exec() const
{
    const auto &thrcount = QThread::idealThreadCount();

    LOG_INFO("QThread::idealThreadCount(): %i", thrcount);

//    // create test list with ~~100~~ 100000 elements
//    QList<int> test;
//    test.reserve(100000);
//    for (auto i = 0; i < 100000; i++)
//        test.append(Randomizer::GetRandomInt<short>());

////    LOG_NOTICE("%s", test);

//    const auto &div = static_cast<int>(test.size() / thrcount);
//    LOG_NOTICE("list/%i: %i", thrcount, div);

//    QVector<QList<int>> subLists;
//    subLists.resize(thrcount);
//    auto currentDiv = 0;
//    for (auto& sub : subLists)
//    {
//        sub.reserve(div);
//        sub.append(test.mid(currentDiv, div));
//        currentDiv += div;

////        LOG_NOTICE("newList=%s", sub);
////        LOG_NOTICE("currentDiv=%i", currentDiv);
//    }

//    if (div != test.size())
//    {
//        subLists.last().append(test.mid(currentDiv));
////        LOG_NOTICE("last_append=%s", subLists.last());
//    }

//    LOG_ERROR("Starting benchmark...");

//    LOG_HINT("full iteration");
//    auto t1 = SimpleTimeBenchmark::Tick();
//    for (auto& i : test)
//        i *= 2;
//    auto t2 = SimpleTimeBenchmark::Tick();
//    LOG_INFO("res = %i", SimpleTimeBenchmark::Duration(t1, t2));

//    LOG_HINT("concurrent iteration");
//    t1 = SimpleTimeBenchmark::Tick();
//    QList<QFuture<void>> future;
//    for (auto& i : subLists)
//    {
//        future.append(QFuture<void>());
//        future.last() = QtConcurrent::run([&]() {
//            for (auto& j : i)
//                j *= 2;
//        });
//    }

//    for (auto&& f : future)
//    {
//        f.waitForFinished();
//    }

//    t2 = SimpleTimeBenchmark::Tick();
//    LOG_INFO("res = %i", SimpleTimeBenchmark::Duration(t1, t2));
}

void KbHitTest::exec() const
{
    KBHIT
        LOG_INFO("......");
    KBHIT_END

    int i = 0;
    KBHIT_NOT_INFINITE(i > 10)
        LOG_INFO("%i", i);
        i++;
    KBHIT_NOT_INFINITE_END
}

void PlaylistTest::exec() const
{
    // test playlist queue
    PlaylistQueue pl{
        MusicConsoleReborn::library()->find(SearchTerms("shining red eyes")),
        MusicConsoleReborn::library()->random(SearchTerms("vocaloid")),
    };

    PlaylistQueue::PlaylistQueueEntry current;
    while ((current = pl.next()).mf)
    {
        AppearanceController::Print(current.mf);
    }

    if (this->hasArgs())
    {
        PlaylistQueue target;
        const auto &res = PlaylistParser::Parse(this->arguments().at(0).toUtf8(), &target);
        LOG_INFO("res=%s", res);

        PlaylistQueue::PlaylistQueueEntry current;
        while ((current = target.next()).mf)
        {
            AppearanceController::Print(current.mf);
        }
    }

    LOG_HINT("%s", PlaylistParser::GetAllPlaylists());
}

void RegExPlayground::exec() const
{
    static const QRegularExpression extract_string(R"("(?:[^"\\]|\\.)*")");
    static const QStringList test_data{
        R"("this is an \"example\"")",
        R"("this is an 'example'")",
        R"('this is an \'example\'')",
        R"('this is an "example"')",
        R"(this is an "example")",
        R"(this is an example)",
        R"(this is an" example)",
        R"(this is an" "example)",
    };

    for (auto&& d : test_data)
    {
        const auto &res = extract_string.match(d);
        LOG_INFO("%s", res.capturedTexts());
    }
}

void NumberTest::exec() const
{
    LOG_INFO("len of    0 = %u", Number::GetIntLength(0));
    LOG_INFO("len of    1 = %u", Number::GetIntLength(1));
    LOG_INFO("len of   10 = %u", Number::GetIntLength(10));
    LOG_INFO("len of   15 = %u", Number::GetIntLength(15));
    LOG_INFO("len of   22 = %u", Number::GetIntLength(22));
    LOG_INFO("len of  111 = %u", Number::GetIntLength(111));
    LOG_INFO("len of 6263 = %u", Number::GetIntLength(6263));

    auto str = QString("%1").arg(3U, 2, 10, QChar('0')).toUtf8();
    LOG_INFO("%s", str);

    str = QString("%1").arg(10U, 2, 10, QChar('0')).toUtf8();
    LOG_INFO("%s", str);

    str = QString("%1").arg(100U, 2, 10, QChar('0')).toUtf8();
    LOG_INFO("%s", str);
}

void TypesTest::exec() const
{
    if (this->hasArgs())
    {
        if (this->arguments().at(0) == "char")
        {
            // character tests
            Char ch(0x20);
            LOG_INFO("32:is_space: %s", ch.is_space());                   // true, 0x20 ASCII space
            LOG_INFO("32:is_space: %s", Char::is_space(0x21));            // false
            LOG_INFO("32:is_space: %s", Char::is_space(0x3000));          // true, IDEOGRAPHIC SPACE U+3000
            LOG_INFO("32:is_space: %s", Char::is_space(0x303F));          // false, IDEOGRAPHIC HALF FILL SPACE U+303F; Category == "Other Symbol"
            LOG_INFO("32:is_uppercase: %s", Char::is_uppercase('A'));     // true, latin script
            LOG_INFO("32:is_uppercase: %s", Char::is_uppercase('a'));     // false, latin script
            LOG_INFO("32:is_uppercase: %s", Char::is_uppercase(L'Д'));    // true, cyrillic script
            LOG_INFO("32:is_uppercase: %s", Char::is_uppercase(L'д'));    // false, cyrillic script
            LOG_INFO("32:is_uppercase: %s", Char::is_uppercase(L'あ'));    // false, no case sensitivity in this script
            LOG_INFO("32:is_lowercase: %s", Char::is_lowercase(L'あ'));    // false, no case sensitivity in this script

            LOG_INFO("32:script: %i", (int) Char::block('A'));             // UBLOCK_BASIC_LATIN = 1
            LOG_INFO("32:script: %i", (int) Char::block(L'Д'));            // UBLOCK_CYRILLIC = 9
            LOG_INFO("32:script: %i", (int) Char::block(L'あ'));            // UBLOCK_HIRAGANA = 62

            LOG_INFO("32:script: %i", (int) Char::script('A'));            // USCRIPT_LATIN = 25
            LOG_INFO("32:script: %i", (int) Char::script(L'Д'));           // USCRIPT_CYRILLIC = 8
            LOG_INFO("32:script: %i", (int) Char::script(L'あ'));           // USCRIPT_HIRAGANA = 20
            LOG_INFO("32:script_name: %s", Char::script_name(L'あ'));
            LOG_INFO("32:script_shortname: %s", Char::script_shortname(L'あ'));

            LOG_INFO("32:is_ltr: %s", Char::is_ltr('A'));                  // true
            LOG_INFO("32:is_ltr: %s", Char::is_ltr(L'Д'));                 // true
            LOG_INFO("32:is_ltr: %s", Char::is_ltr(L'あ'));                 // true
            LOG_INFO("32:is_ltr: %s", Char::is_ltr(0x2));                  // false, not printable either
            LOG_INFO("32:is_ltr: %s", Char::is_ltr(0x05D0));               // false, HEBREW LETTER ALEF' (U+05D0)

            LOG_INFO("32:is_rtl: %s", Char::is_rtl('A'));                  // false
            LOG_INFO("32:is_rtl: %s", Char::is_rtl(L'Д'));                 // false
            LOG_INFO("32:is_rtl: %s", Char::is_rtl(L'あ'));                 // false
            LOG_INFO("32:is_rtl: %s", Char::is_rtl(0x2));                  // false, not printable either
            LOG_INFO("32:is_rtl: %s", Char::is_rtl(0x05D0));               // true, HEBREW LETTER ALEF' (U+05D0)

            LOG_INFO("32:is_cased: %s", Char::is_cased('A'));              // true
            LOG_INFO("32:is_cased: %s", Char::is_cased(L'Д'));             // true
            LOG_INFO("32:is_cased: %s", Char::is_cased(L'あ'));             // false

            LOG_INFO("32:name: %s", Char::name('A'));
            LOG_INFO("32:name: %s", Char::name(L'Д'));
            LOG_INFO("32:name: %s", Char::name(L'あ'));
            LOG_INFO("32:name: %s", Char::name(0x20));
            LOG_INFO("32:name: %s", Char::name(0x05D0));
            LOG_INFO("32:name: %s", Char::name(0x3000));

            LOG_INFO("32:to_uppercase: %s", Char::to_uppercase('a'));
            LOG_INFO("32:to_uppercase: %s", Char::to_uppercase(L'あ'));

            LOG_INFO("32:utf8(static): '%s'", Char::utf8(0x20));

            // operator tests
            LOG_INFO("op+: is_space(0x10 + 0x10): %s", (Char(0x10) + Char(0x10)).is_space()); // true
            LOG_INFO("op-: is_space(0x30 - 0x10): %s", (Char(0x30) - Char(0x10)).is_space()); // true
            LOG_INFO("op(): %i", ch()); // 32
            auto func = [](const Char::type &ch) { return ch; };
            LOG_INFO("op Char: %i", (int) func(ch)); // 32; automatic casting on function call, data type operator

            // random experiments
            QString hira_a(QChar(L'あ'));
            LOG_INFO("qt test 1: %s", Char::script_name(hira_a.utf16()[0]));

            // utf-8
            LOG_INFO("32:%s", Char(0x5B57).utf8());
        }
        else if (this->arguments().at(0) == "str")
        {
            String str;
            str = "test";
            LOG_INFO("'%s'", str);
            str.to_upper();
            LOG_INFO("'%s'", str);
            str.to_lower();
            LOG_INFO("'%s'", str);
            str.append("  ");
            str.prepend(" a   ");
            LOG_INFO("'%s'", str);
            str.trim_front();
            LOG_INFO("'%s'", str);
            str.trim_back();
            LOG_INFO("'%s'", str);
            str.append("   string ");
            str.prepend("   ");
            str.append("\t");
            LOG_INFO("'%s'", str);
            str.simplify();
            LOG_INFO("'%s'", str);                      // 'a test string'

            String str2("abc\n\t def \t ghi\tjkl\n\nmno");
            LOG_INFO("'%s'", str2);
            str2.simplify();
            LOG_INFO("'%s'", str2);                     // 'abc def ghi jkl mno'

            String str3(" あ\n\t かef \t 아абвghi\tjkl\n\nmno  \t x　　x");
            str3.simplify();
            LOG_INFO("'%s'", str3);                     // 'あ かef 아абвghi jkl mno x　　x'
                                                        // U+3000 not removed, no Unicode support
            str3.to_upper();
            LOG_INFO("'%s'", str3);                     // no Unicode support, only ASCII is transformed
            str3.to_lower();
            LOG_INFO("'%s'", str3);

            String str4("hello world");
            LOG_INFO("starts_with: %s", str4.starts_with("hello"));
            LOG_INFO("ends_with: %s", str4.ends_with("world"));
            LOG_INFO("starts_with: %s", str4.starts_with("hello world"));
            LOG_INFO("starts_with: %s", str4.starts_with("world"));
            LOG_INFO("ends_with: %s", str4.ends_with("hello world"));
            LOG_INFO("ends_with: %s", str4.ends_with("hello"));

            LOG_INFO("---");
            str4.to_upper();
            LOG_INFO("starts_with: %s", str4.starts_with("hello", String::CaseInsensitive));
            LOG_INFO("ends_with: %s", str4.ends_with("world", String::CaseInsensitive));
            LOG_INFO("starts_with: %s", str4.starts_with("hello world", String::CaseInsensitive));
            LOG_INFO("starts_with: %s", str4.starts_with("world", String::CaseInsensitive));
            LOG_INFO("ends_with: %s", str4.ends_with("hello world", String::CaseInsensitive));
            LOG_INFO("ends_with: %s", str4.ends_with("hello", String::CaseInsensitive));

            LOG_INFO("%s", String::number(8));
            LOG_INFO("%s", String::number(20.f));
            LOG_INFO("%s", String::number(0.3f));
            LOG_INFO("%s", String::number(2.5));

            // split tests
            String str5("some|string||hello|  ");
            LOG_INFO("%s", str5);
            LOG_INFO("%s", str5.split('|'));
            LOG_INFO("%s", str5.split('|', String::SkipEmptyParts));
            LOG_INFO("%s", str5.split('|', String::TrimAndSkipEmptyParts));
            LOG_INFO("---");
            String str6("some string && some other && &&&& empty");
            LOG_INFO("%s", str6);
            LOG_INFO("%s", str6.split("&&"));
            LOG_INFO("%s", str6.split("&&", String::SkipEmptyParts));
            LOG_INFO("%s", str6.split("&&", String::TrimAndSkipEmptyParts));
            LOG_INFO("---");
            str6.append("&&");
            LOG_INFO("%s", str6);
            LOG_INFO("%s", str6.split("&&"));
            LOG_INFO("%s", str6.split("&&", String::SkipEmptyParts));
            LOG_INFO("%s", str6.split("&&", String::TrimAndSkipEmptyParts));
            LOG_INFO("---");
            String str7("abc   de  fg");
            LOG_INFO("%s", str7);
            LOG_INFO("%s", str7.split(RegExp(R"(\s+)"))); // ("abc", "de", "fg")
            str7 = "abc　　  de　 fg 　"; // contains U+3000
            LOG_INFO("%s", str7.split(RegExp(R"(\s+)"))); // ("abc　　", "de　", "fg", "　"), no Unicode support as expected

            // compare with Qt
            LOG_INFO("%s", QString(str5.c_str()).split('|'));
            LOG_INFO("%s", QString(str5.c_str()).split('|', QString::SkipEmptyParts));
            LOG_INFO("%s", QString(str6.c_str()).split("&&"));
            LOG_INFO("%s", QString(str6.c_str()).split("&&", QString::SkipEmptyParts));

            String command("abc&&&&ab\"&&\"ac a && ac");
            LOG_INFO("%s", command.split(RegExp(R"((?<!["'])(&&)(?!["']))")));

            // number conversion

            String num("10228");
            bool ok;
            LOG_INFO("%i, %s", num.toInt(&ok), ok);
            LOG_INFO("%i, %s", num.toInt64(&ok), ok);
            LOG_INFO("%i, %s", num.toUInt(&ok), ok);
            LOG_INFO("%lu, %s", num.toUInt64(&ok), ok);
            num = "83823.2832";
            LOG_INFO("%f, %s", num.toFloat(&ok), ok);
            LOG_INFO("%f, %s", num.toDouble(&ok), ok);
            //LOG_INFO("%f, %s", num.toLongDouble(&ok), ok);
            num = "abc";
            LOG_INFO("%i, %s", num.toInt(&ok), ok);
        }
        else if (this->arguments().at(0) == "unistr")
        {
            UnicodeString str("ありがとう");
            LOG_INFO("\"%s\"", str);
            LOG_INFO("\"%s\"", UnicodeString("ascii"));
            LOG_INFO("\"%s\"", UnicodeString("ascii+漢字"));

            // test UTF-16 conversion (can be used for ICU operations, since ICU uses UTF-16 for most stuff)
            LOG_INFO("\"%s\"", str.to_utf16()); // "(U+3042, U+308A, U+304C, U+3068, U+3046)"
            LOG_INFO("\"%s\"", UnicodeString::from_utf16(str.to_utf16())); // "ありがとう"
            LOG_INFO("\"%s\"", str.lower());

            LOG_INFO("%s", StringUtils::int_to_hex(0x0000));        // "0x00000000"
            LOG_INFO("%s", StringUtils::int_to_hex(0x0020));        // "0x00000020"
            LOG_INFO("%s", StringUtils::int_to_hex(0xD010));        // "0x0000d010"
            LOG_INFO("%s", StringUtils::int_to_hex(0xFFFF));        // "0x0000ffff"

            LOG_INFO("%i", 0xf000);        // 61440

            UnicodeString test = UnicodeString("ascii+漢字");
            LOG_INFO("test: \"%s\"", test);
            LOG_INFO(" > size:  %i", test.size());
            LOG_INFO(" > front: %s", test.front());
            LOG_INFO(" > back:  %s", test.back());

            // test iterator
            for (auto&& c : test)
            {
                LOG_INFO(" > %s: '%s', '%s', %f, %i, %s", c, c.name(), c.script_name(), c.numeric(), c.digit(), Char::fold_case(c));
            }

            // position test
            LOG_INFO(" > char@2: %s", test.at(1));
            LOG_INFO(" > char@7: %s", test[6]);

            // ---------------------------------------

            auto s = UnicodeString("AbCdEfGh").lower();
            LOG_INFO("%s", s);
            LOG_INFO("%s", s.upper());

            // ---------------------------------------

            test = "utf-8Дか你아a　・*¡";
            test.append(L'─');
            test.prepend("[ñ]％");
            LOG_INFO("test: \"%s\"", test);
            LOG_INFO(" > size:  %i", test.size());
            LOG_INFO(" > front: %s", test.front());
            LOG_INFO(" > back:  %s", test.back());

            // test iterator
            for (auto&& c : test)
            {
                LOG_INFO(" > %s: '%s', '%s', %f, %i, %s", c, c.name(), c.script_name(), c.numeric(), c.digit(), Char::fold_case(c));
            }

            // position test
            LOG_INFO(" > char@2: %s", test.at(1));
            LOG_INFO(" > char@7: %s", test[6]);

            // ---------------------------------------

            UnicodeString str2 = "ａＡ　";
            str2 += " あ\n\t かef \t 아абвghi\tjkl\n\nmno  \t x　　x";
            LOG_INFO("'%s'", str2);
            str2.simplify();
            LOG_INFO("'%s'", str2);   // 'ａＡ あ かef 아абвghi jkl mno x x'; U+3000 is removed too
            str2.to_lower();
            LOG_INFO("'%s'", str2);   // Unicode support, everything which is cased is transformed
            str2.to_upper();
            LOG_INFO("'%s'", str2);
            LOG_INFO(" > size: %i", str2.size());
            LOG_INFO("\"%s\"", UnicodeString::from_utf16(str2.to_utf16()));
            LOG_INFO("'%s'", str2.lower());

            // ---------------------------------------

            UnicodeString str4("ｈello world");
            LOG_INFO("starts_with: %s", str4.starts_with("ｈello"));        // true
            LOG_INFO("ends_with: %s", str4.ends_with("world"));             // true
            LOG_INFO("starts_with: %s", str4.starts_with("ｈello world"));  // true
            LOG_INFO("starts_with: %s", str4.starts_with(" ｈ"));           // false
            LOG_INFO("starts_with: %s", str4.starts_with("ｈ"));            // true
            LOG_INFO("starts_with: %s", str4.starts_with("world"));         // false
            LOG_INFO("starts_with: %s", str4.starts_with(" world"));        // false
            LOG_INFO("ends_with: %s", str4.ends_with("ｈello world"));      // true
            LOG_INFO("ends_with: %s", str4.ends_with("ｈello"));            // false

            LOG_INFO("---");
            str4.to_upper();
            LOG_INFO("starts_with: %s", str4.starts_with("ｈello", UnicodeString::CaseSensitive)); // false
            LOG_INFO("ends_with: %s", str4.ends_with("world", UnicodeString::CaseInsensitive));
            LOG_INFO("starts_with: %s", str4.starts_with("ｈello world", UnicodeString::CaseInsensitive));
            LOG_INFO("starts_with: %s", str4.starts_with(" ｈ", UnicodeString::CaseInsensitive));
            LOG_INFO("starts_with: %s", str4.starts_with("ｈ", UnicodeString::CaseInsensitive));
            LOG_INFO("starts_with: %s", str4.starts_with("world", UnicodeString::CaseInsensitive));
            LOG_INFO("starts_with: %s", str4.starts_with(" world", UnicodeString::CaseInsensitive));
            LOG_INFO("ends_with: %s", str4.ends_with("ｈello world", UnicodeString::CaseInsensitive));
            LOG_INFO("ends_with: %s", str4.ends_with("ｈello", UnicodeString::CaseInsensitive));
            LOG_INFO("---");
            LOG_INFO("starts_with: %s", str4.starts_with("ｈello"));
            LOG_INFO("ends_with: %s", str4.ends_with("world"));
            LOG_INFO("starts_with: %s", str4.starts_with("ｈello world"));
            LOG_INFO("starts_with: %s", str4.starts_with(" ｈ"));
            LOG_INFO("starts_with: %s", str4.starts_with("ｈ"));
            LOG_INFO("starts_with: %s", str4.starts_with("world"));
            LOG_INFO("starts_with: %s", str4.starts_with(" world"));
            LOG_INFO("ends_with: %s", str4.ends_with("ｈello world"));
            LOG_INFO("ends_with: %s", str4.ends_with("ｈello"));

            // ---------------------------------------

            // test digit properties
            for (auto&& c : UnicodeString("1234１２３４一二三四"))
                LOG_INFO(" > %s: '%s', '%s', %f, %i, %i", c, c.name(), c.script_name(), c.numeric(), c.digit(), c.digit_basic());

            // ---------------------------------------

            // test find
            const auto &find = UnicodeString("find me abc me あえいおう　漢字");
            LOG_INFO("%s", find);
            LOG_INFO("%i", find.find(L'漢')); // 21
            LOG_INFO("%i", find.find(L'i')); // 1
            LOG_INFO("%i", find.find(L'm')); // 5
            LOG_INFO("%i", find.find("me")); // 5
            LOG_INFO("%i", find.find("me", 5)); // 5
            LOG_INFO("%i", find.find("me", 6)); // 12
            LOG_INFO("%i", find.find("me", 7)); // 12
            LOG_INFO("%i", find.find("ME", 7)); // npos
            LOG_INFO("%i", find.find("ME", 7, UnicodeString::CaseInsensitive)); // 12
            LOG_INFO("%i", find.find("mee")); // npos
            LOG_INFO("%i", find.find("あえいおう")); // 15
            LOG_INFO("%i", find.find("あえいおう　")); // 15
            LOG_INFO("%i", find.find("あえいおう ")); // npos

            const auto &find2 = UnicodeString("fInd find abcdaec");
            LOG_INFO("%i", find2.find("find")); // 5
            LOG_INFO("%i", find2.find("fInd")); // 0
            LOG_INFO("%i", find2.find("FIND", 0, UnicodeString::CaseInsensitive)); // 0
            LOG_INFO("%i", find2.find("abc")); // 10
            LOG_INFO("%i", find2.find("aec")); // 14

            // test find from end
            LOG_ERROR("×××××××××××××××××××××××××××××××××××××××××");
            LOG_INFO("first: %i", find2.find('c')); // 12
            LOG_INFO("%i", find2.find_last('c')); // 16
            LOG_INFO("%i", find2.find_last('d')); // 13
            LOG_INFO("%i", find2.find_last("find")); // 5
            LOG_INFO("%i", find2.find_last("ab")); // 10
            LOG_INFO("%i", find2.find_last("AB")); // npos
            LOG_INFO("%i", find2.find_last("AB", 0, UnicodeString::CaseInsensitive)); // 10

            // ---------------------------------------

            // test comparision operators and compare with QString

            UnicodeString l("AbCdE"), r("abcde"); QString lQ("AbCdE"), rQ("abcde");
            LOG_INFO("AbCdE - abcde");
            LOG_INFO("%s qt:%s", l < r  , lQ < rQ);
            LOG_INFO("%s qt:%s", l <= r , lQ <= rQ);
            LOG_INFO("%s qt:%s", l > r  , lQ > rQ);
            LOG_INFO("%s qt:%s", l >= r , lQ >= rQ);
            LOG_INFO("---");

            l = "abc"; r = "abc"; lQ = "abc"; rQ = "abc";
            LOG_INFO("abc - abc");
            LOG_INFO("%s qt:%s", l < r  , lQ < rQ);
            LOG_INFO("%s qt:%s", l <= r , lQ <= rQ);
            LOG_INFO("%s qt:%s", l > r  , lQ > rQ);
            LOG_INFO("%s qt:%s", l >= r , lQ >= rQ);
            LOG_INFO("---");

            l = "abc"; r = "abcd"; lQ = "abc"; rQ = "abcd";
            LOG_INFO("abc - abcd");
            LOG_INFO("%s qt:%s", l < r  , lQ < rQ);
            LOG_INFO("%s qt:%s", l <= r , lQ <= rQ);
            LOG_INFO("%s qt:%s", l > r  , lQ > rQ);
            LOG_INFO("%s qt:%s", l >= r , lQ >= rQ);
            LOG_INFO("---");

            l = "abcd"; r = "abc"; lQ = "abcd"; rQ = "abc";
            LOG_INFO("abcd - abc");
            LOG_INFO("%s qt:%s", l < r  , lQ < rQ);
            LOG_INFO("%s qt:%s", l <= r , lQ <= rQ);
            LOG_INFO("%s qt:%s", l > r  , lQ > rQ);
            LOG_INFO("%s qt:%s", l >= r , lQ >= rQ);
            LOG_INFO("---");

            l = "абв"; r = "АБВ"; lQ = QString::fromUtf8("абв"); rQ = QString::fromUtf8("АБВ");
            LOG_INFO("абв - АБВ");
            LOG_INFO("%s qt:%s", l < r  , lQ < rQ);
            LOG_INFO("%s qt:%s", l <= r , lQ <= rQ);
            LOG_INFO("%s qt:%s", l > r  , lQ > rQ);
            LOG_INFO("%s qt:%s", l >= r , lQ >= rQ);
            LOG_INFO("---");

            l = "абвc"; r = "АБВc"; lQ = QString::fromUtf8("абвc"); rQ = QString::fromUtf8("АБВc");
            LOG_INFO("абвc - АБВc");
            LOG_INFO("%s qt:%s", l < r  , lQ < rQ);
            LOG_INFO("%s qt:%s", l <= r , lQ <= rQ);
            LOG_INFO("%s qt:%s", l > r  , lQ > rQ);
            LOG_INFO("%s qt:%s", l >= r , lQ >= rQ);
            LOG_INFO("---");

            l = "aабв"; r = "AАБВ"; lQ = QString::fromUtf8("aабв"); rQ = QString::fromUtf8("AАБВ");
            LOG_INFO("aабв - AАБВ");
            LOG_INFO("%s qt:%s", l < r  , lQ < rQ);
            LOG_INFO("%s qt:%s", l <= r , lQ <= rQ);
            LOG_INFO("%s qt:%s", l > r  , lQ > rQ);
            LOG_INFO("%s qt:%s", l >= r , lQ >= rQ);
            LOG_INFO("---");

            l = "あ"; r = "か"; lQ = QString::fromUtf8("あ"); rQ = QString::fromUtf8("か");
            LOG_INFO("あ - か");
            LOG_INFO("%s qt:%s", l < r  , lQ < rQ);
            LOG_INFO("%s qt:%s", l <= r , lQ <= rQ);
            LOG_INFO("%s qt:%s", l > r  , lQ > rQ);
            LOG_INFO("%s qt:%s", l >= r , lQ >= rQ);
            LOG_INFO("---");

            // substring test

            LOG_INFO("\"%s\"", UnicodeString("abc 123").mid(0));           // "abc 123"
            LOG_INFO("\"%s\"", UnicodeString("abc 123").mid(0, 3));        // "abc"
            LOG_INFO("\"%s\"", UnicodeString("abc 123").mid(0, 6));        // "abc 12"
            LOG_INFO("\"%s\"", UnicodeString("abc 123").mid(0, 7));        // "abc 123"
            try {LOG_INFO("\"%s\"", UnicodeString("abc 123").mid(0, 8));}  // throws
            catch (std::out_of_range e) { LOG_INFO("%s", e.what());}       //  out of range
            LOG_INFO("\"%s\"", UnicodeString("abc 123").mid(4));           // "123"
            LOG_INFO("\"%s\"", UnicodeString("abc 123").mid(4, 2));        // "12"
            LOG_INFO("\"%s\"", UnicodeString("abc 123").mid(4, 3));        // "123"

            // compare

            LOG_INFO("%s", UnicodeString::compare("あaa", "あAA"));
            LOG_INFO("%s", UnicodeString::compare("あAA", "あAA"));
            LOG_INFO("%s", UnicodeString::compare("あaa", "あAA", UnicodeString::CaseInsensitive));

            // replace

            test = "あなたaabbcdef /()&$#)/dl";
            LOG_INFO("\"%s\"", test.replace(1, 2, 'x')); // "あxaabbcdef /()&$#)/dl"
            LOG_INFO("\"%s\"", test.replace(1, 1, "test123")); // "あtest123aabbcdef /()&$#)/dl"
            LOG_INFO("\"%s\"", test.replace(1, 8, "あああ")); // "ああああabbcdef /()&$#)/dl"
            LOG_INFO("\"%s\"", test.replace(L'あ', L'は')); // "ははははabbcdef /()&$#)/dl"
            LOG_INFO("\"%s\"", test.replace("bb", "aa")); // "ははははaaacdef /()&$#)/dl"

            // number conversion

            UnicodeString num("10228");
            bool ok;
            LOG_INFO("%i, %s", num.toInt(&ok), ok);
            LOG_INFO("%i, %s", num.toInt64(&ok), ok);
            LOG_INFO("%i, %s", num.toUInt(&ok), ok);
            LOG_INFO("%lu, %s", num.toUInt64(&ok), ok);
            num = "83823.2832";
            LOG_INFO("%f, %s", num.toFloat(&ok), ok);
            LOG_INFO("%f, %s", num.toDouble(&ok), ok);
            //LOG_INFO("%f, %s", num.toLongDouble(&ok), ok);
            num = "abc";
            LOG_INFO("%i, %s", num.toInt(&ok), ok);
        }
        else if (this->arguments().at(0) == "vec")
        {
            Vector<int> int_test;
            int_test.append({7,0,1,2,3,4,5,6,7});
            LOG_INFO("%s", int_test);
            LOG_INFO("%i", int_test.index_of(0)); // 1
            LOG_INFO("%i", int_test.index_of(4)); // 5
            LOG_INFO("%i", int_test.index_of(7)); // 0
            int_test.remove_at(5); // removes 4
            LOG_INFO("%s", int_test);
            int_test.replace(7, 8); // replaces last 7 with 8
            int_test.replace(0, 1); // replaces first 7 with 1
            LOG_INFO("%s", int_test);
            int_test.std::vector<int>::insert(int_test.end(), 3);
            LOG_INFO("%s", int_test);
            int_test.insert(int_test.size(), 0);
            LOG_INFO("%s", int_test);
            LOG_INFO("%i", int_test.index_of(222));

            StringVector str_test{
                "hello", "world", "string"
            };
            LOG_INFO("%s", str_test);
            LOG_INFO("%i", str_test.index_of("world"));
            str_test.append("test");
            LOG_INFO("%s", str_test);
            str_test.replace(0, "goodbye");
            LOG_INFO("%s", str_test);
            str_test.remove_at(str_test.size() - 1);
            LOG_INFO("%s", str_test);
            LOG_INFO("%i", str_test.index_of("nope"));

            LOG_INFO("%i", str_test.last_index_of("string"));
            LOG_INFO("%i", str_test.last_index_of("world"));
            LOG_INFO("%i", str_test.last_index_of("nope"));
            str_test.append("world");
            LOG_INFO("%i", str_test.last_index_of("world"));
            LOG_INFO("%i", str_test.index_of("world"));

            // -------------------------------------

            LOG_ERROR("───────────────────────────");

            Vector<int> ints{0,1,1,2,7,2,4,6,7,3,3,6,7};
            LOG_INFO("first index 1: %i", ints.index_of(1)); // 1
            LOG_INFO("first index 2: %i", ints.index_of(2)); // 3
            LOG_INFO("first index 6: %i", ints.index_of(6)); // 7
            LOG_INFO("first index 7: %i", ints.index_of(7)); // 4
            LOG_INFO("first index 10: %i", ints.index_of(10)); // npos
            LOG_INFO("last index 1: %i", ints.last_index_of(1)); // 2
            LOG_INFO("last index 2: %i", ints.last_index_of(2)); // 5
            LOG_INFO("last index 6: %i", ints.last_index_of(6)); // 11
            LOG_INFO("last index 7: %i", ints.last_index_of(7)); // 12
            LOG_INFO("last index 10: %i", ints.last_index_of(10)); // npos
            LOG_INFO("last index 3: %i", ints.last_index_of(3)); // 10
            LOG_INFO("last index 3 (from 9): %i", ints.last_index_of(3, 9)); // 10
            LOG_INFO("last index 3 (from 10): %i", ints.last_index_of(3, 10)); // 10
            LOG_INFO("last index 3 (from 11): %i", ints.last_index_of(3, 11)); // -1
            LOG_INFO("%i", ints.__last_index_of(6, 2)); // 5
            LOG_INFO("%i", ints.__last_index_of(5, 2)); // 3
            LOG_INFO("%i", ints.__last_index_of(4, 2)); // 3
            LOG_INFO("%i", ints.__last_index_of(3, 2)); // npos
            LOG_INFO("%s", ints.remove_duplicates());
            LOG_INFO("%i, %i", ints.first(), ints.last());
            LOG_INFO("%s", ints.mid(0));
            LOG_INFO("%s", ints.mid(1));
            LOG_INFO("%s", ints.mid(2));

            // -------------------------------------

            LOG_ERROR("───────────────────────────");

            StringList test{"a", "b"};
            LOG_INFO("%s", test.join());
            LOG_INFO("%s", test.join(", "));
        }
        else if (this->arguments().at(0) == "regex")
        {
            RegularExpression reg(R"(\s+)", RegularExpression::UseUnicodePropertiesOption);
            LOG_INFO("%s", reg.isValid());
            LOG_INFO("%s", reg.errorString());
            LOG_INFO("%s", reg.split("a b c   ef g")); // ("a", "b", "c", "ef", "g")
            LOG_ERROR("──────");
            LOG_INFO("%s", reg.split("あ　　abc  ef")); // ("あ", "abc", "ef"), contains U+3000; Unicode works correctly with UseUnicodePropertiesOption
            LOG_INFO("%s", QString::fromUtf8(UnicodeString("あ　　abc  ef") // ("あ　　abc", "ef") compare with Qt and without UseUnicodePropertiesOption
                                             .to_utf8().data()).split(QRegularExpression(reg.pattern().to_utf8().data())));
            LOG_ERROR("──────");

            UnicodeString str("a b c   ef g");
            LOG_INFO("%s", str.split(RegularExpression(R"(\s+)")));
            LOG_INFO("%s", str.split(reg));
            LOG_ERROR("──────");
            str = "あ　　abc  ef";
            LOG_INFO("%s", str.split(RegularExpression(R"(\s+)"))); // again ("あ　　abc", "ef") because no UseUnicodePropertiesOption
            LOG_ERROR("──────");

            // various command splitting tests, mostly doesn't work because of too many quotes????
            RegularExpression cmd_split(R"((?<!["'])(&&)(?!["']))");
            LOG_INFO("%s", cmd_split.isValid());
            LOG_INFO("%s", cmd_split.errorString());
            UnicodeString cmd("abc");
            LOG_INFO("%s", cmd.split(cmd_split));
            LOG_ERROR("──────");
            cmd += "&&";
            LOG_INFO("%s", cmd.split(cmd_split));
            LOG_ERROR("──────");
            cmd += "cmd ";
            LOG_INFO("%s", cmd.split(cmd_split));
            LOG_ERROR("──────");
            cmd += "\"&&\"";
            LOG_INFO("%s", cmd.split(cmd_split));
            LOG_ERROR("──────");
            cmd += "a\"&&\"";
            LOG_INFO("%s", cmd.split(cmd_split));
            LOG_ERROR("──────");
            cmd += "&& abc '&&'";
            LOG_INFO("%s", cmd.split(cmd_split));
            LOG_ERROR("──────");
            // Qt fails too with the exact same output
            LOG_INFO("%s", QString::fromUtf8(cmd.to_utf8().data()).split(QRegularExpression(cmd_split.pattern().to_utf8().data())));

            LOG_INFO("%s", RegularExpression("(]").isValid()); // invalid regex
            LOG_INFO("%s", RegularExpression("(]").errorString()); // invalid regex

            RegularExpression groupTest(R"((?<day>\d\d)-(?<month>\d\d)-(?<year>\d\d\d\d) (\w+) (?<name>\w+))");
            LOG_INFO("%s", groupTest.namedCaptureGroups()); // ("", "day", "month", "year", "", "name")

            // (last_)index_of (UnicodeString)
            LOG_INFO("%i", UnicodeString("aa u i").index_of(RegularExpression("\\s")));      // 2
            LOG_INFO("%i", UnicodeString("aa u i").last_index_of(RegularExpression("\\s"))); // 4
        }
        else if (this->arguments().at(0) == "dir")
        {
            Directory dir;
            LOG_INFO("%s", dir.cd("test"));
            LOG_INFO("%s", dir.cd("test/123"));
            LOG_INFO("%s", dir.cd("test", "123", "abc"));
            LOG_INFO("%s", dir.dir());
            LOG_INFO("%s", dir.cd("/home/magiruuvelvet"));
            LOG_INFO("%s", dir.entries());
            LOG_INFO("%s", dir.cd("Documents1"));
            LOG_INFO("%s", dir.cd("Documents"));
            LOG_INFO("%s", dir.entries());
            LOG_INFO("%s", dir.dir());
            LOG_INFO("%s", dir.cdUp());
            LOG_INFO("%s", dir.dir());
            LOG_INFO("%s", dir.entries());
            LOG_INFO("%s", dir.cd(".."));
            LOG_INFO("%s", dir.dir());
            LOG_INFO("%s", dir.entries());
            LOG_INFO("%s", Directory::canonicalize(dir.dir()));
            LOG_INFO("---");
            LOG_INFO("%s", dir.entries(Directory::NoSortingFlags, Directory::DirectoriesOnly));
            LOG_INFO("%s", dir.cd("magiruuvelvet"));
            LOG_INFO("%s", dir.entries(Directory::NoSortingFlags, Directory::DirectoriesOnly));
            LOG_INFO("---");
            LOG_INFO("%s", dir.entries(Directory::NoSortingFlags, Directory::FilesOnly));
            LOG_INFO("---"); // v shows directories because the flag value is lower
            LOG_INFO("%s", dir.entries(Directory::NoSortingFlags, Directory::FilesOnly | Directory::DirectoriesOnly));
            LOG_INFO("---");
            LOG_INFO("%s", dir.entries(Directory::NoSortingFlags, Directory::NoDotAndDotDot));
            LOG_INFO("---");
            LOG_INFO("%s", dir.entries(Directory::NoSortingFlags, Directory::NoDotAndDotDot | Directory::NoHiddenFiles));
            LOG_INFO("---");
            LOG_INFO("%i", dir.self_entry().uid());
            LOG_INFO("%s", dir.self_entry().uid_name());
            LOG_INFO("%i", dir.self_entry().gid());
            LOG_INFO("%s", dir.self_entry().gid_name());
            LOG_INFO("%s", dir.self_entry().is_directory());
            LOG_INFO("%s", dir.self_entry().is_file());
            LOG_INFO("%s", dir.self_entry().filesize());

            LOG_ERROR("───────────────────────────");

            Directory tmp("/tmp");
            LOG_INFO("%s", tmp.entries());
            //LOG_INFO("%s", tmp.mkdir("test"));
            //LOG_INFO("%s", tmp.mkdir("test"));
            //LOG_INFO("%s", tmp.mkdir("test2"));
            //LOG_INFO("%s", tmp.rmdir("test"));
            //LOG_INFO("%s", tmp.rmdir("test"));
            //LOG_INFO("%s", tmp.rmpath("test"));
            //LOG_INFO("%s", tmp.mkpath("test/test1/test2/nested_dir"));
            //LOG_INFO("%s", tmp.rmpath("test"));
            //LOG_INFO("%s", tmp.remove("somefile"));
            //LOG_INFO("%s", tmp.remove("somefile"));
        }
        else if (this->arguments().at(0) == "fe")
        {
            FileEntry fe("/home");
            LOG_INFO("%s", fe.birthtime());
            LOG_INFO("%s", fe.atime());
            LOG_INFO("%s", fe.mtime());
            LOG_INFO("%s", fe.ctime());

            // validate functionality of permissions
            LOG_INFO("/home");
            LOG_INFO("%s", fe.is_readable());
            LOG_INFO("%s", fe.is_writable());
            LOG_INFO("%s", fe.is_executable()); // folders must be x to enter them

            LOG_INFO("/home/magiruuvelvet");
            FileEntry fe2("/home/magiruuvelvet");
            LOG_INFO("%s", fe2.is_readable());
            LOG_INFO("%s", fe2.is_writable());
            LOG_INFO("%s", fe2.is_executable());

            LOG_INFO("Directory: /home");
            Directory dir(fe.full_path());
            LOG_INFO("%s", dir.dir());
            LOG_INFO("%s", dir.is_readable());
            LOG_INFO("%s", dir.is_writable());

            LOG_INFO("Directory: /home/magiruuvelvet");
            dir.cd("magiruuvelvet");
            LOG_INFO("%s", dir.dir());
            LOG_INFO("%s", dir.is_readable());
            LOG_INFO("%s", dir.is_writable());

            LOG_INFO("/tmp/forbidden"); // directory 0700 owned by root
            FileEntry fe3("/tmp/forbidden");
            LOG_INFO("%s", fe3.is_readable());
            LOG_INFO("%s", fe3.is_writable());
            LOG_INFO("%s", fe3.is_executable());
            Directory dirfe3(fe3);
            LOG_INFO("%s", dirfe3.dir());
            LOG_INFO("%s", dirfe3.exists());
            LOG_INFO("%s", dirfe3.is_readable());
            LOG_INFO("%s", dirfe3.is_writable());

            LOG_INFO("%llu", fe.birthtime().total_seconds);
            LOG_INFO("%s", fe.birthtime() == fe.atime());
            LOG_INFO("%s", fe.birthtime() == fe.mtime());
            LOG_INFO("%s", fe.birthtime() == fe.ctime());

            LOG_INFO("%s", Timestamp::from_time_t(fe.birthtime().total_seconds));
            LOG_INFO("%s", Timestamp::make_timestamp(2018, 5, 31, 15, 10, 59));
            auto ts = Timestamp::make_timestamp(2018, 5, 31, 15, 10, 59);
            LOG_INFO("%s", Timestamp::from_time_t(ts.total_seconds));
            LOG_INFO("%s", Timestamp::current());

            Timestamp null = Timestamp::make_null();
            LOG_INFO("%s", null);
        }
        else
        {
            LOG_ERROR("unknown type test");
        }
    }
    else
    {
        LOG_INFO("available commands: char, str, unistr, vec, regex, dir, fe");
    }
}

}

#endif // MUSICCONSOLE_DEBUG
