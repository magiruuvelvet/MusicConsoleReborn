#include "RandomShuffleRepeat.hpp"

#include <Core/MusicConsoleReborn.hpp>
#include <Media/MediaLibraryModel.hpp>
#include <Media/PlayerController.hpp>
#include <Core/Logger.hpp>

#include <Utils/AppearanceController.hpp>
#include <Utils/SimpleTimeBenchmark.hpp>
#include <Utils/KbHit.hpp>

void Commands::RandomShuffleRepeat::exec() const
{
    const auto &type = ApplyFilter();
    const auto &st = SearchTerms(this->mergeArgs());

    switch (mode)
    {
        case Random: {
            const auto &result = MusicConsoleReborn::library()->random(st, type);
            result ? PlayerController::Play(result, true, type) : LOG_ERROR("nothing found");
        } break;

        case Shuffle: {
            this->shuffle(st, type);
        } break;

        case Repeat: {
            if (!this->hasArgs()) // play last file when search terms are empty
            {
                this->repeat(MusicConsoleReborn::library()->lastPlayed(), type);
            }
            else
            {
                this->repeat(st, type);
            }
        } break;
    }
}

void Commands::RandomShuffleRepeat::shuffle(const SearchTerms &st, const MediaType &type) const
{
    const auto &test_search_term = MusicConsoleReborn::library()->random(st, type);
    if (!test_search_term)
    {
        LOG_ERROR("nothing found");
        return;
    }
    KBHIT
        const auto &result = MusicConsoleReborn::library()->random(st, type);
        PlayerController::Play(result, true, type);
    KBHIT_END
}

void Commands::RandomShuffleRepeat::repeat(const SearchTerms &st, const MediaType &type) const
{
    const auto &result = MusicConsoleReborn::library()->find(st, type);
    if (!result)
    {
        LOG_ERROR("nothing found");
        return;
    }
    KBHIT
        PlayerController::Play(result, true, type);
    KBHIT_END
}

void Commands::RandomShuffleRepeat::repeat(const MediaFile *mf, const MediaType &type) const
{
    if (!mf)
    {
        LOG_ERROR("no previous file");
        return;
    }
    KBHIT
        PlayerController::Play(mf, true, type);
    KBHIT_END
}
