#include "History.hpp"

#include <Core/Logger.hpp>
#include <Console/Replxx.hpp>

void Commands::History::exec() const
{
    const auto &history = Replxx::GetHistory();
    uint len = static_cast<uint>(history.size());

    if (this->hasArgs())
    {
        bool success = false;
        const auto &conv = this->arguments().at(0).toUInt(&success);
        if (success)
            len = conv;
        else
        {
            LOG_ERROR("%s: not a (positive) number.",
                        this->arguments().at(0));
            return;
        }
    }

    if (len > history.size())
    {
        LOG_ERROR("%s: limit is higher than the history size.", this->command());
        return;
    }

    for (auto i = history.size() - len; i < history.size(); i++)
        LOG_INFO("%s", history.at(i));
}
