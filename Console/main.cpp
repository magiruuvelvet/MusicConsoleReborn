#include <Core/MusicConsoleReborn.hpp>

#include <I18n/I18n.hpp>

#include <Core/ConfigManager.hpp>
#include <Core/Logger.hpp>

#include <Utils/ProcessUptime.hpp>
#include <Utils/TermColor.hpp>

#include <Console.hpp>
#include <Console/Commands/Config.hpp>

#include <memory>
#include <csignal>

#ifdef INCLUDE_GIT_VERSION_DATA
namespace {
#include <GitVersion.hpp>
static const QByteArray GIT_VERSION =
    QByteArray("+git-") + GitVersion::GIT_VERSION_NUMBER +
  + "/" + GitVersion::GIT_BRANCH_NAME;
}
#else
namespace {
static const QByteArray GIT_VERSION;
}
#endif

// application exit function for signal handlers
__attribute__((noreturn))
static void quit_app(int)
{ std::exit(10); }

int main(int argc, char **argv)
{
    I18n::setToActiveLocale();

    MusicConsoleReborn::setApplicationName("musicconsole-reborn");
    MusicConsoleReborn::setApplicationDisplayName("Music Console Reborn");
    MusicConsoleReborn::setApplicationVersion("v0.10.1-1" + GIT_VERSION);
    MusicConsoleReborn::setOrganizationName("マギルゥーベルベット");

    // create application instance
    MusicConsoleReborn instance(&argc, std::move<const char**>(const_cast<const char**>(argv)));

    // print header
    LOG_INFO("%s%s%s %s",
             TermColor::code(TermColor::Bold),
             MusicConsoleReborn::applicationDisplayName(),
             TermColor::code(TermColor::Reset),
             MusicConsoleReborn::applicationVersion());

    // parse command line arguments
    if (instance.hasArgs())
    {
        static const QByteArray library_path_option = "--library-path=";

        for (auto&& arg : instance.arguments())
        {
            if (arg.startsWith(library_path_option))
            {
                const auto &value = arg.mid(library_path_option.size());
                if (!value.isEmpty())
                {
                    Config()->LoadConfigFromDisk();
                    Config()->SetLibraryRootPath(value);
                    Config()->SaveConfigToDisk();
                    LOG_NOTICE(*I18n::get("Changed library root path to: %s"), value);
                    return 0;
                }
                else
                {
                    LOG_ERROR(*I18n::get("Library root path must not be empty!"));
                    return 3;
                }

                break;
            }
        }

        if (instance.arguments().at(0) == "--config")
        {
            Config()->LoadConfigFromDisk();
            const auto &&config = std::make_unique<Commands::Config>(Config()->GetCommand(ConfigManager::Command::config));
            config->setArguments(instance.arguments().mid(1));
            config->exec_from_command_line();
            return config->lastStatus();
        }

        LOG_ERROR(*I18n::get("Unknown command line options!"));
        return 2;
    }

    // check if application is already running
    if (instance.isRunning())
    {
        static constexpr const auto reset = TermColor::code(TermColor::Reset);
        static constexpr const auto italic = TermColor::code(TermColor::Italic);
        static const auto &notice_fmt = TermColor::fg({166, 74, 0}, TermColor::Bold);
        LOG_INFO(*I18n::get("%sNOTICE:%s %sonly one instance is allowed!%s"),
                 notice_fmt, reset, italic, reset);
        return 5;
    }

    // start process uptime timer (has its own event loop)
    ProcessUptime::Start();

    // register unix signals
    std::signal(SIGINT,  quit_app);
    std::signal(SIGTERM, quit_app);
    std::signal(SIGQUIT, quit_app);
    std::signal(SIGHUP,  quit_app);

    // load configuration
    Config()->LoadConfigFromDisk();

    // register local environment variables
    MusicConsoleReborn::processEnvironment().insert("MUSICCONSOLE_CONFIG_DIR", ConfigManager::GetConfigDirectory());
    MusicConsoleReborn::processEnvironment().insert("MUSICCONSOLE_LIB_DIR", Config()->GetLibraryRootPath());
    MusicConsoleReborn::processEnvironment().insert("MUSICCONSOLE_APP_DIR", MusicConsoleReborn::applicationDirPath());

    // run main application
    auto console = std::make_unique<Console>();
    const auto &ret = console->enter();

    return ret;
}
