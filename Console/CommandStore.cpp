#include "CommandStore.hpp"
#include <Core/Command.hpp>

#include <Core/Logger.hpp>

QList<std::shared_ptr<Command>> CommandStore::_commands;
Command* CommandStore::_defaultCommand = nullptr;

bool CommandStore::RegisterCommand(const std::shared_ptr<Command> &cmd)
{
    if (cmd)
    {
        if (!ContainsCommand(cmd.get()))
        {
            _commands.append(cmd);
            return true;
        }
        else
        {
            LOG_WARNING("CommandStore: command '%s' is already registered.", cmd->command());
            return false;
        }
    }
    else
    {
        LOG_ERROR("CommandStore: can not register a null pointer!");
        return false;
    }
}

void CommandStore::SetDefaultCommand(const QString &name)
{
    const auto &res = ContainsCommand(name, nullptr);

    if (res.found)
    {
        _defaultCommand = res.cmd;
    }
    else
    {
        LOG_WARNING("CommandStore: set default command: command '%s' is not registered.", name);
    }
}

void CommandStore::SetDefaultCommand(std::nullptr_t)
{
    _defaultCommand = nullptr;
}

const QList<std::shared_ptr<Command> > &CommandStore::GetCommands()
{
    return _commands;
}

Command *CommandStore::GetDefaultCommand()
{
    return _defaultCommand;
}

int CommandStore::Count()
{
    return _commands.size();
}

void CommandStore::Deinitialize()
{
    _commands.clear();
}

bool CommandStore::ContainsCommand(Command *cmd)
{
    for (auto&& c : _commands)
        if (QString::compare(c->command(), cmd->command(), Qt::CaseInsensitive) == 0)
            return true;
    return false;
}

CommandStore::_Result CommandStore::ContainsCommand(const QString &name, std::nullptr_t)
{
    _Result res;
    for (auto&& c : _commands)
    {
        if (QString::compare(c->command(), name, Qt::CaseInsensitive) == 0)
        {
            res.cmd = c.get();
            res.found = true;
            return res;
        }
    }
    return res;
}
