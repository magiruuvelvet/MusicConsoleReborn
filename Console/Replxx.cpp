#include "Replxx.hpp"
#include "ReplxxUtil.hpp"

#include <Core/Logger.hpp>

#include <Utils/SearchTerms.hpp>

#include <memory>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cerrno>

#include "Console.hpp"

#include <replxx.hxx>

bool Replxx::_isInit = false;

std::unique_ptr<replxx::Replxx> Replxx::rx;
String Replxx::_historyFile;
String Replxx::_replxxPrompt{""};

List<RegularExpression> Replxx::_ignorePatterns;

void Replxx::Init()
{
    if (!_isInit)
    {
        rx = std::make_unique<replxx::Replxx>();

        // recognize window changes to avoid display issues when terminal size changes
        rx->install_window_change_handler();

        // allow empty input to auto complete
        rx->set_complete_on_empty(true);

        // word break and escape characters
        rx->set_word_break_characters(" \"'");
        rx->set_escape_characters("\\");

        // max number of results to show when using the hint callback
        rx->set_max_hint_rows(5);

        // set auto completion hook
        rx->set_completion_callback(ReplxxUtil::completion_hook, static_cast<void*>(&ReplxxUtil::_autocompletionAndHintList));

        _isInit = true;
    }
    else
    {
        LOG_WARNING("Replxx: already initialized");
    }
}

void Replxx::Deinit()
{
    if (_isInit)
    {
        rx.reset();

        _replxxPrompt.clear();
        _historyFile.clear();
        _ignorePatterns.clear();
        ReplxxUtil::_autocompletionAndHintList.clear();

        _isInit = false;
    }
    else
    {
        LOG_WARNING("Replxx: already deinitialized");
    }
}

void Replxx::SetPrompt(const String &prompt)
{
    _replxxPrompt = prompt;
}

void Replxx::SetHistoryFileLocation(const String &history)
{
    // set history file location
    _historyFile = history;

    // load saved history
    rx->history_load(_historyFile);
}

void Replxx::SetHistIgnore(const List<RegularExpression> &ignorePatterns)
{
    _ignorePatterns = ignorePatterns;
}

void Replxx::SetAutocompletionAndHints(const StringList &hints)
{
    ClearAutocompletionAndHints();
    for (auto&& hint : hints)
        ReplxxUtil::_autocompletionAndHintList.append(ReplxxUtil::EscapeStringForReplxx(hint));
}

const StringList &Replxx::CurrentAutocompletionAndHints()
{
    return ReplxxUtil::_autocompletionAndHintList;
}

void Replxx::ClearAutocompletionAndHints()
{
    ReplxxUtil::_autocompletionAndHintList.clear();
}

void Replxx::SetHintsEnabled(const bool &hints_enabled)
{
    if (hints_enabled)
    {
        rx->set_hint_callback(ReplxxUtil::hint_hook, static_cast<void*>(&ReplxxUtil::_autocompletionAndHintList));
    }
    else
    {
        rx->set_hint_callback(nullptr, nullptr);
    }
}

void Replxx::SetHintRows(const uint &hint_rows)
{
    // max number of results to show when using the hint callback
    rx->set_max_hint_rows(static_cast<int>(hint_rows));
}

void Replxx::SetHintColor(const String &color_name)
{
    ReplxxUtil::hint_color = ReplxxUtil::ParseColorName(color_name);
}

void Replxx::SetHighlighterEnabled(const bool &highlighter_enabled)
{
    if (highlighter_enabled)
    {
        rx->set_highlighter_callback(ReplxxUtil::highlight_hook, static_cast<void*>(&ReplxxUtil::highlight_patterns));
    }
    else
    {
        rx->set_highlighter_callback(nullptr, nullptr);
    }
}

void Replxx::UpdateHighlighter(const HighlightListColorPair &commands,        // command names
                               const HighlightListColorPair &filter_commands, // filter commands
                               const HighlightListColorPair &sub_commands,    // sub commands like ::file
                               const HighlightListColorPair &sp_filters,      // special SearchTerm filters
                               const HighlightListColorPairR &separators)     // command separators
{
    // match whole words, but also take special characters into account
    static const UnicodeString base_expression_left = R"((^|\s+))"; // R"((?:\W|^)(\Q)";
    static const UnicodeString base_expression_right = R"((\s+|$))"; // R"(\E)(?:\W|$))";

    ReplxxUtil::highlight_patterns.clear();

    // append raw strings -> convert to regular expression
    for (auto&& i : {commands, filter_commands, sub_commands, sp_filters})
    {
        const auto &color = ReplxxUtil::ParseColorName(i.second);
        for (auto&& pattern : i.first)
        {
            ReplxxUtil::highlight_patterns.push_back({RegularExpression(base_expression_left +
                                                      // FIXME
                                                      UnicodeString(SearchTerms::EscapeReservedChars(pattern).constData()) +
                                                      base_expression_right), color});
        }
    }

    // append existing regular expressions
    for (auto&& i : {separators})
    {
        const auto &color = ReplxxUtil::ParseColorName(i.second);
        for (auto&& pattern : i.first)
        {
            ReplxxUtil::highlight_patterns.push_back({pattern, color});
        }
    }
}

const UnicodeString Replxx::Get()
{
    // wait for user input, supports Ctrl+C for input cancellation
    const char *input = nullptr;
    do {
        input = rx->input(_replxxPrompt);
    } while ((input == nullptr) && (errno == EAGAIN));

    // input can't be nullptr past this line

    // reset terminal formatting (user defined prompts)
    Console::ResetStyle();

    // convert UTF-8 input into a UnicodeString
    auto inputUtf32 = UnicodeString(input);

    // check if input starts with space (Unicode friendly)
    bool starts_with_space = false;
    if (inputUtf32.size() > 0 && inputUtf32[0].is_space())
        starts_with_space = true;

    // simplify input, keep history lines simplified
    inputUtf32.simplify();

    // append input to replxx history
    append_history(inputUtf32, starts_with_space);

    return inputUtf32;
}

const StringVector Replxx::GetHistory()
{
    StringVector history;
    for (auto i = 0; i < rx->history_size(); i++)
        history.append(rx->history_line(i));
    return history;
}

void Replxx::append_history(const UnicodeString &line, const bool &starts_with_space)
{
    // don't add empty or when starts with space
    if (line.isEmpty() || starts_with_space)
        return;

    // don't add duplicates
    if (rx->history_size() != 0)
    {
        if (line == UnicodeString(rx->history_line(rx->history_size() - 1)))
        {
            return;
        }
    }

    // check input against ignore patterns
    for (auto&& ignorePattern : _ignorePatterns)
    {
        // FIXME: double conversion and temporary cast operator in use
        if (ignorePattern.match(line).hasMatch())
        {
            return;
        }
    }

    // add to in-memory history
    rx->history_add(line.to_utf8());
    // write history file instantly
    rx->history_save(_historyFile);
}
