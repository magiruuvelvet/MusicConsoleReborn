#!/usr/bin/env bash

### DON'T CHANGE OR REMOVE THIS LINE ####################################
cd "$(dirname "$(realpath "$0")")"
#########################################################################

### Setup environment

export CMAKE_BIN="/usr/local/bin/cmake"
export QMAKE_BIN="/usr/local/lib/qt5/bin/qmake"
export QMAKESPEC="/usr/local/lib/qt5/mkspecs/freebsd-clang"

# Make sure that there are no old builds
[ -d build ] && rm -rf build

# Create and enter build directory
mkdir build
cd build

#########################################################################

# Use Clang 6.0 (required for modern type_traits features)
export PATH="/build/clang60-path:$PATH"
export CMAKE_C_COMPILER="/build/clang60-path/clang"
export CMAKE_CXX_COMPILER="/build/clang60-path/clang++"

# Build
"$CMAKE_BIN" \
    -DCMAKE_BUILD_TYPE=Debug \
    -DMETADATA_READER_BACKEND=ffmpeg-ld \
    -DCMAKE_C_COMPILER="${CMAKE_C_COMPILER}" \
    -DCMAKE_CXX_COMPILER="${CMAKE_CXX_COMPILER}" \
    ..
make -j4
